<?php
	

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';


	// Set log file
	$log_file = 'ad-import.log';
	
	// Set no new items found flag to true
	$no_new_items_found = TRUE;

	// Scan productinfo directory
    $dir = SERVER_PATH . '/productinfo/';
    $scan = array_diff(scandir($dir, SCANDIR_SORT_DESCENDING), array('..', '.', '.DS_Store'));

    // Set increment
    $i = 0;

    // Loop through files to find the ones created/updated today
	foreach ($scan as $doc) {
		
		if ($doc == 'archive') {
			continue;
		}
		
		// Get file times
		$date = filemtime("$dir"."$doc");
		
		// If filetime is today
		if (date('Y-m-d', $date) == date('Y-m-d')) {
			
			// Flip no new items found flag
			$no_new_items_found = FALSE;
			
			// Explode the file name
			$ex = explode('-weeklyad-', $doc);
			
			// Capture Nugget Markets Valley
			if (
				strstr($ex[0], '.xml') && // Remove && Date comparison after May 15th ad has run
				date('Y-m-d', strtotime(trim($ex[0], 'weeklyad- .xml'))) >= date('Y-m-d', strtotime('2019-05-15'))
			) {
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Nugget Markets Valley';
				$docs[$i]['ad_date'] = trim($ex[0], 'weeklyad- .xml');
				$sort_order = 1;
				
			// Capture Marin
			} else if (
				strstr($ex[0], 'marin') && // Remove && Date comparison after May 15th ad has run
				date('Y-m-d', strtotime(rtrim($ex[1], '.xml'))) >= date('Y-m-d', strtotime('2019-05-15'))
			) {
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Nugget Markets Marin';
				$docs[$i]['ad_date'] = rtrim($ex[1], '.xml');
				$sort_order = 2;
				
			// Capture Sonoma Market
			} else if (strstr($ex[0], 'sonoma')) {
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Sonoma Market';
				$docs[$i]['ad_date'] = rtrim($ex[1], '.xml');
				$sort_order = 3;
			
			/* Use this if Sonoma turns into Nugget Markets Sonoma
			// Capture Sonoma
			} else if (strstr($ex[0], 'sonoma')) {
				
				$docs[$i]['store'] = 'Nugget Markets Sonoma';
				$docs[$i]['ad_date'] = rtrim($ex[1], '.xml');
				
			*/
			
			// Capture Food 4 Less
			} else if (strstr($ex[0], 'f4l')) {
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Food 4 Less';
				$docs[$i]['ad_date'] = rtrim($ex[1], '.xml');
				$sort_order = 5;
				
			// Capture Fork Lift
			} else if (strstr($ex[0], 'forklift')) {
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Fork Lift';
				$docs[$i]['ad_date'] = rtrim($ex[1], '.xml');
				$sort_order = 4;
				
			// Fall back to regular Nugget Markets
			} else if (!strstr($ex[0], 'marin')) { 
				
				// Capture file attributes
				$docs[$i]['file'] = $doc;
				$docs[$i]['date'] = date('Y-m-d', $date);
				$docs[$i]['store'] = 'Nugget Markets';
				$docs[$i]['ad_date'] = trim($ex[0], 'weeklyad- .xml');
				$sort_order = 1;
				
			}
			
			// Validate ad date and store chain
			$date_from = vempty($docs[$i]['ad_date'],1,0);
			$store_chain = vempty($docs[$i]['store'],1,0);
			
			// If ad exists, soft refresh data
			if ($ad = selectAdByDateAndStoreChain ($date_from, $store_chain)) {
				
				// Log message
				writeToLogFile($log_file, "Ad ID {$ad['id']} for " . unescape($date_from) . " " . unescape($store_chain) . " ad found. Refreshing data...");
				
				// Set soft refresh flag to TRUE to prevent growing regions, display names, 
				// and other custom fields from being renamed. This should just refresh pricing and SKUs on ad.
				if ($refresh = refreshAdItems (unescape($date_from), vbool($ad['id']), TRUE)) {
					
					// Update import timestamp
					updateLastAdImport($ad['id']);
					
					// Log message
					writeToLogFile($log_file, "Data refreshed for " . unescape($date_from) . " " . unescape($store_chain) . " ad ID {$ad['id']}.");
					
				} else {
					// Log error
					writeToLogFile($log_file, "WARNING: Data not refreshed for " . unescape($date_from) . " " . unescape($store_chain) . " ad ID {$ad['id']}.");
				}
				
			// Insert ad, then refresh data
			} else {
				
				// Log message
				writeToLogFile($log_file, unescape($store_chain) . " ad for " . unescape($date_from) . " not found. Creating...");
				
				// Set date_through
				$date_through = vempty(date('Y-m-d', strtotime(unescape($date_from) . '+ 6 days')),1,0);
				
				// Build array of ad data
				$data['store_chain'] 					= $store_chain;
				$data['date_from'] 						= $date_from;
				$data['date_through'] 					= $date_through;
				$data['publish'] 						= 0;
				$data['truckload'] 						= 0;
				$data['truckload_dates'] 				= 'NULL';
				$data['ocean_feast'] 					= 0;
				$data['ocean_feast_dates'] 				= 'NULL';
				$data['secret_special_title']			= 'NULL';
				$data['secret_special_container']		= 'NULL';
				$data['secret_special_description']		= 'NULL';
				$data['secret_special_image']			= 'NULL';
				$data['secret_special_disclaimers']		= 'NULL';
				$data['secret_special_limit']			= 'NULL';
				$data['secret_special_skus']			= 'NULL';
				$data['secret_special_retail']			= 'NULL';
				$data['secret_special_regular_retail']	= 'NULL';
				$data['secret_special_from']			= 'NULL';
				$data['secret_special_through']			= 'NULL';
				$data['article_ids']					= 'NULL';
				$data['recipe_ids']						= 'NULL';
				$data['sort_order']						= $sort_order;
				
				// Insert the ad
				if ($insert = insertAd($data)) {
					
					// Log message
					writeToLogFile($log_file, "Ad ID $insert for " . unescape($date_from) . " " . unescape($store_chain) . " created.");
					
					// Refresh items
					if ($refresh = refreshAdItems (unescape($date_from), $insert, FALSE)) {
						
						// Update import timestamp
						updateLastAdImport($insert);
						
						// Log message
						writeToLogFile($log_file, "Data imported for " . unescape($date_from) . " " . unescape($store_chain) . " ad ID $insert.");
						
					} else {
						
						// Log error
						writeToLogFile($log_file, "WARNING: Data not imported for " . unescape($date_from) . " " . unescape($store_chain) . " ad ID $insert.");
					}
					
				} else {
					
					// Log error
					writeToLogFile($log_file, "ERROR: Unable to create " . unescape($date_from) . " ad for $store_chain.");
					
				}
			}
			
			// Increment loop
			$i++;
			
		}
	}

	// Send alert message to slack bot
	if ($no_new_items_found) {
		
		// Compose Slack bot message
		$message = '{
			"blocks": [
				{
					"type": "section",
					"text": {
						"type": "mrkdwn",
						"text": ":bomb: RED ALERT: No new ad data was transmitted to the web server this morning! This likely means the V2 export job is failing. Verify the status of the web server, check the latest modification dates of the XML files, and contact IT if the problem is not on our end."
					}
				}
			]
		}';
		
		// Post message to Slack channel
		$transfer = postCURL(SLACK_WEBSERVER_NOTIFICATIONS, $message);
		
	}
