<?php 

	/**
	  * Select Zones
	  *
	  * Selects all ad zones from the zones table 
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $zones An assoc array of ad zone data
	  *
	  */
	function selectZones () {
		
		if ($zones = runQuery("
			
			SELECT
				*
				
			FROM
				zones
				
		")) {
			
			return $zones;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Zone by ID
	  *
	  * Selects the zone with the specified ID from the zones table 
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $zone_id The ID of the zone you want to return.
	  *
	  * @return array $zones An assoc array of ad zone data
	  *
	  */
	function selectZoneById ($id) {
		
		if ($zone = runQuery("
			
			SELECT
				*
				
			FROM
				zones
				
			WHERE
				id = $id
				
		")) {
			
			return $zone;
			
		} else {
			
			return FALSE;
			
		}
		
	}
