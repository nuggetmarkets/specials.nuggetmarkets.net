<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'DeleteController.php';


	// Declare vars
	$page_attrs['title'] = "Delete";
	$page_attrs['class'] = 'delete';
	$item = FALSE;


	if (isset($_POST['delete'])) {
		
		if ($delete = deleteRecord($_POST['item_category'], $_POST['item_id'])) {
			
			// Set message and redirect
			$_SESSION['confirm'] = ucfirst($delete['singular']) . ' deleted.';
			header("Location: {$_POST['referer']}");
			
		} else {
			
			// Set message and redirect
			$_SESSION['error'] = 'Item not deleted.';
			header("Location: {$_POST['referer']}");
			
		}
		
		exit();
		
	} else {
		
		$item = getItemName($_GET['category'], $_GET['id']);
		
	}
