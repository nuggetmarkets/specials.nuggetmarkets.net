<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';
	require_once 'SpecialsController.php';
	require_once 'LabelsController.php';

	$ad = FALSE;

	// Handle status change post
	if (isset($_POST['approve']) || isset($_POST['submit_correction']) || isset($_POST['cacel_correction'])) {
		
		// Validate input
		$item['ad_id'] = vbool($_POST['ad_id']);
		$item['family_group'] = vempty($_POST['family_group']);
		$item['group_name'] = vempty($_POST['group_name']);
		
		// Approve item
		if (isset($_POST['approve']) || isset($_POST['cacel_correction'])) {
			
			$item['status'] = vempty('approved');
			
		// Submit correction
		} else if (isset($_POST['submit_correction'])) {
			
			$item['message'] = vempty($_POST['message']);
			$item['posted_by'] = vempty($_SESSION['full_name']);
			$item['posted_email'] = vemail($_SESSION['email']);
			$item['status'] = vempty('issue');
			
			// Create message
			createAdItemMessage($item);
			
		// Set to pending status
		} else {
			$item['status'] = vempty('pending');
		}
		
		//  Try to change status
		if (changeAdItemStatus($item)) {
			$_SESSION['confirm'] = 'Item changed to ' . unescape($item['status']) . ' status.';
		} else {
			$_SESSION['error'] = 'Unable to change item status.';
		}
		
		//  Redirect
		header("Location: /specials/{$_GET['store']}/{$_GET['date']}/#{$_POST['item_id']}");
		exit();
		
	}

	if (!empty($_GET['store']) && !empty($_GET['date']) && !empty($_GET['item'])) {
		
		// Get ad data
		$item = getItemByGroup($_GET['store'], $_GET['date'], $_GET['item']);
		
		// Get navigation
		$navigation = $ad['departments'];
		
		// Get labels
		$labels = getLabels(0,9999);
		
		// Set page attributes
		$page_attrs['title'] = $department['store_name'] . ' Weekly Specials Starting ' . date('F j, Y', strtotime($_GET['date']));
		$page_attrs['class'] = 'specials item';
		
	} else if (!empty($_GET['store']) && !empty($_GET['date']) && !empty($_GET['department'])) {
		
		// Get ad data
		$department = getSpecialsByDepartment($_GET['store'], $_GET['date'], $_GET['department']);
		
		// Get navigation
		$navigation = $ad['departments'];
		
		// Get labels
		$labels = getLabels(0,9999);
		
		// Set page attributes
		$page_attrs['title'] = $department['store_name'] . ' Weekly Specials Starting ' . date('F j, Y', strtotime($_GET['date']));
		$page_attrs['class'] = 'specials ' . $department['slug'];
		
	} else if (!empty($_GET['store']) && !empty($_GET['date'])) {
		
		// Get ad data
		$ad = getAllSpecials($_GET['store'], $_GET['date']);
		
		if (isset($ad['id'])) {
			
			// Get manually disabled items
			$disabled = getDisabledAdItems($ad['id']);
			
			// Get navigation
			$navigation = $ad['departments'];
			
			// Get previous and next ad info for navigation
			$ad_nav_dates = getPrevAndNextAds($ad['date_from'], $ad['store_chain']);
			
			// Get this week's ads for other stores
			$sister_ads = getSisterAds($ad['date_from']);
			
		} else {
			
			// Set store chain name
			$ad['store_chain'] = ucwords(str_replace('-', ' ', $_GET['store']));
			
			// Get previous and next ad info for navigation
			$ad_nav_dates = getPrevAndNextAds($_GET['date'], $ad['store_chain']);
			
			// Get this week's ads for other stores
			$sister_ads = getSisterAds($_GET['date']);
			
		}
		
		// Get labels
		$labels = getLabels(0,9999);
		
		// Set page attributes
		$page_attrs['title'] = $ad['store_chain'] . ' Weekly Specials Starting ' . date('F j, Y', strtotime($ad['date_from']));
		$page_attrs['class'] = 'specials builder';
		
	}
