<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaController.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';

	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
		
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}

	// Get ad details
	$ad = selectAdById($_GET['ad_id']);

	// Get the group instaces details
	$item = getAdItemInstance ($_GET['ad_id'], $_GET['family']);
	
	// Decode JSON feature config
	if ($item['feature_config']) {
		
		$feature_config = json_decode($item['feature_config'], TRUE);
		
	}
	
	// Set up array to loop through feature editorial positions
	$feat_edit_pos = array( 'top left', 'top middle', 'top right', 'bottom left', 'bottom middle', 'bottom right' ); 

	// Get all the SKUs in the group
	$upcs['skus'] = getItemGroupUPCs ($_GET['ad_id'], $_GET['family']);

	// Count the number of SKUs in the group
	$upcs['count'] = count($upcs['skus']);

	// Get all labels
	$labels = getlabels(0,9999);

	// Get messages for item
	$messages = getAdItemMessages($_GET['ad_id'], decodeFamilyGroupUrlString($_GET['family']));

	// Get selected labels for the item group
	$item['selected_labels'] = explode(',', $item['labels']);

	if (isset($item['media_links'])) {
		
		$media_ids = explode(',', $item['media_links']);
		
		$item['media_items'] = selectMediaWithIds ($media_ids);
		
	}

	// Get feature image if one is set
	if (isset($item['feature_image'])) {
		$image = selectMediaById($item['feature_image']);
	} else {
		$image = FALSE;
	}


	$page_attrs['title'] = 'Edit ' . strtolower($item['family_group']) . ' Item Group';
	$page_attrs['description'] = 'Manage this ad item group appearing on the ad.';
	$page_attrs['class'] = 'ad-item-group instances';

	if (!empty($_GET['action'] && $_GET['action'] == 'ungroup')) {
		
		// Validate input
		$item['group_name'] 	= 'NULL';
		$item['ad_id'] 			= vbool($_GET['ad_id']);
		$item['family_group'] 	= vempty($_GET['family']);
		
		// Run update
		if (updateAdItemGroupName($item)) {
			
			$_SESSION['confirm'] = "Item ungrouped.";
			
		} else {
			
			$_SESSION['error'] = 'Unable to ungroup item.';
		}
		
		// Redirect
		header("Location: /ad-builder/" . $item['ad_id'] . '/');
		exit();
		
	}

	if (!empty($_GET['action'] && $_GET['action'] == 'disable')) {
		
		// Validate input
		$item['family_group'] 	= vempty(urldecode($_GET['family']));
		$item['ad_id'] 			= vbool($_GET['ad_id']);
		
		// Run update
		if (disableAdItemInstances($item)) {
			
			$_SESSION['confirm'] = "Item disabled.";
			
		} else {
			
			$_SESSION['error'] = 'Unable to disable item.';
		}
		
		// Redirect
		header("Location: /ad-builder/" . $item['ad_id'] . '/');
		exit();
		
	}

	// Handle Post
	if (isset($_POST['save'])) {
		
		// Validate input
		$item['prefix'] 		= vempty ($_POST['prefix'], 0, 1);
		$item['display_name'] 	= vempty ($_POST['display_name'], 0, 1);
		$item['group_name'] 	= vempty ($_POST['group_name'], 0, 1);
		$item['suffix'] 		= vempty ($_POST['suffix'], 0, 1);
		$item['note'] 			= vempty ($_POST['note'], 0, 1);
		$item['description'] 	= vempty ($_POST['description'], 0, 0);
		
		foreach ($_POST['retail'] as $key => $value) {
			$item['retails'][$key] 		= vempty ($value, 0, 1);
		}
		
		foreach ($_POST['size'] as $key => $value) {
			$item['sizes'][$key] 		= vempty ($value, 0, 0);
		}
		
		foreach ($_POST['unit'] as $key => $value) {
			$item['units'][$key] 		= vempty ($value, 0, 0);
		}
		
		foreach ($_POST['container'] as $key => $value) {
			$item['containers'][$key] = vempty ($value, 0, 0);
		}
		
		foreach ($_POST['enable_items'] as $key => $value) {
			$item['enable'][$key] = (int)vbool ($value);
		}
		
		foreach ($_POST['upc_image_active'] as $key => $value) {
			$item['upc_images_active'][$key] = (int)vbool ($value);
		}
		
		// Handle attachments
		$a = 0;
		foreach ($_POST['media'] as $attach) {
			
			if ($a == 0) {
				$attachments .= vbool($attach);
			} else {
				$attachments .= ',' . vbool($attach);
			}
			
			$a++;
			
		}
		
		if ($attachments) {
			$item['media_links'] = "'" . $attachments . "'";
		} else {
			$item['media_links'] = 'NULL';
		}
		
		// Capture and validate labels
		if (!empty($_POST['labels'])) {
			
			$i = 0;
			foreach ($_POST['labels'] as $label) {
				
				if ($i == 0) {
					$labs .= $label;
				} else {
					$labs .= ',' . $label;
				}
				$i++;
				
			}
			
			// Escape label IDs
			$labels = vempty($labs,0,0);
			
		} else {
			
			$labels = 'NULL';
			
		}
		
		$item['labels']				= $labels;
		$item['sale_price'] 		= vempty($_POST['sale_price'], 0, 1);
		$item['override_price']		= vempty($_POST['override_price'], 0, 0);
		$item['limit'] 				= vempty($_POST['limit'], 0, 1);
		$item['supplies_last']		= vbool ($_POST['supplies_last']);
		$item['crv'] 				= vbool ($_POST['crv']);
		$item['feature'] 			= vbool($_POST['feature']);
		
		if ($item['feature']) {
			
			$feature_style 				= vempty($_POST['feature_style']);
			$feature_caption_position 	= vempty($_POST['feature_caption_position']);
			$feature_background_color 	= vempty($_POST['feature_background_color']);
			$feature_font_color 		= vempty($_POST['feature_font_color']);
			$feature_append_recipe 		= vbool($_POST['feature_append_recipe']);
			
			$feature_config['feature_style'] = unescape($feature_style);
			$feature_config['feature_caption_position'] = unescape($feature_caption_position);
			$feature_config['feature_background_color'] = unescape($feature_background_color);
			$feature_config['feature_font_color'] = unescape($feature_font_color);
			if ($feature_append_recipe) {
				$feature_config['append_recipe'] = $feature_append_recipe;
			}
			
			$item['feature_config'] = vempty(json_encode($feature_config));
			
		} else {
			
			$item['feature_config'] = 'NULL';
			
		}
		
		$item['front_page_item']	= vbool($_POST['front_page_item']);
		$item['sort']				= vempty($_POST['sort']);
		$item['family_group']		= vempty(decodeFamilyGroupUrlString($_POST['ad_item_group_id']),1,0);
		
		// Force quotes around family group
		if (substr($item['family_group'], 0) != "'" && substr($item['family_group'], -1) != "'") {
			$item['family_group']	= "'{$item['family_group']}'";
		}
		
		$item['feature_image'] 		= vempty($_POST['featured_image'], 0, 1);
		$item['ad_id'] 				= vbool($_POST['ad_id']);
		$item['update_config']		= vbool($_POST['update_config']);
		$item['item_id'] 			= vempty($_POST['item_id'], 0, 1);
		$item['status'] 			= vempty($_POST['status'], 0, 1);
		$status_changed_message 	= NULL;
		
		if (!empty($_POST['message'])) {
			
			// Validate input
			$message_item['ad_id'] 			= $item['ad_id'];
			$message_item['department'] 	= vempty($_POST['department']);
			$message_item['item_id'] 		= $item['item_id'];
			$message_item['family_group'] 	= $item['family_group'];
			$message_item['group_name'] 	= $item['group_name'];
			$message_item['message'] 		= vempty($_POST['message'], 1, 1);
			$message_item['posted_by'] 		= vempty($_SESSION['full_name']);
			$message_item['posted_email'] 	= vemail($_SESSION['email']);
			
			// Set item name
			if ($_POST['group_name']) {
				$message_item['item_name'] = $item['group_name'];
			} else if ($_POST['display_name']) {
				$message_item['item_name'] = $item['display_name'];
			} else {
				$message_item['item_name'] = vempty(ucwords(strtolower($_POST['message_pos_name'])));
			}
			
		}
		
		// Update items
		if (updateAdItemInstances($item)) {
			
			// Update any message for the group so they stay attached
			updateMessageGroupNameById($item);
			
			// Update the item config if box is checked
			if ($item['update_config']) {
				updateAdItemConfig($item);
			}
			
			// Set sister ad not updated flag
			$sister_ad_not_updated_message = NULL; 
			
			// Update item in sister ad if sister ads box is checked
			if ($_POST['update_sister_ads']) {
				
				// Get sister ads
				$sister_ads = getSisterAds($_POST['ad_date']);
				
				// Loop through sister ads
				foreach ($sister_ads['ads'] as $sister) {
					
					// Only run if it isn’t the current ad
					if ($sister['id'] != $item['ad_id']) {
						
						// Capture vars
					    $sister_ad = $item;
						$sister_ad['ad_id'] = $sister['id'];
						
						// Don’t update sister ad if item is featured (prevents accidetanlly overwriting changes)
						if (checkIfItemIsFeatured($sister_ad)) {
							$sister_ad_not_updated_message = " <p>NOTE: These changes were not applied to sibling ads where this item is&nbsp;featured.</p>";
							continue;
						}
						
						// Get sister ad attributes for ad_id_prefix.
						$sister_ad['attributes'] = getStoreAttributesByName($sister['store_chain']);
						
						// Don’t update sister ad sale price or status since they differ by zone and ad.
						$sister_ad['sale_price'] = 'sale_price';
						$sister_ad['status'] = 'status';
						
						// Check for multiple SKUS
						if (is_array($item['retails'])) {
							
							// Loop through each SKU attribute and replace the ad_id_prefix in the item id with that of the sister ad
							foreach ($sister_ad['retails'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								// Don’t update sister ad retails since they differ by zone.
								$sister_retails[$new_key] = 'retail_price';
							}
							$sister_ad['retails'] = $sister_retails;
							
							foreach ($sister_ad['sizes'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								$sister_sizes[$new_key] = $value;
							}
							$sister_ad['sizes'] = $sister_sizes;
							
							foreach ($sister_ad['units'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								$sister_units[$new_key] = $value;
							}
							$sister_ad['units'] = $sister_units;
							
							foreach ($sister_ad['containers'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								$sister_containers[$new_key] = $value;
							}
							$sister_ad['containers'] = $sister_containers;
							
							foreach ($sister_ad['enable'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								$sister_enable[$new_key] = $value;
							}
							$sister_ad['enable'] = $sister_enable;
							
							foreach ($sister_ad['upc_images_active'] as $key => $value) {
								$new_key = $sister_ad['attributes']['ad_id_prefix'] . substr($key, 2);
								$sister_upc_image_active[$new_key] = $value;
							}
							$sister_ad['upc_images_active'] = $sister_upc_image_active;
							
						}
						
						// Update the record
						updateAdItemInstances($sister_ad);
						
					}
				}
			}
			
			
			// Check for reply to
			if (!empty($_POST['message'])) {
				
				// Create message
				createAdItemMessage($message_item);
				
				// Override notification emails for dev environment
				if (ENVIRONMENT == 'dev') {
					$addresses = array('Mark Eagleton <mark.eagleton@nuggetmarket.com>');
				} else {
					$addresses = array("{$_POST['message_reply_to']}");
				}
				
				// Set up notification message
				$notification_email_id = '5c6c4445-d8bb-46f7-ae64-6b97efb500cf';
				$notification_message = array(
					"To" => $addresses,
					"Data" => array(
						'item_name' => unescape($message_item['item_name']),
						'item_link' => SPECIALS_URL . "/proofs/{$_POST['ad_date']}/{$_POST['department']}/#item-{$_POST['item_id']}",
						'message' => $_POST['message'],
						'from' => $_SESSION['full_name']
					)
				);
				
				sendCMMail($notification_message, $notification_email_id, 'Unchanged', false);
				
			}
			
			
			if ($ad['publish'] == 1) {
				
				$_SESSION['confirm'] = "<p>Item saved. <a href=\"/ad-json-export/{$_GET['ad_id']}\" class=\"button\">Refresh the cache</a> to apply these changes to the ad.</p>$sister_ad_not_updated_message";
				
			} else {
				
				$_SESSION['confirm'] = "<p>Item saved.</p>$sister_ad_not_updated_message";
				
			}
			
		} else {
			
			// Set error
			$_SESSION['error'] = "<p>Item failed to update.</p>\n";
			
		}
		
		// Redirect
		header("Location: /ad-item-group/" . $item['ad_id'] . '/' . encodeFamilyGroupUrlString(unescape(str_replace('\\', '', $item['family_group']))) . '/');
		exit();
		
	}
