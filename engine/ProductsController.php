<?php

    // Include dependencies
	require_once 'ProductsModel.php';

    
    /**
	  * Get Products By Search Tearms
	  *
	  * Searches products table for the provided terms
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $search_terms The terms to search for.
	  *
	  * @return array $products The products that meet the search criteria.
	  *
	  */
	function getProductsBySearchTerms ($search_terms) {
		
		// Declare return vars
		$products = FALSE;
		$items = NULL;
		
		// Validate search terms
		$terms = vempty($search_terms, 1, 0);
		
		// Use full text index if terms arg is 4 or more characters
		if (strlen($search_terms) >= 4 && !is_numeric($terms)) {
			
			$products = selectProductsMatchAgainstTerms($terms);
		
		// Use LIKE clause if terms string is under 4 characters
		} else {
			
			$products = selectProductsLikeTerms($terms);
		}
		
		if ($products) {
			
			$i = 0;
			// Loop through results
			foreach ($products as $item) {
				
				// Create array of results
				$items[$i]['upc'] 			= $item['upc'];
				$items[$i]['family_group'] 	= $item['family_group'];
				$items[$i]['family_uri']	= encodeFamilyGroupUrlString($item['family_group']);
				$items[$i]['pos_name'] 		= $item['pos_name'];
				$items[$i]['prefix'] 		= $item['prefix'];
				$items[$i]['display_name'] 	= $item['display_name'];
				$items[$i]['suffix'] 		= $item['suffix'];
				$items[$i]['note'] 			= $item['note'];
				$items[$i]['description'] 	= $item['description'];
				$items[$i]['size'] 			= $item['size'];
				$items[$i]['unit'] 			= $item['unit'];
				$items[$i]['container'] 	= $item['container'];
				$items[$i]['limit'] 		= $item['limit'];
				$items[$i]['supplies_last'] = $item['supplies_last'];
				$items[$i]['lifestyle'] 	= $item['lifestyle'];
				$items[$i]['labels'] 		= $item['labels'];
				
				if (substr($item['retail_price'], 0, 1) == '.') {
					$items[$i]['retail_price'] 	= str_replace('.', '', $item['retail_price']) . '&cent;';
				} else {
					$items[$i]['retail_price'] 	= '$' . $item['retail_price'];
				}
					
				$items[$i]['crv'] 			= $item['crv'];
				$items[$i]['department'] 	= $item['department'];
				$items[$i]['media_links'] 	= $item['media_links'];
				$items[$i]['feature_image'] = $item['feature_image'];
				
				$i++;
				
			}
			
			$items['listing'] = $items;
			
			$items['pagination'] = FALSE;
			
			// Return items
			return $items;
		
		} else {
			
			// Return false
			return FALSE;
			
		}
		
		
	}
	
	/**
	  * Get Product By UPC
	  *
	  * Gets product data with the specified UPC
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $upc UPC code of the product you want to retrieve.
	  *
	  * @return array $product An array of product attributes
	  *
	  */
	function getProductByUpc ($upc) {
		
		if ($product = selectProductByUpc($upc)) {
			
			$product['family_uri'] = encodeFamilyGroupUrlString($product['family_group']);
			
			return $product;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Product By Family Group
	  *
	  * Gets product data with the specified UPC
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $products An array of products that match the family group name
	  *
	  */
	function getProductByFamilyGroup ($family_group) {
		
		// Validate input
		$family = vempty($family_group);
		
		if ($products = selectProductsByFamilyGroup($family)) {
			
			return $products;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Check for Duplicate UPC
	  *
	  * Checks to see if upc is already in the products table.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $upc The UPC of the product being submitted.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function checkForDuplicateUpc ($upc) {
		
		if (!selectProductByUpc($upc)) {
			
			return FALSE;
			
		} else {
			
			return TRUE;
			
		}
		
	}
