<?php
	
	/**
	  * Select Labels
	  *
	  * Selects all labels from database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Takes page number from Labels class call
	  * @param int $limit Number of results to return
	  *
	  * @return array $labels An array of selected the records
	  *
	  */
	function selectLabels ($offset, $limit) {
		
		if ($labels['listing'] = runQuery("
			
			SELECT
				labels.id, 
				labels.name, 
				labels.description,
				labels.long_description,
				labels.type
				
			FROM
				labels
				
			ORDER BY
				labels.type DESC, 
				labels.name ASC
				
			LIMIT 
				$offset, $limit
				
		")) {
			
			$count = runQuery("
				
				SELECT
					COUNT(labels.id)
					
				FROM
					labels
					
			");
			
			$labels['count'] = $count[0];
			
			return $labels;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Label by ID
	  *
	  * Selects the specified label with the given ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $label_id Takes page number from Labels class call
	  *
	  * @return array $label An array of selected the records
	  *
	  */
	function selectLabelById ($label_id) {
			
		if ($label = runQuery("
			
			SELECT
				labels.id, 
				labels.name, 
				labels.description,
				labels.long_description,
				labels.type
				
			FROM
				labels
				
			WHERE
				labels.id = $label_id
				
				
		")) {
			
			return $label[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Labels with IDs
	  *
	  * Selects labels with ids contained in the $where argument
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $where A specific where clause containing OR operators with label ids
	  *
	  * @return array $label An array of selected the records
	  *
	  */
	function selectLabelsWithIds ($where) {
		
		if ($labels = runQuery("
			
			SELECT
				*
				
			FROM
				labels
				
			WHERE
				$where
				
		")) {
			
			return $labels;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert Label
	  *
	  * Inserts a new label record into the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $label An array containing the label data
	  *
	  * @return array $insert An array containing the MYSQL insert_id
	  *
	  */
	function insertLabel ($label) {
		
		if ($insert = runQuery("
			
			INSERT INTO
				labels
				
			SET
				name 				= {$label['name']},
				description 		= {$label['description']}, 
				long_description 	= {$label['long_description']}, 
				type				= {$label['type']}
				
		")) {
			
			return $insert;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update Label
	  *
	  * Updates a label entry in the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $label An array containing the label data
	  *
	  * @return TRUE|FALSE $update
	  *
	  */
	function updateLabel ($label) {
		
		if ($update = runQuery("
			
			UPDATE
				labels
				
			SET
				name 				= {$label['name']},
				description 		= {$label['description']}, 
				long_description 	= {$label['long_description']}, 
				type				= {$label['type']}
				
			WHERE
				id = {$label['label_id']}
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
	}
