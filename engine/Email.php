<?php

	// Require dependencies
	require_once 'Bootstrap.php';
	require_once 'MediaModel.php';
	require_once 'AdsController.php';
	require_once 'EmailController.php';
	require_once 'LabelsController.php';


	if (!empty($_GET['id'])) {
		
		// Get the appropreate ad ID
		if (is_numeric($_GET['id'])) {
			
			$ad_id = $_GET['id'];
			
		} else {
			
			if (strstr($_GET['id'], 'nugget')) {
				$ad_id = getAdIdByStoreChain($_GET['id'] . '-valley');
			} else {
				$ad_id = getAdIdByStoreChain($_GET['id']);
			}
			
		}
		
		// Get the ad
		$ad = getAdById($ad_id);
		
		// Handle any zone URL conversions
		$ad['secret_special_description'] = str_replace('[zoneurl]', $ad['store_attributes']['url'], $ad['secret_special_description']);
		$ad['prepended_message'] = str_replace('[zoneurl]', $ad['store_attributes']['url'], $ad['prepended_message']);
		
		// Check for multiple item pricing
		if (strstr($ad['secret_special_retail'], '|')) {
			
			// Markup item name CRV and 
			$find = array('(', ')', '+CRV', '+ CRV', '+crv', '+ crv');
			$replace = array('<div>(', ')</div>', '<small class="crv">+CRV</small>', '<small class="crv">+CRV</small>', '<small class="crv">+CRV</small>', '<small class="crv">+CRV</small>');
			$retail = str_replace($find, $replace, $ad['secret_special_retail']);
			
			// Set up retail var
			$secret_special_retail = NULL;
			
			// Explode the formatted retail price
			$exp = explode('|', $retail);
			
			// Loop through the prices and mark them up
			foreach ($exp as $ex) {
				$secret_special_retail .= '<h3 class="ws-sale-price">' . $ex . '</h3>';
			}
			
			// Overwrite the retail array member
			$ad['secret_special_retail'] = $secret_special_retail;
			
		// Check for CRV
		} else {
			
			if (strstr($ad['secret_special_retail'], '+CRV')) {
				$exp = explode('+', $ad['secret_special_retail']);
				$ad['secret_special_retail'] = '<h3 class="ws-sale-price">' . $exp[0] . '<small class="crv">+'. $exp[1] .'</small></h3>';
			} else {
				$ad['secret_special_retail'] = '<h3 class="ws-sale-price">' . $ad['secret_special_retail'] . '</h3>';
			}
			
		}
		
		// Get any attached articles
		if (isset($ad['articles'])) {
			$articles = getArticles($ad);
		}
		
		// Get any attached recipes
		if (isset($ad['recipes'])) {
			$recipes = getRecipes($ad);
		}
		
		// Get weekly meal planning page
		if ($ad['store_chain'] != 'Food 4 Less') {
			$meal_planning =  selectWeeklyMealPlanningPage();
		}
		
		// Get all the labels
		$labels = getLabels(0,9999);
		
		// Handle print version
		if (!empty($_GET['print'])) {
			$page_attrs['class'] = ' print';
		}
		
	} else {
		header("HTTP/1.0 404 Not Found");
	}
