<?php

	/**
	  * Get Articles
	  *
	  * Gets specified articles from relivant store website. Direct database select from Sonoma,
	  * and API call for Nugget Markets websites.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $ad An array of all ad data including ad_id, store_chain and aritcle IDs.
	  *
	  * @return array $articles An array of article data
	  *
	  */
    function getArticles ($ad) {
		
		// Declare return array
		$articles = array();
	  
		// Convert articles into an array
		$article_ids = $ad['articles'];
		
		// Count the number of articles
		$count = count($article_ids);
		
		// Loop through articles and build where clause for query
		$i = 1;
		foreach ($article_ids as $article_id) {
			
			// Validate article ID
			$id = vbool($article_id);
			
			if ($count === $i) {
				$where .= "articles.id = $article_id";
			} else {
				$where .= "articles.id = $article_id OR ";
			}
			
			$i++;
			
		}
		
		if ($articles = runQuery("
			
			SELECT
				articles.id AS `article_id`,
				articles.title AS `article_title`,
				articles.subtitle AS `article_subtitle`,
				articles.excerpt AS `article_excerpt`,
				
				articles.dposted AS `article_date`,
				articles.publish_nugget AS `publish_nugget`,
				articles.publish_sonoma AS `publish_sonoma`,
				articles.publish_forklift AS `publish_forklift`,
				articles.publish_food4less AS `publish_food4less`,
				articles.slug AS `article_slug`,
				
				photos.tmb AS `photo_thumb`,
				photos.img AS `photo_large`,
				photos.original AS `photo_original`,
				photos.description AS `photo_description`,
				photos.caption AS `photo_caption`,
				photos.id AS `photo_id`,
				
				new_media.id AS `image_id`, 
				new_media.name AS `image_name`, 
				new_media.caption AS `image_caption`, 
				new_media.alt_text AS `image_alt_text`, 
				new_media.file AS `image_original`,
				new_media.mime_type AS `image_mime_type`,
				new_media.large_image AS `image_large`,
				new_media.medium_image AS `image_medium`,
				new_media.small_image AS `image_small`,
				new_media.date_created AS `image_date_created`
				
			FROM
				articles
				
			LEFT JOIN
				photos ON photos.item_id = articles.id AND
				photos.item_table = 'articles' AND
				photos.feature = 1 AND
				photos.publish = 1
				
			LEFT JOIN
				new_media ON new_media.id = articles.featured_image
				
			WHERE
				$where
			
		", 'nugget')) {
			
			// Return results
			return $articles;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Recipes
	  *
	  * Gets specified recipes from relivant store website. Direct database select from Sonoma,
	  * and API call for Nugget Markets websites.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $ad An array of all ad data including ad_id, store_chain and aritcle IDs.
	  *
	  * @return array $articles An array of article data
	  *
	  */
	function getRecipes ($ad) {
		
		// Declare return array
		$recipes = array();
		
		// Convert recipes into an array
		$recipe_ids = $ad['recipes'];
		
		// Count the number of recipes
		$count = count($recipe_ids);
		
		// Loop through recipes and build where clause for query
		$i = 1;
		foreach ($recipe_ids as $recipe_id) {
			
			// Validate recipe ID
			$id = vbool($recipe_id);
			
			if ($count === $i) {
				$where .= "recipes.id = $recipe_id";
			} else {
				$where .= "recipes.id = $recipe_id OR ";
			}
			
			$i++;
			
		}
		
		if ($recipes = runQuery("
			
			SELECT
				recipes.id AS `recipe_id`,
				recipes.name AS `recipe_name`,
				recipes.publish_nugget,
				recipes.publish_sonoma,
				recipes.publish_forklift,
				recipes.publish_food4less,
				recipes.description AS `recipe_description`,
				recipes.slug AS `recipe_slug`,
				recipes.media_links AS `media_links`, 
				recipes.featured_image AS `featured_image`,
				
				photos.id AS `photo_id`,
				photos.tmb AS `photo_small`,
				photos.img AS `photo_medium`,
				photos.img AS `photo_large`,
				photos.original AS `photo_original`,
				photos.description AS `photo_alt_text`, 
				
				new_media.id AS `image_id`, 
				new_media.name AS `image_name`, 
				new_media.caption AS `image_caption`, 
				new_media.alt_text AS `image_alt_text`, 
				new_media.file AS `image_original`,
				new_media.mime_type AS `image_mime_type`,
				new_media.large_image AS `image_large`,
				new_media.medium_image AS `image_medium`,
				new_media.small_image AS `image_small`,
				new_media.date_created AS `image_date_created`
				
			FROM
				recipes
				
			LEFT JOIN
				photos ON photos.item_id = recipes.id AND
				photos.item_table = 'recipes' AND
				photos.feature = 1 AND
				photos.publish = 1
				
			LEFT JOIN
				new_media ON new_media.id = recipes.featured_image
				
			WHERE
				$where
			
		", 'nugget')) {
			
			// Return results
			return $recipes;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Weekly Meal Planning
	  *
	  * Gets the weekly meal planning landing page from the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $page An array of page data
	  *
	  */
	function selectWeeklyMealPlanningPage () {
		
		if ($page = runQuery("
			
			SELECT
				landing_pages.*,
				
				photos.id AS `photo_id`,
				photos.tmb AS `photo_small`,
				photos.img AS `photo_medium`,
				photos.img AS `photo_large`,
				photos.original AS `photo_original`,
				photos.description AS `photo_alt_text`, 
				
				new_media.id AS `image_id`, 
				new_media.name AS `image_name`, 
				new_media.caption AS `image_caption`, 
				new_media.alt_text AS `image_alt_text`, 
				new_media.file AS `image_original`,
				new_media.mime_type AS `image_mime_type`,
				new_media.large_image AS `image_large`,
				new_media.medium_image AS `image_medium`,
				new_media.small_image AS `image_small`,
				new_media.date_created AS `image_date_created`
				
			FROM
				landing_pages
				
			LEFT JOIN
				photos ON photos.item_id = landing_pages.id AND
				photos.item_table = 'landing_pages' AND
				photos.feature = 1
				
			LEFT JOIN
				new_media ON new_media.id = landing_pages.featured_image
				
			WHERE
				landing_pages.id = 45
				
		", 'nugget')) {
			
			return $page[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Sonoma Recipe
	  *
	  * Runs select query of recipes table on Sonoma Market website database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $recipe_id The ID of the recipe to select.
	  *
	  * @return array $recipe[0] An array of recipe data
	  *
	  */
	function selectSonomaRecipe ($recipe_id) {
		
		if ($recipe = runQuery("
			
			SELECT
				recipes.id,
				recipes.title AS `name`,
				recipes.description,
				recipes.date_created,
				recipes.publish,
				recipes.feature,
				recipes.slug,
				
				media.alt_text AS `photo_description`,
				media.small_image AS `photo_thumb`
				
			FROM
				recipes
				
			LEFT JOIN
				media ON media.id = recipes.featured_image
				
			WHERE
				recipes.id = $recipe_id
				
		", 'sonoma')) {
			
			return $recipe[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}
