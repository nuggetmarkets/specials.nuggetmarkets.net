<?php
	
	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';

	
	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
			
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}

	// Handle Post
	if (!empty($_GET['date']) && !empty($_GET['id'])) {
		
		$ad_id = vbool($_GET['id']);
		
		if (!empty($_GET['soft'])) {
			$soft_import = TRUE;
		} else {
			$soft_import = FALSE;
		}
		
		// Update product config and refresh ad data
		if (refreshAdItems($_GET['date'], $ad_id, $soft_import)) {

			// Update last_imported date
			updateLastAdImport($ad_id);
			
			// Set confirmation
			$_SESSION['confirm'] = "<p>Products have been imported.</p>\n";
			
		} else {
			
			// Set error
			$_SESSION['error'] = "<p>No products to import.</p>\n";
			
		}
		
		// Redirect
		if (strlen($_SERVER['HTTP_REFERER']) > 30) {
			header("Location: {$_SERVER['HTTP_REFERER']}");
		} else {
			header("Location: /ads/");
		}
		exit();
		
		
	} else {
		
		$ads = selectAds();
		
	}
