<?php 

	require_once 'ZonesModel.php';
	
	
	/**
	  * Get Zones
	  *
	  * Gets all ad zones from the database 
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $zones An assoc array of ad zone data
	  *
	  */
	function getZones () {
		
		$zones = selectZones();
		
		return $zones;
		
	}
	
	
	/**
	  * Get Zone by ID
	  *
	  * Gets the specified zone zones from the database 
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $zone_id The ID of the zone you want to return.
	  *
	  * @return array $zones An assoc array of ad zone data
	  *
	  */
	function getZoneById ($zone_id) {
		
		// Validate ID
		$id = vbool($zone_id);
		
		$zone = selectZoneById($id);
		
		return $zones;
		
	}


	/**
	  * Get Remapped Zones
	  *
	  * Gets all the zones from the database and remaps their keys to their zone number
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $zones An assoc array of ad zone data
	  *
	  */
	function getRemappedZones () {
		
		// Set up output array
		$zones = array();
		
		// Get Zones
		$getzones = getZones();
		
		foreach ($getzones as $zone) {
			
			$zones[$zone['zone_number']] = $zone; 
			
		}
		
		// Return zones
		return $zones;
		
	}
