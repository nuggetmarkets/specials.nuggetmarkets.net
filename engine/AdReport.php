<?php
	
	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaModel.php';
	require_once 'AdsController.php';

	
	// Validate ad id
	$ad_id = vbool($_GET['id']);
	
	// Get ad data
	$ad = selectAdById($ad_id);
	
	// Set store slug
	$ad['store_slug'] = formatSlug($ad['store_chain']);
	
	// Set page vars
	$page_attrs['title'] = 'Ad Report';
	$page_attrs['description'] = 'Review ad items for ' . $ad['store_chain'] . '.';
	$page_attrs['class'] = 'report';

	if (!empty($ad['date_from'])) {

		// Get previous and next ad info for navigation
		$ad_nav_dates = getPrevAndNextAds($ad['date_from'], $ad['store_chain']);

		// Get this week's ads for other stores
		$sister_ads = getSisterAds($ad['date_from']);
		
		// Get featured items
		$features = getWeeklyAd($ad_id, $ad['date_from'], FALSE, TRUE);

		// Get items by department
		$meat 		= getWeeklyAd($ad_id, $ad['date_from'], 'meat');
		$produce	= getWeeklyAd($ad_id, $ad['date_from'], 'produce');
		$grocery 	= getWeeklyAd($ad_id, $ad['date_from'], 'staples');
		$dairy 		= getWeeklyAd($ad_id, $ad['date_from'], 'dairy');
		$frozen 	= getWeeklyAd($ad_id, $ad['date_from'], 'frozen');
		$adultbev	= getWeeklyAd($ad_id, $ad['date_from'], 'adult beverages');
		$bakery		= getWeeklyAd($ad_id, $ad['date_from'], 'bakery');
		$deli 		= getWeeklyAd($ad_id, $ad['date_from'], 'deli');
		$general 	= getWeeklyAd($ad_id, $ad['date_from'], 'general merchandise');
		$healthy 	= getWeeklyAd($ad_id, $ad['date_from'], 'healthy living');
		$cheese 	= getWeeklyAd($ad_id, $ad['date_from'], 'specialty cheese');
		$coffeebar 	= getWeeklyAd($ad_id, $ad['date_from'], 'coffee bar');
		$kitchen 	= getWeeklyAd($ad_id, $ad['date_from'], 'kitchen');
		$floral 	= getWeeklyAd($ad_id, $ad['date_from'], 'floral');
		
		$labels = selectLabels();
		
		$page_attrs['title'] = $ad['store_chain'] . ' ' . date('F j, Y', strtotime($ad['date_from'])) . ' Ad Report';
		$page_attrs['description'] = 'Review items appearing on the ' . date('F j, Y', strtotime($ad['date_from'])) . ' ' . $ad['store_chain'];
	}
