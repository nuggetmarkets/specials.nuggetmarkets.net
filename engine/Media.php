<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'MediaController.php';

	// Declare vars
	if (!empty($_GET['category'])) {
		
		$page_attrs['title'] = "Media Select";
		$page_attrs['class'] = 'media-select';
		
	} else {
		
		$page_attrs['title'] = "Media";
		$page_attrs['class'] = 'media';
		
	}

	$media_items = FALSE;
	$media_item = FALSE;
	$query_string = NULL;
	$search_terms = NULL;

	// Get media_links if browsing attachments for an item.
	if (!empty($_GET['category'])) {
		
		$links = getItemMediaLinks($_GET['category'], $_GET['id'], $_GET['ad_id']);
		
	}
	
	// Handle post
	if (isset($_POST['save']) || isset($_POST['add'])) {
		
		$file = FALSE;
		
		// Require and validate title
		if (!$media['alt_text'] = vempty($_POST['alt_text'], 1, 1)) {
			$error[] = 'An alt description is required.';
		}
		
		// Validate everything else
		$media['name'] = vempty($_POST['name'],0,1);
		$media['caption'] = vempty($_POST['caption']);
		$media['catalog_text'] = vempty($_POST['catalog_text']);
		
		// Process file uploads if saving
		if (isset($_POST['save'])) {
			
			// Get current file data
			$current_files = getMediaById($_POST['item_id']);
			
			// Handle new file upload
			if (!empty($_FILES['new_file']['name'])) {
				
				// Replace new file with old file
				if ($media['file'] = fileReplace($_FILES['new_file'], MEDIA_UPLOAD_DIR . $current_files['file'])) {
					
					$media['mime_type'] = vempty($_FILES['new_file']['type'],0,0);
					
					// Create additional sizes and overwrite if file is image
					if (strstr($_FILES['new_file']['type'], 'image')) {
						
						unlink(MEDIA_UPLOAD_DIR . $current_files['large_image']);
						unlink(MEDIA_UPLOAD_DIR . $current_files['medium_image']);
						unlink(MEDIA_UPLOAD_DIR . $current_files['small_image']);
						
						$media['large_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),1200, MEDIA_UPLOAD_DIR . $_FILES['new_file']['name']);
						$media['medium_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),800, MEDIA_UPLOAD_DIR . $_FILES['new_file']['name']);
						$media['small_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),600, MEDIA_UPLOAD_DIR . $_FILES['new_file']['name']);
						
					}
					
				// Throw error
				} else {
					
					$error[] = 'Unable to update file. Make sure it is a valid format and not too large.';
					
				}
				
			// Use current file names for update query
			} else {
				
				$media['file'] = vempty($current_files['file']);
				$media['mime_type'] = vempty($current_files['mime_type'],0,0);
				
				if (strstr($current_files['mime_type'], 'image')) {
					
					$media['large_image'] 	= vempty($current_files['large_image']);
					$media['medium_image'] 	= vempty($current_files['medium_image']);
					$media['small_image'] 	= vempty($current_files['small_image']);
					
					// Handle any image version replacements
					if (
						!empty($_FILES['new_image_original']['name']) ||
						!empty($_FILES['new_image_large']['name']) ||
						!empty($_FILES['new_image_medium']['name']) ||
						!empty($_FILES['new_image_small']['name'])) {
						
						// Replace original
						if (!empty($_FILES['new_image_original']['name'])) {
							$media['file'] = fileReplace($_FILES['new_image_original'], MEDIA_UPLOAD_DIR . $current_files['file']);
						}
						// Replace large
						if (!empty($_FILES['new_image_large']['name'])) {
							$media['large_image'] = fileReplace($_FILES['new_image_large'], MEDIA_UPLOAD_DIR . $current_files['large_image']);
						}
						// Replace medium
						if (!empty($_FILES['new_image_medium']['name'])) {
							$media['medium_image'] = fileReplace($_FILES['new_image_medium'], MEDIA_UPLOAD_DIR . $current_files['medium_image']);
						}
						// Replace small
						if (!empty($_FILES['new_image_small']['name'])) {
							$media['small_image'] = fileReplace($_FILES['new_image_small'], MEDIA_UPLOAD_DIR . $current_files['small_image']);
						}
					}
					
				} else {
					
					$media['large_image'] 	= 'NULL';
					$media['medium_image'] 	= 'NULL';
					$media['small_image'] 	= 'NULL';
					
				}
				
			}
			
		// Process file uploads if adding
		} else if (isset($_POST['add'])) {
			
			// Require file
			if (empty($_FILES['file']['name'])) {
				
				$error[] = 'A file is required.';
				
			// Upload file
			} else if ($media['file'] = fileUpload($_FILES['file'], MEDIA_UPLOAD_DIR . $_FILES['file']['name'])) {
				
				$media['mime_type'] = vempty($_FILES['file']['type'],0,0);
				
				// Create additional sizes if file is image
				if (strstr($media['mime_type'], 'image')) {
					
					$media['large_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),1200, MEDIA_UPLOAD_DIR . $_FILES['file']['name']);
					$media['medium_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),800, MEDIA_UPLOAD_DIR . $_FILES['file']['name']);
					$media['small_image'] 	= imgResize(MEDIA_UPLOAD_DIR . unescape($media['file']),600, MEDIA_UPLOAD_DIR . $_FILES['file']['name']);
					
				} else {
					
					$media['large_image'] 	= 'NULL';
					$media['medium_image'] 	= 'NULL';
					$media['small_image'] 	= 'NULL';
					
				}
				
			} else {
				
				$error[] = 'Unable to upload file. Make sure it is a valid format and not too large.';
				
			}
			
		}
		
		// Handle submit if there are no errors
		if (!$error) {
			
			if (isset($_POST['save'])) {
			
				$media['item_id'] = vbool($_POST['item_id']);
				
				// Update article
				updateMedia($media);
				
				// Set message and redirect
				$_SESSION['confirm'] = 'Item updated.';
				
			} else if (isset($_POST['add'])) {
				
				// Insert article
				$result = insertMedia($media);
				
				// Capture and set user_id for redirect
				$media['item_id'] = $result['insert_id'];
				
				// Set message and redirect
				$_SESSION['confirm'] = 'Item created.';
				
			}
			
			header("Location: /media/edit/{$media['item_id']}/");
			exit();
			
		}
		
	} else if (isset($_POST['attach'])) {
		
		$item_category = vempty($_POST['item_category']);
		$item_id = vbool($_POST['item_id']);
		
		if (appendMediaIds($item_category, $item_id, $_POST['media'])) {
			
			// Set message and redirect
			$_SESSION['confirm'] = 'Items attached.';
			
		} else {
			
			// Set message and redirect
			$_SESSION['error'] = 'Unable to attach selected items.';
			
		}
		
		header("Location: /{$_POST['item_category']}/{$_POST['item_id']}/");
		exit();
		
	// Get specifed item for editing
	} else if (isset($_GET['id']) && $_GET['action'] == 'edit') {
		
		$id = vbool($_GET['id']);
		$media = getMediaById ($id);
		$page_attrs['action'] = 'edit';
		
	// Get specifed item for view
	} else if (isset($_GET['id']) && $_GET['action'] == 'view') {
		
		$id = vbool($_GET['id']);
		$media = getMediaById ($id);
		$page_attrs['action'] = 'view';
		
	// Handle new entry screen
	} else if (!empty($_GET['action']) == 'new') {
		
		$media = TRUE;
		$page_attrs['action'] = 'new';
		
	// Handle search
	} else if (isset($_GET['terms'])) {
		
		$search_terms = $_GET['terms'];
		
		$media_items = getMediaBySearchTerms($search_terms);
		
		if ($_SESSION['user_level'] === 1) {
			$action = 'edit';
		} else {
			$action = 'view';
		}
		
	} else {
		
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}
		
		$media_items = getMedia($page_attrs['page_no'], 24);
		
		if ($_SESSION['user_level'] === 1) {
			$action = 'edit';
		} else {
			$action = 'view';
		}
		
	}
