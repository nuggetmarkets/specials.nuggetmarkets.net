<?php

    /** 
      * Select Upcoming Ad Dates
      *  
      * Selects date ranges of upcoming ads in publish status and groups by date_from
      *  
      * @author Mark Eagleton <mark@thebigreason.com> 
      * 
      * @return array $ads An array of ad date ranges
      * 
      */ 
    function selectCurrentAdDates () { 

        // Calculate start date for currnt ad
        $start_date = date('Y-m-d',strtotime('last wednesday', strtotime('tomorrow')));
		
	    if ($ads = runQuery("
            
            SELECT
                date_from, 
                date_through
            
            FROM
                ads
            
            WHERE
                publish = 1 AND
                date_from >= $start_date AND 
                date_through >= CURDATE()
                
            GROUP BY 
                date_from, 
                date_through
            
            ORDER BY
                date_from DESC
            

        ")) {

            return $ads;
        
        } else {
        
            return FALSE;
        
        }
         
    }


    /** 
      * Select Current Ads Features
      *  
      * Selects date ranges of upcoming ads in publish status and groups by date_from
      *  
      * @author Mark Eagleton <mark@thebigreason.com> 
      * 
      * @return array $features An array of ad features by ad date
      * 
      */ 
    function selectCurrentAdsFeatures () { 

        // Calculate start date for currnt ad
        $start_date = date('Y-m-d',strtotime('last wednesday', strtotime('tomorrow')));

        if ($ads = runQuery("
            
            SELECT
                instances.ad_id, 
                instances.feature, 
                ads.store_chain, 
                instances.on_ad_date, 
                instances.status
            
            FROM
                instances
                
            INNER JOIN
                ads on instances.ad_id = ads.id
            
            WHERE
                instances.feature > 0 AND
                instances.on_ad_date >= $start_date AND 
                instances.off_ad_date >= CURDATE()
            
            ORDER BY
                instances.on_ad_date DESC, 
                ads.sort_order, 
                ads.store_chain, 
                instances.feature            

        ")) {

            return $ads;
        
        } else {
        
            return FALSE;
        
        }
         
    }


    /** 
      * Select Department Statuses Items By Date 
      *  
      * Selects department name and status of enabled ad item instances for the specified date
      *  
      * @author Mark Eagleton <mark@thebigreason.com> 
      *  
      * @param string $date The date the ad starts 
      * 
      * @return array $items An array of ad items with their atrtributes 
      * 
      */ 
    function selectDepartmentStatusesByDate ($date) {
        
        if ($items = runQuery("
            
            SELECT
                ad_id, 
                family_group, 
                group_name, 
                department, 
                status
            
            FROM
                instances
            
            WHERE
                enabled = 1 AND
                on_ad_date = $date
                
            ORDER BY
                department ASC
           
        ")) {

            return $items;
        
        } else {
        
            return FALSE;
        
        }
    }


    /** 
      * selectAdIdsWithStartDate 
      *  
      * Selects the ad IDs from the database with the given start date 
      *  
      * @author Mark Eagleton <mark@thebigreason.com> 
      *  
      * @param string $date The start date for the ad IDs to retrive 
      * 
      * @return array $ad_ids The IDs of the ads returned 
      * 
      */ 
    function selectAdsWithStartDate ($date) { 
        
        if ($ad_ids = runQuery("

            SELECT
                *

            FROM
                ads

            WHERE
                date_from = $date

            ORDER BY 
              sort_order
        
        ")) {

            return $ad_ids;
        
        } else {

            return FALSE;

        }

    }
