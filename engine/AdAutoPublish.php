<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';
	
	// Get the date of the ad that needs to be published
	$ad_date = date('Y-m-d',strtotime('+1 week', strtotime('next wednesday')));
	
	// Get all upcoming ads
	$ads = getUpcomingAds();
	
	if (is_array($ads)) {
		
		// Loop through ads
		foreach ($ads['listing'] as $ad) {
			
			// Find ads that that need to go into proofing.
			if ($ad['date_from'] == $ad_date && $ad['publish'] != 1) {
				
				// Validate the ad_id
				$id = vbool($ad['id']);
				
				updateAdPublishStatus($id, 1);
				
			}
			
		}
		
	}
