<?php 

	/**
	  * Select media items with offset and limit for pagination
	  *
	  * Selects rows from the media table based on offset and limit provided
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Takes page number from Media class call
	  * @param int $limit Number of results to return
	  *
	  * @return array $media An array of selected the records
	  *
	  */
	function selectMedia ($offset, $limit) {
		
		$media = NULL;
		$count = NULL;
		
		if ($media['listing'] = runQuery("
			
			SELECT
				id,
				name,
				caption, 
				alt_text, 
				catalog_text, 
				file, 
				mime_type, 
				large_image, 
				medium_image, 
				small_image
				
			FROM
				media
				
			ORDER BY
				id DESC
				
			LIMIT 
				$offset, $limit
				
		")) {
			
			$count = runQuery("
				
				SELECT
					COUNT(media.id)
					
				FROM
					media
					
			");
			
			$media['count'] = $count[0];
			
			return $media;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select specified media row by ID
	  *
	  * Selects the specified row from the media table by ID
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $id The ID of the row.
	  *
	  * @return array $media[0] Array of the values from the row
	  *
	  */
	function selectMediaById ($id) {
		
		if ($media = runQuery("
			
			SELECT
				id,
				name,
				caption, 
				alt_text,
				catalog_text,  
				file, 
				mime_type, 
				large_image, 
				medium_image, 
				small_image
				
			FROM
				media
				
			WHERE
				id = $id
				
		")) {
			
			return $media[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select media items with specfied IDs
	  *
	  * Selects rows from the media table specified by the IDs in the arguments
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $media_ids An array of IDs to select.
	  *
	  * @return array $media Array of the values selected
	  *
	  */
	function selectMediaWithIds ($media_ids) {
		
		$id = NULL;
		$item = NULL;
		$i = 0;
		
		foreach ($media_ids as $id) {
			
			if ($i == 0) {
				$item .= "id = $id ";
			} else {
				$item .= "OR id = $id ";
			}
			
			$i++;
		}
		
		if ($media = runQuery("
			
			SELECT
				id,
				name,
				caption, 
				alt_text,
				catalog_text, 
				file, 
				mime_type, 
				large_image, 
				medium_image, 
				small_image
				
			FROM
				media
				
			WHERE
				$item
				
			ORDER BY
				mime_type, 
				name
				
		")) {
			
			return $media;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select media IDs by parent
	  *
	  * Selects media_links from specified table
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $terms The search terms to match against.
	  *
	  * @return array $result All search results in an assoc. array
	  *
	  */
	function selectMediaLinksByParent ($item_table, $item_id) {
		
		if ($links = runQuery("
			
			SELECT
				*
				
			FROM
				$item_table 
				
			WHERE 
				id = $item_id
				
		")) {
			
			return $links[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * A natural language search
	  *
	  * Selects rows from media table using FULLTEXT index
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string    $terms The search terms to match against.
	  *
	  * @return array $result All search results in an assoc. array
	  *
	  */
	function selectMediaMatchAgainstTerms ($terms) {
		
		if ($result = runQuery("
			
			SELECT
				id,
				name,
				caption, 
				alt_text, 
				catalog_text, 
				file, 
				mime_type, 
				large_image, 
				medium_image, 
				small_image
				
			FROM
				media
				
			WHERE 
				
				MATCH (
					alt_text, 
					catalog_text, 
					caption, 
					file, 
					mime_type, 
					name
				) AGAINST (
					$terms
				) 
				
		")) {
			
			return $result;	
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * A litteral search
	  *
	  * Selects rows from the media table using LIKE. This should be used when 
	  * search_terms string is too short for a FULLTEXT index search.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string    $terms The search terms to match against.
	  *
	  * @return array $result All search results in an assoc. array
	  *
	  */
	function selectMediaLikeTerms ($terms) {
		
		$search_terms = unescape($terms);
		
		if ($result = runQuery("
			
			SELECT
				id,
				name,
				caption, 
				alt_text, 
				catalog_text, 
				file, 
				mime_type, 
				large_image, 
				medium_image, 
				small_image
				
			FROM
				media
				
			WHERE 
				
				(
					alt_text LIKE '%$search_terms%' OR 
					catalog_text LIKE '%$search_terms%' OR 
					caption LIKE '%$search_terms%' OR 
					file LIKE '%$search_terms%' OR 
					mime_type LIKE '%$search_terms%' OR 
					name LIKE '%$search_terms%'
				)  
				
		")) {
			
			return $result;	
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert row into media table
	  *
	  * Inserts row into the media table with specified arguements
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $media An assoc. array of records to insert.
	  *
	  * @return array $insert A single item array containing mysqli_insert_id()
	  *
	  */
	function insertMedia ($media) {
		
        if ($insert = runQuery("
			
            INSERT INTO
                media
				
            SET
				name			= {$media['name']},
				caption			= {$media['caption']}, 
				alt_text		= {$media['alt_text']}, 
				catalog_text	= {$media['catalog_text']}, 
				file			= {$media['file']}, 
				mime_type		= {$media['mime_type']}, 
				large_image		= {$media['large_image']}, 
				medium_image	= {$media['medium_image']}, 
				small_image		= {$media['small_image']}
				
        ")) {
			
            return $insert;
			
        }
		
	}


	/**
	  * Update row in media table
	  *
	  * Updates the specified record in the media table with the specified info
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array    $media An assoc. array of records to insert.
	  *
	  * @return boolean $update True/False
	  *
	  */
	function updateMedia ($media) {
		
		if ($update = runQuery("
			
            UPDATE
                media
				
            SET
				name			= {$media['name']},
				caption			= {$media['caption']}, 
				alt_text		= {$media['alt_text']}, 
				catalog_text	= {$media['catalog_text']}, 
				file			= {$media['file']}, 
				mime_type		= {$media['mime_type']}, 
				large_image		= {$media['large_image']}, 
				medium_image	= {$media['medium_image']}, 
				small_image		= {$media['small_image']}
				
			WHERE
				id = {$media['item_id']}
				
        ")) {
			
            return $update;
			
        }	
	}
	
