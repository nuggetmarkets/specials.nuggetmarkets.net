<?php

	/**
	  * Start a session
	  */
	session_start();
	date_default_timezone_set('America/Los_Angeles');

	/**
	  * Require user to be logged in
	  */
	if (
		!isset($_SESSION['user_id']) &&
		!strstr($_SERVER['PHP_SELF'],'login.php') &&
		!strstr($_SERVER['PHP_SELF'],'logout.php') &&
		!strstr($_SERVER['PHP_SELF'],'register.php') &&
		!strstr($_SERVER['PHP_SELF'],'reset.php') &&
		!strstr($_SERVER['PHP_SELF'],'email.php') &&
		!strstr($_SERVER['PHP_SELF'],'email-text.php') && 
		!strstr($_SERVER['PHP_SELF'],'email-subscribed.php') &&
		!strstr($_SERVER['PHP_SELF'],'AdItemsAutoImport.php') && 
		!strstr($_SERVER['PHP_SELF'],'AutoAdJsonExport.php') && 
		!strstr($_SERVER['PHP_SELF'],'AutoBannersJsonExport.php') && 
		!strstr($_SERVER['PHP_SELF'],'AdPreviewAutoEmail.php') && 
		!strstr($_SERVER['PHP_SELF'],'UpdateSubscriberTemplate.php') && 
		!strstr($_SERVER['PHP_SELF'],'AdAutoPublish.php')
	) {
		
		// Capture the requested page to redirect upon login
		$_SESSION['referer'] = $_SERVER['REQUEST_URI'];
		
		// Redirect to login screen
		header("Location: /login/");
		exit();
		
	}


	/**
	  * Require global dependancies
	  */
	require_once('Query.php');
    require_once('Helpers.php');


    /**
	  * Define site constants.
	  */
    define('SITE_NAME', 'Nugget Market, Inc. E-Ad Builder');
	define('MEDIA_UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/media/');
    define('TABLES_WITH_MEDIA_LINKS', 'instances,products');
	define('CAMPAIGN_MONITOR_API_KEY', '8d0f3b191d658fbfd1a4335c5bf0e3166e4360a24393ac8e');
	define('CAMPAIGN_MONITOR_CLIENT_ID', 'b7a834fd22ab9adef76e12f431112ef8');
	define('SLACK_WEBSERVER_NOTIFICATIONS', 'https://hooks.slack.com/services/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');
	define('ITEM_LIMIT', '4');

	// File prefixes for DROSTE XML ad dumps
	define('VALLEY_AD_PREFIX', '');
	define('MARIN_AD_PREFIX', 'marin-');
	define('SONOMA_AD_PREFIX', 'sonoma-');
	define('SONOMA_MARKET_AD_PREFIX', 'sonoma-');
	define('FORKLIFT_AD_PREFIX', 'forklift-');
	define('FOOD4LESS_AD_PREFIX', 'f4l-');

	// Development/Live domains
	if (strstr(gethostname(),'.net') || strstr(gethostname(), 'fxlr-6kxv')) {
		define('ENVIRONMENT', 'live');
		define('SERVER_PATH', '/var/www/vhosts/nuggetmarkets.net/subdomains/specials.nuggetmarkets.net');
		define('NUGGET_PATH', '/var/www/vhosts/nuggetmarket.com');
		define('PROFILES_URL', 'https://profiles.nuggetmarkets.net');
		define('SPECIALS_URL', 'https://specials.nuggetmarkets.net');
		define('NUGGET_WEBSITE', 'https://www.nuggetmarket.com');
		define('SONOMA_WEBSITE', 'https://sonomamarket.net');
		define('FORK_LIFT_WEBSITE', 'https://forkliftgrocery.com');
		define('FOOD_4_LESS_WEBSITE', 'https://f4lwoodland.com');
	} else {
		define('ENVIRONMENT', 'dev');
		define('SERVER_PATH', $_SERVER['DOCUMENT_ROOT'] . '/..');
		define('NUGGET_PATH', '/Volumes/Macintosh HD/Users/marke/Sites/nuggetmarket');
		define('PROFILES_URL', 'https://profiles.nuggetmarkets.dev');
		define('SPECIALS_URL', 'https://specials.nuggetmarkets.dev');
		define('NUGGET_WEBSITE', 'https://nuggetmarket.dev');
		define('SONOMA_WEBSITE', 'https://sonomamarket.dev');
		define('FORK_LIFT_WEBSITE', 'https://forkliftgrocery.dev');
		define('FOOD_4_LESS_WEBSITE', 'https://f4lwoodland.dev');
	}


	/**
	  * Declare page header vars
	  */

	$error = FALSE;

    $page_attrs = array(
        'title' 		=> NULL,
        'class' 		=> NULL,
        'page_no' 		=> NULL,
        'action'		=> NULL,
        'post_slug' 	=> NULL,
        'append_head' 	=> NULL,
        'append_footer' => NULL
    );
