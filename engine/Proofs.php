<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';
    require_once 'ProofsController.php';
	require_once 'LabelsController.php';

    // Set page vars
	$page_attrs['title'] = "Ad Proofs";
	$page_attrs['class'] = 'proofs';


	// Handle status change post
	if (isset($_POST['approve']) || isset($_POST['submit_correction']) || isset($_POST['form_action'])) {
		
		// Validate input
		$item['ad_id'] = vbool($_POST['ad_id']);
		$item['department'] = vempty($_POST['department'], 1);
		$item['item_id'] = vempty($_POST['item_id'], 1);
		$item['item_name'] = vempty($_POST['item_name'], 1, 1);
		
		/*
			Commented out decoding of family group because it cause group names with + signs
			would not approve. I think this is because we are decoding or encoding it differently alsewhere.
			This matches the setting on the ad builder approval script, so it should be fine.
			Just want to keep this note here incase it fails again.
			
			Family group names need to be decoded for status changes to work:
			$item['family_group'] = vempty(decodeFamilyGroupUrlString($_POST['family_group']), 0, 0);
			
			Unless the family group name contains a +:
			$item['family_group'] = (string)vempty($_POST['family_group'], 0, 1);
			
			Still not sure what to do when family group names contain + and other characters that need to be converted.
			
		*/
		if (strstr($_POST['family_group'], ' + ')) {
			$item['family_group'] = (string)vempty($_POST['family_group'], 0, 1);
		} else {
			$item['family_group'] = vempty(decodeFamilyGroupUrlString((string)$_POST['family_group']), 0, 0);
		}
		
		// Force item family group to be a string so it doesn’t strip out leading zeros
		if (is_numeric($item['family_group'])) {
			$item['family_group'] = "'" . $item['family_group'] . "'";
		}
		
		$item['group_name'] = vempty($_POST['group_name'], 1, 1);
		
		// Approve item
		if (
			isset($_POST['approve']) ||
			isset($_POST['cancel_correction']) ||
			$_POST['form_action'] == 'approve' ||
			$_POST['form_action'] == 'cancel_correction'
		) {
			
			$item['status'] = vempty('approved');
			
		// Submit correction
		} else if (
			isset($_POST['submit_correction']) ||
			$_POST['form_action'] == 'submit_correction'
		) {
			
			// Check for all caps.
			if (strtoupper($_POST['message']) == $_POST['message']) {
				
				// Convert to sentence case and append heart emojis
				$message = strtolower(str_replace('!', '.', $_POST['message']) . ' ❤️❤️❤️');
				
			} else {
				$message = $_POST['message'];
			}
			
			$item['message'] = vempty($message);
			$item['posted_by'] = vempty($_SESSION['full_name']);
			$item['posted_email'] = vemail($_SESSION['email']);
			$item['status'] = vempty('issue');
			
			// Create message
			createAdItemMessage($item);
			
		// Set to pending status
		} else {
			$item['status'] = vempty('pending');
		}
		
		//  Try to change status
		if (changeAdItemStatus($item)) {
			$_SESSION['confirm'] = 'Item changed to ' . unescape($item['status']) . ' status.';
		} else {
			$_SESSION['error'] = 'Unable to change item status.';
		}
		
		//  Redirect
		header("Location: /proofs/{$_GET['date']}/{$_GET['department']}/#item-{$_POST['item_id']}");
		exit();
		
	}


	// Proof detail page
	if (isset($_GET['date']) && isset($_GET['department'])) {
		
		$ad_zones = array();
		$display_date = date('F j, Y', strtotime($_GET['date']));
		$department = ucwords($_GET['department']);
		
		// Validate input
		$date = vempty($_GET['date']);
		
		// Get ads
		$ads = getAdsWithStartDate($date);
		
		$labels = getLabels(0,9999);
		
		if ($ads) {
			
			// Loop through ids and get ad items for specified department
			foreach ($ads as $ad) {
				
				// Only return ads in published status
				if ($ad['publish'] == 1) {
					
					$ad_id = vbool($ad['id']);
					
					$ad_zones[$ad_id]['attrs'] = $ad;
					$ad_zones[$ad_id]['attrs']['store_slug'] = formatSlug($ad['store_chain']);
					$ad_zones[$ad_id]['attrs']['store_attrs'] = getStoreAttributesByName($ad['store_chain']);
					
					if ($_GET['department'] != 'features') {
						$ad_zones[$ad_id]['items'] = getWeeklyAd($ad_id, $_GET['date'], str_replace('-', ' ', $_GET['department']));
					} else {
						$ad_zones[$ad_id]['items'] = getWeeklyAd($ad_id, $_GET['date'], NULL, TRUE);
					}
				}
			}
		}
		
		// Unset the ads var
		unset($ads);
		
	// Proof listing page
	} else {
		
		// Get upcoming ad dates for proofing
		$ads = getCurrentAds();
		
		$features = getCurrentAdsFeatures();
		
	}
