<?php 
	
	// Include ads model
	include 'AdPlansModel.php';

	/**
	  * Get Upcoming Ad Plans
	  *
	  * Gets the upcoming ad plans
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $plans An array of upcoming ad plans and attributes 
	  * or next ad date if no results are found
	  *
	  */
	function getUpcomingAdPlans () {
		
		// Get upcoming ad plans
		$ad_plans = selectUpcomingAdPlans();
		
		if (is_array($ad_plans)) {
			
			// Loop through results
			$i = 0;
			foreach ($ad_plans as $plan) {
				
				// Build output array
				
				$plans['items'][$i]['id'] = $plan['id'];
				$plans['items'][$i]['ad_start_date'] = $plan['ad_start_date'];
				$plans['items'][$i]['ad_end_date'] = date('Y-m-d',strtotime($plans['items'][$i]['ad_start_date'] . "+ 6 days"));
				
				if (strtotime($plan['ad_start_date']) <= time() && strtotime($plan['ad_start_date'] . ' + 7 days') > time()) {
					$plans['items'][$i]['class'] = 'current';
				} else if (strtotime($plan['ad_start_date'] . ' + 7 days') < time()) {
					$plans['items'][$i]['class'] = 'past';
				} else {
					$plans['items'][$i]['class'] = 'upcoming';
				}
				
				// Create date ranges for ad and secret special
				$plans['items'][$i]['ad_date_range'] = convertDatesToRange($plan['ad_start_date'], $plans['items'][$i]['ad_end_date']);
				$plans['items'][$i]['secret_special_range'] = convertDatesToRange($plan['secret_special_from'], $plan['secret_special_through']);
				
				$plans['items'][$i]['secret_special_from'] = $plan['secret_special_from'];
				$plans['items'][$i]['secret_special_through'] = $plan['secret_special_through'];
				$plans['items'][$i]['secret_special_description'] = $plan['secret_special_description'];
				
				// Check for secret special alerts
				$plans['items'][$i]['secret_special_alert'] = secretSpecialAlert($plan['secret_special_description']);
				
				$plans['items'][$i]['secret_special_title'] = $plan['secret_special_title'];
				$plans['items'][$i]['secret_special_retail'] = $plan['secret_special_retail'];
				
				$plans['items'][$i]['nugget_notes'] = $plan['nugget_notes'];
				$plans['items'][$i]['sonoma_notes'] = $plan['sonoma_notes'];
				$plans['items'][$i]['fork_lift_notes'] = $plan['fork_lift_notes'];
				$plans['items'][$i]['food_4_less_notes'] = $plan['food_4_less_notes'];
				$plans['items'][$i]['general_notes'] = $plan['general_notes'];
				$plans['items'][$i]['last_updated'] = $plan['last_updated'];
				$plans['items'][$i]['updated_by'] = $plan['updated_by'];
				
				$i++;
			}
			
			// Decrement array key
			$i--;
			
			// Get update and next ad info
			$plans['last_updated'] = $plans['items'][$i]['last_updated'];
			$plans['updated_by'] = $plans['items'][$i]['updated_by'];
			$plans['next_ad'] = date('Y-m-d', strtotime($plans['items'][$i]['ad_end_date'] . "+ 1 day"));
			
		} else {
			
			$plans['items'] = FALSE;
			$plans['next_ad'] = date('Y-m-d', strtotime("next Wednesday"));
			
		}
		
		// Return results
		return $plans;
		
	}
	
	
	/**
	  * Get Ad Plan by ID
	  *
	  * Gets an ad plan based on the ID passed to it.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_plan_id The ID of the ad plan to return.
	  *
	  * @return array $plan An array of ad plan attributes.
	  *
	  */
	function getAdPlanById ($ad_plan_id) {
		
		// Validate ad plan ID
		$id = vbool($ad_plan_id);
		
		// Select ad plan from database
		$plan = selectAdPlanById($id);
		
		if ($plan['secret_special_upcs']) {
			
			// Explode comma separated list
			$plan['secret_special_upcs'] = explode(',', $plan['secret_special_upcs']);
			$i = 0;
			foreach ($plan['secret_special_upcs'] as $upc) {
				
				if (!$plan['secret_special_products'][$i] = getProductByUpc($upc)) {
					$plan['secret_special_products'][$i]['upc'] = $upc;
					$plan['secret_special_products'][$i]['pos_name'] = '(Item not in ad builder yet)';
				}
				
				$i++;
				
			}
			
		}
		
		if ($plan['secret_special_picture']) {
			$plan['secret_special_picture'] = explode(',', $plan['secret_special_picture']);
		}
		
		if ($plan['secret_special_labels']) {
			
			// Explode comma separated list
			$plan['secret_special_labels'] = explode(',', $plan['secret_special_labels']);
			
		}
		
		if (is_array($plan)) {
			
			return $plan;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
		* Secret Special Alert
		*
		* Checks for certain items in messaging to determen whether or not to append an alert to secret special UI elements
		*
		* @author Mark Eagleton <mark@thebigreason.com>
		*
		* @param string $check The string to check for certain keywords.
		*
		* @return string $alert The alert message to display
		*
		*/
	function secretSpecialAlert ($check) {
		
		$flags = array();
		$alert = NULL;
		
		if (strlen($check) < 5) {
			
			$flags[] = 'product';
			
		}
		
		if (!strstr($check, '$') && !strstr($check, '¢') && !stristr($check, 'free') && !stristr($check, '%')) {
			
			$flags[] = 'pricing';
			
		}
		
		if (!stristr($check, 'limit')) {
			
			$flags[] = 'a limit';
			
		}
		
		if (!stristr($check, 'free') && !stristr($check, '%') && (substr_count($check, '$') < 2 && substr_count($check, '¢') < 2 && !strstr($check, '¢'))) {
			
			$flags[] = 'savings';
			
		}
		
		if (!empty($flags)) {
			
			$alert = 'We need ';
			
			$i = 1;
			foreach ($flags as $flag) {
				
				if ($i == 1) {
					
					$alert .= $flag;
					
				} else if ($i >= 1 && $i == count($flags)) {
					
					$alert .= ", and $flag";
					
				} else {
					
					$alert .= ", $flag";
					
				}
				
				$i++;
			}
			
			$alert .= ' for this secret special.';
			
			return $alert;
			
		} else {
			
			return FALSE;
			
		}
		
	}
