<?php

	// Include dependencies
	require_once('Bootstrap.php');
	
	
	// Decode the error
	if (!empty($_GET['error'])) {
		
		$error['code'] = $_GET['error'];
		
		switch ($_GET['error']) {
			
			case '400' : 
				$error['message'] 		= 'Bad Request';
				$error['description'] 	= 'We&rsquo;re sorry, there seems to be a programming error on our end that broke this page.';
				break;
			
			case '401' : 
				$error['message'] 		= 'Unauthorized';
				$error['description'] 	= 'We&rsquo;re sorry, this section of the website is for Associates only.';
				break;
			
			case '403' : 
				$error['message'] 		= 'Forbidden';
				$error['description'] 	= 'We&rsquo;re sorry, you don&rsquo;t have access to view this page.';
				break;
				
			case '404' : 
				$error['message'] 		= 'Not Found';
				$error['description'] 	= 'We&rsquo;re sorry, the page you are looking for is not here.';
				break;
				
			case '408' : 
				$error['message'] 		= 'Request Timeout';
				$error['description'] 	= 'We&rsquo;re sorry, our web server seems to be bogged down for the time being. Try this page again in a little bit.';
				break;
			
			case '500' : 
				$error['message'] 		= 'Internal Server Error';
				$error['description'] 	= 'We&rsquo;re sorry, our web server seems to be broken. We&rsquo;re looking into it!';
				break;
		
		}
		
	}
	
	
	// Set relivant page attributes
	$page_attrs['title'] = $error['message'];
	$page_attrs['class'] = 'error';
	$page_attrs['description'] = $error['description'];

