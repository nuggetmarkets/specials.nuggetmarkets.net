<?php
	
	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'ProductsController.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';
	

	// Redirect if not admin
	if (!in_array('products', $_SESSION['access'])) {
		
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the products, yo.';
		header("Location: /");
		exit();
		
	}

	// Declare vars
	$products = NULL;
	$product = NULL;

	
	// Set page attrs
	$page_attrs['title'] = 'Manage Products';
	$page_attrs['description'] = 'Manage products.';
	$page_attrs['class'] = 'products';
	
	
	// Get all labels
	$labels = getLabels(0,9999);

	
	// Handle post
	if (isset($_POST['add']) || isset($_POST['save'])) {
		
		// Require UPC
		if (!$upc = vempty($_POST['upc'],1,0)) {
			$error[] = 'A unique UPC is required.';
		} else {
			// Format 13 digit UPC and validate.
			$item['upc'] = "'" . str_pad(vempty($_POST['upc'],0,0),13,STR_PAD_LEFT) . "'";
		}
		
		// Validate these if adding...
		if (isset($_POST['add'])) {
			
			// Check for duplicate UPC
			if (checkForDuplicateUpc($item['upc'])) {
				$error[] = 'This UPC already exists in the product table.';
			}
			
			// Require and validate family_group
			if (!$item['family_group'] = vempty($_POST['family_group'],1,0)) {
				$error[] = 'A family group is required';
			}
			
			// Require and validate pos_name
			if (!$item['pos_name'] = vempty($_POST['pos_name'],1,0)) {
				$error[] = 'A POS name is required';
			}
			
			$item['lifestyle'] = vempty($_POST['lifestyle']);
			$item['department'] = vempty($_POST['department']);
		
		}
		
		$item['prefix'] 		= vempty($_POST['prefix'],0,1);
		$item['display_name'] 	= vempty($_POST['display_name'],0,1); 
		$item['suffix'] 		= vempty($_POST['suffix'],0,1); 
		$item['note'] 			= vempty($_POST['note'],0,1);
		$item['description'] 	= vempty($_POST['description']); 
		$item['retail_price'] 	= vempty($_POST['retail_price']); 
		$item['size'] 			= vempty($_POST['size']); 
		$item['unit'] 			= vempty($_POST['unit']); 
		$item['container'] 		= vempty($_POST['container']);
		$item['limit'] 			= vempty($_POST['limit']);
		
		// Capture and validate labels
		if (!empty($_POST['labels'])) {
			
			$i = 0;
			foreach ($_POST['labels'] as $label) {
				
				if ($i == 0) {
					$labs .= $label;
				} else {
					$labs .= ',' . $label;
				}
				$i++;

			}
			
			// Escape label IDs
			$labels = vempty($labs,0,0);
						
		} else {
			
			$labels = 'NULL';
			
		}
		
		$item['labels'] = $labels;
		
		// Handle post if there are no errors
		if (!$error) {
			
			if (isset($_POST['add'])) {
				
				
				
				// Insert item data
				if ($productInsert = insertProduct($item)) {
					
					// Upload UPC image
					if ($_FILES['upc_image']['name']) {
						move_uploaded_file($_FILES['upc_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/img/upc/' . unescape($item['upc']) . '.jpg');
					}
					
					// Set confirmation
					$_SESSION['confirm'] = '<p>Product created.</p>';
					
					// Redirect
					header("Location: /products/edit/" . unescape($productInsert) . "/");
					exit();

				}
				
			} else if (isset($_POST['save'])) {
				
				// Insert item data
				if ($productUpdate = updateProduct($item)) {
					
					// Upload UPC image
					if ($_FILES['upc_image']['name']) {
						move_uploaded_file($_FILES['upc_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/img/upc/' . unescape($item['upc']) . '.jpg');
					}
					
					// Set confirmation
					$_SESSION['confirm'] = '<p>Product saved.</p>';
					
					// Redirect
					header("Location: /products/edit/" . unescape($productUpdate['upc']) . "/");
					exit();
					
				}
				
			}
			
		}

		
		
		
		
	} else if (isset($_GET['upc']) && isset($_GET['action']) == 'edit') {
		
		$upc = vbool($_GET['upc']);
		$item = getProductByUpc ($upc);
		
		// Get selected labels for the item group
		$item['selected_labels'] = explode(',', $item['labels']);
		
		// Check for UPC image
		if (file_exists('img/upc/' . $item['upc'] . '.jpg')) {
			$item['upc_image'] = $item['upc'] . '.jpg';
		} else {
			$item['upc_image'] = NULL;
		}
		
		$page_attrs['title'] = 'Edit ' . $item['upc'];
		$page_attrs['description'] = 'Editing ' . $item['upc'];
		$page_attrs['class'] .= ' edit';
		$page_attrs['post_slug'] = "edit/{$item['upc']}/";

		
	} else if (isset($_GET['action']) == 'new') {
	
		$item = TRUE;
		$page_attrs['title'] = 'Add New Product';
		$page_attrs['description'] = 'Add a new product.';
		$page_attrs['class'] .= ' new';
		$page_attrs['post_slug'] = "new/";
		
	// Handle search	
	} else if (isset($_GET['terms'])) {
		
		$search_terms = $_GET['terms'];
		
		$products = getProductsBySearchTerms($search_terms);
	
	} else {
		
		
		
	}
