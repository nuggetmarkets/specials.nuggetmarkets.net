<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'RetractionsController.php';
	require_once 'AdsController.php';


	// Declare vars
	$retractions = NULL;
	$retraction = NULL;


	// Set page attrs
	$page_attrs['title'] = 'Manage Retractions';
	$page_attrs['description'] = 'Manage retractions.';
	$page_attrs['class'] = 'retractions';


	// Handle form post
	if (isset($_POST['save']) || isset($_POST['add'])) {

		// Require and validate name
		if (!$retraction['name'] = vempty($_POST['name'],1,1)) {
			$error[] = 'A name is required';
		}

		$retraction['description'] = vempty($_POST['description'],0,0);
		$retraction['ad_id'] = vbool($_POST['ad_id']);

		if (!$error) {

			if (isset($_POST['save'])) {

				$retraction['retraction_id'] = vbool($_POST['retraction_id']);

				// Update article
				updateRetraction($retraction);

				// Set message and redirect
				$_SESSION['confirm'] = 'Retraction updated.';

			} else if (isset($_POST['add'])) {

				// Insert article
				$result = insertRetraction($retraction);

				// Capture and set user_id for redirect
				$retraction['retraction_id'] = $result['insert_id'];

				// Set message and redirect
				$_SESSION['confirm'] = 'Retraction created.';

			}

			header("Location: /retractions/edit/{$retraction['retraction_id']}/");
			exit();

		}

	// Set up for new item form
	} else if (!empty($_GET['action']) && $_GET['action'] == 'new') {

		// Redirect if not admin
		if ($_SESSION['user_level'] > 1) {

			$_SESSION['error'] = 'You don&rsquo;t have access to create retractions.';
			header("Location: /retractions/");
			exit();

		}

		$retraction = NULL;
		$ads = getAds();

	// Set up for Edit, Duplicate and Print
	} else if (!empty($_GET['id']) && $_GET['action'] != 'new') {

		// Get retraction
		$retraction = getRetraction ($_GET['id']);

		// Set additional options if printing
		if ($_GET['action'] == 'print') {

			$retraction['store_slug'] = formatSlug($retraction['store_chain']);

			// Set logo file name
			$retraction['logo'] = $retraction['store_slug'] . '-black.svg';

			// Set retraction file name
			$retraction['file_name'] = formatSlug($retraction['store_chain'] . '-retraction-' . $retraction['date_from']) . '-' . formatSlug($retraction['name']);

			// Populate date grid
			$ad['date_1'] = date('j',strtotime($retraction['date_from']));
			$ad['date_2'] = date('j',strtotime("+1 day", strtotime($retraction['date_from'])));
			$ad['date_3'] = date('j',strtotime("+2 day", strtotime($retraction['date_from'])));
			$ad['date_4'] = date('j',strtotime("+3 day", strtotime($retraction['date_from'])));
			$ad['date_5'] = date('j',strtotime("+4 day", strtotime($retraction['date_from'])));
			$ad['date_6'] = date('j',strtotime("+5 day", strtotime($retraction['date_from'])));
			$ad['date_7'] = date('j',strtotime("+6 day", strtotime($retraction['date_from'])));

			$ad['day_1'] = date('D',strtotime($retraction['date_from']));
			$ad['day_2'] = date('D',strtotime("+1 day", strtotime($retraction['date_from'])));
			$ad['day_3'] = date('D',strtotime("+2 day", strtotime($retraction['date_from'])));
			$ad['day_4'] = date('D',strtotime("+3 day", strtotime($retraction['date_from'])));
			$ad['day_5'] = date('D',strtotime("+4 day", strtotime($retraction['date_from'])));
			$ad['day_6'] = date('D',strtotime("+5 day", strtotime($retraction['date_from'])));
			$ad['day_7'] = date('D',strtotime("+6 day", strtotime($retraction['date_from'])));

		}

		// Get ads if editing or creating new retraction
		if ($_GET['action'] != 'print') {

			// Redirect if not admin
			if ($_SESSION['user_level'] > 1) {

				$_SESSION['error'] = 'You don&rsquo;t have access to edit retractions.';
				header("Location: /retractions/");
				exit();

			}

			$ads = getAds();

		}

	// List retractions
	} else {

		// Get current page number
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}

		// Get the ads
		$retractions = getRetractions($page_attrs['page_no'], 25);

	}
