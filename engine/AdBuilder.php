<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaModel.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';


	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
		
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}

	// Handle group items post
	if (isset($_POST['group_items'])) {
		
		$result = NULL;
		
		// Loop through the item groups
		$i = 0;
		foreach ($_POST['group'] as $key => $group) {
			
			// Validate input
			$item['group_name'] 	= vempty($_POST['group_name'], 0, 1);
			$item['ad_id'] 			= vbool($_POST['ad_id']);
			$item['family_group'] 	= vempty(decodeFamilyGroupUrlString($group),1,0);
			$item['item_id']		= vempty($_POST['ad_id'] . '-' . str_replace ( 'family-', '', $key ));
			
			if ($i < 1) {
				$group_id = unescape($item['item_id']);
			}
			
			// Update the items
			if (updateAdItemGroupName($item)) {
				
				// Update any messages with the new group name so they stays attached
				updateMessageGroupNameById($item);
				
				// Update item in sister ad if sister ads box is checked
				if (isset($_POST['update_sister_ads'])) {
					
					// Get sister ads
					$sister_ads = getSisterAds($_POST['ad_date']);
					
					// Loop through sister ads
					foreach ($sister_ads['ads'] as $sister) {
						
						// Only run if it isn’t the current ad
						if ($sister['id'] != $item['ad_id']) {
							
							// Update vars for sister ads
							$item['ad_id'] 	= vbool($sister['id']);
							$item['item_id'] = vempty($sister['id'] . '-' . str_replace ( 'family-', '', $key ));
							
							// Update the sister ad item
							updateAdItemGroupName($item);
							
							// Update any messages with the new group name so they stays attached
							updateMessageGroupNameById($item);
							
						}
					}
				}
				
				$result = "Items grouped as {$item['group_name']}.";
				
			} else {
				
				$result = 'Unable to group items.';
			}
			
			$i++;
			
		}
		
		// Set confirmation message
		$_SESSION['confirm'] = $result;
		
		// Redirect
		header("Location: /ad-builder/" . $_POST['ad_id'] . '/#item-' . $group_id);
		exit();
		
	// Handle status change
	} else if (isset($_POST['rename_group'])) {
		
		$item['new_group_name'] = vempty($_POST['new_group_name'],true,true);
		$item['current_group_name'] = vempty($_POST['current_group_name'],true,true);
		$item['ad_id'] = vempty($_POST['ad_id']);
		
		// Update name
		if ($update = updateManualGroupName($item)) {
			
			// Set confirmation
			$_SESSION['confirm'] = 'Group name updated.';
			
		} else {
			
			// Set error
			$_SESSION['confirm'] = 'An error occurred. Unable to rename group.';
		}
		
		// Redirect
		header("Location: /ad-builder/{$item['ad_id']}/#{$_POST['item_id']}");
		exit();
		
	// Handle status change
	} else if (isset($_POST['approve']) || isset($_POST['request_approval'])) {
		
		// Validate input
		$item['ad_id'] 			= vbool($_POST['ad_id']);
		$item['department'] 	= vempty($_POST['department'], 1);
		$item['item_id'] 		= vempty($_POST['item_id'], 1);
		$item['item_name'] 		= vempty($_POST['item_name'], 1, 1);
		$item['family_group'] 	= (string)vempty(decodeFamilyGroupUrlString($_POST['family_group']), 0, 0);
		$item['group_name'] 	= vempty($_POST['group_name'], 1, 1);
		
		//  Set item name for notification email
		if (isset($_POST['item_name'])) {
			$display_name = $_POST['item_name'];
		} else if (isset($_POST['group_name'])) {
			$display_name = $_POST['group_name'];
		} else {
			$display_name = $_POST['family_group'];
		}
		
		// Check for reply to
		if (isset($_POST['message_reply_to'])) {
			
			// Build array of reply to addresses
			foreach ($_POST['message_reply_to'] as $address) {
				$addresses[] = $address;
			}
			
			// Override notification emails for dev environment
			if (ENVIRONMENT == 'dev') {
				$addresses = array('Mark Eagleton <mark.eagleton@nuggetmarket.com>');
			}
			
			// Set up notification message
			$notification_email_id = '5c6c4445-d8bb-46f7-ae64-6b97efb500cf';
			$notification_message = array(
				"To" => $addresses,
				"Data" => array(
					'item_name' => $display_name,
					'item_link' => SPECIALS_URL . "/proofs/{$_POST['ad_date']}/{$_POST['department']}/#item-{$_POST['item_id']}",
					'message' => $_POST['message'],
					'from' => $_SESSION['full_name']
				)
			);
		}
		
		if (isset($_POST['approve'])) {
			$item['status'] = vempty('approved');
		} else {
			$item['status'] = vempty('pending');
		}
		
		// Submit requiest approval message
		if (isset($_POST['request_approval']) && isset($_POST['message'])) {
			
			$item['message'] = vempty($_POST['message']);
			$item['posted_by'] = vempty($_SESSION['full_name']);
			$item['posted_email'] = vemail($_SESSION['email']);
			
			// Create message
			createAdItemMessage($item);
			
		}
		
		//  Try to change status
		if (changeAdItemStatus($item)) {
			$_SESSION['confirm'] = 'Item in ' . unescape($item['status']) . ' status.';
			
			// Send email notification to request approval
			if (ENVIRONMENT == 'live') {
				sendCMMail($notification_message, $notification_email_id, 'Unchanged', false);
			}
			
		} else {
			$_SESSION['error'] = 'Unable to change item status.';
		}
		
		//  Redirect
		header("Location: /ad-builder/{$_POST['ad_id']}/#item-{$_POST['item_id']}");
		exit();
		
	}

	// Validate ad id
	$ad_id = vbool($_GET['id']);

	// Get ad data
	$ad = selectAdById($ad_id);
	
	// Get store attributes
	$ad['store_attributes'] = getStoreAttributesByName($ad['store_chain']);

	$page_attrs['title'] = 'Ad Builder';
	$page_attrs['description'] = 'Manage ad items appearing on the ' . $ad['store_chain'] . '.';
	$page_attrs['class'] = "builder";

	if (!empty($_GET['enable'])) {
		
		if (enableAdItemInstance($ad_id, $_GET['enable'])) {
			
			$confirm = "{$_GET['enable']} reenabled.";
			
		} else {
			
			$error = "Unable to reenable {$_GET['enable']}.";
			
		}
		
	}

	if (!empty($ad['date_from'])) {
		
		// Get previous and next ad info for navigation
		$ad_nav_dates = getPrevAndNextAds($ad['date_from'], $ad['store_chain']);
		
		// Get this week's ads for other stores
		$sister_ads = getSisterAds($ad['date_from']);
		
		// Get featured items
		$features = getWeeklyAd($ad_id, $ad['date_from'], FALSE, TRUE);
		
		// Get manually disabled items
		$disabled = getDisabledAdItems($ad_id);
		
		// Get items by department
		$meat 		= getWeeklyAd($ad_id, $ad['date_from'], 'meat');
		$produce	= getWeeklyAd($ad_id, $ad['date_from'], 'produce');
		$grocery 	= getWeeklyAd($ad_id, $ad['date_from'], 'staples');
		$dairy 		= getWeeklyAd($ad_id, $ad['date_from'], 'dairy');
		$frozen 	= getWeeklyAd($ad_id, $ad['date_from'], 'frozen');
		$adultbev	= getWeeklyAd($ad_id, $ad['date_from'], 'adult beverages');
		$bakery		= getWeeklyAd($ad_id, $ad['date_from'], 'bakery');
		$deli 		= getWeeklyAd($ad_id, $ad['date_from'], 'deli');
		$general 	= getWeeklyAd($ad_id, $ad['date_from'], 'general merchandise');
		$healthy 	= getWeeklyAd($ad_id, $ad['date_from'], 'healthy living');
		$cheese 	= getWeeklyAd($ad_id, $ad['date_from'], 'specialty cheese');
		$coffeebar 	= getWeeklyAd($ad_id, $ad['date_from'], 'coffee bar');
		$kitchen 	= getWeeklyAd($ad_id, $ad['date_from'], 'kitchen');
		$floral 	= getWeeklyAd($ad_id, $ad['date_from'], 'floral');
		
		$labels = getLabels(0,9999);
		
		$page_attrs['title'] = $ad['store_chain'] . ' ' . date('F j, Y', strtotime($ad['date_from'])) . ' Ad Builder';
		$page_attrs['description'] = 'Manage items appearing on the ' . date('F j, Y', strtotime($ad['date_from'])) . ' ' . $ad['store_chain'];
		$page_attrs['class'] .= ' detail publish-' . $ad['publish'];
	}
