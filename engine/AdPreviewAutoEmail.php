<?php 

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'createsend/csrest_campaigns.php';
	
	// Get the ad date
	$ad_date = date('Y-m-d',strtotime('next wednesday', strtotime('tomorrow')));

	// Set up the display date
	$display_date = date('F j, Y', strtotime($ad_date));

	// Set CM API key
	$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);

	// Override notification emails for dev environment
	// Note: there is a limit of 25 addresses for these emails across to, cc and bcc
	if (ENVIRONMENT == 'dev') {
		$addresses = array('Mark Eagleton <mark.eagleton@nuggetmarket.com>');
	} else {
		$addresses = array(
			'Grocery Managers <grocerymanagers@nuggetmarket.com>', 
			'Store Directors <storedirectors@nuggetmarket.com>',
			'POS <pos@nuggetmarket.com>', 
			'Adnug <adnug@nuggetmarket.com>', 
			'Kitchen Managers <kitchenmanagers@nuggetmarket.com>', 
			'PICs <pics@nuggetmarket.com>', 
			'Meat Deli <meatdeli@nuggetmarket.com>', 
			'Meat Managers <meatmanagers@nuggetmarket.com>', 
			'Bakery Managers <bakerymanagers@nuggetmarket.com>',
			'Produce <produce@nuggetmarket.com>', 
			'Dairy <Dairy@nuggetmarket.com>', 
			'Frozen <Frozen@nuggetmarket.com>',
			'Bulk <bulk@nuggetmarket.com>', 
			'Wine <winenug@nuggetmarket.com>',
			'General Merchandise <gm@nuggetmarket.com>', 
			'HBC <hbc@nuggetmarket.com>',
			'Healthy Living <healthyliving@nuggetmarket.com>', 
			'Cheese Specialists <cheesespecialists@nuggetmarket.com>', 
			'Bakery LV1 <bakerylv1@nuggetmarket.com>', 
			'Stock Crew Managers <StockCrewManagers@nuggetmarket.com>', 
			'Specialty <specialty@nuggetmarket.com>', 
			'Receiving <receiving@nuggetmarket.com>'
		);
	}

	// Set up the message message
	$email_id = '350a9a5f-46e7-4fa5-999b-8361366ebc7d';
	$message = array(
		"To" => $addresses,
		"Data" => array(
			'ad_date' => $ad_date,
			'display_date' => $display_date
		)
	);

	// Send the email
	$send = sendCMMail($message, $email_id, 'Unchanged', false);
