<?php

	/*
		This template generator is not currently used.
		It didn’t have the intended effect of updating a Campaign Monitor Journy :(
	*/

	// Require dependencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';


	// Set up zones array
	$zones = array(
		'Nugget Markets Valley', 
		'Nugget Markets Marin', 
		'Sonoma Market', 
		'Fork Lift'
	);



	// Loop through zones 
	foreach ($zones as $store) {

		// Get zone attributes
		$zone = getStoreAttributesByName($store);



		updateCMTemplate($template_id, $template_name, $html_file, $zip_file);

	}
