<?php
	
	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaModel.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';
	
	
	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
			
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}
	
	// Validate ad id
	$ad_id = vbool($_GET['id']);
	
	// Get ad data
	$ad = selectAdById($ad_id);
	$ad['ad_duration'] = convertDatesToDuration($ad['date_from'], $ad['date_through']);
	
	// Set up file names and directory structure for cache and PDF version
	$store_slug = formatSlug($ad['store_chain']);
	$year = date('Y',strtotime($ad['date_from']));
	
	$cache_file_name = $store_slug . '-' . $ad['date_from'] . '.json';
	$cache_file_path = $_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/cache/' . $store_slug . '/' . $year . '/';
	
	$pdf_file_name = $store_slug . '-specials-starting-' . $ad['date_from'] . '.pdf';
	$pdf_file_path = $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/print-versions/';
	
	// Get a list of departments with items in the ad
	$departments = selectDepartmentsByAd ($ad['id']);
	
	
	// See if print ad exists by getting file size
	if ($filesize = @filesize($pdf_file_path . $pdf_file_name)) {
		
		// Set file name
		$ad['pdf']['file'] = '/print-versions/' . $pdf_file_name;
		
		// Round file size to MB
		$ad['pdf']['size'] = round($filesize/1000000, 1) . ' MB';
		
	} else {
		
		$ad['pdf'] = NULL;
		
	}
	
	// Get labels
	$ad['labels'] = getLabels(0,9999);
	
	// Set up ad departments array
	$ad['departments'] = array();

	// If ad exists		
	if (!empty($ad['date_from'])) {
		
		// Append the features to the ad array 
		$features = getWeeklyAd($ad_id, $ad['date_from'], FALSE, TRUE);
		
		if (count($features) >= 1) {
			$ad['departments']['features'] = $features;
		}
		
		// Loop through departments and append the products
		foreach ($departments as $department) {
			$ad['departments'][$department] = getWeeklyAd($ad_id, $ad['date_from'], $department);
		}
		
	}
	
	// Encode JSON
	$json = json_encode($ad, JSON_PRETTY_PRINT);
	
	// Create directory if nessasary
	if (!is_dir($cache_file_path)) {
		mkdir($cache_file_path, 0755, TRUE);
	}
	
	$file = $cache_file_path . $cache_file_name;
	
	// Write JSON to cache file
	file_put_contents($file, $json);
	
	/*
	// Copy file to front end website
	$dest = NUGGET_PATH . '/httpdocs/cache/' . $store_slug . '-' . $ad['date_from'] . '.json';
	copy($file, $dest);
	*/
	
	$_SESSION['confirm'] = 'Ad data exported to cache. These changes may take up to 10 minutes to propagate to the live website.';
	
	
	header("Location: {$_SERVER['HTTP_REFERER']}");
	exit();
