<?php
	
	/**
	  * Select Retractions
	  *
	  * Selects all retractions from database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Takes page number from Retractions class call
	  * @param int $limit Number of results to return
	  *
	  * @return array $retractions An array of selected the records
	  *
	  */
	function selectRetractions ($offset, $limit) {
			
		if ($retractions['listing'] = runQuery("
			
			SELECT
				retractions.id, 
				retractions.name, 
				retractions.description,
				
				ads.store_chain, 
				ads.date_from, 
				ads.date_through
				
			FROM
				retractions
				
			INNER JOIN
				ads ON retractions.ad_id = ads.id
				
			ORDER BY
				ads.date_from DESC
				
			LIMIT 
				$offset, $limit
		
		")) {
			
			$count = runQuery("
			
				SELECT
					COUNT(retractions.id)
					
				FROM
					retractions
					
				INNER JOIN
					ads ON retractions.ad_id = ads.id
			
			");
			
			$retractions['count'] = $count[0];

			return $retractions;
				
		} else {
				
			return FALSE;
				
		}
			
	}
	
	/**
	  * Select Retraction by ID
	  *
	  * Selects the specified retraction with the given ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $retraction_id Takes page number from Retractions class call
	  *
	  * @return array $retraction An array of selected the records
	  *
	  */
	function selectRetractionById ($retraction_id) {
			
		if ($retraction = runQuery("
			
			SELECT
				retractions.id, 
				retractions.ad_id,
				retractions.name, 
				retractions.description,
				
				ads.store_chain, 
				ads.date_from, 
				ads.date_through
				
			FROM
				retractions
				
			INNER JOIN
				ads ON retractions.ad_id = ads.id
				
			WHERE
				retractions.id = $retraction_id
				
			
		")) {
			
			return $retraction[0];
				
		} else {
				
			return FALSE;
				
		}
			
	}

	/**
	  * Insert Retraction
	  *
	  * Inserts a new retraction record into the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $retraction An array containing the retraction data
	  *
	  * @return array $insert An array containing the MYSQL insert_id
	  *
	  */
	function insertRetraction ($retraction) {
		
        if ($insert = runQuery("

            INSERT INTO
                retractions

            SET
                ad_id 			= {$retraction['ad_id']},
				name 			= {$retraction['name']},
				description 	= {$retraction['description']}

        ")) {

            return $insert;

        }
	
	}

	/**
	  * Update Retraction
	  *
	  * Updates a retraction entry in the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $retraction An array containing the retraction data
	  *
	  * @return TRUE|FALSE $update
	  *
	  */
	function updateRetraction ($retraction) {
		
		if ($update = runQuery("

            UPDATE
                retractions

            SET
                ad_id 			= {$retraction['ad_id']},
				name 			= {$retraction['name']},
				description 	= {$retraction['description']}
				
			WHERE
				id = {$retraction['retraction_id']}

        ")) {

            return $update;

        }	
	}
	