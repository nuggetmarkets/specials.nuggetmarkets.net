<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'AdPlansController.php';
	require_once 'AdPlansController.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';
	require_once 'ProductsController.php';

	// Handle new ad plan insert
	if (!empty($_GET['action']) && !empty($_GET['date'])) {
		
		// Redirect if not admin
		if ($_SESSION['user_level'] > 1) {
			
			$_SESSION['error'] = 'You don&rsquo;t have access to create ad plans.';
			header("Location: /ad-plans/");
			exit();
			
		}
		
		// Validate date
		$new_plan['date'] = vempty($_GET['date'],1,0);
		$new_plan['user'] = vempty($_SESSION['full_name'],1,0);
		
		// Insert the record
		if (insertAdPlan($new_plan)) {
			
			// Set confirmation on sucess
			$_SESSION['confirm'] = 'New ad plan created.';
			
		} else {
			
			// Set error on fail
			$_SESSION['error'] = 'The new ad plan couldn&rsquo;t be created.';
			
		}
		
		// Redirect
		header("Location: /ad-plans/");
		exit();
		
	// Handle save
	} else if (isset($_POST['save'])) {
		
		// Validate input
		$plan['id'] 						= vbool($_POST['ad_plan_id']);
		$plan['secret_special_from'] 		= vempty($_POST['secret_special_from']);
		$plan['secret_special_through'] 	= vempty($_POST['secret_special_through']);
		$plan['secret_special_title'] 		= vempty($_POST['secret_special_title']);
		$plan['secret_special_description'] = vempty($_POST['secret_special_description']);
		$plan['secret_special_retail'] 		= vempty($_POST['secret_special_retail']);
		$plan['secret_special_limit'] 		= vempty($_POST['secret_special_limit']);
		
		$plan['secret_special_valley_regular_retail'] 		= vempty($_POST['secret_special_valley_regular_retail']);
		$plan['secret_special_marin_regular_retail'] 		= vempty($_POST['secret_special_marin_regular_retail']);
		$plan['secret_special_sonoma_regular_retail'] 		= vempty($_POST['secret_special_sonoma_regular_retail']);
		$plan['secret_special_fork_lift_regular_retail'] 	= vempty($_POST['secret_special_fork_lift_regular_retail']);
		$plan['secret_special_food_4_less_regular_retail'] 	= vempty($_POST['secret_special_food_4_less_regular_retail']);
		
		// Remove any empty UPCs
		$upcs = array_filter($_POST['secret_special_upcs']);
		$pics = array_filter($_POST['secret_special_picture']);
		
		// Check for UPCs
		if (count($upcs) > 0) {
			
			// Implode into a comma separated list for storage
			$plan['secret_special_upcs'] = vempty(implode(',', $upcs));
			$plan['secret_special_picture'] = vempty(implode(',', $pics));
			
			// Zero pad UPCs and escape
			if (strlen($plan['secret_special_upcs']) < 13) {
				$plan['secret_special_upcs'] = "'" . str_pad(unescape($plan['secret_special_upcs']), 13, "0", STR_PAD_LEFT) . "'";
			}
			
			// Zero pad pic UPCs and escape
			if (strlen($plan['secret_special_picture']) < 13) {
				$plan['secret_special_picture'] = "'" . str_pad(unescape($plan['secret_special_picture']), 13, "0", STR_PAD_LEFT) . "'";
			}
			
		} else {
			
			// Set UPCs to null
			$plan['secret_special_upcs'] = vempty($_POST['secret_special_upcs']);
			$plan['secret_special_picture'] = vempty($_POST['secret_special_upcs']);
			
		}
		
		// Implode labels into a comma separated list for storage
		$plan['secret_special_labels'] = vempty(implode(',', $_POST['secret_special_labels']));
		
		$plan['nugget_notes'] 				= vempty($_POST['nugget_notes']);
		$plan['sonoma_notes'] 				= vempty($_POST['sonoma_notes']);
		$plan['fork_lift_notes'] 			= vempty($_POST['fork_lift_notes']);
		$plan['food_4_less_notes'] 			= vempty($_POST['food_4_less_notes']);
		$plan['general_notes'] 				= vempty($_POST['general_notes']);
		$plan['updated_by'] 				= vempty($_SESSION['full_name']);
		
		if (updateAdPlan($plan)) {
			
			$_SESSION['confirm'] = 'Ad plan updated.';
			
		} else {
			
			$_SESSION['error'] = 'The ad plan could not be updated.';
			
		}
		
		header("Location: /ad-plans/edit/{$plan['id']}/");
		exit();
		
	// Handle append famiy group
	} else if (isset($_POST['append_family_group'])) {
		
		// Get the products for the specified family group
		$products = getProductByFamilyGroup($_POST['secret_special_family_group']);
		
		// Validate plan ID and updated by
		$plan['id'] 					= vbool($_POST['ad_plan_id']);
		$plan['updated_by'] 			= vempty($_SESSION['full_name']);
		
		// Check for results
		if (is_array($products)) {
			
			// Merge current UPCs with new ones and remove any empty UPCs
			$upcs = array_filter(array_merge($_POST['secret_special_upcs'], array_column($products, 'upc')));
			$pics = array_filter(array_merge($_POST['secret_special_picture'], array_column($products, 'upc')));
			
			// Implode into a comma separated list for storage
			$plan['secret_special_upcs'] = vempty(implode(',', $upcs));
			$plan['secret_special_picture'] = vempty(implode(',', $pics));
			
			// Insert the UPCs into the ad plan
			updateAdPlanUPCs($plan);
			
			// Set confirmation message
			$_SESSION['confirm'] = 'Family group UPCs added to ad plan.';
			
		// Set error if products not found
		} else {
			
			$_SESSION['error'] = 'The specified familyl group was not found.';
			
		}
		
		// Redirect
		header("Location: /ad-plans/edit/{$plan['id']}/");
		exit();
		
	// Handle edit ad plan
	} else if (isset($_GET['id']) && isset($_GET['action']) == 'edit') {
		
		$plan = getAdPlanById($_GET['id']);
		
		// die(var_dump($plan));
		
		// Get all labels
		$labels = getLabels(0,9999);
		
	} else {
		
		$plans = getUpcomingAdPlans();
		
	}


	$page_attrs['title'] = 'Ad Plans ';
	$page_attrs['description'] = 'Manage upcoming ad plans.';
	$page_attrs['class'] = 'ad-plans';
