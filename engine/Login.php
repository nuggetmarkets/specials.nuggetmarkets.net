<?php

    // Include dependencies
	require_once('Bootstrap.php');
	require_once('UsersModel.php');

	// Declare vars
    $error = NULL;
    $profile = NULL;
    $password = NULL;

	
	/**
	  * Log in user
	  *
	  * Populates user session by selecting profile information from database based on email and password
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $email An email address.
	  * @param string $password A password.
	  *
	  * @return boolean Returns TRUE or FALSE
	  *
	  */
	function logInUser ($email, $password) {

        // Select profile based on email
        if ($profile = getUserByEmail($email)) {
	        
	        // Set message and redirect if user is disabled
            if ($profile['disabled'] == 1) {
	            
	            // Set error message
                $_SESSION['error'] = 'This profile is disabled.';

                // Return false
                return FALSE;
                
            }
            
            $profile['access'] = json_decode($profile['access'], TRUE);
            
            // Set message and redirect if user does not have access to Sonoma Market Website
            if ($profile['access']['active'] != 1) {
	            
	            // Set error message
                $_SESSION['error'] = 'You do not have access to the ' . SITE_NAME . ' content management system. Contact the Nugget Markets Marketing department if you require access.';

                // Return false
                return FALSE;
                
            }

            // Verify correct password
            if (password_verify(unescape($password), $profile['password'])) {

                // Set session variables
                $_SESSION['user_id']        		= $profile['id'];
                $_SESSION['email']          		= $profile['email'];
                $_SESSION['date_created']   		= $profile['date_created'];
                $_SESSION['last_login']     		= $profile['last_login'];
				$_SESSION['first_name']     		= $profile['first_name'];
				$_SESSION['last_name']    			= $profile['last_name'];

				if ($profile['first_name'] && $profile['last_name']) {
					$_SESSION['full_name']    		= $profile['first_name'] . ' ' . $profile['last_name'];
				} else {
					$_SESSION['full_name']			= NULL;
				}
				
				if ($profile['avatar']) {
					$_SESSION['avatar']    			= PROFILES_URL . '/media/' . $profile['avatar'];
				} else {
					$_SESSION['avatar']				= PROFILES_URL . '/img/default-avatar.svg';
				}
				
				$_SESSION['user_level'] = $profile['access']['level'];
				$_SESSION['access'] 	= $profile['access']['sections'];
				
				// Update last_login
				$user_id = vbool($_SESSION['user_id']);
				updateLastLogin($user_id);

                // Unset the $profile variable
                unset($profile);

                // Return true
                return TRUE;

            } else {

                // Set error message
                $_SESSION['error'] = 'Your email address or password is incorrect.';

                // Return false
                return FALSE;

            }

        } else {

            // Set error message
            $_SESSION['error'] =  'Please check your email address or password.';

            // Return false
            return FALSE;

        }

    }


	// Handle post
    if (isset($_POST['login'])) {

        if (!$email = vemail($_POST['email'], 1)) {
            $error[] = "A valid email address is required.";
        }

        if (!$password = vempty($_POST['password'], 1, 0)) {
            $error[] = "A password is required.";
        }

        if (!$error) {

            if ($profile = loginUser($email, $password)) {
	            
				// Set redirect location
				if (isset($_SESSION['referer'])) {
		            
					$location = $_SESSION['referer'];
		            
				} else {

					$location = '/';

				}

                header("Location: $location");

                $_SESSION['confirm'] = 'Hi ' . $_SESSION['first_name'] . '!';
                exit;

            }

        }

    }
    
    
    // Set page variables
    $page_attrs['title'] = 'Log in to Your Account';
	$page_attrs['class'] = 'login';
	