<?php

	/**
	  * Generate a random string
	  *
	  * Can be used to generate password reset tokens, etc.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $length Length of string to generate.
	  * @param string $list Characters to use in string.
	  *
	  * @return string $newstring
	  *
	  */
	function token ($length = 32, $list = "0123456789abcdefghijklmnopqrstuvwxyz") {

		mt_srand((double)microtime()*1000000);
		$newstring = "";

		if($length > 0) {

			while(strlen($newstring) < $length) {
				$newstring .= $list[mt_rand(0, strlen($list)-1)];
			}

		}

		return $newstring;

	}


	/**
	  * Generate previous and next page links
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page visitor is on.
	  * @param int $limit Numner of results to show per page.
	  * @param int $total Total number of results.
	  *
	  * @return array $paginate Returns associative array of page numbers, totals,
	  * previous, next and current pages to use for links and display.
	  *
	  */
	function pagination ($page, $limit, $total) {

		// Set up pagination array
		$paginate = array();

		// Get total pages
		$pages = ceil($total / $limit);


		// Set first and previous pages
		if ($page != 1) {

			$paginate['first'] = '1';
			$paginate['previous'] = $page - 1;

		} else {

			$paginate['first'] = NULL;
			$paginate['previous']= NULL;

		}

		// Set current and total pages
		$paginate['current'] = $page;
		$paginate['total'] = $pages;

		// Set next and last pages
		if (($total - ($limit * $page)) > 0) {

			$paginate['next'] = $page + 1;
			$paginate['last'] = $pages;

		} else {

			$paginate['next'] = NULL;
			$paginate['last'] = NULL;

		}

		// Return results
		return $paginate;
	}


	/**
	  * Calculate date values to populate date select menus
	  *
	  * Calculates date values for select menus based on optional date string or current date
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date A date string.
	  * @param boolean $require TRUE = required, FALSE not required.
	  *
	  * @return array $date_select Array of date values to populate select menu
	  *
	  */
	function dateSelect ($date = FALSE, $require = FALSE) {

		// Pass selected date to var
		if ($date) {

			// Set selected values to $date arg
			$select_year = date('Y',strtotime($date));
			$select_month = date('m',strtotime($date));
			$select_day = date('d',strtotime($date));

		} else if ($require) {

			// Set selected values to current date  if required
			$select_year = date('Y');
			$select_month = date('m');
			$select_day = date('d');

		} else {

			// Set year value to 'Year'
			$select_year = 'Year';

		}

		// Set up date arrays where keys are same as values
		$years = array(date('Y') - 1 => date('Y') - 1, date('Y') => date('Y'), date('Y') + 1 => date('Y') + 1);
		$months = array(1 => 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		$days = range(1,31);

		// Declare select menu array
		$date_select = array();

		if (!$require) {

			// Prepend arrays with null options if dates aren't required. Must have an empty key so output has empty value
			$date_select['years'][''] = 'Year';
			$date_select['months'][''] = 'Month';
			$date_select['days'][''] = 'Day';

		}

		// Loop through years array
		foreach	($years as $key => $year) {

			// Populate years array for output
			$date_select['years'][$key] = $year;

			// Capture year to select
			if ($key == $select_year) {
				$date_select['select_year'] = $year;
			}

		}

		// Loop through months array
		foreach	($months as $key => $month) {

			// Populate months array for output
			$date_select['months'][str_pad($key,2,"0",STR_PAD_LEFT)] = $month;

			// Capture month to select
			if ($key == $select_month) {
				$date_select['select_month'] = str_pad($key,2,"0",STR_PAD_LEFT);
			}

		}

		// Loop through days
		foreach	($days as $day) {

			// Populate days array for output
			$date_select['days'][str_pad($day,2,"0",STR_PAD_LEFT)] = $day;

			// Capture day to select
			if ($day == $select_day) {
				$date_select['select_day'] = str_pad($day,2,"0",STR_PAD_LEFT);
			}

		}

		// Return output array
		return $date_select;

	}


	/**
	  * Calculate time values to pupulate time select menus
	  *
	  * Calculates time values for select menus based on optional time string or current time
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $time A time string.
	  * @param boolean $require TRUE = required, FALSE not required.
	  *
	  * @return array $time_select Array of time values to populate select menu
	  *
	  */
	function timeSelect ($time = FALSE, $require = FALSE) {

		if ($time) {

			// Set selected values to $date arg
			$select_hour = date('h',strtotime($time));
			$select_minute = date('i',strtotime($time));
			$select_ampm = date('a',strtotime($time));

		} else {

			// Set selected values to current date
			$select_hour = date('h');
			$select_minute = date('i');
			$select_ampm = date('a');

		}

		// Set up time arrays
		$hours = array(1 => '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
		$minutes = array('00', '15', '30', '45',);

		// Loop through hours
		foreach	($hours as $key => $hour) {

			$time_select['hours'][str_pad($key,2,"0",STR_PAD_LEFT)] = $hour;

			// Capture hour to select
			if ($key == $select_hour) {
				$time_select['select_hour'] = str_pad($key,2,"0",STR_PAD_LEFT);
			}

		}

		// Loop through minutes
		foreach	($minutes as $minute) {

			$time_select['minutes'][] = $minute;

			// Capture minute to select
			if ($minute >= $select_minute && $minute < $select_minute + 4) {
				$time_select['select_minute'] = $minute;
			}

		}

		// Select AM PM
		$time_select['select_ampm'] = $select_ampm;

		return  $time_select;
	}


	/**
	  * Convert dates to range
	  *
	  * Converts to dates to a date range in a single string
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $start A date string in YYYY-MM-DD format
	  * @param string $end A date string in YYYY-MM-DD format
	  *
	  * @return string $date_range The resulting date range string
	  *
	  */
	function convertDatesToRange ($start = FALSE, $end = FALSE) {

		// Make sure both dates are provided
		if (!$start || !$end) {

			return FALSE;

		} else {

			// Explode dates
			$ex['start_date'] = explode('-', $start);
			$ex['end_date'] = explode('-', $end);

			// Build date range
			if ($ex['start_date'][1] == $ex['end_date'][1]) {

				// Month day–day, year (weekday–weekday)
				$date_range = date('M j',strtotime($start)) . '&ndash;' . date('j, Y',strtotime($end)) . ' (' . date('D',strtotime($start)) . '&ndash;' . date('D',strtotime($end)) . ')';

			} else if ($ex['secret_special_from'][0] == $ex['secret_special_through'][0]) {

				// Month day–Month day, year (weekday–weekday)
				$date_range = date('M j',strtotime($start)) . '&ndash;' . date('M j, Y',strtotime($end)) . ' (' . date('D',strtotime($start)) . '&ndash;' . date('D',strtotime($end)) . ')';

			} else {

				// Month day, year—Month day, year (weekday–weekday)
				$date_range = date('M j, Y',strtotime($start)) . '&ndash;' . date('M j, Y',strtotime($end)) . ' (' . date('D',strtotime($start)) . '&ndash;' . date('D',strtotime($end)) . ')';

			}

		}

		return $date_range;

	}



	/**
	  * Replace a file with a new version
	  *
	  * Replaces an existing file on the server with a new one
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $file A $_FILES upload object.
	  * @param array $target The full server path to the file being replaced.
	  *
	  * @return string $return_file Path to the new file on the server.
	  *
	  */
	function fileReplace ($file, $target) {

		// Set up file path
		$file_path = $target;

		// Ensure file currently exists and new file is of valid type
		if (file_exists($file_path) && validateFileTypes($file)) {
			
			// Get file path info of target 
			$target_parts = pathinfo($target);
			
			// Get file path info of uploaded file
			$upload_parts = pathinfo($file['name']);
						
			// Compare file extensions. If they don’t match, delete the orignial file, and update the target with the new file extension.
			if ($upload_parts['extension'] != $target_parts['extension']) {
				
				// Delete the old file
				unlink($target);
				
				// Move the new file to the target with the new extension.
				move_uploaded_file($file['tmp_name'], $target_parts['dirname'] . '/' . $target_parts['filename'] . '.' . $upload_parts['extension']);
				
				// Rebuild destination file name with the target file name, and the uploaded file extension
				$return_file_name = $target_parts['filename'] . '.' . $upload_parts['extension'];
				
			// Replace the old file with the new one and retain it’s name
			} else {
				
				// Move it to the specified directory
				move_uploaded_file($file['tmp_name'], $target);
				
				// Use the target basename for the destitnation file name
				$return_file_name = $target_parts['basename'];
			
			}

			// Explode the directory structure
			$parts = explode('/',$target);

			// Count the number of parts
			$int = count($parts);

			// Return the numerical file path based on the last three items in array
			$return_file = $parts[$int-3] . '/' . $parts[$int-2] . '/' . $return_file_name;
			
			// Return the file path
			return escape($return_file);

		} else {

			return FALSE;

		}

	}


	/**
	  * Upload a file to the server
	  *
	  * Uploads a file to a subdirectory in the specified directory on the server.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $file A $_FILES upload object.
	  * @param array $target The full server path to the file location.
	  *
	  * @return string $target Path to the file on the server.
	  *
	  */
	function fileUpload ($file, $target) {

		// Ensure file is of valid type
		if (validateFileTypes($file)) {

			// Give it a unique name if necessary
			$target = uniqueName($target);

			// Move it to the specified directory
			move_uploaded_file($file['tmp_name'],$target['full_path']);

			// Set permissions
			chmod($target['full_path'],0644);

			// Return the file path
			return escape($target['media_path']);

		} else {

			return FALSE;

		}

	}
	
	
	/**
	  * Upload a file to the server in the specified directory
	  *
	  * Uploads a file to the specified directory on the server without creating a subdirectory.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $file A $_FILES upload object.
	  * @param array $target The full server path to the file location.
	  *
	  * @return string $target Path to the file on the server.
	  *
	  */
	function fileNoSubdirectoryUpload ($file, $target) {

		// Ensure file is of valid type
		if (validateFileTypes($file)) {

			// Move it to the specified directory
			move_uploaded_file($file['tmp_name'],$target);

			// Set permissions
			chmod($target,0644);

			// Return the file path
			return escape($target);

		} else {

			return FALSE;

		}

	}
	

	/**
	  * Copy and resize image file
	  *
	  * Creates a copy of an uploaded image file in WebP format with the specified width.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $image Path to image on the server.
	  * @param string $maxW Desited width of the final image.
	  * @param string $target Path to where the image should be saved on the server.
	  *
	  * @return string $return Path to the image on the server.
	  *
	  */
	function imgResize($image, $maxD, $target = NULL) {
		
		// Get dimensions or fail
		if ($size = getimagesize($image)) {
			
			$nowW = $size[0];
			$nowH = $size[1];
			
			// Calcualte new sizes if landcape or square
			if ($nowW >= $nowH) {
				
				// Get ratios
				$ratioX = $maxD / $nowW;
				$newW = $maxD;
				$newH = ceil($ratioX * $nowH);
				
			// Calculate new sizes if portrait
			} else {
				
				// Get ratios
				$ratioX = $maxD / $nowH;
				$newH = $maxD;
				$newW = ceil($ratioX * $nowW);
				
			}
			
		} else {
			
			return FALSE;
			
		}
		
		// Open new destination image file
		$dst = imagecreatetruecolor($newW,$newH);
		
		// Create new source image file based on mimetype
		if ($size[2] == 2) {
			
			$src = imagecreatefromjpeg($image);
			
		} else if ($size[2] == 3) {
			
			$src = imagecreatefrompng($image);
			
			// Turn on alpha channel for .png
			imagealphablending($dst, FALSE);
			
		} else if ($size[2] == 1) {
			
			$src = imagecreatefromgif($image);
			
		} else if ($size[2] == 18) {
			
			$src = imagecreatefromwebp($image);
			
			// Turn on alpha channel for .webp
			imagealphablending($dst, FALSE);
			
		}
		
		// Paste source image to destination and resize
		imagecopyresampled($dst,$src,0,0,0,0,$newW,$newH,$nowW,$nowH);
		
		// Generate name and save image to location
		if ($target = uniqueName($target)) {
			
			if ($size[2] == 2) {
				imagejpeg($dst,$target['full_path'],75);
			} else if ($size[2] == 3) {
				imagesavealpha($dst, true);
				imagepng($dst,$target['full_path'],6);
			} else if ($size[2] == 1) {
				imagegif($dst,$target['full_path']);
			} else if ($size[2] == 18) {
				imagewebp ($dst, $target['full_path'], 75);
			}
			
			$return = $target['media_path'];
			
		} else {
			
			$return = FALSE;
			
		}
		
		// Cleanup
		imagedestroy($src);
		imagedestroy($dst);
		
		return escape($return);
	}


	/**
	  * Generate unique file name and subdirectory for uploaded file
	  *
	  * Generates a unique file name in randomized directory structure based on current
	  * file name and what files are already in the directory
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $file Path to the file on the server.
	  *
	  * @return array $name An array containing the full path to the file on the server, file
	  * name, and the partial path
	  *
	  */
	function uniqueName($file) {

		// Parse file path
		$parts = pathinfo($file);
		$dir = $parts['dirname'];

		// Format the file name
		$file = formatSlug($parts['basename']);

		// Prepend dot to file extension
		$ext = '.' . $parts['extension'];

		// Remove file extension from name
		if ($parts['extension']) {
			$file = substr($file,0,-strlen($parts['extension']));
		}

		// Generate random file path
		$random_path = '/' . str_pad(rand(0,32),2,'0',STR_PAD_LEFT) . '/' . str_pad(rand(0,32),2,'0',STR_PAD_LEFT) . '/';

		// Create a recursive random directory if none exists
		if (!is_dir($dir.$random_path)) {

			mkdir($dir.$random_path, 0777, TRUE);

		} else {

			// If file exists, update name
			while (file_exists($dir.$random_path.$file.$i.$ext)) {
				if (!$i) $i=0;
				$i++;
			}

		}

		// Set return array
		$name['full_path'] = $dir . $random_path . $file . $i . $ext;
		$name['file_path'] = $dir . $random_path;
		$name['file_name'] = $file . $i . $ext;

		// Front end is hard coded for /media/ so we omit this in the returned string
		$name['media_path'] = ltrim($random_path,'/') . $file . $i . $ext;

		// Return file and path info array
		return $name;
	}


	/**
	  * Validate file upload types
	  *
	  * Validates file uploads based on MIME/Type. WARNING: Not totally secure.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string|array $file A $_FILE upload resource or a string.
	  *
	  * @return boolean Returns TRUE or FALSE
	  *
	  */
	function validateFileTypes ($file) {


		// Declare vaild MIME/types
		$allowedMimeTypes = array(
			'image/jpeg',
			'image/png',
			'image/gif',
			'image/webp',

			'application/x-compressed',
			'application/x-zip-compressed',
			'application/zip',
			'image/multipart/x-zip',

			'text/plain',
			'text/richtext',
			'application/pdf',

			'application/msword',
			'application/excel',
			'application/vnd.ms-excel',
			'application/x-excel',
			'application/x-msexcel',
			'application/mspowerpoint',
			'application/powerpoint',
			'application/vnd.ms-powerpoint',
			'application/x-mspowerpoint',

			'audio/mpeg',
			'audio/mpeg3',
			'audio/x-mpeg-3',
			'audio/mp4',

			'video/mpeg',
			'video/x-mpeg',
			'video/H264',
			'video/mp4'
		);

		// Declare valid file extensions
		$allowedFileExtensions = array(
			'gif',
			'png',
			'jpg',
			'jpeg',
			'webp',

			'zip',

			'doc',
			'xls',
			'ppt',
			'pdf',

			'txt',
			'csv',

			'mp4',
			'mp3',
			'm4a'
		);


		// If the file input is an array
		if (is_array($file)) {

			// Get the MIME/Type
			$mime = mime_content_type($file['tmp_name']);

			// Extract the file extension
			$ex = explode('.', $file['name']);
			$extension = end($ex);

			if (in_array($mime, $allowedMimeTypes) && in_array($extension, $allowedFileExtensions)) {

				return TRUE;

			} else {

				return FALSE;

			}

		// If the file input is a string
		} else {

			// Get the MIME/Type
			$mime = mime_content_type($file);

			// Compare MIME/type approved array
			if (in_array($mime, $allowedMimeTypes)) {

				return TRUE;

			} else {

				return FALSE;

			}
		}
	}


	/**
	  * Format string to URL-friendly version
	  *
	  * Converts a string to a URL-friendly version, replacing all UTF-8 to nearest ASCII counterparts,
	  * replace spaces with -, and converts it to lower case.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $string The string to be formatted.
	  * @param string $delimiter The character you want to replaces spaces with.
	  *
	  * @return string $clean The formatted string.
	  *
	  */
	function formatSlug($string, $delimiter = '-') {

		// Convert special characters to nearest ASCII equivalent
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);

		// Remove punctuation
		$clean = preg_replace("/[^a-zA-Z0-9_|+ -]/", '', $clean);

		// Convert to lower case and trip whitespace
		$clean = strtolower(trim($clean, '-'));

		// Insert specified delimiter
		$clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);

		// Return results
		return $clean;
	}

	/**
	  * Encode DROSTE item names for URLs
	  *
	  * Replaces common characters in DROSTE items names with valid URI characters
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $group DROSTE family group name.
	  *
	  * @return string $family_group Family group name with invalid charcters replaced
	  *
	  */
	function encodeFamilyGroupUrlString ($group) {
		
		// URL unfriendly charactars to find
		$find = array('/','#','&','"','+','.','%');
		
		// URL friendly charactars to replace them with
		$replace = array('__bs__','__lb__','__and__','__inch__','__pl__','__per__','__perc__');
		
		// Find and replace charactars
		$family_group = urlencode(str_replace($find,$replace,$group));
		
		// Return result
		return $family_group;
		
	}


	/**
	  * Get is perishable flag
	  *
	  * Gets returns a boolean value if department falls under perishable or not
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $department The department to check.
	  *
	  * @return boolean $is_perishable Boolean value true or false
	  *
	  */
	function getIsPerishableFlag ($department) {
		
		// Set is perishable flag
		switch ($department) {
			case 'meat':
			case 'produce':
			case 'bakery':
			case 'coffee bar':
			case 'kitchen':
			case 'floral':
			case 'specialty cheese':
				$is_perishable = true;
				break;
			default:
				$is_perishable = false;
		}
		
		return $is_perishable;
		
	}

	/**
	  * Decode DROSTE item names for URLs
	  *
	  * Replaces converted DROSTE charcters back to their orignal charcters
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $group DROSTE family group name.
	  *
	  * @return string $family_group Family group name with invalid charcters returned
	  *
	  */
	function decodeFamilyGroupUrlString ($group) {

		// URL friendly charactars to look for in string
		$find = array('__bs__','__lb__','__and__','__inch__','__pl__','__per__','__perc__');

		// Charaters to convert them to
		$replace = array('/','#','&','"','+','.','%');

		// Find and replace charactars
		$family_group = str_replace($find,$replace,urldecode($group));

		// Return result
		return $family_group;

	}


	/**
	  * Send Campaign Monitor Transactional Email
	  *
	  * Sends a transactional email using Campaign Monitor
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $message An array of message vars.
	  * @param string $email_id The CM smart list ID.
	  * @param string $consent Yes, No, or Unchanged
	  * @param boolean $subscribe Add email to subscriber list
	  *
	  * @return array $response Array containing API response
	  *
	  */
	function sendCMMail ($message, $email_id, $consent, $subscribe) {
		
		// Include CM libraries
		require_once('createsend/csrest_transactional_smartemail.php');
		
		// Authenticate with your API key
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		// The unique identifier for this smart email
		$smart_email_id = $email_id;
		
		// Create a new mailer and define your message
		$wrap = new CS_REST_Transactional_SmartEmail($smart_email_id, $auth);
		
		// Set consent to track value to unchanged
		$consent_to_track = $consent;
		
		// Set add to subcriber list to false
		$add_recipients_to_subscriber_list = $subscribe;
		
		// Send the message and save the response
		$result = $wrap->send($message, $consent_to_track, $add_recipients_to_subscriber_list);
		
		// Get response
		$response = $result->response;
		
		// Return response
		return $response;
		
	}


	/**
	  * Update Campaign Monitor Email Template
	  *
	  * Updates the template files for the specified campaign monitor template
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $template_id ID of the template to modify.
	  * @param string $template_name Name of the template.
	  * @param string $html_file URL to the html template file.
	  * @param string $zip_file URL to the zip file of assets.
	  *
	  * @return array $response Array containing API response
	  *
	  */
	function updateCMTemplate ($template_id, $template_name, $html_file, $zip_file) {

		// Include CM libraries
		require_once('createsend/csrest_templates.php');

		// Authenticate with your API key
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);

		// Create a new template update request
		$wrap = new CS_REST_Templates($template_id, $auth);

		// Update the template
		$result = $wrap->update(array(
			'Name' 			=> $template_name,
			'HtmlPageURL' 	=> $html_file,
			'ZipFileURL' 	=> $zip_file
		));

		// Get response
		$response = $result->response;

		// Return response
		return $response;

	}

	/**
	  * Convert to Duration
	  *
	  * Converts two date strings into a single duration string
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date_from The start date.
	  * @param string $date_through The end date.
	  *
	  * @return string $duration The new string.
	  *
	  */
	function convertDatesToDuration ($date_from = FALSE, $date_through = FALSE) {

		$duration = FALSE;

		// Require both dates
		if (!$date_from || !$date_through) {
			return FALSE;
		}

		// If duration is more than 3 days
		if ((strtotime($date_through) - strtotime($date_from)) > 172800) {

			if (date('F',strtotime($date_from)) == date('F',strtotime($date_through))) {
				$duration = date('F j',strtotime($date_from)) . '&ndash;' . date('j, Y',strtotime($date_through));
			} else {
				$duration = date('F j',strtotime($date_from)) . '&ndash;' . date('F j, Y',strtotime($date_through));
			}

		// If duration is 2 days
		} else if ((strtotime($date_through) - strtotime($date_from)) > 86400) {

			if (date('F',strtotime($date_from)) == date('F',strtotime($date_through))) {
				$duration = date('F j',strtotime($date_from)) . ' &amp; ' . date('j, Y',strtotime($date_through));
			} else {
				$duration = date('F j',strtotime($date_from)) . ' &amp; ' . date('F j, Y',strtotime($date_through));
			}

		// If sale is 1 day
		} else {

			$duration = date('F j, Y',strtotime($date_from));

		}

		return $duration;

	}


	/**
	  * Get cURL
	  *
	  * Runs a cURL requiest on the specified resource
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $uri The URL of the resource.
	  *
	  * @return string $result The contnets of the file.
	  *
	  */
	function getCurl($uri) {
		
		// Create cURL resource
		$ch = curl_init();
		
		// Set options
		curl_setopt($ch, CURLOPT_URL, $uri);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		
		// Get URL and pass it to the browser
		$result = curl_exec($ch);
		
		// Close cURL resource
		curl_close($ch);
		
		return $result;
		
	}


	function getCurlTest($uri) {
		
		// Create cURL resource
		$ch = curl_init();
		
		// Set options
		curl_setopt($ch, CURLOPT_URL, $uri);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		
		// Get URL and pass it to the browser
		$result = curl_exec($ch);
		
		$err     = curl_errno($ch);
		$errmsg  = curl_error($ch);
		$header  = curl_getinfo($ch);
		
		// Close cURL resource
		curl_close($ch);
		
		if (!$errmsg =='') {
			die($err . ': ' . $errmsg . ' - $uri');
		}
		
		die(var_dump($result));
		
		return $result;
		
	}

	/**
	  * Write to Log File
	  *
	  * Writes passed message to specified log file
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $log_file The name of the file to append the message to.
	  * @param string $message The message to append to the log file
	  *
	  * @return bool true|false
	  *
	  */
	function writeToLogFile ($log_file, $message) {

		$log = '[' . date('Y-m-d H:i:s') . '] ' . $message . "\r\n";

		if (file_put_contents(SERVER_PATH . '/logs/' . $log_file, $log, FILE_APPEND)) {

			return TRUE;

		} else {

			return FALSE;

		}

	}


	/**
	  * Format Bytes
	  *
	  * Formats file size string to nearest bytes
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $bytes File size in bytes
	  * @param int $precision How many decimal places to round to
	  *
	  * @return string Formatted file size string
	  *
	  */
	function formatBytes($bytes, $precision = 2) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

	    // Uncomment one of the following alternatives
	    $bytes /= pow(1024, $pow);
	    // $bytes /= (1 << (10 * $pow));

		return round($bytes, $precision) . ' ' . $units[$pow];
	}


	/**
	  * Post cURL
	  *
	  * Posts data to specified curl resource
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $url URL to resource on the web.
	  * @param string $data Data to post to the URL.
	  *
	  * @return string $transfer Any return values from the transfer.
	  *
	  */
	function postCURL ($url, $data) {
		
		// create a new cURL resource
		$ch = curl_init();
		
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('payload' => $data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// grab URL and pass it to the browser
		$transfer = curl_exec($ch);
		
		// close cURL resource, and free up system resources
		curl_close($ch);
		
		// Return results
		return $transfer;
		
	}
