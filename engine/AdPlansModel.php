<?php

	/**
	  * Select Upcoming Ad Plans
	  *
	  * Selects current and all upcoming ad plans from the ad_plans table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $plans An array of ad plans and attributes
	  *
	  */
	function selectUpcomingAdPlans () {
		
		if ($plans = runQuery("
			
			SELECT
				*
				
			FROM
				ad_plans
				
			WHERE
				ad_start_date >= DATE(DATE_ADD(NOW(), INTERVAL -14 DAY))
				
			ORDER BY
				ad_start_date ASC
				
		")) {
			
			return $plans;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Plan by Date
	  *
	  * Selects an ad plan by the ad_start_date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The ad start date to seach by
	  *
	  * @return array $plan An array of ad plans and attributes
	  *
	  */
	function selectAdPlanByDate ($ad_date) {
		
		if ($plan = runQuery("
			
			SELECT
				*
				
			FROM
				ad_plans
				
			WHERE
				ad_start_date = $ad_date
				
		")) {
			
			return $plan[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	
	/**
	  * Select Ad Plan by Date
	  *
	  * Selects an ad plan by the ad_start_date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $id The ID of the ad plan to select
	  *
	  * @return array $plan An array of ad plans and attributes
	  *
	  */
	function selectAdPlanById ($id) {
		
		if ($plan = runQuery("
			
			SELECT
				*
				
			FROM
				ad_plans
				
			WHERE
				id = $id
				
		")) {
			
			return $plan[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert Ad Plan
	  *
	  * Inserts a new row in the ad plan table with the specified date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $new_plan An array of the ad date to create and full name of the person creating it.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function insertAdPlan ($new_plan) {
		
		if ($insert = runQuery("
			
			INSERT INTO
				ad_plans
				
			SET
				ad_start_date = {$new_plan['date']},
				updated_by = {$new_plan['user']}
				
		")) {
			
			return $insert;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update Ad Plan
	  *
	  * Inserts a new row in the ad plan table with the specified date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $new_plan An array of the ad date to create and full name of the person creating it.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdPlan ($plan) {
		
		if ($update = runQuery("
			
			UPDATE
				ad_plans
				
			SET
				secret_special_from 						= {$plan['secret_special_from']},
				secret_special_through 						= {$plan['secret_special_through']},
				secret_special_title 						= {$plan['secret_special_title']},
				secret_special_description 					= {$plan['secret_special_description']},
				secret_special_retail 						= {$plan['secret_special_retail']},
				secret_special_limit 						= {$plan['secret_special_limit']},
				secret_special_valley_regular_retail 		= {$plan['secret_special_valley_regular_retail']},
				secret_special_marin_regular_retail 		= {$plan['secret_special_marin_regular_retail']},
				secret_special_sonoma_regular_retail 		= {$plan['secret_special_sonoma_regular_retail']},
				secret_special_fork_lift_regular_retail 	= {$plan['secret_special_fork_lift_regular_retail']},
				secret_special_food_4_less_regular_retail 	= {$plan['secret_special_food_4_less_regular_retail']},
				secret_special_upcs 						= {$plan['secret_special_upcs']},
				secret_special_picture 						= {$plan['secret_special_picture']},
				secret_special_labels 						= {$plan['secret_special_labels']},
				nugget_notes 								= {$plan['nugget_notes']},
				sonoma_notes 								= {$plan['sonoma_notes']},
				fork_lift_notes 							= {$plan['fork_lift_notes']},
				food_4_less_notes 							= {$plan['food_4_less_notes']},
				general_notes 								= {$plan['general_notes']},
				last_updated 								= NOW(),
				updated_by 									= {$plan['updated_by']}
				
			WHERE
				id = {$plan['id']}
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}



	/**
	  * Update Ad Plan UPCs
	  *
	  * Updates the secret_special_upcs column in the spefied ad plan
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $plan An array ad plan data with the following required fields
	  * array(
	    	id => int id of the ad plan, 
			secret_special_upcs => string comma separated list of UPCs, 
			update_by => string full name of the user updating the plan
	  * )
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdPlanUPCs ($plan) {
		
		if ($update = runQuery("
			
			UPDATE
				ad_plans
				
			SET
				
				secret_special_upcs 						= {$plan['secret_special_upcs']},
				secret_special_picture 						= {$plan['secret_special_picture']},
				updated_by 									= {$plan['updated_by']}
				
			WHERE
				id = {$plan['id']}
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}
