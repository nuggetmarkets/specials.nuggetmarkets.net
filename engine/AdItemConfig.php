<?php
	
	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaController.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';

	
	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
			
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}
	
	
	// Get the group instaces details
	$item = getAdItems ($_GET['family']);
	
	// Get all the SKUs in the group
	$upcs['skus'] = getItemUPCs ($_GET['family']);
	
	// Count the number of SKUs in the group
	$upcs['count'] = count($upcs['skus']);
	
	// Get all labels
	$labels = getLabels(0,9999);
	
	// Get selected labels for the item group
	$item['selected_labels'] = explode(',', $item['labels']);
	
	$ads = getItemOnUpcomingAds ($_GET['family']);
	
	if (isset($item['media_links'])) {
		
		$media_ids = explode(',', $item['media_links']);
		
		$item['media_items'] = selectMediaWithIds ($media_ids);
		
	}
	
	// Get feature image if one is set
	if (isset($item['feature_image'])) {
		$image = selectMediaById($item['feature_image']);
	} else {
		$image = FALSE;
	}
	
	
	$page_attrs['title'] = 'Edit ' . strtolower($item['family_group']) . ' Item Configuration';
	$page_attrs['description'] = 'Manage the ' . strtolower($item['family_group']) . ' ad item group configuration.';
	$page_attrs['class'] = 'ad-item-config products';
	
	// Handle Post
	if (isset($_POST['save'])) {
		
		$attachments = FALSE;
		
		// Validate input
		$item['prefix'] 		= vempty ($_POST['prefix'], 0, 1);
		$item['display_name'] 	= vempty ($_POST['display_name'], 0, 1);
		$item['suffix'] 		= vempty ($_POST['suffix'], 0, 1);
		$item['note'] 			= vempty ($_POST['note'], 0, 1);
		$item['description'] 	= vempty ($_POST['description'], 0, 0);
		
		foreach ($_POST['retail'] as $key => $value) {
			$item['retails'][$key] 		= vempty ($value, 0, 1);
		}
		
		foreach ($_POST['size'] as $key => $value) {
			$item['sizes'][$key] 		= vempty ($value, 0, 0);
		}
		
		foreach ($_POST['unit'] as $key => $value) {
			$item['units'][$key] 		= vempty ($value, 0, 0);
		}
		
		foreach ($_POST['container'] as $key => $value) {
			$item['containers'][$key] = vempty ($value, 0, 0);
		}
		
		foreach ($_POST['upc_image_active'] as $key => $value) {
			$item['upc_images_active'][$key] = (int)vbool ($value);
		}
		
		// Handle attachments
		$a = 0;
		foreach ($_POST['media'] as $attach) {
			
			if ($a == 0) {
				$attachments .= vbool($attach);
			} else {
				$attachments .= ',' . vbool($attach);
			}
			
			$a++;
			
		}
		
		if ($attachments) {
			$item['media_links'] = "'" . $attachments . "'";
		} else {
			$item['media_links'] = 'NULL';
		}
		
		// Capture and validate labels
		if (!empty($_POST['labels'])) {
			
			$i = 0;
			foreach ($_POST['labels'] as $label) {
				
				if ($i == 0) {
					$labs .= $label;
				} else {
					$labs .= ',' . $label;
				}
				$i++;
				
			}
			
			// Escape label IDs
			$labels = vempty($labs,0,0);
			
		} else {
			
			$labels = 'NULL';
			
		}
		
		$item['labels']				= $labels;
		$item['limit'] 				= vempty($_POST['limit'], 0, 1);
		$item['supplies_last']		= vbool($_POST['supplies_last']);
		$item['crv']				= vbool($_POST['crv']);
		$item['family_group']		= vempty(decodeFamilyGroupUrlString($_POST['ad_item_group_id']),1,0);
		
		// Force quotes around family group
		if (substr($item['family_group'], 0) != "'" && substr($item['family_group'], -1) != "'") {
			$item['family_group']	= "'{$item['family_group']}'";
		}
		
		$item['feature_image'] 		= vempty($_POST['featured_image']);
		
		// Update items
		if (updateAdItemConfig($item)) {
			
			// Set confirmation
			$_SESSION['confirm'] = "<p>Item configuration updated.</p>\n";
			
		} else {
			
			// Set error
			$_SESSION['error'] = "<p>Configuration failed to update.</p>\n";
			
		}
		
		// Redirect
		header("Location: /ad-item-config/" . encodeFamilyGroupUrlString(unescape($item['family_group'])) . '/');
		exit();
		
	}
