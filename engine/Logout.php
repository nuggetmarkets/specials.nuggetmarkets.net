<?php

    // Include dependencies
	require_once('Bootstrap.php');

    // Destroy the session
    $_SESSION = array();
    session_destroy();
    
    // Set confirmation message
    $confirm = 'You have been logged out.';
    
        // Set page variables
    $page_attrs['title'] = 'Logged Out';
	$page_attrs['class'] = 'logout';
