<?php

    // Include dependencies
    require_once 'Bootstrap.php';
	require_once 'ZonesController.php';
	require_once 'BannersController.php';

	// Declare vars
	$banner = FALSE;
	$banners = FALSE;
	$search_terms = NULL;

	$page_attrs['title'] = "Banners";
	$page_attrs['class'] = 'banners dim';


	// Handle post
	if (isset($_POST['save']) || isset($_POST['add'])) {
		
		// Require and validate title
		if (!$banner['name'] = vempty($_POST['name'], 1, 1)) {
			$error[] = 'A name is required';
		}
		
		// Validate everything else
		$banner['url']          = vempty($_POST['url']);
		$banner['department']   = vempty($_POST['department']);
		$banner['banner_id']    = vbool($_POST['banner_id']);
		$banner['body_copy']	= vempty ($_POST['body_copy'], 0, 0);
        $banner['date_from']    = vdate(
			$_POST['year_from'], 
			$_POST['month_from'], 
			$_POST['day_from']
		);
		$banner['date_through'] = vdate(
			$_POST['year_through'], 
			$_POST['month_through'], 
			$_POST['day_through']
		);
		
		$banner['sponsored'] = vbool($_POST['sponsored']);
		
		// Loop through zones, validate and convert to JSON
		foreach ($_POST['zones'] as $zone) {
			$zones[] = vbool($zone);
		}
		
		$banner['zones'] = vempty(json_encode($zones));
		
		// Loop through media links, validate and append them to string
		$i = 0;
		$media_links = NULL;
		foreach ($_POST['media'] as $item) {
			
			if ($i == 0) {
				$media_links .= vbool($item);
			} else {
				$media_links .= ',' . vbool($item);
			}
			$i++;
			
		}
		
		$banner['media_links'] = vempty($media_links,0,0);
		if (in_array($_POST['featured_image'], $_POST['media'])) {
			$banner['featured_image'] = vbool($_POST['featured_image']);
		} else {
			$banner['featured_image'] = 0;
		}
		
		// Handle submit if there are no errors
		if (!$error) {
			
			if (isset($_POST['save'])) {
				
				$banner['banner_id'] = vbool($_POST['banner_id']);
				
				// Update banner
				updateBanner($banner);
				
				// Set message and redirect
				$_SESSION['confirm'] = 'Banner updated.';
				
			} else if (isset($_POST['add'])) {
				
				// Insert banner
				$result = insertBanner($banner);
				
				// Capture and set user_id for redirect
				$banner['banner_id'] = $result['insert_id'];
				
				// Set message and redirect
				$_SESSION['confirm'] = 'Banner created.';
				
			}
			
			// Trigger refresh of banner cache
			include 'AutoBannersJsonExport.php';
			
			header("Location: /banners/edit/{$banner['banner_id']}/");
			exit();
			
		}
		
	// Handle edit banner
	} else if (isset($_GET['id']) && isset($_GET['action']) == 'edit') {
		
		$id = vbool($_GET['id']);
		$banner = getBannerById ($id);
		$zones = getZones();
		
        $date_from = dateSelect(date('Y-m-d',strtotime($banner['date_from'])),TRUE);
        $date_through = dateSelect(date('Y-m-d',strtotime($banner['date_through'])),TRUE);
		
    // Handle add banner
	} else if (isset($_GET['action']) == 'new') {
		
		$banner = TRUE;
		$zones = getZones();
		
        $date_from = dateSelect(FALSE,TRUE);
        $date_through = dateSelect(FALSE,TRUE);
		
    // Handle list banners
	} else {
		
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}
		
		$banners = getBanners($page_attrs['page_no'], 25);
		
		$zones = getRemappedZones();
		
	}
