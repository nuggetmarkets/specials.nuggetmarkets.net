<?php

    // Require dependencies
    require_once 'Bootstrap.php';
	require_once 'BannersModel.php';
	require_once 'MediaController.php';


	/**
	  * Get all banners
	  *
	  * Gets paginated list of banners in chronilogical order
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit Number of results to return.
	  *
	  * @return array $items An array of banners and attributes
	  *
	  */
	function getBanners ($page = 1, $limit = 25) {
		
		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}
		
		$page = vbool($page);
		$limit = vbool($limit);
		
		// Calculate page offset
		$offset = $page * $limit - ($limit);
		
		if ($banners = selectBanners($offset, $limit)) {
			
			foreach ($banners['listing'] as $item) {
				
				// Create array of results
				$items[$item['id']]['id']				= $item['id'];
				$items[$item['id']]['name']			    = $item['name'];
				$items[$item['id']]['url']			    = $item['url'];
				$items[$item['id']]['body_copy']		= $item['body_copy'];
				$items[$item['id']]['sponsored']		= $item['sponsored'];
				$items[$item['id']]['department']		= $item['department'];
				$items[$item['id']]['zones']			= json_decode($item['zones']);
				$items[$item['id']]['date_from']		= $item['date_from'];
				$items[$item['id']]['date_through']		= $item['date_through'];
				$items[$item['id']]['media_links']		= $item['media_links'];
				$items[$item['id']]['feature_image']	= $item['feature_image'];
				
				$items[$item['id']]['date_range']		= convertDatesToRange($item['date_from'], $item['date_through']);
				
				$items[$item['id']]['image_id']			= $item['image_id'];
				$items[$item['id']]['image_name']		= $item['image_name'];
				$items[$item['id']]['image_caption']	= $item['image_caption'];
				$items[$item['id']]['image_alt_text']	= $item['image_alt_text'];
				$items[$item['id']]['image_file']		= $item['image_file'];
				$items[$item['id']]['image_mime_type']	= $item['image_mime_type'];
				$items[$item['id']]['large_image']		= $item['large_image'];
				$items[$item['id']]['medium_image']		= $item['medium_image'];
				$items[$item['id']]['small_image']		= $item['small_image'];
				
				if (strtotime($item['date_from']) < time()) {
					$items[$item['id']]['publish_timer'] = TRUE;
				} else {
					$items[$item['id']]['publish_timer'] = FALSE;
				}
				
			}
			
			$items['listing'] = $items;
			
			$items['pagination'] = pagination($page, $limit, $banners['count']);
			
			return $items;
			
		} else {
			
			$items = FALSE;
			
		}
		
	}


	/**
	  * Gets running banners
	  *
	  * Gets list of running banners in chronilogical order
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit Number of results to return.
	  *
	  * @return array $banners An array of banners and attributes
	  *
	  */
	function getRunningBanners ($date = FALSE) {
		
		// Check for date 
		if ($date) {
			$current_date = vempty($date);	
		} else {
			$current_date = vempty(date('Y-m-d'));
		}
		
		// Select banners from database
		if (is_array($running_banners = selectBannersInDateRange($current_date, $current_date))) {
			
			// Loop through results and rebuild array by department
			$i = 0;
			foreach ($running_banners as $banner) {
				
				$banners[$banner['department']][$i] = $banner;
				$banners[$banner['department']][$i]['image_file'] = SPECIALS_URL . "/media/" . $banner['image_file'];
				$banners[$banner['department']][$i]['large_image'] = SPECIALS_URL . "/media/" . $banner['large_image'];
				$banners[$banner['department']][$i]['medium_image'] = SPECIALS_URL . "/media/" . $banner['medium_image'];
				$banners[$banner['department']][$i]['small_image'] = SPECIALS_URL . "/media/" . $banner['small_image'];
				
				$i++;
			}
			
			return $banners;
			
		} else {
			
			$banners = FALSE;
			
		}
		
	}


	/**
	  * Select banner by ID
	  *
	  * Selects rows from the banner table with the specified ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $banner_id The ID of the item you wish to retrieve.
	  *
	  * @return array $banner Values of the banner row columns.
	  *
	  */
	function getBannerById ($banner_id) {
		
		$id = vbool($banner_id);
		
		// Set up vars
		$media_items = array();
		$banner = array();
		$items = array();
		$path = NULL;
		$type = NULL;
		
		// Get banner
		if ($banner = selectBannerById($id)) {
			
			// Check for attachments
			if ($banner['media_links']) {
				
				// Get attachmaents
				$media_items = getMediaWithIds($banner['media_links']);
				
				$i = 0;
				
				// Loop through attachments and build output array
				foreach ($media_items as $media_item) {
					
					if (strstr($media_item['mime_type'], 'image')) {
						$type = 'images';
					} else if (strstr($media_item['mime_type'], 'audio')) {
						$type = 'audio';
					} else if (strstr($media_item['mime_type'], 'video')) {
						$type = 'video';
					} else if (strstr($media_item['mime_type'], 'embed')) {
						$type = 'embed';
					} else {
						$type = 'documents';
					}
					
					$banner['media_items'][$type][$i]['id'] 				= $media_item['id'];
					$banner['media_items'][$type][$i]['name'] 				= $media_item['name'];
					$banner['media_items'][$type][$i]['caption'] 			= $media_item['caption'];
					$banner['media_items'][$type][$i]['alt_text'] 			= $media_item['alt_text'];
					$banner['media_items'][$type][$i]['file'] 				= $media_item['file'];
					$banner['media_items'][$type][$i]['mime_type'] 			= $media_item['mime_type'];
					$banner['media_items'][$type][$i]['small_image'] 		= $media_item['small_image'];
					$banner['media_items'][$type][$i]['medium_image'] 		= $media_item['medium_image'];
					$banner['media_items'][$type][$i]['large_image'] 		= $media_item['large_image'];
					
					if ($type == 'embed') {
						$banner['media_items'][$type][$i]['frame_source'] 	= parseMediaSource($media_item['file']);
					}
					
					// Get file path info
					$path = pathinfo($media_item['file']);
					
					// Set file name var
					$banner['media_items'][$type][$i]['file_name'] 	= $path['basename'];
					
					// Set file extension var
					$banner['media_items'][$type][$i]['extension'] 	= $path['extension'];
					
					$i++;
					
				}
				
			}
			
			$banner['zones'] = json_decode($banner['zones']);
			
			// Return banners
			return $banner;
			
		} else {
			
			return FALSE;
			
		}
		
	}
