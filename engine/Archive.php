<?php
	
	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';


	// Get listing
	$page_attrs['title'] = "Archive";
	$page_attrs['class'] = 'archive';
	
	
	// Get current page number
	if (isset($_GET['page'])) {
		$page_attrs['page_no'] = $_GET['page'];
	}

	// Get upcoming ads for proofing
	$ads = getAds($page_attrs['page_no'], 25);
	