<?php

    /**
	  * Get Store Specials By Date
	  *
	  * Loads the specials from the JSON file cache and decodes the output.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store_chain The name of the store.
	  * @param string $ad_date The date of the ad.
	  *
	  * @return array $ad An array of ad data.
	  *
	  */
	function getStoreSpecialsByDate ($store_chain, $ad_date) {
		
		// Validate store name
		switch ($store_chain) {
			
			case 'nugget-markets-valley' :
			case 'nugget-markets-marin' :
			case 'nugget-markets-sonoma' :
			case 'nugget-markets' :
			case 'fork-lift' :
			case 'sonoma-market' :
			case 'food-4-less' :
				
				$store = $store_chain;
				break;
				
			default :
				// Return false if not found
				return FALSE;
				break;
				
		}
		
		// Build file path elements
		$date = $ad_date;
		$year = date('Y', strtotime($ad_date));
		
		// Build file path
		$file_path = $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $store . '/' . $year . '/' . $store . '-' . $date . '.json';
		
		// Get file contents
		$cache = file_get_contents($file_path);
		
		// Convert to PHP array
		$ad = json_decode($cache, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_PRETTY_PRINT);
		
		// Set store slug
		$ad['store_slug'] = $store;
		
		return $ad;
		
	}


	/**
	  * Get All Specials
	  *
	  * Gets all the items on special by department from the cache file and formats them for display.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store_chain The name of the store.
	  * @param string $ad_date The date of the ad.
	  *
	  * @return array $ad An array of ad data.
	  *
	  */
	function getAllSpecials ($store_chain, $ad_date) {
		
		// Set up departments array
		$departments = array();
		
		// Load the ad data from cache
		if ($ad = getStoreSpecialsByDate($store_chain, $ad_date)) {
			
			// Loop through items and rebuild output array with additional department attributes
			$i = 0;
			foreach ($ad['departments'] as $name => $items) {
				
				if ($items) {
					
					$departments[$i]['name'] = ucwords($name);
					$departments[$i]['slug'] = formatSlug($name);
					$departments[$i]['items'] = $items;
					
				}
				
				$i++;
			}
			
			// Overwrite departments array with additional name data
			$ad['departments'] = $departments;
			
			// Return array
			return $ad;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Get Specials By Department
	  *
	  * Gets specials for the given department from the ad cache.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store_chain The name of the store.
	  * @param string $ad_date The date of the ad.
	  * @param string $department_name The department whose items you want.
	  *
	  * @return array $department An array of ad data.
	  *
	  */
	function getSpecialsByDepartment ($store_chain, $ad_date, $department_name = NULL) {

		$department = NULL;

		// Load the ad data from cache
		if ($department_name && $ad = getStoreSpecialsByDate($store_chain, $ad_date)) {

			$department_val = str_replace('-', ' ', $department_name);

			// If the department exists in the ad
			if ($ad['departments'][$department_val]) {

				$department['store_name'] = $ad['store_chain'];
				$department['store_slug'] = formatSlug($ad['store_chain']);
				$department['ad_date'] = $ad_date;
				$department['name'] = ucwords($department_val);
				$department['slug'] = formatSlug($department_name);
				$department['items'] = $ad['departments'][$department_val];

				// Loop through departments and build navigation array
				$i = 0;
				foreach ($ad['departments'] as $name => $items) {

					if ($items) {

						$nav[$i]['name'] = ucwords($name);
						$nav[$i]['slug'] = formatSlug($name);

					}

					$i++;
				}

				$department['nav'] = $nav;

				// Return array
				return $department;

			} else {

				// Return false
				return FALSE;

			}

		} else {

			// Return false
			return FALSE;

		}

	}

	/**
	  * Get Item By Group
	  *
	  * Gets item attributes for the given family group from the ad cache.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store_chain The name of the store.
	  * @param string $ad_date The date of the ad.
	  * @param string $department_name The department whose items you want.
	  *
	  * @return array $department An array of ad data.
	  *
	  */
	function getItemByGroup ($store_chain, $ad_date, $group = NULL) {

		// Load the ad data from cache
		if ($group && $ad = getStoreSpecialsByDate($store_chain, $ad_date)) {

			// If the department exists in the ad
			if ($ad['departments'][$department_name]) {

				$department['store_name'] = $ad['store_chain'];
				$department['name'] = ucwords($department_name);
				$department['slug'] = formatSlug($department_name);
				$department['items'] = $ad['departments'][$department_name];

				// Return array
				return $department;

			} else {

				// Return false
				return FALSE;

			}

		} else {

			// Return false
			return FALSE;

		}

	}
