<?php

	// Include dependencies
	require_once 'AdsModel.php';
	require_once 'MediaModel.php';


	/**
	  * Get Weekly Ad
	  *
	  * Gets all the ad items from the database, groups and formats pricing and other data.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID the items appear on.
	  * @param string $date The date of the ad.
	  * @param string $department Optional department name if you only want to select items from a specific department.
	  * @param boolean $featured Optional flag to pull only featured items.
	  * @param boolean $disabled Optional flag to pull only items that have been disabled.
	  *
	  * @return array $products An array of product data and atributes
	  *
	  */
	function getWeeklyAd ($ad_id, $date = NULL, $department = NULL, $featured = FALSE, $front_page = FALSE, $disabled = FALSE) {
		
		$items = FALSE;
		$products = FALSE;
		
		// Pass specified date to detail link
		if (isset($_GET['date'])) {
			$append_date = '&date=' . $_GET['date'];
		} else {
			$append_date = NULL;
		}
		
		// If date is set
		if (isset($ad_id)) {
			
			if ($department) {
				
				$items = selectAdItemsByDepartment($ad_id, $department);
				
			} else if ($featured) {
				
				$items = selectFeaturedAdItems($ad_id);
				
            } else if ($front_page) {
				
				$items = selectFrontPageAdItems($ad_id);
				
			} else {
				
				$items = selectWeeklyAdItems($date);
				
			}
			
			// Everything
			if (is_array($items)) {
				
				$families = array();
				$groups = array();
				
				// Loop through items
				foreach ($items as $key => $val) {
					
					// Rebuild array by family group or manual group
					
					if ($val['group_name']) {
						$tmp = $val['group_name'];
					} else {
						$tmp = $val['family_group'];
					}
					
					$families[$tmp][] = $val;
					
					$i = 0;
					
					foreach ($families as $family) {
						
						$groups[$i]['group_name'] 	= $family[0]['group_name'];
						$groups[$i]['family_group'] = $family[0]['family_group'];
						$groups[$i]['display_name'] = $family[0]['display_name'];
						$groups[$i]['family_items'] = $family;
						
						$i++;
					}
					
				}
				
				$i = 0;
				
				// Loop through new array and list items as family groups
				foreach ($groups as $group) {
					
					$products[$i] = array();
					
					$products[$i]['group_name'] = $group['group_name'];
					$products[$i]['family_group'] = $group['family_group'];
					$products[$i]['display_name'] = $group['display_name'];
					
					// Encode family_uri for URL string
					$products[$i]['family_uri'] = encodeFamilyGroupUrlString($group['family_group']);
					
					// Create a unique ID for family group
					$products[$i]['family_id'] = hash('md5',$products[$i]['family_uri']);
					
					$items = $group['family_items'];
					$per_lb = NULL;
					$per_ea = NULL;
					$crv = NULL;
					$up_to = NULL;
					$photo = NULL;
					
					$a = 0;
					
					// Loop through attributes of group items
					foreach ($items as $attr) {
						
						$upc[$a] 				= $attr['upc'];
						$ad_item_id[$a]			= $attr['ad_item_id'];
						$name[$a] 				= $attr['display_name'];
						$pos_name[$a] 			= $attr['pos_name'];
						$override_name[$a] 		= $attr['group_name'];
						$family_group_uris[$a] 	= encodeFamilyGroupUrlString($attr['family_group']);
						$size[$a] 				= $attr['size'];
						$pack[$a] 				= $attr['pack'];
						$unit[$a] 				= $attr['unit'];
						$container[$a] 			= $attr['container'];
						$retail_price[$a] 		= $attr['retail_price'];
						$upc_images_active[$a]	= $attr['upc_image_active'];
						
						// Set sale price to retail if item is not on sale
						if ($attr['sale_price'] == '0/0.00') {
							$sale_price[$a] = $attr['retail_price'];
							$items[$a]['sale_price'] = $attr['retail_price'];
						} else {
							$sale_price[$a] = $attr['sale_price'];
						}
						
						$override_price[$a] 	= $attr['override_price'];
						$photo[$a] 				= selectMediaById($attr['feature_image']);
						$labels[$attr['upc']] 	= getLabelsWithIds($attr['labels']);
						
						$a++;
						
					}
					
					// Capture UPCs, sizes, packs, price, etc.
					$products[$i]['upcs'] 					= $upc;
					$products[$i]['family_group_uris'] 		= array_unique($family_group_uris);
					$products[$i]['ad_item_ids'] 			= $ad_item_id;
					$products[$i]['pos_name'] 				= $pos_name;
					$products[$i]['override_names'] 		= $override_name;
					$products[$i]['sizes']					= $size;
					$products[$i]['packs']					= $pack;
					$products[$i]['units']					= $unit;
					$products[$i]['retail_prices'] 			= $retail_price;
					$products[$i]['sale_prices'] 			= $sale_price;
					$products[$i]['override_prices'] 		= $override_price;
					$products[$i]['labels'] 				= $labels;
					$products[$i]['upc_images_active'] 		= $upc_images_active;
					
					if (count($products[$i]['family_group_uris']) == 1) {
						$products[$i]['manual_group'] 		= FALSE;
						$products[$i]['messages']			= getAdItemMessages($ad_id, $products[$i]['family_group'], FALSE);
						
					} else {
						$products[$i]['manual_group'] 		= TRUE;
						$products[$i]['messages']			= getAdItemMessages($ad_id, FALSE, $products[$i]['group_name']);
					}
					
					$products[$i]['message_reply_to']		= $products[$i]['messages']['reply_to'];
					
					// Declare photo and label variables
					$products[$i]['group_labels'] 			= NULL;
					
					$products[$i]['feature_photo_original']	= NULL;
					$products[$i]['feature_photo_large'] 	= NULL;
					$products[$i]['feature_photo_medium'] 	= NULL;
					$products[$i]['feature_photo_small'] 	= NULL;					
					$products[$i]['upc_photos'] 			= NULL;
					$products[$i]['special_diet'] 			= NULL;
					
					// Recursively loop through labels and build a label ids array
					$label_ids = array();
					foreach ($labels as $item_labels) {
						if ($item_labels) {
							foreach ($item_labels as $label) {
								$label_ids[] = $label['id'];
							}
						}
					}
					
					// Count occurances of ids
					$label_id_counts = array_count_values($label_ids);
					
					// Loop through counts and build group labels array
					foreach ($label_id_counts as $key => $value) {
					
						// If number of label ids matches number of skus, add it to the group array
						if ($value == count($upc)) {
						
							$products[$i]['group_labels'][$key] = $value;
							
						}
						
					}
					
					// Use manually uploaded image by default
					$b = 0;
					foreach ($photo as $img) {
						
						// Use only the last uploaded image in array
						if (strstr($img['mime_type'], 'image/')) {
						
							$products[$i]['feature_photo_original'] = $img['file'];
							$products[$i]['feature_photo_large'] 	= $img['large_image'];
							$products[$i]['feature_photo_medium'] 	= $img['medium_image'];
							$products[$i]['feature_photo_small'] 	= $img['small_image'];
							
							// Capture the UPC the image is attached to
							$products[$i]['feature_photo_upc'] 		= $products[$i]['upcs'][$b];
							
						}
						
						$b++;
						
					}
					unset($b);
					
					$products[$i]['upc_photos'] = array();
					
					// Find any UPC based images if no image has been uploaded
					if (!isset($products[$i]['feature_photo_original'])) {
						$b = 0;
						foreach ($products[$i]['upcs'] as $img) {
							
							if ($products[$i]['upc_images_active'][$b] == 1) {
								
								if (file_exists(SERVER_PATH . "/httpdocs/img/upc/$img.webp")) {
									$products[$i]['upc_photos'][] = '/img/upc/' . $img . '.webp';
								} else if (file_exists(SERVER_PATH . "/httpdocs/img/upc/$img.jpg")) {
									$products[$i]['upc_photos'][] = '/img/upc/' . $img . '.jpg';
								}
								
							}
							
							$b++;
						}
						unset($b);
					}
					
					$products[$i]['upc_photos_count'] = count($products[$i]['upc_photos']);
					
					// Determine if grouped items are priced differently
					if (min($retail_price) < max($retail_price)) {
						$up_to = 'up to ';
					}
					
					// Parse containers and sizes based on units
					$unique_units = array_unique($unit);
					$unique_containers = array_unique($container);
					
					// If there are multipe unit types, capture sizes and containers for each unit
					if (count($unique_units) > 1) {
						
						// Loop through unique unit types in group
						foreach ($unique_units as $unique_unit) {
							
							// Loop though all units in item group
							foreach ($unit as $key => $val) {
								
								// If this item has the current unit
								if ($unique_unit == $val) {
									
									// Capture the container size an the container type
									$item_container_size[$unique_unit][] = $size[$key];
									$item_containers[$unique_unit] = $items[$key]['container'];
									
								}
							}
							
							// Use a range of smallest and largest size pack counts of items in group
							if (min($item_container_size[$unique_unit]) < max($item_container_size[$unique_unit])) {
								$products[$i]['container_sizes'][$unique_unit] = rtrim(rtrim(min($item_container_size[$unique_unit]), '0'), '.') . '&ndash;' . rtrim(rtrim(max($item_container_size[$unique_unit]), '0'), '.');
							// Or use the smallest if there is only one size
							} else {
								$products[$i]['container_sizes'][$unique_unit] = rtrim(rtrim(min($item_container_size[$unique_unit]), '0'), '.');
							}
							
							// Set a containers var
							$products[$i]['containers'] = $item_containers;
							
							// Set units var and reset the $unique_units array keys for the loop
							$products[$i]['units'] = array_combine(range(1,count($unique_units)), array_values($unique_units));
							
						}
						
					// If there are multiple container types, capture containers and sizes for each container
					} else if (count($unique_containers) > 1) {
						
						// Loop through unique containers in group
						foreach ($unique_containers as $unique_container) {
							
							// Loop though all containers in item group
							foreach ($container as $key => $val) {
								
								// If this item has the current unit
								if ($unique_container == $val) {
									
									// Capture the container size an the container type
									$item_container_size[$unique_container][] = $size[$key];
									$item_containers[$unique_container] = $items[$key]['container'];
									
								}
							}
							
							// Use a range of smallest and largest size pack counts of items in group
							if (min($item_container_size[$unique_container]) < max($item_container_size[$unique_container])) {
								$products[$i]['container_sizes'][$unique_container] = rtrim(rtrim(min($item_container_size[$unique_container]), '0'), '.') . '&ndash;' . rtrim(rtrim(max($item_container_size[$unique_container]), '0'), '.');
							// Or use the smallest if there is only one size
							} else {
								$products[$i]['container_sizes'][$unique_container] = rtrim(rtrim(min($item_container_size[$unique_container]), '0'), '.');
							}
							
							// Set a containers var
							$products[$i]['containers'] = $item_containers;
							
							// Set units var and reset the $unique_units array keys for the loop
							$products[$i]['units'] = array_combine(range(1,count($unit)), array_values($unit));
							
						}
						
						
						
					// If there is only one unit type in the group, set singular size vars.
					} else {
					
						// Use smallest and largest size and pack counts of group members
						if (min($size) < max($size)) {
							$products[$i]['size'] = rtrim(rtrim(min($size), '0'), '.') . '&ndash;' . rtrim(rtrim(max($size), '0'), '.');
						} else {
							$products[$i]['size'] = rtrim(rtrim(min($size), '0'), '.');
						}
						
						// And the singular container type
						$products[$i]['container'] = $items[0]['container'];
						
					}
					
					// Use attribute of first group member for everything else
					$products[$i]['prefix'] 		= $items[0]['prefix'];
					$products[$i]['department'] 	= $items[0]['department'];
					$products[$i]['department_url'] = str_replace(' ','-',$items[0]['department']);
					
					// Set selected varieties
					if (
						isset($products[$i]['department']) &&
						count($products[$i]['upcs']) > 1 &&
						$products[$i]['department'] != 'meat' &&
						$products[$i]['department'] != 'coffee bar' && 
						$products[$i]['department'] != 'kitchen'
					) {
						$products[$i]['selected_varieties'] = TRUE;
					} else {
						$products[$i]['selected_varieties'] = FALSE;
					}
					
					// Set item name
					if ($products[$i]['group_name']) {
						$products[$i]['name'] = $products[$i]['group_name'];
					} else if ($products[$i]['display_name']) {
						$products[$i]['name'] = $products[$i]['display_name'];
					} else {
						$products[$i]['name'] = ucwords(strtolower($items[0]['pos_name']));
					}
					
					$products[$i]['status'] 		= $items[0]['status'];
					$products[$i]['suffix'] 		= $items[0]['suffix'];
					$products[$i]['note'] 			= $items[0]['note'];
					$products[$i]['description'] 	= $items[0]['description'];
					$products[$i]['unit'] 			= $items[0]['unit'];
					
					// Set limit
					if ($items[0]['limit']) {
						$products[$i]['limit'] = '<span class="limit">Limit ' . $items[0]['limit'] . ' per guest</span>';
					} else {
						$products[$i]['limit'] = NULL;
						// $products[$i]['limit'] = '<span class="limit">Limit ' . ITEM_LIMIT . ' per guest</span>';
					}
					
					// Show per lb. pricing for certain departments
					if (
						isset($products[$i]['size']) &&
						is_null($items[0]['container']) &&
						$items[0]['unit'] == 'LB' &&
						$products[$i]['size'] == '1' && ((
							($products[$i]['department'] == 'produce' && !$products[$i]['container']) ||
							($products[$i]['department'] == 'meat' && !$products[$i]['container']) ||
							$products[$i]['department'] == 'deli' ||
							$products[$i]['department'] == 'bakery' ||
							$products[$i]['department'] == 'kitchen' ||
							$products[$i]['department'] == 'specialty cheese'
						) || (
							$products[$i]['department'] == 'staples' &&
							stristr($products[$i]['suffix'],'bin #')
						))) {
						
						$per_lb = '<span class="perlb">/lb.</span>';
						
						// Don't show unit for per lb. pricing
						$products[$i]['unit'] = NULL;
						
						// Only set per lb. limit if manually specified
						if (!$items[0]['limit']) {
							$products[$i]['limit'] = NULL;
						} else {
							$products[$i]['limit'] = '<span class="limit">Limit ' . $items[0]['limit'] . ' lbs. per guest</span>';
						}
					}
					
					// Show per ea. pricing for certain departments
					if (
						(
							$items[0]['unit'] == 'EA' || (
								$items[0]['unit'] == 'CT' &&
								!isset($products[$i]['container'])
							)
						) &&
						isset($products[$i]['size']) &&
						$products[$i]['size'] == '1' && (
							(
								$products[$i]['department'] == 'produce' ||
								$products[$i]['department'] == 'deli' ||
								$products[$i]['department'] == 'kitchen' ||
								$products[$i]['department'] == 'specialty cheese' ||
								$products[$i]['department'] == 'staples' ||
								$products[$i]['department'] == 'floral' ||
								$products[$i]['department'] == 'bakery' ||
								$products[$i]['department'] == 'general merchandise' ||
								$products[$i]['department'] == 'meat'
							)
						)
					) {
						
						$per_ea = '<span class="perea">/ea.</span>';
						
					}
					
					// Show +crv
					if ($items[0]['crv'] == 1) {
						$crv = '<span class="crv">+crv</span>';
					}
					
					$products[$i]['container'] 		= $items[0]['container'];
					$products[$i]['supplies_last'] 	= $items[0]['supplies_last'];
					
					// Determine format of sale price
					if (!strstr($items[0]['sale_price'],'/')) {
						
						// Convert currency to float
						$sale = number_format($items[0]['sale_price'], 2);
						$retail = number_format(max($retail_price), 2);
						
						// Set and format sale price and savings
						$ex = explode('.', $sale);
						
						// Format price
						if ($ex[0] != 0 && $ex[1] != 0) {
							$products[$i]['sale_price'] = '<span class="usd">$</span>' . $ex[0] . '<span class="cents"><span class="decimal">.</span>' . $ex[1] . '</span>' . $per_lb . $per_ea . $crv;
						// Use ¢ if no dollars
						} else if ($ex[0] == 0) {
							$products[$i]['sale_price'] = $ex[1] . '<span class="cent">&cent;</span>' . $per_lb . $per_ea . $crv;
						// Remove cents if even dollar amount
						} else {
							$products[$i]['sale_price'] = '<span class="usd">$</span>' . $ex[0] . $per_lb . $per_ea . $crv;
						}
						
						// Format regular retail
						$products[$i]['regular_retail'] = '<span class="usd">$</span>' . $retail  . $per_lb . $per_ea;
						
						// Calculate savings
						$savings = str_replace('.00','',number_format($retail - $sale, 2));
						
						/* Deprecated
						// Determine half off status
						foreach ($items as $item) {
							
							// Calculate the retail and sale difference
							$halfoff_eval = str_replace('.00','',number_format($item['retail_price'] - $item['sale_price'], 2));
							
							// Build array to flag items with three possible vals (half off, below half off, and none)
							if ($item['retail_price'] / 2 == $halfoff_eval || $item['retail_price'] / 2 == $savings - .005) {
								$halfoff[] = 2;
							} else if ($item['retail_price'] / 2 < $halfoff_eval) {
								$halfoff[] = 3;
							} else {
								$halfoff[] = 1;
							}
							
						}
						
						
						// Remove duplicate values from array
						$unique = array_unique($halfoff);
						
						// Count the remaining items
						$count = count($unique);
						
						// If all items have the same half off status, label them accordingly
						if ($count == 1) {
							
							if ($unique[0] == 2) {
								
								$products[$i]['class'] = ' class="halfoff"';
								$products[$i]['half_off'] = '<em>1/2</em> Off!';
								
							} else if ($unique[0] == 3) {
								
								$products[$i]['class'] = ' class="below halfoff"';
								$products[$i]['half_off'] = '<em>Below 1/2</em> Price!';
								
							}
							
						}
						
						// Unset vars used for comparison
						unset($unique, $count, $halfoff, $halfoff_eval);
						*/
						
						if (min($sale_price) == max($sale_price)) {
							
							// Format savings
							if ($savings > 0) {
								
								if ($savings < 1) {
									$products[$i]['savings'] = $up_to . str_replace('0.','',$savings) . '&cent;' . $per_lb . $per_ea;
								} else {
									$products[$i]['savings'] = $up_to . '$' . $savings . $per_lb . $per_ea;
								}
								
							} else {
								$products[$i]['savings'] = FALSE;
							}
							
							
						} else {
							// Don’t display savings
							$products[$i]['savings'] = FALSE;
						}
						
						
					} else {
						
						// Format sale price for 2/for items
						$ex = explode('/', $items[0]['sale_price']);
						
						$retail = number_format(max($retail_price), 2);
						
						// Strip .00 from price
						if (strstr($ex[1], '.00')) {
							$products[$i]['sale_price'] = $ex[0] . '/$' . substr($ex[1], 0, -3) . $crv;
						} else {
							$products[$i]['sale_price'] = $ex[0] . '/$' . $ex[1] . $crv;
						}
						
						// Format regular retail
						$products[$i]['regular_retail'] = '<span class="usd">$</span>' . $retail  . $per_lb . $per_ea;
						
						// Format produce 2lbs/for pricing
						if (
							$products[$i]['department'] == 'produce' &&
							$items[0]['unit'] == 'LB' &&
							!isset($products[$i]['container'])
						) {
						
							// Strip .00 from price
							if (strstr($ex[1], '.00')) {
								$products[$i]['sale_price'] = $ex[0] . '<span class="perlb">lbs.</span>/$' . substr($ex[1], 0, -3) . $crv;
							} else {
								$products[$i]['sale_price'] = $ex[0] . '<span class="perlb">lbs.</span>/$' . $ex[1] . $crv;
							}
							
						}
						
						// Don't show size/unit/container for 2/for produce items that don't have a container
						if ($products[$i]['department'] == 'produce' && !$items[0]['container']) {
							$products[$i]['size'] = NULL;
							$products[$i]['unit'] = NULL;
						}
						
						// Calculate savings
						$sale = @number_format($ex[1] / $ex[0], 2);
						$retail = number_format(max($retail_price), 2);
						$savings = number_format(($retail - $sale) * $ex[0], 2);
						
						if ($savings > 0) {
							if ($savings >= 1 && strstr($products[$i]['sale_price'], 'lbs.')) {
								$products[$i]['savings'] = $up_to . '$' . $savings  . ' on&nbsp;' . $ex[0] . '<span class="perlb">lbs.</span>';
							} else if ($savings >= 1) {
								$products[$i]['savings'] = $up_to . '$' . $savings  . ' on&nbsp;' . $ex[0];
							} else {
								$products[$i]['savings'] = $up_to . str_replace('0.','',$savings)  . '&cent; on&nbsp;' . $ex[0];
							}
						} else {
							$products[$i]['savings'] = FALSE;
						}
					}
					
					if (strlen($items[0]['override_price']) >= 3) {
						$products[$i]['sale_price'] = '<span class="override">' . $items[0]['override_price'] . '</span>' . $crv;
						
						// Disable savings for any item with a price override
						$products[$i]['savings'] = FALSE;
						
						/**
						  * Currently disabling savings for any item with a custom price, not just a % off or lowest price in town as configured below:
						  *
						// If custom price contains % off or “Lowest price in town&ldquo;&rdquo;, do not show it
						if (stristr($products[$i]['sale_price'],'%') || stristr($products[$i]['sale_price'],'lowest')) {
							$products[$i]['savings'] = FALSE;
						}
						*/
						
					}
					
					// Turn of pricing if coffee bar item or bakery item is not on sale
					if (
						!$products[$i]['savings'] && 
						($products[$i]['department'] == 'coffee bar')
					) {
						$products[$i]['sale_price'] = NULL;
					}
					
					// Capture features spot
					if ($items[0]['feature']) {
						$products[$i]['feature'] = $items[0]['feature'];
						$products[$i]['feature_config'] = json_decode($items[0]['feature_config'], TRUE);
						
						if (!$products[$i]['feature_photo_small']) {
							$products[$i]['status'] .= ' feature-image-needed';
						}
					}
					
					// Capture sort
					if (isset($items[0]['sort'])) {
						$products[$i]['sort'] = $items[0]['sort'];
					} else {
						$products[$i]['sort'] = NULL;
					}
					
					
					
					// Reset group member attributes for next loop
					unset(
						$upc,
						$ad_item_id,
						$name,
						$pos_name,
						$override_name,
						$family_group_uris,
						$size,
						$pack,
						$unit,
						$container,
						$item_units_size,
						$item_containers,
						$item_container_size,
						$retail_price,
						$sale_price,
						$retail,
						$sale,
						$lowest_retail,
						$highest_retail,
						$lowest_sale,
						$highest_sale,
						$savings,
						$override_price, 
						$photo,
						$labels,
						$label_ids,
						$label_id_counts
					);
					
					$i++;
					
				}
				
			}
			
			return $products;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Weekly Ad Item
	  *
	  * Gets a specific ad item group from the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $faimily The family ID of the items.
	  * @param string $sku The date of the ad.
	  *
	  * @return array $sku An array of the product data
	  *
	  */
	function weeklyAdItem ($family, $date) {
		
		$items = FALSE;
		
		// If family is set
		if (isset($family)) {
			
			// Valiate and format family id
			$family_id = decodeFamilyGroupUrlString($family);
			
			// Get item
			$items = getWeeklyAdItem($family_id, $date);
			
			$sku['group'] = $items[0]['group'];
			$sku['family_group'] = $items[0]['family_group'];
			$sku['family_group_name'] = $items[0]['family_group_name'];
			
			$per_lb 			= NULL;
			$per_ea 			= NULL;
			$crv 				= NULL;
			$a 					= 0;
			$upc 				= NULL;
			$name				= NULL;
			$prefix 			= NULL;
			$family_group_name 	= NULL;
			$size 				= NULL;
			$pack 				= NULL;
			$retail_price 		= NULL;
			$sale_price 		= NULL;
			$override_price 	= NULL;
			$photo 				= NULL;
			
			// Loop through attributes of group items
			if ($items) {
				
				foreach ($items as $attr) {
					
					$upc[$a] 				= $attr['upc'];
					$ad_item_id[$a] 		= $attr['ad_item_id'];
					$name[$a] 				= ucwords(strtolower($attr['name']));
					$prefix[$a] 			= $attr['prefix'];
					$family_group_name[$a] 	= $attr['family_group_name'];
					$size[$a] 				= $attr['size'];
					$pack[$a] 				= $attr['pack'];
					$retail_price[$a] 		= $attr['retail_price'];
					
					// Set sale price to retail if item is not on sale
					if ($attr['sale_price'] == '0/0.00') {
						$sale_price[$a] = $attr['retail_price'];
						$items[$a]['sale_price'] = $attr['retail_price'];
					} else {
						$sale_price[$a] = $attr['sale_price'];
					}
					
					$override_price[$a] 	= $attr['override_price'];
					$photo[$a] 				= selectMediaById($attr['feature_image']);
					$labels[$attr['upc']] 	= getLabelsWithIds($attr['labels']);
					
					$a++;
					
				}
			}
			
			// Capture UPCs, sizes, packs, price, etc.
			$sku['upcs'] 				= $upc;
			$sku['ad_item_ids'] 		= $ad_item_id;
			$sku['names'] 				= $name;
			$sku['prefixes'] 			= $prefix;
			$sku['family_group_names'] 	= $family_group_name;
			$sku['sizes'] 				= $size;
			$sku['packs'] 				= $pack;
			$sku['retail_prices'] 		= $retail_price;
			$sku['sale_prices'] 		= $sale_price;
			$sku['override_prices'] 	= $override_price;
			$sku['photos'] 				= $photo;
			$sku['labels'] 				= $labels;
			
			// Recursively loop through labels and build a label ids array
			foreach ($labels as $item_labels) {
				
				foreach ($item_labels as $label) {
					
					$label_ids[] = $label['id'];
					
				}
				
			}
			
			// Count occurances of ids
			$label_id_counts = array_count_values($label_ids);
			
			// Loop through counts and build group labels array
			foreach ($label_id_counts as $key => $value) {
				
				// If number of label ids matches number of skus, add it to the group array
				if ($value == count($upc)) {
					
					$sku['group_labels'][$key] = $value;
					
				}
				
			}
			
			// Use manually uploaded image by default
			$b = 0;
			foreach ($photo as $img) {
				
				// Use only the last uploaded image in array
				if (strstr($img['mime_type'], 'image/jpeg')) {
					
					$sku['feature_photo_original'] 	= $img['file'];
					$sku['feature_photo_large'] 	= $img['large_image'];
					$sku['feature_photo_medium'] 	= $img['medium_image'];
					$sku['feature_photo_small'] 	= $img['small_image'];
					
					// Capture which UPC the image is attached to
					$sku['feature_photo_upc'] 		= $sku['upcs'][$b];
					
				}
				
				$b++;
				
			}
			unset($b);
			
			// Determine if grouped items are priced differently
			if (count($retail_price) > 1) {
				if (min($retail_price) < max($retail_price)) {
					$up_to = ' up to ';
				}
			}
			
			// Use smallest and largest size and pack counts of group members
			if (count($size) > 1) {
				if (min($size) < max($size)) {
					$sku['size'] = rtrim(rtrim(min($size), '0'), '.') . '-' . rtrim(rtrim(max($size), '0'), '.');
				} else {
					$sku['size'] = rtrim(rtrim(min($size), '0'), '.');
				}
			} else {
				$sku['size'] = rtrim(rtrim($size[0], '0'), '.');
			}
			
			// Use attribute of first group member for everything else
			$sku['family_name'] = $items[0]['family_name'];
			$sku['prefix'] 		= $items[0]['prefix'];
			$sku['department'] 	= $items[0]['department'];
			
			// Set item name
			if ($sku['group_name']) {
				$sku['name'] 	= $sku['group_name'];
			} else if ($sku['family_group_name']) {
				$sku['name'] 	= $sku['family_group_name'];
			} else {
				$sku['name']	= ucwords(strtolower($sku['names'][0]));
			}
			
			$sku['suffix'] 			= $items[0]['suffix'];
			$sku['note'] 			= $items[0]['note'];
			$sku['description'] 	= $items[0]['description'];
			$sku['unit'] 			= $items[0]['unit'];
			
			// Set limit
			if ($items[0]['limit']) {
				$sku['limit'] = '<span class="limit">Limit ' . $items[0]['limit'] . ' per guest</span>';
			} else {
				$sku['limit'] = NULL;
				// $sku['limit'] = '<span class="limit">Limit ' . ITEM_LIMIT . ' per guest</span>';
			}
			
			// Show per lb. pricing for certain departments
			if (
				$items[0]['unit'] == 'LB' &&
				$sku['size'] == '1' && ((
					$sku['department'] == 'meat' ||
					$sku['department'] == 'deli' ||
					$sku['department'] == 'bakery' ||
					$sku['department'] == 'kitchen' ||
					$sku['department'] == 'produce' ||
					$sku['department'] == 'specialty cheese'
				) || (
					$sku['department'] == 'staples' &&
					stristr($sku['suffix'],'bin #')
				))) {
				
				$per_lb = '<span class="perlb">/lb.</span>';
				
				// Only set per lb. limit if manually specified
				if (!$items[0]['limit']) {
					$sku['limit'] = NULL;
				} else {
					$sku['limit'] = '<span class="limit">Limit ' . $items[0]['limit'] . ' lbs. per guest</span>';
				}
			}
			
			
			// Show per ea. pricing for certain departments
			if (($items[0]['unit'] == 'EA' || ($items[0]['unit'] == 'CT' && !$sku['container'])) && $sku['size'] == '1' && (($sku['department'] == 'produce' || $sku['department'] == 'specialty cheese') || $sku['department'] == 'deli' || $sku['department'] == 'kitchen' || $sku['department'] == 'staples' || $sku['department'] == 'floral' || $sku['department'] == 'bakery' || $sku['department'] == 'general merchandise' || $products[$i]['department'] == 'meat')) {
				$per_ea = '<span class="perea">/ea.</span>';
			}
			
			if ($items[0]['crv'] == 1) {
				$crv = '<span class="crv">+crv</span>';
			}
			
			$sku['container'] = $items[0]['container'];
			$sku['limit'] = $items[0]['limit'];
			$sku['supplies_last'] = $items[0]['supplies_last'];
			
			// Determine format of sale price
			if (!strstr($items[0]['sale_price'],'/')) {
				
				// Convert currency to float
				$sale = number_format($items[0]['sale_price'], 2);
				
				if (count($retail_price) > 1) {
					$retail = number_format(max($retail_price), 2);
				} else {
					$retail = number_format($retail_price[0], 2);
				}
				
				// Set and format sale price and savings
				$ex = explode('.', $sale);
				
				if ($ex[0] == 0) {
					$sku['sale_price'] = $ex[1] . '<span class="cent">&cent;</span>' . $per_lb . $per_ea . $crv;
				} else {
					$sku['sale_price'] = '<i>$</i>' . $ex[0] . '<span><i>.</i>' . $ex[1] . '</span>' . $per_lb . $per_ea . $crv;
				}
				
				// Calculate and format savings
				if (count($sale_price) > 1) {
					$min_sale_price = min($sale_price);
				} else {
					$min_sale_price = $sale_price[0];
				}
				
				if (count($sale_price) > 1) {
					$max_sale_price = max($sale_price);
				} else {
					$max_sale_price = $sale_price[0];
				}
				
				if ($min_sale_price == $max_sale_price) {
					
					$savings = str_replace('.00','',number_format($retail - $sale, 2));
					
					// Format savings
					if ($savings > 0) {
						
						if ($savings < 1) {
							$sku['savings'] = $up_to . str_replace('0.','',$savings) . '&cent;' . $per_lb . $per_ea;
						} else {
							$sku['savings'] = $up_to . '$' . $savings . $per_lb . $per_ea;
						}
						
					} else {
						
						$sku['savings'] = FALSE;
					}
					
				} else {
					
					$sku['savings'] = FALSE;
					
				}
				
				/* Deprecated
				if ($items) {
					// Determine half off status
					foreach ($items as $item) {
						
						// Calculate the retail and sale difference
						$halfoff_eval = str_replace('.00','',number_format($item['retail_price'] - $item['sale_price'], 2));
						
						// Build array to flag items with three possible vals (half off, below half off, and none)
						if ($item['retail_price'] / 2 == $halfoff_eval || $item['retail_price'] / 2 == $savings - .005) {
							$halfoff[] = 2;
						} else if ($item['retail_price'] / 2 < $halfoff_eval) {
							$halfoff[] = 3;
						} else {
							$halfoff[] = 1;
						}
						
					}
				}
				
				// Remove duplicate values from array
				if (count($halfoff) > 1) {
					$unique = array_unique($halfoff);
				} else {
					$unique = $halfoff;
				}
				
				// Count the remaining items
				$count = count($unique);
				
				// If all items have the same half off status, label them accordingly
				if ($count == 1) {
					
					if ($unique[0] == 2) {
						
						$sku['class'] = ' class="halfoff"';
						$sku['half_off'] = '<em>1/2</em> Off!';
						
					} else if ($unique[0] == 3) {
						
						$sku['class'] = ' class="below halfoff"';
						$sku['half_off'] = '<em>Below 1/2</em> Price!';
						
					}
					
				}
				
				// Unset vars used for comparison
				unset($unique, $count, $halfoff, $halfoff_eval);
				
				*/
				
			} else {
				
				// Format sale price for 2/for items
				$ex = explode('/', $items[0]['sale_price']);
				$sku['sale_price'] = $ex[0] . '/$' . substr($ex[1], 0, -3) . $crv;
				
				// Calculate savings
				$sale = @number_format($ex[1] / $ex[0], 2);
				$retail = number_format(max($retail_price), 2);
				$savings =  number_format($retail - $sale, 2) * $ex[0];
				
				if ($savings < 1) {
					$sku['savings'] = $up_to . '$' . $savings . ' on&nbsp;' . $ex[0];
				} else {
					$sku['savings'] = FALSE;
				}
				
			}
			
			if (strlen($items[0]['override_price']) >= 3) {
				$sku['sale_price'] = '<span class="override">' . $items[0]['override_price'] . '</span>' . $crv;
				
				// Disable savings for any item with a price override
				$sku['savings'] = FALSE;
				
				/**
				  * Currently disabling savings for any item with a custom price, not just a % off or lowest price in town as configured below:
				  *
				// If custom price contains % off, do not show it
				if (stristr($sku['sale_price'],'% off')) {
					$sku['savings'] = FALSE;
				}
				*/
			}
			
			return $sku;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Get disabled ad items
	  *
	  * Gets manually disabled ad items from the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID the items appear on.
	  * @param string $date The date of the ad.
	  *
	  * @return array $items An array of product data and atributes
	  *
	  */
	function getDisabledAdItems ($ad_id) {
		
		$id = vbool($ad_id);
		
		if (is_array($items = selectDisabledAdItemsByAd ($id))) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Get Previous and Next Ads
	  *
	  * Gets attributes of the next upcoming ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $current_ad_date the date of the currently selected ad to offset
	  * @param string $store The name of the store chain
	  *
	  * @return array $ads An array of the next and previous ads
	  *
	  */
	function getPrevAndNextAds ($current_ad_date, $store) {
		
		// Validate and require store chain
		if (!$store_chain = vempty($store, 1)) {
			return FALSE;
		}
		
		// Validate store slug
		$store_slug = vslug($store_chain);
		
		// Set up output array
		$ads = array();
		
		// Calculate dates
		$prev = vempty(date('Y-m-d', strtotime($current_ad_date . ' - 7 days')));
		$next = vempty(date('Y-m-d', strtotime($current_ad_date . ' + 7 days')));
		
		// Retrieve ad data
		$ads['prev'] = selectAdByDateAndStoreChain($prev, $store_chain);
		$ads['next'] = selectAdByDateAndStoreChain($next, $store_chain);
		
		$ads['prev']['store_slug'] = unescape($store_slug);
		$ads['next']['store_slug'] = unescape($store_slug);
		
		// Return results
		return $ads;
		
	}


	/**
	  * Enable Ad Item Instance
	  *
	  * Sets the enable flag on specified ad item instance
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param type(int, string, boolean) $arg description
	  *
	  * @return type(int, string, boolean) $arg description
	  *
	  */
	function enableAdItemInstance ($ad_id, $item_upc) {
	  
		// Validate UPC
		$upc = "'" . str_pad(vempty($item_upc,0,0),13,STR_PAD_LEFT) . "'";
		
		if ($update = runQuery("
			
			UPDATE
				instances
				
			SET
				enabled = 1
				
			WHERE
				ad_id = $ad_id AND
				upc = $upc
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Gets Ads
	  *
	  * Gets paginated list of ads in chronilogical order
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit How many results to return.
	  *
	  * @return array $items An array of ad objects
	  *
	  */
	function getAds ($page = 1, $limit = 25) {
		
		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}
		
		$page = vbool($page);
		$limit = vbool($limit);
		
		// Calculate page offset
		$offset = $page * $limit - ($limit);
		
		if ($ads = selectAds($offset, $limit)) {
			
			foreach ($ads['listing'] as $item) {
				
				// Create array of results
				$items[$item['id']]['id']								= $item['id'];
				$items[$item['id']]['store_chain']						= $item['store_chain'];
				$items[$item['id']]['date_from']						= $item['date_from'];
				$items[$item['id']]['date_through']						= $item['date_through'];
				$items[$item['id']]['publish']							= $item['publish'];
				$items[$item['id']]['secret_special_title']				= $item['secret_special_title'];
				$items[$item['id']]['secret_special_container']			= $item['secret_special_container'];
				$items[$item['id']]['secret_special_description']		= $item['secret_special_description'];
				$items[$item['id']]['secret_special_image']				= $item['secret_special_image'];
				$items[$item['id']]['secret_special_limit']				= $item['secret_special_limit'];
				$items[$item['id']]['secret_special_skus']				= $item['secret_special_skus'];
				$items[$item['id']]['secret_special_retail']			= $item['secret_special_retail'];
				$items[$item['id']]['secret_special_regular_retail']	= $item['secret_special_regular_retail'];
				$items[$item['id']]['secret_special_from']				= $item['secret_special_from'];
				$items[$item['id']]['secret_special_through']			= $item['secret_special_through'];
				$items[$item['id']]['last_import']						= $item['last_import'];
				$items[$item['id']]['last_save']						= $item['last_save'];
				$items[$item['id']]['store_attributes'] 				= getStoreAttributesByName($item['store_chain']);
				
				// Format cache file path and name
				$cache = $items[$item['id']]['store_attributes']['slug'] . '/' . date('Y', strtotime($item['date_from'])) . '/' . $items[$item['id']]['store_attributes']['slug'] . '-' . $item['date_from'] . '.json';
				
				// Check ad cache for file time/size
				if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache)) {
					$items[$item['id']]['cache_file_updated'] = date('M j, Y @ g:i a', filemtime($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache));
				} else {
					$items[$item['id']]['cache_file_updated'] = NULL;
				}
				
				// Determine if ad is currently running and flag accordingly
				if (strtotime($item['date_from']) <= time() && strtotime("{$item['date_through']} +1 day") >= time()) {
					$items[$item['id']]['current_ad'] = 1;
				} else {
					$items[$item['id']]['current_ad'] = 0;
				}
				
				// Flag ads by week
				// Set defaults if on the first iteration of the loop
				if (!isset($last_date_from)) {
					$last_date_from = $item['date_from'];
					$last_group = 'group-tick';
				}
				
				// Calculate difference between last ad and this ad
				$dif = strtotime($last_date_from) - strtotime($item['date_from']);
				
				// Flip the switch if different than last iteration
				if ($dif >= 514800) {
					if ($last_group == 'group-tick') {
						$last_group = 'group-tock';
					} else {
						$last_group = 'group-tick';
					}
				}
				
				// Set the flag
				$items[$item['id']]['ad_group'] = $last_group;
				
				// Capture the date for next iteration
				$last_date_from = $item['date_from'];
				
			}
			
			$items['listing'] = $items;
			
			$items['pagination'] = pagination($page, $limit, $ads['count']);
			
			return $items;
			
		} else {
			
			$ads = FALSE;
			
		}
		
	}


	/**
	  * Gets Current Ads
	  *
	  * Gets all future and currently running ads in the system
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $ads A multidimensional array of ads and attributes
	  *
	  */
	function getUpcomingAndCurrentAds ($sort = 'DESC') {
		
		switch ($sort) {
			case 'asc' : 
				$sort = 'ASC';
				break;
				
			default : 
				$sort = 'DESC';
				break;
		}
		
		if ($ads = selectUpcomingAds($sort)) {
			
			// Capture the current date 
			$cur_date = date('Y-m-d');
			
			// Loop through the ads
			foreach ($ads['listing'] as $ad) {
				
				// Capture zone info
				$items[$ad['date_from']]['zones'][$ad['id']]['id']				= $ad['id'];
				$items[$ad['date_from']]['zones'][$ad['id']]['store_chain'] 	= $ad['store_chain'];
				$items[$ad['date_from']]['zones'][$ad['id']]['date_from'] 		= $ad['date_from'];
				$items[$ad['date_from']]['zones'][$ad['id']]['date_through'] 	= $ad['date_through'];
				$items[$ad['date_from']]['zones'][$ad['id']]['publish'] 		= $ad['publish'];
				$items[$ad['date_from']]['zones'][$ad['id']]['last_import'] 	= $ad['last_import'];
				$items[$ad['date_from']]['zones'][$ad['id']]['last_save'] 		= $ad['last_save'];
				$items[$ad['date_from']]['zones'][$ad['id']]['sort_order'] 		= $ad['sort_order'];
				
				// Format cache file path and name
				$items[$ad['date_from']]['zones'][$ad['id']]['store_attributes'] = getStoreAttributesByName($ad['store_chain']);
				$cache = $items[$ad['date_from']]['zones'][$ad['id']]['store_attributes']['slug'] . '/' . date('Y', strtotime($ad['date_from'])) . '/' . $items[$ad['date_from']]['zones'][$ad['id']]['store_attributes']['slug'] . '-' . $ad['date_from'] . '.json';
				
				// Check ad cache for file time/size
				if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache)) {
					$items[$ad['date_from']]['zones'][$ad['id']]['cache_file_updated'] = date('M j, Y @ g:i a', filemtime($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache));
				} else {
					$items[$ad['date_from']]['zones'][$ad['id']]['cache_file_updated'] = NULL;
				}
				
				// Capture run dates
				$items[$ad['date_from']]['ad_date'] 			= $ad['date_from'];
				$items[$ad['date_from']]['running_through'] 	= $ad['date_through'];
				
				// Capture zone information
				// $items[$ad['date_from']][]
				
				// Set milestones
				$milestones['start_build'] 		= date('Y-m-d', strtotime($ad['date_from'] . '-16 days'));
				$milestones['shoot_products']	= date('Y-m-d', strtotime($ad['date_from'] . '-13 days'));
				$milestones['start_proofing'] 	= date('Y-m-d', strtotime($ad['date_from'] . '-9 days'));
				$milestones['finish_ad_copy'] 	= date('Y-m-d', strtotime($ad['date_from'] . '-8 days'));
				$milestones['send_email_proof'] = date('Y-m-d', strtotime($ad['date_from'] . '-6 days'));
				$milestones['schedule_email'] 	= date('Y-m-d', strtotime($ad['date_from'] . '-1 days'));
				$milestones['live'] 			= date('Y-m-d', strtotime($ad['date_from']));
				
				// Capture the ad date slug to use as container ID
				$items[$ad['date_from']]['date_slug'] = $milestones['live'];
				
				// Capture status of ad to use as the container class
				if ($milestones['live'] <= $cur_date) {
					$items[$ad['date_from']]['status'] = 'running';
				} else {
					$items[$ad['date_from']]['status'] = 'pending';
				}
				
				// Set number of days in ad cycle
				$no_days = 16;
				
				// Loop through days
				while ($no_days >= 0) {
					
					// Capture the date
					$date =  date('Y-m-d', strtotime($ad['date_from'] . "-$no_days day"));
					
					// Look for milestone
					$milestone = array_search($date, $milestones);
					
					// Capture past, present, future
					if ($date < $cur_date) {
						$timeframe = 'past';
					} else if ($date > $cur_date) {
						$timeframe = 'future';
					} else {
						$timeframe = 'today';
					}
					
					// Build array of days for timeline
					if ($milestone) {
						
						if ($timeframe != 'future') {
							$items[$ad['date_from']]['current_milestone'] = $milestone;
						}
						
						$items[$ad['date_from']]['days'][$no_days] = array(
							'date' => $date, 
							'milestone' => str_replace('_', ' ', $milestone),  
							'class' => $milestone . ' ' . $timeframe
						);
						
					} else {
						$items[$ad['date_from']]['days'][$no_days] = array('date' => $date, 'class' => $timeframe);
					}
					
					$no_days--;
				}
				
			}
			
			return $items;
			
		}
		
	}


	/**
	  * Gets Ad By ID
	  *
	  * Gets an ad by the ID provided
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id Id of the ad.
	  *
	  * @return array $ad An array of ad attributes
	  *
	  */
	function getAdById ($ad_id) {
		
		// Validate id
		$id = vbool($ad_id);
		
		// Select ad data
		if ($ad = selectAdById($id)) {
			
			$store_slug = formatSlug($ad['store_chain']);
			
			// Format print ad name
			$print_name = $store_slug . '-specials-starting-' . $ad['date_from'] . '.pdf';
			
			// Look for print version of ad
			if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/print-versions/' . $print_name)) {
				$ad['print_size'] = round(filesize($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/print-versions/' . $print_name) / pow(1024, 2), 1) . " MB";
				$ad['print_file']['name'] = $print_name;
			} else {
				$ad['print_file'] = NULL;
			}
			
			// Format cache file path and name
			$cache = $store_slug . '/' . date('Y', strtotime($ad['date_from'])) . '/' . $store_slug . '-' . $ad['date_from'] . '.json';
			
			// Check ad cache for file time/size
			if (file_exists($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache)) {
				$ad['cache_file_updated'] = date('M j, Y @ g:i a', filemtime($_SERVER['DOCUMENT_ROOT'].'/../httpdocs/cache/' . $cache));
			} else {
				$ad['cache_file_updated'] = NULL;
			}
			
			// Get featured items
			$ad['items'] = getWeeklyAd($ad_id, NULL, NULL, TRUE);
			
			// Convert dates to duration
			$ad['ad_duration'] = convertDatesToDuration($ad['date_from'], $ad['date_through']);
			$ad['secret_special_duration'] = convertDatesToDuration($ad['secret_special_from'], $ad['secret_special_through']);
			
			// Format per ea. and per lb. pricing
			$find = array('/lb.', '/ea.');
			$replace = array('<span class="perlb">/lb.</span>', '<span class="perea">/ea.</span>');
			$ad['secret_special_retail'] = str_replace($find, $replace, $ad['secret_special_retail']);
			
			// convert article ids to array
			if ($ad['article_ids']) {
				$ad['articles'] = explode(',', $ad['article_ids']);
			}
			
			// convert recipe ids to array
			if ($ad['recipe_ids']) {
				$ad['recipes'] = explode(',', $ad['recipe_ids']);
			}
			
			$ad['store_attributes'] = getStoreAttributesByName($ad['store_chain']);
			
			return $ad;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Check for Duplicate Ad
	  *
	  * Checks to see if an ad with the specifed date and store chain has already been created
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date The date of the ad being submitted.
	  * @param string $chain The store chain of the ad being submitted.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function checkForDuplicateAd ($date, $chain) {
		
		if (!selectAdByDateAndStoreChain($date, $chain)) {
			
			return FALSE;
			
		} else {
			
			return TRUE;
			
		}
		
	}


	/**
	  * Append Item To Ad
	  *
	  * Selects, and appends item data to an add
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array containing the UPC, retail and sale price of the item
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function appendItemToAd ($appended_upc) {
		
		// Get product config data
		if ($item = selectProductConfig($appended_upc['upc'])) {
			
			// Validate from Product Config
			$append['upc']					= "'" . str_pad(escape($item['upc']),13,STR_PAD_LEFT) . "'";
			$append['family_group']  		= vempty ($item['family_group']);
			$append['pos_name']  			= vempty ($item['pos_name']);
			$append['prefix']  				= vempty ($item['prefix']);
			$append['display_name']  		= vempty ($item['display_name']);
			$append['suffix']  				= vempty ($item['suffix']);
			$append['note']  				= vempty ($item['note']);
			$append['description']  		= vempty ($item['description']);
			$append['pack']  				= vempty ($item['pack']);
			$append['size']  				= vempty ($item['size']);
			$append['unit']  				= vempty ($item['unit']);
			$append['container']  			= venum ($item['container'],0);
			$append['limit']  				= vempty ($item['limit']);
			$append['supplies_last']  		= vbool ($item['supplies_last']);
			$append['lifestyle']  			= vempty ($item['lifestyle']);
			$append['labels']  				= escape ($item['labels']);
			$append['crv']  				= vbool ($item['crv']);
			$append['department']  			= vempty ($item['department']);
			$append['media_links'] 			= vempty ($item['media_links']);
			$append['feature_image']		= vbool ($item['feature_image']);
			
			// Set vars coming from validated form submision
			$append['ad_id']				= $appended_upc['ad_id'];
			$append['id']					= $appended_upc['append_id'];
			$append['sale_price']			= $appended_upc['sale'];
			$append['retail_price']			= $appended_upc['retail'];
			$append['allow_override']		= $appended_upc['append_override'];
			$append['on_ad_date']			= $appended_upc['on_ad_date'];
			$append['off_ad_date']			= $appended_upc['off_ad_date'];
			
			// Handle Insert/Update
			if ($insert_updated = insertOrUpdateItemInstance($append)) {
				
				return TRUE;
				
			} else {
				
				return FALSE;
				
			}
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Sister Ads
	  *
	  * Gets the ads from the other stores running the same week.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date The date of the ad requesting the info
	  *
	  * @return array $sister_ads A multidimensional array of the sister ads
	  *
	  */
	function getSisterAds ($date) {
		
		// Declare output var
		$sister_ads = NULL;
		
		// Calculate date range
		$date_1 = vempty(date('Y-m-d', strtotime($date . '-1 day')));
		$date_2 = vempty($date);
		$date_3 = vempty(date('Y-m-d', strtotime($date . '+1 day')));
		
		$ads = selectSisterAds ($date_1, $date_2, $date_3);
		
		if (!is_array($ads)) {
			
			return FALSE;
			
		} else {
			
			$sister_ads['ads'] = $ads;
			
			$sister_ads['secret_special_emails'] = formatSisterSecretSpecialEmailLinks($sister_ads['ads']);
			
			return $sister_ads;
			
		}
		
	}


	/**
	  * Check if Item is Featured
	  *
	  * Checks to see if an item is featured in the specified ad instance
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $sister_ad A multidimensional array of the sister ad data
	  *
	  * @return boolean True if featured, false if not
	  *
	  */
	function checkIfItemIsFeatured ($sister_ad) {
		
		$instance = selectAdItemInstance($sister_ad['ad_id'], $sister_ad['family_group']);
		
		if ($instance['feature'] > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}


	/**
	  * Format Sister Secret Special Email Links
	  *
	  * Parses sister ads for secret special email dates and IDs
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $sister_ads A multidimensional array of the sister ads
	  *
	  * @return array $email_links An array of secret special links
	  *
	  */
	function formatSisterSecretSpecialEmailLinks ($sister_ads) {
		
		if (!is_array($sister_ads)) {
			
			return FALSE;
			
		} else {
			
			foreach ($sister_ads as $ad) {
				
				if ($ad['secret_special_title']) {
					
					$store_slug = unescape(vslug($ad['store_chain']));
					
					$email_links[$store_slug]['id'] = $ad['id'];
					$email_links[$store_slug]['name'] = $ad['store_chain'];
					$email_links[$store_slug]['date_from'] = $ad['date_from'];
					$email_links[$store_slug]['date_through'] = $ad['date_through'];
					
				}
				
			}
			
			return $email_links;
			
		}
		
	}


	/**
	  * Get Upcoming Ads
	  *
	  * Gets all upcoming ads regardess of publish status
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $ads An array of ads and their attributes
	  *
	  */
	function getUpcomingAds () {
		
		$ads = selectUpcomingAds();
		
		if (is_array($ads)) {
			
			return $ads;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Ad Id By Store Chain
	  *
	  * Get the curently running ad ID for the specified store
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store The slug slug of the store chain to check.
	  *
	  * @return int $ad_id The ID of the ad to return
	  *
	  */
	function getAdIdByStoreChain ($store_slug) {
		
		// Set return var
		$ad_id = NULL;
		
		// Get store name
		$store = getStoreAttributesByName($store_slug);
		
		// Vaidate store slug
		$store_chain = vempty($store['name']);
		
		// Set current ad date
		$ad_date = vempty(date('Y-m-d',strtotime('last wednesday', strtotime('tomorrow'))));
		
		if ($ad_id = selectAdByDateAndStoreChain($ad_date, $store_chain)) {
			
			return $ad_id['id'];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Ad Item Messages
	  *
	  * Gets any messages attached to an ad item group
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to match
	  * @param string $family_group The family group to match
	  * @param string $group_name The manual group to match
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function getAdItemMessages ($ad_id, $family_group = FALSE, $group_name = FALSE) {
		
		// Validate ad ID
		$id = vbool($ad_id);
		
		// Get messages based on Family Group
		if ($family_group) {
			
			$group = vempty($family_group);
			
			$messages['listing'] = selectAdItemMessagesByFamilyGroup($id, $group);
			
		// Get messages based on Manual Group
		} else if ($group_name) {
			
			$group = vempty($group_name);
			
			$messages['listing'] = selectAdItemMessagesByGroupName($id, $group);
			
		} else {
			
			return FALSE;
			
		}
		
		// Check for results and find the appropreate reply to address
		if (is_array($messages['listing'])) {
			
			$addresses = array();
			
			// Loop through results and build an array of email address
			foreach ($messages['listing'] as $message) {
				
				// If manually triggerd by user
				if (isset($_SESSION['email'])) {
					
					// Omit the current logged in user's address
					if ($message['posted_email'] != $_SESSION['email']) {
						$addresses[] = $message['posted_by'] . ' <' . $message['posted_email'] . '>';
					}
					
				} else {
					$addresses[] = $message['posted_by'] . ' <' . $message['posted_email'] . '>';
				}
				
			}
			
			// Reduce the array to unique values and append reply to element to the array
			$messages['reply_to'] = array_unique($addresses);
			
			// Return results
			return $messages;
			
		} else {
			
			return FALSE;
		}
		
	}


	/**
	  * Get Ad Item Messages by Item Hash
	  *
	  * Gets any messages attached to an ad item by the hashed item ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_hash The ad_id appened to the MD5 Hashed family_uri
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function getAdItemMessagesByItemHash ($item_hash) {
		
		// Validate ad ID
		$hash = vempty($item_hash);
		
		// Get messages based on Family Group
		$messages['listing'] = selectAdItemMessagesByItemHash($hash);
		
		// Check for results and find the appropreate reply to address
		if (is_array($messages['listing'])) {
			
			// Loop through results and build an array of email address
			foreach ($messages['listing'] as $message) {
				
				// Omit the current logged in user's address
				if ($message['posted_email'] != $_SESSION['email']) {
					$addresses[] = $message['posted_by'] . ' <' . $message['posted_email'] . '>';
				}
				
			}
			
			// Reduce the array to unique values and append reply to element to the array
			$messages['reply_to'] = array_unique($addresses);
			
			// Return results
			return $messages;
			
		} else {
			
			return FALSE;
		}
		
	}


	/**
	  * Change Item Status
	  *
	  * Changes the status of an ad item instance based on family group or manual group
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item date
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function changeAdItemStatus ($item) {
		
		if ($item['group_name']) {
			
			if (updateAdItemInstancesStatusByGroupName($item['ad_id'], $item['status'], $item['group_name'])) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		} else if ($item['family_group']) {
			
			// Force string
			if (is_numeric($item['family_group'])) {
				$item['family_group'] = "'" . $item['family_group'] . "'";
			}
			
			if (updateAdItemInstancesStatusByFamilyGroup($item['ad_id'], $item['status'], $item['family_group'])) {
				return TRUE;
			} else {
				return FALSE;
			}
			
		} else {
			
			return FALSE;
			
		}
	}


	/**
	  * Add Item Message
	  *
	  * Adds a message to a ad item instance
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item an array of message attributes
	  *
	  * @return int $insert['insert_id] The ID of the newly inserted message
	  *
	  */
	function createAdItemMessage ($item) {
		
		// Convert false values to NULL for inserting into database
		if (!$item['group_name']) {
			$item['group_name'] = 'NULL';
		}
		
		if (!$item['family_group']) {
			$item['family_group'] = 'NULL';
		}
		
		if ($insert = insertAdItemMessage($item)) {
			
			return $insert['insert_id'];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Current Messages
	  *
	  * Gets recent all messages for current ads and groups them by thread
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $messages A multi dimensional array of messages by thread
	  *
	  */
	function getCurrentMessages () {
		
		// Declare last item var for loop
		$last_item = NULL;
		
		// Calculate start date for currnt ad
		$start_date = vempty(date('Y-m-d',strtotime('last wednesday', strtotime('tomorrow'))));
		
		// Get the current messages
		$select_messages = selectCurrentMessages($start_date);
		
		if (!is_array($select_messages)) {
			
			return FALSE;
			
		} else {
			
			// Loop through messages
			$t = 0;
			foreach ($select_messages as $msg) {
				
				// Convert dismissed users into array
				$dismissed = explode(',', $msg['dismissed']);
				
				// If current logged in user is not in array
				if (!in_array($_SESSION['user_id'], $dismissed)) {
					
					// Increment thread counter if different item than thread
					if ($last_item != $msg['item_id']) {
						$t++;
					}
					
					// Add message to array
					$messages['thread-'.$t]['messages'][$msg['id']] = $msg;
					$messages['thread-'.$t]['status'] = $msg['status'];
					$messages['thread-'.$t]['thread'] = 'thread-'.$t;
					
					// Grab the address of the last message that isn’t logged in user
					if ($msg['posted_email'] != $_SESSION['email']) {
						$messages['thread-'.$t]['reply_to'] = $msg['posted_by'] . ' <' . $msg['posted_email'] . '>';
					}
					
					// Capture item ID for next itteration
					$last_item = $msg['item_id'];
					
				}
			}
			
			return $messages;
			
		}
	}


	/**
	  * Get User Message Threads
	  *
	  * Gets recent user messages by thread for current ads
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $email The email address of the user
	  *
	  * @return array $messages A multi dimensional array of messages and data
	  *
	  */
	function getUserMessageThreads () {
		
		$email = $_SESSION['email'];
		
		// Validate email address
		$user_email = vemail($email);
		
		// Calculate start date for currnt ad
		$start_date = vempty(date('Y-m-d',strtotime('last wednesday', strtotime('tomorrow'))));
		
		// Get threads by email address
		$threads = selectMessageThreadsByEmail($user_email, $start_date);
		
		if (is_array($threads)) {
			
			// Loop through threads and select messages
			$t = 1;
			foreach ($threads as $thread) {
				
				$item_id = vempty($thread);
				
				$messages['thread-'.$t] = selectMessageThreadByItemId($item_id);
				
				$t++;
			}
			
			return $messages;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	function getUserMessages () {
		
		$threads = getUserMessageThreads();
		
		foreach ($threads as $key => $thrd) {
			
			$i = 0;
			foreach ($thrd as $msg) {
				
				// Convert dismissed users into array
				$dismissed = explode(',', $msg['dismissed']);
				
				// If current logged in user is not in array
				if (!in_array($_SESSION['user_id'], $dismissed)) {
					
					$messages[$key]['messages'][$i] = $msg;
					$messages[$key]['status'] = $msg['status'];
					$messages[$key]['thread'] = $key;
					
					$i++;
				}
			}
			
		}
		
		return $messages;
		
	}


	/**
	  * Dismiss Message
	  *
	  * Dismisses selected message for the currently logged in user
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $message_id The ID of the message to mark dismissed
	  *
	  * @return NOTHING
	  *
	  */
	function dismissMessage ($message_id) {
		
		// Validate comment ID
		$id = vbool ($message_id);
		
		// Get message data for ready by users
		$message = selectMessageById($id);
		
		// If readby not null, parse readby users
		if ($message['dismissed']) {
			
			// Convert read by users into array
			$dismissedBy = explode(',', $message['dismissed']);
			
			// Append user ID of currently logged in user
			$dismissedBy[] = vbool($_SESSION['user_id']);
			
			// Implode the array into a string
			$newDismissedBy = vempty(implode(',', array_unique($dismissedBy)),0,0);
			
		// Else just validate logged in user_id
		} else {
			
			$newDismissedBy = vempty($_SESSION['user_id'],0 ,0);
			
		}
		
		// Update the dismissed record
		$updateReadBy = runQuery ("
			
			UPDATE
				messages
				
			SET
				dismissed = $newDismissedBy
				
			WHERE
				id = $id
				
		");
		
	}


	/**
	  * Get Ad Item Instance
	  *
	  * Gets a family group of items as appearing on a given ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to retrieve.
	  * @param string $family_group The family group name.
	  *
	  * @return array $item An array of item attributes
	  *
	  */
	function getAdItemInstance($ad_id, $family_group) {
		
		$id = vbool($ad_id);
		
		// Decode family group from URL string and validate
		$group = vempty(decodeFamilyGroupUrlString($family_group));
		
		if ($item = selectAdItemInstance ($id, $group)) {
			
			// Encode item url
			$item['family_uri'] = encodeFamilyGroupUrlString($family_group);
			
			// Set is_perishable flag for proper V2 ad link
			$item['is_perishable'] = getIsPerishableFlag($item['department']);
			
			return $item;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Item Group UPCs
	  *
	  * Gets all the UPCs in an ad item group.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to retrieve.
	  * @param string $family_group The family group name.
	  *
	  * @return array $skus An array of SKUs.
	  *
	  */
	function getItemGroupUPCs ($ad_id, $family_group) {
		
		$id = vbool($ad_id);
		
		// Decode family group from URL string and validate
		$group = vempty(decodeFamilyGroupUrlString($family_group));
		
		if ($skus = selectItemGroupUPCs($id, $group)) {
			
			return $skus;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Ad Items
	  *
	  * Gets a family group of items.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $item An array of item attributes
	  *
	  */
	function getAdItems($family_group) {
		
		// Decode family group from URL string and validate
		$group = vempty(decodeFamilyGroupUrlString($family_group));
		
		if ($item = selectAdItems ($group)) {
			
			// Set is_perishable flag for proper V2 ad link
			$item['is_perishable'] = getIsPerishableFlag($item['department']);
			
			return $item;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Item UPCs
	  *
	  * Gets all the UPCs for a family group of items.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $skus An array of SKUs.
	  *
	  */
	function getItemUPCs ($family_group) {
		
		// Decode family group from URL string and validate
		$group = vempty(decodeFamilyGroupUrlString($family_group));
		
		if ($skus = selectItemUPCs($group)) {
			
			return $skus;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Get Item On Upcoming Ads
	  *
	  * Gets all the upcoming ads for a family group of items.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $ads An array of SKUs.
	  *
	  */
	function getItemOnUpcomingAds ($family_group) {
		
		// Decode family group from URL string and validate
		$group = vempty(decodeFamilyGroupUrlString($family_group));
		
		// Select all instances on upcoming ads
		if ($upcoming_ads = selectItemOnUpcomingAds($group)) {
			
			// Reduce the array by store chain and set the key to the first instance ID found
			$reduce_ads = array_unique(array_column($upcoming_ads, 'store_chain', 'instance_id'));
			
			// Loop through all instances
			foreach ($upcoming_ads AS $upcoming) {
				
				// Loop through the reduced ad
				foreach	($reduce_ads AS $key => $val) {
					
					// Capture the item with the matching instance ID to rebuld the array
					if (in_array($key, $upcoming)) {
						$ads[] = $upcoming;
					}
					
				}
				
			}
			
			// Return results
			return $ads;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}

	/**
	  * Update Ad Item Config
	  *
	  * Updates product config for specified items.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdItemConfig ($item) {
		
		// Select item details
		if ($update = updateProductsByFamilyGroup($item)) {
			
			// Return results
			return TRUE;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}
