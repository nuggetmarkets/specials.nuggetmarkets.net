<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';
	require_once 'SecretSpecialsController.php';

	// Redirect if not admin
	if ($_SESSION['user_level'] > 1) {
		
		$_SESSION['error'] = 'You don&rsquo;t have access to mess with the ads, yo.';
		header("Location: /");
		exit();
		
	}


	$ads = FALSE;
	$ad = FALSE;
	$printFile = NULL;

	// Include Campaign Monitor API
	require_once('createsend/csrest_campaigns.php');
	$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);


	// Handle post
	if (isset($_POST['save']) || isset($_POST['add'])) {
		
		$ad = array();
		
		$ad['store_attributes'] = getStoreAttributesByName($_POST['store_chain']);
		
		// Validate input
		$ad['store_chain'] = vempty($_POST['store_chain'],0,1);
		$ad['ad_id'] = vbool($_POST['id']);
		$ad['publish'] = vbool($_POST['publish']);
		$ad['truckload'] = vbool($_POST['truckload']);
		$ad['truckload_dates'] = vempty($_POST['truckload_dates']);
		$ad['ocean_feast'] = vbool($_POST['ocean_feast']);
		$ad['ocean_feast_dates'] = vempty($_POST['ocean_feast_dates']);
		
		$ad['date_from'] = vdate(
			$_POST['from_year'],
			$_POST['from_month'],
			$_POST['from_day']
		);
		
		$ad['date_through'] = vdate(
			$_POST['through_year'],
			$_POST['through_month'],
			$_POST['through_day']
		);
		
		$ad['sort_order'] = vbool($ad['store_attributes']['sort_order']);
		
		// Make sure there isn’t already an ad with this date for this store
		if (isset($_POST['add']) && checkForDuplicateAd($ad['date_from'], $ad['store_chain'])) {
			$error[] = 'The ' . date('n/j/Y', strtotime(unescape($ad['date_from']))) . ' ad for ' . unescape($ad['store_chain']) . ' has already been created.';
		}
		
		$ad['features_file'] = $_POST['features_file'];
		
		$ad['secret_special_title']				= vempty($_POST['secret_special_title'],0,1);
		$ad['secret_special_container']			= vempty($_POST['secret_special_container'],0,1);
		$ad['secret_special_description']		= vempty($_POST['secret_special_description']);
		$ad['secret_special_disclaimers']		= vempty($_POST['secret_special_disclaimers']);
		$ad['secret_special_limit']				= vempty($_POST['secret_special_limit'],0,1);
		$ad['secret_special_skus']				= vempty($_POST['secret_special_skus'],0,1);
		$ad['secret_special_retail']			= vempty($_POST['secret_special_retail'],0,1);
		$ad['secret_special_regular_retail']	= vempty($_POST['secret_special_regular_retail'],0,1);
		
		$ad['secret_special_from'] = vdate(
			$_POST['secret_special_from_year'],
			$_POST['secret_special_from_month'],
			$_POST['secret_special_from_day']
		);
		
		// This needs to vdatetime so date ranges calculate correctly
		$ad['secret_special_through'] = vdatetime(
			$_POST['secret_special_through_year'],
			$_POST['secret_special_through_month'],
			$_POST['secret_special_through_day'],
			'11',
			'59',
			'pm'
		);
		
		$ad['prepended_message']			= vempty($_POST['prepended_message']);
		$ad['prepended_message_position']	= vbool($_POST['prepended_message_position']);
		
		// Handle secret special image upload
		if (!empty($_FILES['secret_special_image']['name'])) {
			
			// Delete old image first
			if (!empty($_POST['secret_special_current_image'])) {
				unlink(MEDIA_UPLOAD_DIR . $_POST['secret_special_current_image']);
			}
			
			// Create and upload new image
			$ad['secret_special_image'] = imgResize($_FILES['secret_special_image']['tmp_name'],2000, MEDIA_UPLOAD_DIR . $_FILES['secret_special_image']['name']);
			
		// Handle secret special current image
		} else if (!empty($_POST['secret_special_current_image'])) {
			
			$ad['secret_special_image'] = vempty($_POST['secret_special_current_image']);
			
		// Handle no secret special image
		} else {
			
			$ad['secret_special_image'] = 'NULL';
			
		}
		
		// Check for appeneded SKUs
		if (isset($_POST['append_upc'])) {
			
			$appended_upc = NULL;
			
			// Loop through UPCs
			$u = 0;
			foreach ($_POST['append_upc'] as $upc) {
				
				$appended_upc['ad_id'] 			= $ad['ad_id'];
				$appended_upc['on_ad_date'] 	= $ad['date_from'];
				$appended_upc['off_ad_date'] 	= $ad['date_through'];
				
				$appended_upc['upc'] 			= "'" . str_pad(vempty($upc,0,0),13,STR_PAD_LEFT) . "'";
				$appended_upc['retail'] 		= "'" . vempty($_POST['append_retail'][$u],0,0) . "'";
				
				// Wrap in extra quotes if sale price is numeric
				if (is_numeric($_POST['append_sale'][$u])) {
					
					$appended_upc['sale'] = "'" . vempty($_POST['append_sale'][$u],0,0) . "'";
					
				// Let normal escape happen for 2/for pricing
				} else {
					
					$appended_upc['sale'] = vempty($_POST['append_sale'][$u],0,0);
					
				}
				
				$appended_upc['append_id'] = "'" . $ad['store_attributes']['ad_id_prefix'] . unescape($upc) . date('Ymd',strtotime(unescape($ad['date_from']))) . "'";
				
				if ($_POST['append_override'][$u] == 1) {
					$appended_upc['append_override'] = 0;
				} else {
					$appended_upc['append_override'] = 1;
				}
				
				appendItemToAd($appended_upc);
				
				$u++;
			}
			
		}
		
		// Handle UPC file uploads
		if (isset($_FILES['upc_images']['name'][0])) {
			
			// Set directory
			$dir = $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/img/upc/';
			
			// Loop through files
			foreach ($_FILES["upc_images"]["error"] as $key => $error) {
				if ($error == UPLOAD_ERR_OK) {
					$tmp_name = $_FILES["upc_images"]["tmp_name"][$key];
					$name = $_FILES["upc_images"]["name"][$key];
					move_uploaded_file($tmp_name, "$dir/$name");
				}
			}
			
			unset($error);
			
		}
		
		//  Check for appended articles
		if (is_array($_POST['article'][$ad['ad_id']])) {
			$article_ids = implode(',', $_POST['article'][$ad['ad_id']]);
			$ad['article_ids'] = vempty($article_ids);
		} else {
			$ad['article_ids'] = 'NULL';
		}
		
		//  Check for appended recipes
		if (is_array($_POST['recipe'][$ad['ad_id']])) {
			$recipe_ids = implode(',', $_POST['recipe'][$ad['ad_id']]);
			$ad['recipe_ids'] = vempty($recipe_ids);
		} else {
			$ad['recipe_ids'] = 'NULL';
		}
		
		// Handle post if there are no errors
		if (!$error) {
			
			if (isset($_POST['add'])) {
				
				// Insert ad data
				if ($adInsert = insertAd($ad)) {
					
					// Upload PDF version of the ad
					if ($_FILES['print_version']['name']) {
						if (validateFileTypes($_FILES['print_version']['tmp_name'])) {
							move_uploaded_file($_FILES['print_version']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/print-versions/' . unescape(formatSlug($ad['store_chain'])) . '-specials-starting-' . unescape($ad['date_from']) . '.pdf');
						} else {
							$_SESSION['error'] = '<p>Print ad must be a PDF.</p>';
						}
					}	
					
					// Set confirmation
					$_SESSION['confirm'] = '<p>Ad created.</p>';
					
					// Redirect
					header("Location: /ads/edit/{$adInsert}/");
					exit();
					
				}
				
			} else if ($_POST['save']) {
				
				// Update ad data
				if ($adUpdate = updateAd($ad)) {
					
					// Upload PDF version of the ad
					if ($_FILES['print_version']['name']) {
						if (validateFileTypes($_FILES['print_version']['tmp_name'])) {
							move_uploaded_file($_FILES['print_version']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/print-versions/' . unescape(formatSlug($ad['store_chain'])) . '-specials-starting-' . unescape($ad['date_from']) . '.pdf');
						} else {
							$_SESSION['error'] = '<p>Print ad must be a PDF.</p>';
						}
					}
					
					// Set confirmation
					if ($ad['publish'] == 1) {
						
						$_SESSION['confirm'] = '<p>Ad saved. <a href="/ad-json-export/' . $ad['ad_id'] . '/" class="button">Refresh the cache</a> to apply changes to the ad.</p>';
						
					} else {
						
						$_SESSION['confirm'] = '<p>Ad saved.</p>';
						
					}
					
					
					// Redirect
					header("Location: /ads/edit/{$adUpdate['ad_id']}/");
					exit();
					
				}
				
			}
			
		}
		
	} else if (!empty($_GET['id']) && $_GET['action'] == 'publish') {
		
		$id = vbool($_GET['id']);
		
		switch ($_GET['status']) {
			case 0 :
				$status = 1;
				break;
			case 1 :
				$status = 0; 
				break;
		}
		
		updateAdPublishStatus($id, $status);
		exit();
		
	} else if (!empty($_GET['id']) && $_GET['action'] == 'edit') {
		
		// Get specified ad
		$ad = getAdById($_GET['id']);
		
		// Get Sister Ads
		$sister_ads = getSisterAds($ad['date_from']);
		
		// Get all secret special data
		$secret_special = getSecretSpecial($ad['date_from']);
		
		$f = array("UPCs:", " ");
		$r = array('','');
		$upcs = str_replace($f, $r, explode(',', $ad['secret_special_skus']));
		
		// Set picture label flag
		$picture_label = FALSE;
		
		if (isset($secret_special['secret_special_picture'] )) {
			foreach ($secret_special['secret_special_picture'] as $picture) {
				if (in_array($picture['upc'], $upcs)) {
					// Flip picture label flag
					$picture_label = TRUE;
				}
			}
		}
		
		// Build Secret Special Proof Email
		if ($sister_ads['secret_special_emails']) {
			
			$secret_special_email_subject = rawurlencode(html_entity_decode($ad['secret_special_title'])) . ' Secret Special Email Proofs';
			
			$secret_special_email_proof_body = 'The ' . rawurlencode(html_entity_decode($ad['secret_special_title'])) . ' Secret Special emails for the week of ' . date('F j, Y', strtotime($ad['date_from'])) . ' are ready to proof.';
			
			foreach ($sister_ads['secret_special_emails'] as $email) {
				$secret_special_email_proof_body .= '%0D%0D' . $email['name'] . ':%0Dhttps://specials.nuggetmarkets.net/email/' . $email['id'] . '/';
			}
			
			$secret_special_email_proof_body .= '%0D%0DThanks!';
			
			rawurlencode($secret_special_email_subject);
			rawurlencode($secret_special_email_proof_body);
			
		}
		
		// Calculate email_send_date for secret special email
		if (time() < strtotime($ad['date_from'] . ' 06:00:00')) {
			
			$email_send_date['value'] = "{$ad['date_from']} 06:00";
			
		} else {
			
			$email_send_date['value'] = date('Y-m-d H:i', strtotime('+30 minutes'));
			
		}
		
		$email_send_date['display'] = date('M. j, Y @ g:i a', strtotime($email_send_date['value']));
		
		// Set page attrs
		$page_attrs['title'] = $ad['store_chain'] . ' Ad Starting ' . date('M j, Y',strtotime($ad['date_from']));
		$page_attrs['description'] = 'Manage settings for the ' . date('M j, Y',strtotime($ad['date_from'])) . ' ' . $ad['store_chain'];
		$page_attrs['class'] = 'ads detail';
		$page_attrs['action'] = 'edit';
		
	} else if (!empty($_GET['action']) && $_GET['action'] == 'new') {
		
		// Prepopulate dates with today
		$ad['date_from']	= date('Y-m-d');
		$ad['date_through'] = date('Y-m-d', strtotime('+6 days'));
		$ad['secret_special_from'] = NULL;
		$ad['secret_special_through'] = NULL;
		$page_attrs['action'] = 'new';
		
		$page_attrs['title'] = 'Create New Ad';
		$page_attrs['description'] = 'Create a new ad.';
		$page_attrs['class'] = 'ads';
		
	} else if (!empty($_GET['sort'])) {
		
		$_SESSION['ad_sort'] = $_GET['sort'];
		header("Location: /ads/");
		exit();
		
	} else {
		
		// Get current page number
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}
		
		// Get the ads
		$ads = getUpcomingAndCurrentAds($_SESSION['ad_sort']);
		
		$page_attrs['title'] = 'Ads ';
		$page_attrs['description'] = 'Manage ads.';
		$page_attrs['class'] = 'ads';
		
	}
