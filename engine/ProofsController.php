<?php

	// Include depenencies
    require_once 'ProofsModel.php';


    function getCurrentAds () {
		
        // Setup output array
        $ads = array();
		
        // Attempt to get the current ad dates
        if (!$ad_dates = getCurrentAdDates()) {
			
            return FALSE;
			
        } else {
			
            // Loop through ad dates, get status counts, and format date headings
            $i = 0;
            foreach ($ad_dates as $ad_date) {
				
                $ads[$i] = getAdStatusCounts($ad_date);
                
                $ads[$i]['date_from'] = date('F j, Y', strtotime($ad_date['date_from']));
                $ads[$i]['date_through'] = date('F j, Y', strtotime($ad_date['date_through']));
                $ads[$i]['date_slug'] = $ad_date['date_from'];
				
                if ($ad_date['date_from'] <= date('Y-m-d') && $ad_date['date_through'] >= date('Y-m-d')) {
                    $ads[$i]['current'] = 1;
                } else {
                    $ads[$i]['current'] = 0;
                }
				
                $i++;
            }
            
            // Return results
            return $ads;
			
        }
		
    }
    
    
    /**
      * Get Current Ads Features
      *
      * Gets the features from the current ads for proofing
      *
      */
    function getCurrentAdsFeatures () {
        
        $current_features = selectCurrentAdsFeatures();
        
        if (!is_array($current_features)) {
        
            return FALSE;

        } else {
            
            $i = 0;
            foreach ($current_features as $current_feature) {
                
				$features[$current_feature['on_ad_date']]['zones'][$current_feature['store_chain']]['ad_id'] = $current_feature['ad_id'];
                $features[$current_feature['on_ad_date']]['zones'][$current_feature['store_chain']]['sort_order'] = $current_feature['sort_order'];
				$features[$current_feature['on_ad_date']]['zones'][$current_feature['store_chain']]['features'][$current_feature['feature']] = $current_feature['status'];
                
                $i++;
            }

            return $features;

        }
        
    }


    /**
     * Get Current Ad Dates
     * 
     * Gets the currently running ad and any published upcoming ads in the database
     * 
     * @author Mark Eagleton <mark@thebigreason.com>
     * 
     * @return array $ad_dates An array of start and end dates for each of the ads
     * 
     */
    function getCurrentAdDates () {

        $ad_dates = selectCurrentAdDates();

        if (!is_array($ad_dates)) {

            return FALSE;

        } else {

            return $ad_dates;

        }

    }


    /**
     * Get Ad Status Counts
     * 
     * Gets all items with the specified ad date, groups by family, then counts and parses the statuses, 
     * and finally formats the output for display.
     * 
     * @author Mark Eagleton <mark@thebigreason.com>
     * 
     * @param array $ad_date An array contaning the date_from and date_through of a given ad week
     * 
     * @return array $output An array of 
     * 
     */
    function getAdStatusCounts ($ad_date) {

        // Set up array variables
        $instances = array();
        $ad = array();
        $output = array();

        // Validate start date
        $date = vempty($ad_date['date_from']);

        // Select ad item instances within date range
        $items = selectDepartmentStatusesByDate($date);

        // Loop through items and regroup them by family 
        foreach ($items as $item) {

            // Set family name
            if ($item['group_name']) {
                $fam = $item['group_name'];
            } else {
                $fam = $item['family_group'];
            }

            // Rebuild array by department/ad/family
            $instances[$item['department']][$item['ad_id']][$fam][] = $item;
            
        }

        // Itterate through full array to count statuses
        foreach ($instances as $dept => $dept_vals) {

            foreach ($dept_vals as $store => $store_vals) {

                foreach ($store_vals as $family => $family_vals) {

                    // Capture statuses
                    $ad['departments'][$dept]['statuses'][] = $family_vals[0]['status']; 

                }

                // Count statuses
                $ad['departments'][$dept]['statuses_breakdown'] = array_count_values($ad['departments'][$dept]['statuses']);

            }
        }

        // Loop through departments
        $d = 0;
        foreach ($ad['departments'] as $dept => $statuses) {
                
            // Capture department name
            $output['departments'][$d]['department_name'] = ucwords($dept);
            $output['departments'][$d]['department_slug'] = formatSlug($dept);

            // Set up statuses container
            $output['departments'][$d]['status'] = '<span class="statuses">';

            // Check for non-approved statuses
            if (array_key_exists('pending', $statuses['statuses_breakdown']) || array_key_exists('issue', $statuses['statuses_breakdown'])) {

                // Loop through problem statueses
                foreach ($statuses['statuses_breakdown'] as $key => $val) {

                    // Append pending message and count
                    if ($key == 'pending') {
                        $output['departments'][$d]['status'] .= '<span class="pending">' . $val . ' awaiting approval</span>';
                    }

                    // Append issue message and count
                    if ($key == 'issue') {
                        if ($val > 1) {
                            $output['departments'][$d]['status'] .= '<span class="issue">' . $val . ' corrections</span>';
                        } else {
                            $output['departments'][$d]['status'] .= '<span class="issue">' . $val . ' correction</span>';
                        }
                    }
                }

            // Set and append status to approved
            } else {

                $output['departments'][$d]['status'] .= '<span class="approved">approved</span>';
            
            }

            // Append closing tag
            $output['departments'][$d]['status'] .= '</span>';

            // Increment key
            $d++;

        }

        // Return final output
        return $output;

    }

    
    /** 
      * getAdIdsByStartDate 
      *  
      * Gets all the IDs for ads with the specfied start date. 
      *  
      * @author Mark Eagleton <mark@thebigreason.com> 
      *  
      * @param string $date The start date of the ads to return 
      * 
      * @return array $ad_ids An array of ad IDs 
      * 
      */ 
    function getAdsWithStartDate ($date) { 

        $ad_ids = selectAdsWithStartDate($date);

        if (!is_array($ad_ids)) {

            return FALSE;
        
        } else {

            return $ad_ids; 
        
        }
    }
