<?php


	/**
	  * Update Last Ad Import
	  *
	  * Updates the last_import field of an entry in the ad table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_id The ID of the ad to update.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateLastAdImport ($ad_id) {
		
		if ($result = runQuery("
			
			UPDATE
				ads
				
			SET
				last_import = NOW()
				
			WHERE
				id = $ad_id
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Import Weely Ad
	  *
	  * Converts DROSTE ad XML file dump into php array
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the ad being imported.
	  * @param int $ad_id The ID of the ad being imported.
	  *
	  * @return array $items An array of items and attributes
	  *
	  */
	function importWeeklyAd ($ad_date, $ad_id) {
		
		// Get ad data
		$ad = selectAdById($ad_id);
		
		// Set file import path
		switch ($ad['store_chain']) {
			
			case 'Nugget Markets Sonoma' :
				$ad_path = SONOMA_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'ns';
				$zone_no = '111';
				break;
				
			case 'Sonoma Market' :
				$ad_path = SONOMA_MARKET_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'sm';
				$zone_no = '111';
				break;
				
			case 'Nugget Markets Marin' :
				$ad_path = MARIN_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'nm';
				$zone_no = '110';
				break;
				
			case 'Nugget Markets Valley' :
				$ad_path = VALLEY_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'nv';
				$zone_no = '100';
				break;
				
			case 'Nugget Markets' :
				$ad_path = VALLEY_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'nn';
				$zone_no = '100';
				break;
				
			case 'Fork Lift' :
				$ad_path = FORKLIFT_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'fl';
				$zone_no = '121';
				break;
				
			case 'Food 4 Less' :
				$ad_path = FOOD4LESS_AD_PREFIX . 'weeklyad-';
				$id_prefix = 'f4';
				$zone_no = '130';
				break;
				
			default :
				$ad_path = 'weeklyad-';
				$id_prefix = 'nm';
				$zone_no = '100';
				break;
				
		}
		
		// Load XML file
		if ($products = simplexml_load_file(SERVER_PATH . '/productinfo/' . $ad_path . $ad_date . '.xml')) {
			
			$i = 0;
			
			// Loop through array
			foreach ($products as $product) {
				
				// Set vars
				$i++;
				$items[$i]['name'] = $product->Name;
				$items[$i]['halfOff'] = NULL;
				$items[$i]['savings'] = NULL;
				$items[$i]['id_prefix'] = $id_prefix;
				$items[$i]['zone_no'] = $zone_no;
				
				// Loop through multidimensional array
				foreach ($product->AdItems as $adItems) {
					
					foreach ($adItems->MarketingItem as $attrs) {
						
						// Set attribute vars
						$items[$i]['items'][] = $attrs;
						
					}
					
				}
				
			}
			
			// Return products array
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Product Config
	  *
	  * Gets the product info from the products table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $upc The UPC of the item to retrieve.
	  *
	  * @return array $item[0] An array of product data
	  *
	  */
	function selectProductConfig ($upc) {
		
		if ($item = runQuery("
			
			SELECT
				products.*
				
			FROM
				products
				
			WHERE
				upc = $upc
				
		", 'specials')) {
			
			return $item[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get Store Attributes
	  *
	  * Gets store name, URL, and other attributes based on input
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $store_chain The name of the store.
	  *
	  * @return array $store An array of attributes for the store chain
	  *
	  */
	function getStoreAttributesByName ($store_chain) {
		
		if (stristr($store_chain, 'nugget')) {
			
			$store = array(
				'url' 				=> NUGGET_WEBSITE,
				'media_url'			=> NUGGET_WEBSITE . '/media/images/',
				'recipe_archive' 	=> NUGGET_WEBSITE . '/recipes/',
				'article_archive' 	=> NUGGET_WEBSITE . '/articles/',
				'ad_url' 			=> NUGGET_WEBSITE . '/specials/',
				'ad_id_prefix'		=> 'nm',
				'email_logo_header'	=> 'nugget-markets-white.png',
				'email_logo_footer'	=> 'nugget-markets-white.png',
				'email_logo_print'	=> 'nugget-markets-black.png',
				'email_from_address'=> 'enews@nuggetmarket.com'
			);
			
			if (stristr($store_chain, 'valley')) {
				
				$store['name'] = 'Nugget Markets Valley';
				$store['public_name'] = 'Nugget Markets';
				$store['slug'] = 'nugget-markets-valley';
				$store['zone'] = '100';
				$store['ad_id_prefix'] = 'nv';
				$store['sort_order'] = 1;
				
			} else if (stristr($store_chain, 'marin')) {
				
				$store['name'] = 'Nugget Markets Marin';
				$store['public_name'] = 'Nugget Markets';
				$store['slug'] = 'nugget-markets-marin';
				$store['zone'] = '110';
				$store['sort_order'] = 2;
				
			} else if (stristr($store_chain, 'sonoma')) {
				
				$store['name'] = 'Nugget Markets Sonoma';
				$store['public_name'] = 'Nugget Markets';
				$store['slug'] = 'nugget-markets-sonoma';
				$store['zone'] = '111';
				$store['sort_order'] = 3;
				
			} else {
				
				$store['name'] = 'Nugget Markets';
				$store['slug'] = 'nugget-markets';
				$store['zone'] = '100';
				$store['sort_order'] = 1;
				
			}
			
			return $store;
			
		} else if (stristr($store_chain, 'sonoma')) {
			
			$store = array(
				'name' 				=> 'Sonoma Market',
				'public_name'		=> 'Sonoma Market',
				'slug' 				=> 'sonoma-market',
				'zone' 				=> '111',
				'url' 				=> SONOMA_WEBSITE,
				'media_url'			=> SONOMA_WEBSITE . '/media/',
				'recipe_archive' 	=> SONOMA_WEBSITE . '/recipes/',
				'article_archive' 	=> SONOMA_WEBSITE . '/articles/',
				'ad_url' 			=> SONOMA_WEBSITE . '/specials/',
				'ad_id_prefix'		=> 'sm',
				'email_logo_header'	=> 'sonoma-market-white.png',
				'email_logo_footer'	=> 'sonoma-market-white.png',
				'email_logo_print'	=> 'sonoma-market-black.png',
				'email_from_address'=> 'info@sonomamarket.net',
				'sort_order'		=> 3
				
			);
			
			return $store;
			
		} else if (stristr($store_chain, 'fork')) {
			
			$store = array(
				'name' 				=> 'Fork Lift',
				'public_name'		=> 'Fork Lift',
				'slug' 				=> 'fork-lift',
				'zone' 				=> '121',
				'url' 				=> FORK_LIFT_WEBSITE,
				'media_url'			=> FORK_LIFT_WEBSITE . '/media/',
				'recipe_archive' 	=> FORK_LIFT_WEBSITE . '/recipes/',
				'article_archive' 	=> FORK_LIFT_WEBSITE . '/articles/',
				'ad_url' 			=> FORK_LIFT_WEBSITE . '/specials/',
				'ad_id_prefix'		=> 'fl',
				'email_logo_header'	=> 'fork-lift-white.png',
				'email_logo_footer'	=> 'fork-lift-white.png',
				'email_logo_print'	=> 'fork-lift-black.png',
				'email_from_address'=> 'online@forkliftgrocery.com',
				'sort_order'		=> 4
				
			);
			
			return $store;
			
		} else if (stristr($store_chain, 'less')) {
			
			$store = array(
				'name' 				=> 'Food 4 Less',
				'public_name'		=> 'Food 4 Less',
				'slug' 				=> 'food-4-less',
				'zone' 				=> '130',
				'url' 				=> FOOD_4_LESS_WEBSITE,
				'media_url'			=> FOOD_4_LESS_WEBSITE . '/media/',
				'recipe_archive' 	=> FOOD_4_LESS_WEBSITE . '/recipes/',
				'article_archive' 	=> FOOD_4_LESS_WEBSITE . '/articles/',
				'ad_url' 			=> FOOD_4_LESS_WEBSITE . '/specials/',
				'ad_id_prefix'		=> 'f4',
				'email_logo_header'	=> 'food-4-less-white.png',
				'email_logo_footer'	=> 'food-4-less-white.png',
				'email_logo_print'	=> 'food-4-less-color.png',
				'email_from_address'=> 'enews@nuggetmarket.com',
				'sort_order'		=> 5
				
			);
			
			return $store;
			
		} else {
			
			return NULL;
			
		}
		
	}


	/**
	  * Select Departments By Ad
	  *
	  * Retrives list of departments that have items in a particular ad.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to select.
	  *
	  * @return array $departments A list of departments that have items in a given ad.
	  *
	  */
	function selectDepartmentsByAd ($ad_id) {
		
		$id = vbool($ad_id);
		
		if ($departments = runQuery("
			
			SELECT
				department
				
			FROM
				instances
				
			WHERE
				ad_id = $id
				
			GROUP BY
				department
				
			ORDER BY
				department ASC
				
		")) {
			
			return $departments;
			
		} else {
			
			return FALSE;
			
		}
	}


	/**
	  * Decode Ad Item Departments
	  *
	  * Converts DROSTE department codes into human readable codes
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $code DROSTE department code.
	  *
	  * @return string $department Decoded department name
	  *
	  */
	function decodeAdItemDepartments ($code) {
		
		/*
		$departments = array(
			'11' => 'grocery',
			'12' => 'dairy',
			'13' => 'frozen',
			'14' => 'cigarette',
			'15' => 'health and beauty',
			'16' => 'general merchandise',
			'17' => 'liquor',
			'18' => 'beer',
			'19' => 'wine',
			'1A' => 'pharmacy',
			'1B' => 'video',
			'1C' => 'healthy living',
			'1D' => 'housewares',
			'1E' => 'grocery special',
			'1F' => 'grocery tax special',
			'1G' => 'soda special',
			'1H' => 'grocery tax',
			'21' => 'meat',
			'22' => 'sushi',
			'23' => 'seafood',
			'24' => 'meat/deli',
			'25' => 'produce',
			'26' => 'soda',
			'27' => 'bakery',
			'28' => 'deli',
			'29' => 'chinese',
			'2A' => 'coffee bar',
			'2B' => 'cooking school',
			'2C' => 'wine tasting',
			'2D' => 'specialty cheese',
			'2E' => 'pasta shop',
			'2F' => 'dairy special',
			'2G' => 'frozen special',
			'31' => 'bulk foods',
			'32' => 'corporate a/p',
			'33' => 'corporate a/r',
			'34' => '1-hour photo',
			'35' => 'floral',
			'36' => 'janitorial',
			'37' => 'mic cards',
			'4A' => 'supplies',
			'4B' => 'bottle department',
			'4C' => 'gift card',
			'4D' => 'scrip card',
			'4E' => 'fixtures',
			'4F' => 'gold coins',
			'4G' => 'community',
			'4H' => 'postal station',
			'4I' => 'gift depletion',
			'4J' => 'blackhawk',
			'4K' => 'associate discount',
			'4L' => 'atm fee',
			'4M' => 'paid out',
			'4N' => 'lottery',
			'4O' => 'coinstar'
		);
		*/
		
		// Frozen
		$frozen = array('13','2G');
		
		// Non food items
		$general = array('16','1H','1D','1F');
		
		// Dairy
		$dairy = array('2F','12');
		
		// Meat and seafood
		$meat = array('21','23');
		
		// Deli
		$deli = array('24');
		
		// Coffee Bar
		$coffee = array('2A');
		
		// Kitchen
		$kitchen = array('22','28','29');
		
		// Specialty cheese
		$cheese = array('2D');
		
		// Produce
		$produce = array('25');
		
		// Bakery
		$bakery = array('27');
		
		// Adult bev
		$adultbev = array('17','18','19','2C');
		
		// Floral
		$floral = array('35');
		
		// Healthy living
		$healthyliving = array('15','1A','1C',);
		
		if (in_array($code, $meat)) {
			$department = 'meat';
		} else if (in_array($code, $cheese)) {
			$department = 'specialty cheese';
		} else if (in_array($code, $produce)) {
			$department = 'produce';
		} else if (in_array($code, $deli)) {
			$department = 'deli';
		} else if (in_array($code, $coffee)) {
			$department = 'coffee bar';
		} else if (in_array($code, $kitchen)) {
			$department = 'kitchen';
		} else if (in_array($code, $bakery)) {
			$department = 'bakery';
		} else if (in_array($code, $adultbev)) {
			$department = 'adult beverages';
		} else if (in_array($code, $floral)) {
			$department = 'floral';
		} else if (in_array($code, $healthyliving)) {
			$department = 'healthy living';
		} else if (in_array($code, $frozen)) {
			$department = 'frozen';
		} else if (in_array($code, $general)) {
			$department = 'general merchandise';
		} else if (in_array($code, $dairy)) {
			$department = 'dairy';
		} else {
			$department = 'staples';
		}
		
		// Return department name
		return $department;
		
	}


	/**
	  * Refresh Ad Items
	  *
	  * Imports ad data from the specified DROSTE XML dump, updates the products config with the new data,
	  * refreshes the instances table from products config, and deletes any product instances that have been
	  * removed since the last import.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the ad to import.
	  * @param int $ad_id The ID of the ad whos instances to update.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function refreshAdItems ($ad_date, $ad_id, $soft_refresh = FALSE) {
		
		// Import the XML file
		if ($products = importWeeklyAd($ad_date, $ad_id)) {
			
			// Iterate through items
			foreach ($products as $product) {
				
				// Set vars for product attributes
				foreach ($product['items'] as $attr) {
					
					
					// Skip products that don’t have a department
					if (strlen($attr->Department) < 1) {
						continue;
					}
					
					// Convert integers and zero pad
					$item['upc'] =  str_pad(vempty($attr->UPC13,0,0),13,STR_PAD_LEFT);
					
					// Validate imported data
					$item['ad_id'] = vbool($ad_id);
					$item['pack'] = vempty($attr->Pack,0,0);
					$item['size'] = vempty($attr->Size,0,0);
					
					if ($attr->CRV > 0) {
						$item['crv'] = 1;
					} else {
						$item['crv'] = 0;
					}
					
					$item['pos_name'] 	= vempty($attr->Description,0,0);
					$item['unit'] 		= vempty($attr->UnitOfMeasure,0,0);
					$item['labels'] 	= 'NULL';
					
					// Account for empty special diet strings
					if (strlen($attr->SpecialDietType) >= 5 && $attr->SpecialDietType != '     ') {
						$item['lifestyle'] = vempty($attr->SpecialDietType,0,0);
					} else {
						$item['lifestyle'] = 'NULL';
					}
					
					$item['retail_price'] 	= vempty($attr->RegularRetail,0,0);
					$item['sale_price']		= vempty($attr->AdRetail,0,0);
					$item['family_group'] 	= vempty($attr->FamilyGroup,0,0);
					
					// Use $ad_date attr because some items are keyed for multiple week runs
					$item['on_ad_date']		= vempty($ad_date,0,0);
					$item['off_ad_date'] 	= vempty($attr->Ends,0,0);
					$item['department']		= vempty(decodeAdItemDepartments($attr->Department),0,0);
					
					// Create a unique id for ad_item_prices by concatenating UPC and ad date
					$rm_chars 			= array("'","-");
					$item['id'] 		= "'" . $product['id_prefix'] . str_replace($rm_chars,'',$item['upc']) . str_replace($rm_chars,'',$item['on_ad_date']) . "'";
					
					// Set zone retail ID for zone pricing
					$item['retail_id'] = "'" . vempty($product['zone_no']) . str_replace($rm_chars,'',$item['upc']) . "'";
					$item['zone_no'] = vempty($product['zone_no']);
					$item['zone_prefix'] = vempty($product['id_prefix']);
					
					// Capture ids in array for comparison
					$instance_ids[] 	= $product['id_prefix'] . str_replace($rm_chars,'',$item['upc']) . str_replace($rm_chars,'',$item['on_ad_date']);
					
					// Set labels to NULL
					$item['labels'] = 'NULL';
					
					// Retrieve label id for lifestyle/special diet flag. This will only populate label field on initial import.
					if ($item['lifestyle'] != 'NULL') {
						
						if ($label = runQuery("
							
							SELECT
								id
								
							FROM
								labels
								
							WHERE
								name = {$item['lifestyle']}
								
						")) {
							
							$item['labels'] = "'" . $label[0] . "'";
							
						}
						
					}
					
					// Insert or Update product configuration
					insertOrUpdateItemConfig($item);
					
					// This function should be deprecated… I think! Insert or Update retail price
					// insertOrUpdateRetailPrice($item);
					
					// Get item from product config
					if ($line_item = selectProductConfig($item['upc'])) {
						
						// Escape output from product config
						$item['prefix'] 		= vempty($line_item['prefix']);
						$item['display_name'] 	= vempty($line_item['display_name']);
						$item['suffix'] 		= vempty($line_item['suffix']);
						$item['note'] 			= vempty($line_item['note']);
						$item['description'] 	= vempty($line_item['description']);
						$item['container'] 		= venum($line_item['container'],0);
						$item['limit'] 			= vempty($line_item['limit']);
						$item['supplies_last'] 	= vbool($line_item['supplies_last']);
						$item['pack'] 			= vempty($line_item['pack']);
						$item['size'] 			= vempty($line_item['size']);
						$item['unit'] 			= vempty($line_item['unit']);
						$item['lifestyle'] 		= vempty($line_item['lifestyle']);
						$item['labels'] 		= vempty($line_item['labels']);
						$item['crv'] 			= vbool($line_item['crv']);
						$item['allow_override'] = 'allow_override';
						$item['media_links'] 	= vempty($line_item['media_links']);
						$item['feature_image'] 	= vbool($line_item['feature_image']);
						
						if ($soft_refresh) {
							
							// Soft Insert or Update on duplicate
							softInsertOrUpdateItemInstance ($item);
							
						} else {
							
							// Hard Insert or Update on duplicate
							insertOrUpdateItemInstance ($item);
							
						}
					}
				}
			}
			
			// Compare ad items in database with last import
			$current = runQuery("
				
				SELECT
					id
					
				FROM
					instances
					
				WHERE
					ad_id = $ad_id
					
			");
			
			// Compare all instances or ad in DB with what was just imported
			$remove = array_diff($current, $instance_ids);
			
			// Loop through and remove items not in last import.
			foreach ($remove as $item) {
				
				// Validate item
				$item = vempty($item);
				
				// Delete item
			    runQuery("
					
			    	DELETE FROM
			    		instances
						
			    	WHERE
			    		id = $item AND
			    		allow_override = 0
						
			    	LIMIT 1
					
			    ");
			}
			
			// Return true
			return TRUE;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Insert or Update Item Config
	  *
	  * Inserts or updates on duplicate key item config info
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item data.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function insertOrUpdateItemConfig ($item) {
		
		if ($insert_update = runQuery("
			
			INSERT INTO
				products
				
			SET
				upc 			= {$item['upc']},
				family_group	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				pack 			= {$item['pack']},
				size 			= {$item['size']},
				unit 			= {$item['unit']},
				lifestyle 		= {$item['lifestyle']},
				labels 			= {$item['labels']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']}
				
			ON DUPLICATE KEY UPDATE
				family_group	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				lifestyle 		= {$item['lifestyle']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']}
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Soft Insert or Update Item Instance
	  *
	  * Inserts or updates on duplicate key item instance info, but leaves
	  * certain instance info alone so as to not override certain information.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item data.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function softInsertOrUpdateItemInstance ($item) {
		
		// Insert/Update item
		if ($insert_update = runQuery("
			
			INSERT INTO
				instances
				
			SET
				id 				= {$item['id']},
				ad_id			= {$item['ad_id']},
				upc 			= {$item['upc']},
				family_group 	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				prefix 			= {$item['prefix']},
				display_name 	= {$item['display_name']},
				suffix 			= {$item['suffix']},
				note 			= {$item['note']},
				description		= {$item['description']},
				pack 			= {$item['pack']},
				size 			= {$item['size']},
				unit 			= {$item['unit']},
				container 		= {$item['container']},
				`limit`			= {$item['limit']},
				supplies_last	= {$item['supplies_last']},
				lifestyle	 	= {$item['lifestyle']},
				`labels`		= {$item['labels']},
				sale_price		= {$item['sale_price']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']},
				on_ad_date		= {$item['on_ad_date']},
				off_ad_date		= {$item['off_ad_date']},
				media_links		= {$item['media_links']},
				feature_image 	= {$item['feature_image']},
				status			= 'pending'
				
			ON DUPLICATE KEY UPDATE
				ad_id			= {$item['ad_id']},
				upc 			= {$item['upc']},
				family_group 	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				pack 			= {$item['pack']},
				size 			= {$item['size']},
				unit 			= {$item['unit']},
				container 		= {$item['container']},
				`limit`			= {$item['limit']},
				supplies_last	= {$item['supplies_last']},
				lifestyle	 	= {$item['lifestyle']},
				sale_price		= {$item['sale_price']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']},
				on_ad_date		= {$item['on_ad_date']},
				off_ad_date		= {$item['off_ad_date']}
				
		")) {
			
			// Return true
			return TRUE;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Hard Insert or Update Item Instance
	  *
	  * Inserts or updates on duplicate key item instance info
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item data.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function insertOrUpdateItemInstance ($item) {
		
		// Insert/Update item
		if ($insert_update = runQuery("
			
			INSERT INTO
				instances
				
			SET
				id 				= {$item['id']},
				ad_id			= {$item['ad_id']},
				upc 			= {$item['upc']},
				family_group 	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				prefix 			= {$item['prefix']},
				display_name 	= {$item['display_name']},
				suffix 			= {$item['suffix']},
				note 			= {$item['note']},
				description		= {$item['description']},
				pack 			= {$item['pack']},
				size 			= {$item['size']},
				unit 			= {$item['unit']},
				container 		= {$item['container']},
				`limit`			= {$item['limit']},
				supplies_last	= {$item['supplies_last']},
				lifestyle	 	= {$item['lifestyle']},
				`labels`		= {$item['labels']},
				sale_price		= {$item['sale_price']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']},
				on_ad_date		= {$item['on_ad_date']},
				off_ad_date		= {$item['off_ad_date']},
				allow_override	= {$item['allow_override']},
				media_links		= {$item['media_links']},
				feature_image 	= {$item['feature_image']},
				status			= 'pending'
				
			ON DUPLICATE KEY UPDATE
				ad_id			= {$item['ad_id']},
				upc 			= {$item['upc']},
				family_group 	= {$item['family_group']},
				pos_name		= {$item['pos_name']},
				
				prefix 			= {$item['prefix']},
				display_name 	= {$item['display_name']},
				suffix 			= {$item['suffix']},
				note 			= {$item['note']},
				description		= {$item['description']},
				
				pack 			= {$item['pack']},
				size 			= {$item['size']},
				unit 			= {$item['unit']},
				container 		= {$item['container']},
				`limit`			= {$item['limit']},
				supplies_last	= {$item['supplies_last']},
				lifestyle	 	= {$item['lifestyle']},
				
				`labels`		= {$item['labels']},
				
				sale_price		= {$item['sale_price']},
				retail_price 	= {$item['retail_price']},
				crv 			= {$item['crv']},
				department 		= {$item['department']},
				on_ad_date		= {$item['on_ad_date']},
				off_ad_date		= {$item['off_ad_date']},
				allow_override	= {$item['allow_override']},
				
				media_links		= {$item['media_links']},
				feature_image 	= {$item['feature_image']}
				
		")) {
			
			// Return true
			return TRUE;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Insert or Update Retail Price
	  *
	  * Inserts or updates on duplicate key retail pricing for given zone
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item data.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function insertOrUpdateRetailPrice ($item) {
		
		if ($insert_update = runQuery("
			
			INSERT INTO
				retail_prices
				
			SET
				id 				= {$item['retail_id']},
				upc 			= {$item['upc']},
				zone			= {$item['zone_no']},
				zone_prefix		= {$item['zone_prefix']},
				retail_price 	= {$item['retail_price']}
				
			ON DUPLICATE KEY UPDATE
				upc 			= {$item['upc']},
				zone			= {$item['zone_no']},
				zone_prefix		= {$item['zone_prefix']},
				retail_price 	= {$item['retail_price']}
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ads
	  *
	  * Selects rows from the ads table based on offset and limit provided
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Takes page number from Ads class call
	  * @param int $limit Number of results to return
	  *
	  * @return array $ads An array of selected the records
	  *
	  */
	function selectAds ($offset, $limit) {
		
		$ads = NULL;
		$count = NULL;
		
		if ($ads['listing'] = runQuery("
			
			SELECT
				*
				
			FROM
				ads
				
			ORDER BY
				date_from DESC,
				sort_order ASC
				
			LIMIT
				$offset, $limit
				
		", 'specials')) {
			
			$count = runQuery("
				
				SELECT
					COUNT(id)
					
				FROM
					ads
					
			", 'specials');
			
			$ads['count'] = $count[0];
			
			return $ads;
			
		} else {
		
			return FALSE;
			
		}
		
	}


	/**
	  * Select Upcoming Ads
	  *
	  * Selects rows from the ads table whose dates are now or in the future.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $ads An array of selected the records
	  *
	  */
	function selectUpcomingAds ($sort = 'DESC') {
		
		$ads = NULL;
		$count = NULL;
		
		if ($ads['listing'] = runQuery("
			
			SELECT
				*
				
			FROM
				ads
				
			WHERE
				date_through >= CURDATE()
				
			ORDER BY
				date_from $sort, 
				sort_order ASC
				
		", 'specials')) {
			
			return $ads;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad By ID
	  *
	  * Selects the attributes of the ad with the specified ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to retrive.
	  *
	  * @return int $ad[0] An array of ad attributes
	  *
	  */
	function selectAdById ($ad_id) {
		
		$id = vbool ($ad_id);
		
		if ($ad = runQuery ("
			
			SELECT
				*
				
			FROM
				ads
				
			WHERE
				id = $id
				
		")) {
			
			return $ad[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad By Date
	  *
	  * Selects the attributes of the ad with the specified date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the ad to retrive.
	  *
	  * @return int $ad[0] An array of ad attributes
	  *
	  */
	function selectAdByDateAndStoreChain ($ad_date, $store_chain) {
		
		if ($ad = runQuery ("
			
			SELECT
				*
				
			FROM
				ads
				
			WHERE
				date_from = $ad_date AND
				store_chain = $store_chain
				
		", 'specials')) {
			
			return $ad[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Dates
	  *
	  * Selects ad_dates from the Ads table and groups result by ad dates
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Takes page number from Ads class call
	  * @param int $limit Number of results to return
	  *
	  * @return array $ads An array of selected the records
	  *
	  */
	function selectAdDates ($offset, $limit) {
		
		$ad_dates = NULL;
		$count = NULL;
		
		if ($ad_dates['listing'] = runQuery("
			
			SELECT
				date_from,
				date_through
				
			FROM
				ads
				
			GROUP BY
				date_from,
				date_through
				
			ORDER BY
				date_from DESC
				
			LIMIT
				$offset, $limit
				
		", 'specials')) {
			
			$count = runQuery("
				
				SELECT
					COUNT(id)
					
				FROM
					ads
					
				GROUP BY
					date_from
					
			", 'specials');
			
			$ad_dates['count'] = $count[0];
			
			return $ad_dates;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert Ad
	  *
	  * Inserts a new row into the ads table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of ad data.
	  *
	  * @return int $ad['insert_id'] The mysql_insert_id of the record
	  *
	  */
	function insertAd($data) {
		
		if ($ad = runQuery("
			
			INSERT INTO
				ads
				
			SET
				store_chain						= {$data['store_chain']},
				date_from 						= {$data['date_from']},
				date_through 					= {$data['date_through']},
				publish 						= {$data['publish']},
				truckload 						= {$data['truckload']},
				truckload_dates 				= {$data['truckload_dates']},
				ocean_feast 					= {$data['ocean_feast']},
				ocean_feast_dates				= {$data['ocean_feast_dates']},
				secret_special_title 			= {$data['secret_special_title']},
				secret_special_container		= {$data['secret_special_container']},
				secret_special_description 		= {$data['secret_special_description']},
				secret_special_image 			= {$data['secret_special_image']},
				secret_special_disclaimers 		= {$data['secret_special_disclaimers']},
				secret_special_limit 			= {$data['secret_special_limit']},
				secret_special_skus 			= {$data['secret_special_skus']},
				secret_special_retail 			= {$data['secret_special_retail']},
				secret_special_regular_retail 	= {$data['secret_special_regular_retail']},
				secret_special_from 			= {$data['secret_special_from']},
				secret_special_through 			= {$data['secret_special_through']},
				article_ids 					= {$data['article_ids']},
				recipe_ids 						= {$data['recipe_ids']},
				sort_order						= {$data['sort_order']}
				
		")) {
			
			return $ad['insert_id'];
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Update Ad
	  *
	  * Updates a record in the ads table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of ad data.
	  *
	  * @return array $result The id of the ad record that was updated
	  *
	  */
	function updateAd($data) {
		
		if ($ad = runQuery("
			
			UPDATE
				ads
				
			SET
				store_chain						= {$data['store_chain']},
				date_from 						= {$data['date_from']},
				date_through 					= {$data['date_through']},
				publish 						= {$data['publish']},
				truckload 						= {$data['truckload']},
				truckload_dates 				= {$data['truckload_dates']},
				ocean_feast 					= {$data['ocean_feast']},
				ocean_feast_dates				= {$data['ocean_feast_dates']},
				secret_special_title 			= {$data['secret_special_title']},
				secret_special_container		= {$data['secret_special_container']},
				secret_special_description 		= {$data['secret_special_description']},
				secret_special_image 			= {$data['secret_special_image']},
				secret_special_disclaimers 		= {$data['secret_special_disclaimers']},
				secret_special_limit 			= {$data['secret_special_limit']},
				secret_special_skus 			= {$data['secret_special_skus']},
				secret_special_retail 			= {$data['secret_special_retail']},
				secret_special_regular_retail 	= {$data['secret_special_regular_retail']},
				secret_special_from 			= {$data['secret_special_from']},
				secret_special_through 			= {$data['secret_special_through']},
				prepended_message				= {$data['prepended_message']}, 
				prepended_message_position		= {$data['prepended_message_position']},
				article_ids 					= {$data['article_ids']},
				recipe_ids 						= {$data['recipe_ids']}
				
			WHERE
				id = {$data['ad_id']}
				
		")) {
			
			$result = array('ad_id' => $data['ad_id']);
			
			return $result;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Update Ad Campaign ID
	  *
	  * Updates the campaign_id of the specied ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The id of the ad to updated
	  * @param string $campaign_id The campaign_id to insert
	  * @param string $campaign_status The status of the campaign
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdCampaignId ($ad_id, $campaign_id, $campaign_status = 'NULL') {
		
		if ($update = runQuery("
			
			UPDATE
				ads
				
			SET
				campaign_id = $campaign_id,
				campaign_status = $campaign_status
				
			WHERE
				id = $ad_id
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Update Ad Publish Status
	  *
	  * Updates the publish flag of an ad entry
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The id of the ad to updated
	  * @param string $status The publish status to set the ad to
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdPublishStatus ($ad_id, $status) {
		
		if ($update = runQuery("
			
			UPDATE
				ads
				
			SET
				publish = $status
				
			WHERE
				id = $ad_id
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	

	/**
	  * Select Ad Items By Department
	  *
	  * Selects all of the items from a department on the given ad date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the ad.
	  * @param string $department The department of the items to retrieve.
	  *
	  * @return array $items An array of items and atributes
	  *
	  */
	function selectAdItemsByDepartment ($ad_id, $department = 'staples') {
		
		$id = vbool($ad_id);
		$department = vempty($department);
		
		if ($items = runQuery("
			
			SELECT
				id AS `ad_item_id`,
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix,
				note,
				description,
				pack,
				size,
				unit,
				container,
				`limit`,
				supplies_last,
				lifestyle,
				`labels`,
				sale_price,
				retail_price,
				override_price,
				crv,
				department,
				on_ad_date,
				feature, 
				feature_config, 
				sort,
				front_page_item,
				enabled,
				allow_override, 
				upc_image_active,
				media_links,
				feature_image,
				status
				
			FROM
				instances
				
			WHERE
				ad_id = $id AND
				department = $department AND
				enabled = 1
				
			ORDER BY
				-sort DESC
				
		")) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Select Featured Ad Items
	  *
	  * Selects all of the featured items on the given ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The date of the ad.
	  *
	  * @return array $items An array of items and atributes
	  *
	  */
	function selectFeaturedAdItems ($ad_id) {
		
		$id = vbool($ad_id);
		
		if ($items = runQuery("
			
			SELECT
				id AS `ad_item_id`,
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix,
				note,
				description,
				pack,
				size,
				unit,
				container,
				`limit`,
				supplies_last,
				lifestyle,
				labels,
				sale_price,
				retail_price,
				override_price,
				crv,
				department,
				on_ad_date,
				feature,
				feature_config, 
				front_page_item,
				enabled,
				allow_override,
				upc_image_active, 
				media_links,
				feature_image,
				status
				
			FROM
				instances
				
			WHERE
				ad_id = $id AND
				feature > 0 AND
				enabled = 1
				
			ORDER BY
				feature ASC
				
		")) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Featured Ad Items
	  *
	  * Selects all of the featured items on the given ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The date of the ad.
	  *
	  * @return array $items An array of items and atributes
	  *
	  */
	function selectFrontPageAdItems ($ad_id) {
		
		$id = vbool($ad_id);
		
		if ($items = runQuery("
			
			SELECT
				id AS `ad_item_id`,
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix,
				note,
				description,
				pack,
				size,
				unit,
				container,
				`limit`,
				supplies_last,
				lifestyle,
				labels,
				sale_price,
				retail_price,
				override_price,
				crv,
				department,
				on_ad_date,
				feature,
				feature_config, 
				front_page_item,
				enabled,
				allow_override, 
				upc_image_active, 
				media_links,
				feature_image,
				status
				
			FROM
				instances
				
			WHERE
				ad_id = $id AND
				front_page_item = 1 AND
				enabled = 1
				
			ORDER BY
				feature ASC
				
		")) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Disabled Ad Items
	  *
	  * Selects all of the disabled items on the given ad date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the ad.
	  *
	  * @return array $items An array of items and atributes
	  *
	  */
	function selectDisabledAdItems ($ad_date = '2015-09-16') {
		
		$date= vempty($ad_date,0,0);
		
		if ($items = runQuery("
			
			SELECT
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix
				
			FROM
				instances
				
			WHERE
				on_ad_date = $date AND
				enabled = 0
				
			ORDER BY
				family_group ASC
				
		")) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Weekly Ad Items
	  *
	  * Slects all the enabled records from the instances table.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date The date of the ad items to pull.
	  *
	  * @return array $items An array of ad items and their attributes
	  *
	  */
	function selectWeeklyAdItems ($date = '2015-09-16') {
		
		$date= vempty($date,0,0);
		
		if ($items = runQuery("
			
			SELECT
				id AS `ad_item_id`,
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix,
				note,
				description,
				pack,
				size,
				unit,
				container,
				`limit`,
				supplies_last,
				lifestyle,
				labels,
				sale_price,
				retail_price,
				override_price,
				crv,
				department,
				on_ad_date,
				feature, 
				feature_config, 
				front_page_item,
				enabled,
				allow_override, 
				upc_image_active, 
				media_links,
				feature_image,
				status
				
			FROM
				instances
				
			WHERE
				on_ad_date = $date AND
				enabled = 1
				
			ORDER BY
				family_group ASC
				
		")) {
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	function getWeeklyAdItem ($family, $date) {
		
		// Validate input
		$family_group = vempty($family,0,0);
		$date = vempty($date,0,0);
		
		// Check for manual grouping of items
		if ($group = runQuery("
			
			SELECT
				`group`
				
			FROM
				ad_item_instances
				
			WHERE
				family_group = $family_group
				AND CHAR_LENGTH(`group_name`) > 1
				AND on_ad_date = $date
				
			LIMIT 1
			
		")) {
			
			// Pull items in manual group
			$manual_group = vempty($group[0],0,0);
			$where = "`group_name` = $manual_group";
			
		} else {
			
			// Pull items in family group
			$where = "family_group = $family_group";
			
		}
		
		// Query items in group
		if ($skus = runQuery("
			
			SELECT
				ad_item_instances.id AS `ad_item_id`,
				upc,
				family_group,
				pos_name,
				prefix,
				display_name,
				group_name,
				suffix,
				note,
				description,
				pack,
				size,
				unit,
				container,
				`limit`,
				supplies_last,
				lifestyle,
				labels,
				sale_price,
				retail_price,
				override_price,
				crv,
				department,
				on_ad_date,
				feature,
				front_page_item,
				enabled,
				allow_override,
				feature_image
				
			FROM
				ad_item_instances
				
			WHERE
				$where AND
				on_ad_date = $date AND
				enabled = 1
				
			ORDER BY
				family_group ASC
				
		")) {
			
			// Return results
			return $skus;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Items
	  *
	  * Selects a family group of items from the products table and groups by family_group.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int string $family_group The family group name.
	  *
	  * @return array $item[0] An array of attributes of the grouped item.
	  *
	  */
	function selectAdItems ($family_group) {
		
		// Select item details
		if ($item = runQuery("
			
			SELECT
				*
				
			FROM
				products
				
			WHERE
				family_group = $family_group
				
		")) {
			
			// Return results
			return $item[0];
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Item Instance
	  *
	  * Selects a family group of items on a specifiec ad from the database and groups by family_group.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to retrieve.
	  * @param int string $family_group The family group name.
	  *
	  * @return array $item[0] An array of attributes of the grouped item.
	  *
	  */
	function selectAdItemInstance ($ad_id, $family_group) {
		
		// Select item details
		if ($item = runQuery("
			
			SELECT
				*
				
			FROM
				instances
				
			WHERE
				ad_id = $ad_id AND
				family_group = $family_group
				
			LIMIT 1
			
		")) {
			
			// Return results
			return $item[0];
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Select Item UPCs
	  *
	  * Selects all the UPCs in a given family group from the products table.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int string $family_group The family group name.
	  *
	  * @return array $upcs An array of UPCs and their attributes.
	  *
	  */
	function selectItemUPCs ($family_group) {
		
		// Select item details
		if ($upcs = runQuery("
			
			SELECT
				upc,
				pos_name,
				retail_price,
				size,
				unit,
				container, 
				upc_image_active
				
			FROM
				products
				
			WHERE
				family_group = $family_group
				
		")) {
			
			// Return results
			return $upcs;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}

	/**
	  * Select Item Group UPCs
	  *
	  * Selects all the UPCs in a given family group on a specifiec ad from the database
	  * IMPORTANT: Selection is limted to 100 as it causes the update query to time out
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to retrieve.
	  * @param int string $family_group The family group name.
	  *
	  * @return array $upcs An array of UPCs and their attributes.
	  *
	  */
	function selectItemGroupUPCs ($ad_id, $family_group) {
		
		// Select item details
		if ($upcs = runQuery("
			
			SELECT
				id,
				upc,
				pos_name,
				enabled,
				retail_price,
				size,
				unit,
				container, 
				upc_image_active
				
			FROM
				instances
				
			WHERE
				ad_id = $ad_id AND
				family_group = $family_group
				
			LIMIT
				100
				
		")) {
			
			// Return results
			return $upcs;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}

	/**
	  * Select Item On Upcoming Ads
	  *
	  * Selects the upcoming ads for a given item
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $ads An array of SKUs.
	  *
	  */
	function selectItemOnUpcomingAds ($family_group) {
		
		if ($ads = runQuery("
			
			SELECT
				ads.id,
				ads.date_from,
				ads.store_chain,
				ads.publish,
				
				instances.id AS `instance_id`,
				instances.family_group,
				instances.pos_name,
				instances.display_name,
				instances.group_name,
				instances.retail_price,
				instances.sale_price
				
			FROM
				instances
				
			LEFT JOIN
				ads ON ads.id = instances.ad_id
				
			WHERE
				instances.family_group = $family_group AND (
					ads.date_from > NOW() OR
					ads.date_from > DATE_SUB(NOW(), INTERVAL 1 WEEK)
				)
				
		")) {
			
			// Return results
			return $ads;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Update Products by Family Group
	  *
	  * Updates products with the specified family group.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateProductsByFamilyGroup ($item) {
		
		// Update shared data for all SKUs in group
		$updateProducts = runQuery("
			
			UPDATE
				products
				
			SET
				prefix 			= {$item['prefix']},
				display_name 	= {$item['display_name']},
				suffix 			= {$item['suffix']},
				note 			= {$item['note']},
				description 	= {$item['description']},
				labels 			= {$item['labels']},
				`limit` 		= {$item['limit']},
				supplies_last	= {$item['supplies_last']},
				crv				= {$item['crv']},
				media_links 	= {$item['media_links']},
				feature_image 	= {$item['feature_image']}
				
			WHERE
				family_group 	= {$item['family_group']}
				
		");
		
		// Update unique data for all SKUs in group
		foreach ($item['retails'] as $key => $value) {
			
			// Check for instance ID
			if (strlen($key) == 23) {
				
				// Strip off zone prefix and ad date suffix and validate
				$item_id = (string)vempty(substr($key, 2, -8),0,0);
				
			} else {
				
				// Validate UPC
				$item_id = (string)vempty($key,0,0);
			}
			
			if ($item['upc_images_active'][$key] != 1) {
				$item['upc_images_active'][$key] = 0;
			}
			
			$updateSKU = runQuery("
				
				UPDATE
					products
					
				SET
					retail_price 		= {$item['retails'][$key]},
					size 				= {$item['sizes'][$key]},
					unit 				= {$item['units'][$key]},
					container 			= {$item['containers'][$key]}, 
					upc_image_active 	= {$item['upc_images_active'][$key]}
					
				WHERE
					upc = $item_id
					
			");
			
		}
		
		if ($updateProducts) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Update Ad Item Instances
	  *
	  * Updates the instances of family group items in a given ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdItemInstances ($item) {
		
		// Update shared data for all SKUs in group
		$updateGlobal = runQuery("
			
			UPDATE
				instances
				
			SET
				prefix 			= {$item['prefix']},
				display_name 	= {$item['display_name']},
				group_name 		= {$item['group_name']},
				suffix 			= {$item['suffix']},
				note 			= {$item['note']},
				description 	= {$item['description']},
				labels 			= {$item['labels']},
				sale_price 		= {$item['sale_price']},
				override_price 	= {$item['override_price']},
				`limit` 		= {$item['limit']},
				supplies_last 	= {$item['supplies_last']},
				crv 			= {$item['crv']},
				feature 		= {$item['feature']},
				feature_config  = {$item['feature_config']},
				front_page_item = {$item['front_page_item']},
				sort 			= {$item['sort']},
				media_links 	= {$item['media_links']},
				feature_image 	= {$item['feature_image']},
				status 			= {$item['status']}
				
			WHERE
				ad_id 			= {$item['ad_id']} AND
				family_group 	= {$item['family_group']}
				
		");
		
		
		// Update unique data for all SKUs in group
		foreach ($item['retails'] as $key => $value) {
			
			$item_id = (string)vempty($key,0,0);
			
			if ($item['enable'][$key] != 1) {
				$item['enable'][$key] = 0;
			}
			
			if ($item['upc_images_active'][$key] != 1) {
				$item['upc_images_active'][$key] = 0;
			}
			
			$updateSKU = runQuery("
				
				UPDATE
					instances
					
				SET
					retail_price 		= {$item['retails'][$key]},
					size 				= {$item['sizes'][$key]},
					unit 				= {$item['units'][$key]},
					container 			= {$item['containers'][$key]},
					enabled 			= {$item['enable'][$key]}, 
					upc_image_active 	= {$item['upc_images_active'][$key]}
					
				WHERE
					id = $item_id
					
			");
			
		}
		
		if ($updateGlobal) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}

	/**
	  * Update Ad Item Group Name
	  *
	  * Updates the family group value of an item instance
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateAdItemGroupName ($item) {
		
		// Update shared data for all SKUs in group
		if ($updateGlobal = runQuery("
			
			UPDATE
				instances
				
			SET
				group_name = {$item['group_name']}
				
			WHERE
				ad_id = {$item['ad_id']} AND
				family_group = {$item['family_group']}
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Update Manual Group Name
	  *
	  * Updates the manual family group value of an item instance
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function updateManualGroupName ($item) {
		
		// Update shared data for all SKUs in group
		if ($update = runQuery("
			
			UPDATE
				instances
				
			SET 
				group_name = {$item['new_group_name']}
				
			WHERE
				ad_id = {$item['ad_id']} AND 
				group_name = {$item['current_group_name']}
				
		")) {
			
			return TRUE;
			
		} else {
		
			return FALSE;
			
		}
		
	}


	/**
	  * Select Disabled Ad Items By Ad
	  *
	  * Selects all manually disabled items on a given ad
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID the items appear on.
	  * @param string $date The date of the ad.
	  *
	  * @return array $items An array of product data and atributes
	  *
	  */
	function selectDisabledAdItemsByAd ($id) {
		
		if ($items = runQuery("
			
			SELECT
				*
				
			FROM
				instances
				
			WHERE
				ad_id = $id AND
				enabled = 0
				
			ORDER BY
				pos_name
				
		")) {
			
			return $items;
			
		} else {
		
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Item Labels
	  *
	  * Selects all ad item labels from the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return array $labels An array of labels and their attributes
	  *
	  */
	function selectAdItemLabels () {
		
		if ($labels = runQuery ("
			
			SELECT
				*
				
			FROM
				labels
				
			ORDER BY
				type,
				name
				
		", 'nugget')) {
			
			return $labels;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Sister Ads
	  *
	  * Selects ads from database within a 3 day range
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date_1 First date to check
	  * @param string $date_2 Second date to check
	  * @param string $date_3 Third date to check
	  *
	  * @return array $sister_ads multidimensional array of ads and their attributes
	  *
	  */
	function selectSisterAds ($date_1, $date_2, $date_3) {
		
		if ($sister_ads = runQuery("
			
			SELECT
				*
				
			FROM
				ads
				
			WHERE
				date_from = $date_1 OR
				date_from = $date_2 OR
				date_from = $date_3
				
			ORDER BY
				sort_order
				
		")) {
			
			return $sister_ads;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Item Messages By Family Group
	  *
	  * Selects proofing messages attached to ad items by ad_id and family_group
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to match
	  * @param string $family_group The family group to match
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function selectAdItemMessagesByFamilyGroup ($ad_id, $family_group) {
		
		if ($messages = runQuery("
			
			SELECT
				*
				
			FROM
				messages
				
			WHERE
				ad_id = $ad_id AND
				family_group = $family_group
				
			ORDER BY
				date_posted ASC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Item Messages By Group Name
	  *
	  * Selects proofing messages attached to ad items by ad_id and group_name
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id The ID of the ad to match
	  * @param string $group_name The manual group to match
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function selectAdItemMessagesByGroupName ($ad_id, $group_name) {
		
		if ($messages = runQuery("
			
			SELECT
				*
				
			FROM
				messages
				
			WHERE
				ad_id = $ad_id AND
				group_name = $group_name
				
			ORDER BY
				date_posted DESC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Ad Item Messages By Item Hash
	  *
	  * Selects proofing messages attached to ad items by its ad_id and MD5 hashed family_uri
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_hash The manual group to match
	  *
	  * @return array $messages multidimensional array of messages and attibutes
	  *
	  */
	function selectAdItemMessagesByItemHash ($item_hash) {
		
		if ($messages = runQuery("
			
			SELECT
				*
				
			FROM
				messages
				
			WHERE
				item_id = $item_hash
				
			ORDER BY
				date_posted DESC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	 * Update message group name by ID
	 *
	 * Updates the group name of messages that match the item id
	 *
	 * @author Mark Eagleton <mark@thebigreason.com>
	 *
	 * @param array $item An array of item data in the following format:
	 *		$item['group_name']
	 *		$item['item_id']
	 *
	 * @return boolean True if updated, False if not updated
	 *
	 */
	function updateMessageGroupNameById ($item) {
		
		if ($update = runQuery("
			
			UPDATE
				messages
				
			SET
				group_name = {$item['group_name']}
				
			WHERE
				item_id = {$item['item_id']}
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	 * Update ad item instances status by family group
	 *
	 * Updates the status of the specified item instances to the specified value
	 *
	 * @author Mark Eagleton <mark@thebigreason.com>
	 *
	 * @param int $ad_id The ID of the Ad the items appear on
	 * @param string $status The status to set the items to
	 * @param string $family_group The family group the items are under
	 *
	 * @return boolean True if updated, False if not updated
	 *
	 */
	function updateAdItemInstancesStatusByFamilyGroup ($ad_id, $status, $family_group) {
		
		if ($update = runQuery("
			
			UPDATE
				instances
				
			SET
				status = $status
				
			WHERE
				ad_id = $ad_id AND
				family_group = $family_group
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	 * Update ad item instances status by group name
	 *
	 * Updates the status of the specified item instances to the specified value
	 *
	 * @author Mark Eagleton <mark@thebigreason.com>
	 *
	 * @param int $ad_id The ID of the Ad the items appear on
	 * @param string $status The status to set the items to
	 * @param string $group_name The group name the items are under
	 *
	 * @return boolean True if updated, False if not updated
	 *
	 */
	function updateAdItemInstancesStatusByGroupName ($ad_id, $status, $group_name) {
		
		if ($update = runQuery("
			
			UPDATE
				instances
				
			SET
				status = $status
				
			WHERE
				ad_id = $ad_id AND
				group_name = $group_name
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * insertAdItemMessage
	  *
	  * Inserts an ad item message into the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of message attributes
	  *
	  * @return boolean $insert The insert ID of the message or false
	  *
	  */
	function insertAdItemMessage ($item) {
		
		if ($insert = runQuery("
			
			INSERT INTO
				messages
				
			SET
				ad_id 			= {$item['ad_id']},
				department 		= {$item['department']},
				item_name		= {$item['item_name']},
				item_id 		= {$item['item_id']},
				family_group 	= {$item['family_group']},
				group_name 		= {$item['group_name']},
				message 		= {$item['message']},
				posted_by 		= {$item['posted_by']},
				posted_email 	= {$item['posted_email']}
				
		")) {
			
			return $insert;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Current Messages
	  *
	  * Selects messages in the system for any currently running ads
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $start_date The start date of the ads to limit results to
	  *
	  * @return array $messages A multi dimensional array of message data
	  *
	  */
	function selectCurrentMessages ($start_date) {
		
		if ($messages = runQuery("
			
			SELECT
				messages.*,
				
				instances.status,
				instances.on_ad_date AS `date_from`,
				instances.off_ad_date AS `date_through`,
				
				ads.store_chain
				
			FROM
				messages
				
			INNER JOIN
				ads on ads.id = messages.ad_id
				
			LEFT JOIN
				instances on instances.ad_id = messages.ad_id AND (
					instances.family_group = messages.family_group OR
					instances.group_name = messages.group_name OR
					instances.pos_name = messages.item_name
				)
				
			WHERE
				instances.on_ad_date >= $start_date
				
			GROUP BY
				messages.id
				
			ORDER BY
				instances.on_ad_date ASC,
				messages.item_id,
				messages.date_posted ASC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
			
		}
	}


	/**
	  * selectMessageThreadsByEmail
	  *
	  * Selects message item ids with the specified email address and groups them by item ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $email The email address to select for
	  * @param string $start_date The start date of the ads to limit results to
	  *
	  * @return array $threads An array of item ids that will be used to find message threads
	  *
	  */
	function selectMessageThreadsByEmail ($email, $start_date) {
		
		if ($threads = runQuery("
			
			SELECT
				messages.item_id
				
			FROM
				messages
				
			INNER JOIN
				ads on ads.id = messages.ad_id
				
			WHERE
				ads.date_from >= $start_date AND
				messages.posted_email = $email
				
			GROUP BY
				messages.item_id
				
				ORDER BY
				messages.date_posted DESC
				
		")) {
			
			return $threads;
			
		} else {
			
			return FALSE;
		}
		
	}


	/**
	  * Select Current Messages By Email
	  *
	  * Selects messages in the system with the specified email address
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $email The email address to search for
	  * @param string $start_date The start date of the ads to limit results to
	  *
	  * @return array $messages A multi dimensional array of message data
	  *
	  */
	function selectCurrentMessagesByEmail ($email, $start_date) {
		
		if ($messages = runQuery("
			
			SELECT
				messages.*,
				
				instances.status,
				
				ads.date_from,
				ads.date_through,
				ads.store_chain
				
			FROM
				messages
				
			INNER JOIN
				ads on ads.id = messages.ad_id
				
			LEFT JOIN
				instances on instances.ad_id = messages.ad_id AND (
					instances.family_group = messages.family_group OR
					instances.group_name = messages.group_name OR
					instances.pos_name = messages.item_name
				)
				
			WHERE
				ads.date_from >= $start_date AND
				messages.posted_email = $email
				
			ORDER BY
				ads.date_from ASC,
				messages.item_id,
				messages.date_posted ASC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
		}
		
	}


	/**
	  * Select Message Thread By item ID
	  *
	  * Selects messages in the system with the specified item ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_id The item ID to search for
	  *
	  * @return array $messages A multi dimensional array of message data
	  *
	  */
	function selectMessageThreadByItemId ($item_id) {
		
		if ($messages = runQuery("
			
			SELECT
				messages.*,
				
				instances.status,
				
				ads.date_from,
				ads.date_through,
				ads.store_chain
				
			FROM
				messages
				
			INNER JOIN
				ads on ads.id = messages.ad_id
				
			LEFT JOIN
				instances on instances.ad_id = messages.ad_id AND (
					instances.family_group = messages.family_group OR
					instances.group_name = messages.group_name OR
					instances.pos_name = messages.item_name
				)
				
			WHERE
				messages.item_id = $item_id
				
			GROUP BY
					messages.id
					
			ORDER BY
				messages.date_posted ASC
				
		")) {
			
			return $messages;
			
		} else {
			
			return FALSE;
		}
		
	}

	/**
	  * Select Message By Id
	  *
	  * Selects the message by the ID given
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $message_id The ID of the message to return
	  *
	  * @return array $message An array of message data
	  *
	  */
	function selectMessageById ($message_id) {
	
		if ($message = runQuery("
			
			SELECT
				messages.*,
				
				ads.date_from,
				ads.date_through,
				ads.store_chain
				
			FROM
				messages
				
			INNER JOIN
				ads on ads.id = messages.ad_id
				
			WHERE
				messages.id = $message_id
				
		")) {
			
			return $message[0];
			
		} else {
			
			return FALSE;
		}
		
	}


	/**
	  * Disable Ad Item Instances
	  *
	  * Sets enabled flag to 0 for item instances of the given and and family group.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $item An array of item attributes.
	  *		This array must take the following form:
      * 	array(
      *			'family_group' => 'the family group string',
	  *         'ad_id' => the ad ID
	  		)
	  *
	  * @return TRUE|FALSE
	  *
	  */
	function disableAdItemInstances ($item) {
		
		if ($message = runQuery("
			
			UPDATE
				instances
				
			SET
				enabled = 0
				
			WHERE
				ad_id = {$item['ad_id']} AND
				family_group = {$item['family_group']}
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}
