<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'LabelsController.php';


	// Declare vars
	$labels = NULL;
	$label = NULL;


	// Set page attrs
	$page_attrs['title'] = 'Manage Labels';
	$page_attrs['description'] = 'Manage labels.';
	$page_attrs['class'] = 'labels';


	// Handle form post
	if (isset($_POST['save']) || isset($_POST['add'])) {
		
		// Require and validate description (labeled as name on the front end)
		if (!$label['description'] = vempty($_POST['description'],1,1)) {
			$error[] = 'A name is required';
		}
		
		// Set name to description and convert to slug format
		$label['name'] = vslug($_POST['description']);
		
		// Require an image
		if (empty($_FILES['new_file']['name']) && empty($_POST['current_file'])) {
			$error[] = 'An image file is required';
		}
		
		$label['current_file'] = vempty($_POST['current_file'],0,0);
		$label['long_description'] = vempty($_POST['long_description'],0,1);
		$label['type'] = vempty($_POST['type'],0,0);
		
		// Set up seasoning prefix for file name
		if ($_POST['type'] == 'seasoning') {
			$prefix = 'seasoning-';
		}
		
		// Build a file name variable out of the modified description
		$file_name =  $prefix . unescape($label['name']) . '.png';
		
		if (!$error) {

			if (isset($_POST['save'])) {

				$label['label_id'] = vbool($_POST['item_id']);
				
				// Compare current file name with new name and rename if different
				$current_file = $_POST['current_file'];
				
				if ($file_name != $current_file) {
					rename("../httpdocs/img/labels/$current_file","../httpdocs/img/labels/$file_name");
				}
				
				// Handle new file upload if one is set
				if (!empty($_FILES['new_file']['name'])) {
					fileReplace($_FILES['new_file'], '../httpdocs/img/labels/' . $file_name);
				}

				// Update article
				updateLabel($label);

				// Set message and redirect
				$_SESSION['confirm'] = 'Label updated.';

			} else if (isset($_POST['add'])) {
				
				fileNoSubdirectoryUpload($_FILES['new_file'], '../httpdocs/img/labels/' . $file_name);

				// Insert article
				$result = insertLabel($label);

				// Capture and set user_id for redirect
				$label['label_id'] = $result['insert_id'];

				// Set message and redirect
				$_SESSION['confirm'] = 'Label created.';

			}

			header("Location: /labels/edit/{$label['label_id']}/");
			exit();

		}
		
	// Edit item
	} else if (!empty($_GET['action']) && $_GET['action'] == 'edit' && !empty($_GET['id'])) {
		
		// Redirect if not admin
		if ($_SESSION['user_level'] > 1) {

			$_SESSION['error'] = 'You don&rsquo;t have access to create retractions.';
			header("Location: /retractions/");
			exit();

		}
		
		$label = getLabel($_GET['id']);
		
	// Set up for new item form
	} else if (!empty($_GET['action']) && $_GET['action'] == 'new') {

		// Redirect if not admin
		if ($_SESSION['user_level'] > 1) {

			$_SESSION['error'] = 'You don&rsquo;t have access to create retractions.';
			header("Location: /retractions/");
			exit();

		}

		$label = TRUE;


	// List retractions
	} else {

		// Get current page number
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}

		// Get the ads
		$labels = getLabels($page_attrs['page_no'], 54);

	}
