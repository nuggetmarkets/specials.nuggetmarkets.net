<?php

	// Include dependencies
	require_once 'LabelsModel.php';


	/**
	  * Gets Labels
	  *
	  * Gets paginated list of labels
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit How many results to return.
	  *
	  * @return array $items An array of retraction objects
	  *
	*/
	function getLabels ($page = 1, $limit = 25) {
		
		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}
		
		$page = vbool($page);
		$limit = vbool($limit);
		
		// Calculate page offset
		$offset = $page * $limit - ($limit);
		
		if ($labels = selectLabels ($offset, $limit)) {
			
			// Loop through results and create set image file name
			foreach ($labels['listing'] AS $key => $val) {
				
				// Prepend seasoning file name with type
				if ($val['type'] == 'seasoning') {
					$labels['listing'][$key]['image'] = $val['type'] . '-' . $val['name'] . '.png';
				} else {
					$labels['listing'][$key]['image'] = $val['name'] . '.png';
				}
				
			}
			
			$labels['pagination'] = pagination($page, $limit, $labels['count']);
			
			return $labels;
			
		} else {
			
			$labels = FALSE;
			
		}
		
	}

	/**
	  * Gets Label
	  *
	  * Gets as specific label
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit How many results to return.
	  *
	  * @return array $items An array of retraction objects
	  *
	  */
	function getLabel ($label_id) {
		
		// Validate retraction id
		$id = vbool($label_id);
		
		if ($label = selectLabelById ($id)) {
			
			if ($label['type'] == 'seasoning') {
				$label['image'] = $label['type'] . '-' . $label['name'] . '.png';
			} else {
				$label['image'] = $label['name'] . '.png';
			}
			
			return $label;
			
		} else {
			
			$label = FALSE;
			
		}
		
	}


	/**
	  * Get Specified Labels
	  *
	  * Selects the labels from the ads specified in a comma delimeted list.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param mixed $label_ids A comma delimeted list of label IDs or array of label IDs
	  *
	  * @return array $labels An array of labels and attributes
	  *
	  */
	function getLabelsWithIds ($label_ids) {
		
		// Set up output array
		$labels = array();
		
		// Check for array
		if (is_array($label_ids)) {
			
			$items = $label_ids;
			
		} else {
			
			// Explode the list of label IDs
			$items = explode(',', $label_ids);	
			
		}
		
		// Loop through the array and build the WHERE clause for the query
		$i = 0;
		foreach ($items as $item) {
			
			if ($i == 0) {
				
				$where = " id = $item ";
				
			} else {
				
				$where .= " OR id = $item ";
				
			}
			
			$i++;
		}
		
		// Select the labels
		$results = selectLabelsWithIds($where);
		
		if (is_array($results)) {
			
			// Loop through results
			$r = 0;
			foreach ($results as $result) {
				
				$labels[$r] = $result;
				
				// Prepend seasoning file name with type
				if ($result['type'] == 'seasoning') {
					$labels[$r]['image'] = $result['type'] . '-' . $result['name'] . '.png';
				} else {
					$labels[$r]['image'] = $result['name'] . '.png';
				}
				
				$r++;
			}
			
			
			// Return results
			return $labels;
			
		} else {
			
			// Return FALSE
			return FALSE;
			
		}
		
	}
