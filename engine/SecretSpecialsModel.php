<?php


	/**
	  * Select Secret Special by Ad Date
	  *
	  * Selects secret special data from the ads table based on the ad start date provided
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The start date of the ad in YYYY-MM-DD format
	  *
	  * @return array $secret_special_zones A multidimensional array of secret special data for all zones on the given date
	  *
	  */
	function selectSecretSpecialZonesByAdDate ($ad_date) {
		
		$secret_special_zones = array();
		
		if ($secret_special_zones = runQuery("
			
			SELECT
				id AS `ad_id`,
				store_chain,
				secret_special_title,
				secret_special_container,
				secret_special_description,
				secret_special_image,
				secret_special_disclaimers,
				secret_special_limit,
				secret_special_skus,
				secret_special_retail,
				secret_special_regular_retail,
				secret_special_from,
				secret_special_through, 
				prepended_message, 
				prepended_message_position, 
				campaign_status,
				campaign_id,
				article_ids,
				recipe_ids,
				truckload,
				truckload_dates,
				ocean_feast,
				ocean_feast_dates,
				publish
				
			FROM
				ads
				
			WHERE
				date_from = $ad_date
				
			ORDER BY
				sort_order
				
		")) {
			
			return $secret_special_zones;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update Secret Specials
	  *
	  * Updates the secret specials data for the specified record in the ads table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of secret special data
	  *
	  * @return boolean TRUE|FALSE $update True if successful, FALSE if not
	  *
	  */
	function updateSecretSpecials ($data) {
		
		if ($update = runQuery("
			
			UPDATE
				ads
				
			SET
				secret_special_title 			= {$data['secret_special_title']},
				secret_special_container		= {$data['secret_special_container']},
				secret_special_description  	= {$data['secret_special_description']},
				secret_special_image			= {$data['secret_special_image']},
				secret_special_disclaimers		= {$data['secret_special_disclaimers']},
				secret_special_limit			= {$data['secret_special_limit']},
				secret_special_skus				= {$data['secret_special_skus']},
				secret_special_retail			= {$data['secret_special_retail']},
				secret_special_regular_retail	= {$data['secret_special_regular_retail']},
				secret_special_from				= {$data['secret_special_from']},
				secret_special_through			= {$data['secret_special_through']}, 
				prepended_message				= {$data['prepended_message']}, 
				prepended_message_position		= {$data['prepended_message_position']},
				article_ids						= {$data['article_ids']},
				recipe_ids						= {$data['recipe_ids']}
				
			WHERE
				id = {$data['ad_id']};
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Update Zoned Secret Specials
	  *
	  * Updates the portion of the secret special data that isn’t zoned on the regular secret special screen.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of secret special data
	  *
	  * @return boolean TRUE|FALSE $update True if successful, FALSE if not
	  *
	  */
	function updateZonedSecretSpecials ($data) {
		
		if ($update = runQuery("
			
			UPDATE
				ads
				
			SET
				secret_special_regular_retail	= {$data['secret_special_regular_retail']},
				secret_special_from				= {$data['secret_special_from']},
				secret_special_through			= {$data['secret_special_through']}, 
				prepended_message				= {$data['prepended_message']}, 
				prepended_message_position		= {$data['prepended_message_position']},
				article_ids						= {$data['article_ids']},
				recipe_ids						= {$data['recipe_ids']}
				
			WHERE
				id = {$data['ad_id']};
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update Secret Special Images
	  *
	  * Updates the secret_special_image row of the ad table where for all records containing the entry
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $image_path The path to the image file
	  *
	  * @return boolean TRUE|FALSE $update True if successful, FALSE if not
	  *
	  */
	function updateSecretSpecialImages ($image_path) {
		
		if ($update = runQuery("
			
			UPDATE
				ads
				
			SET
				secret_special_image = NULL
				
			WHERE
				secret_special_image = $image_path;
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
	}
