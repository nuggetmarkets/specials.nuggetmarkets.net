<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	require_once 'SecretSpecialsController.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';

	$page_attrs['title'] = 'Secret Specials';
	$page_attrs['description'] = 'Manage secret special emails.';
	$page_attrs['class'] = 'secret-specials';

	// Handle post
	if (isset($_POST['save'])) {
		
		$secret_special['date'] = $_POST['date'];
		
		// Validate input
		$secret_special['secret_special_from'] = vdate(
			$_POST['secret_special_from_year'],
			$_POST['secret_special_from_month'],
			$_POST['secret_special_from_day']
		);
		
		// This needs to vdatetime so date ranges calculate correctly
		$secret_special['secret_special_through'] = vdatetime(
			$_POST['secret_special_through_year'],
			$_POST['secret_special_through_month'],
			$_POST['secret_special_through_day'],
			'11',
			'59',
			'pm'
		);
		
		$secret_special['secret_special_title']			= vempty($_POST['secret_special_title'],0,1);
		$secret_special['secret_special_container']		= vempty($_POST['secret_special_container'],0,1);
		$secret_special['secret_special_description']	= vempty($_POST['secret_special_description']);
		$secret_special['secret_special_disclaimers']	= vempty($_POST['secret_special_disclaimers']);
		$secret_special['secret_special_limit']			= vempty($_POST['secret_special_limit'],0,1);
		$secret_special['secret_special_skus']			= vempty($_POST['secret_special_skus'],0,1);
		$secret_special['secret_special_retail']		= vempty($_POST['secret_special_retail'],0,1);
		$prepended_message								= vempty($_POST['prepended_message']);
		$secret_special['prepended_message_position']	= vbool($_POST['prepended_message_position']);

		// Only process images if the secret special isn’t zoned
		if (!isset($_POST['zoned'])) {
			
			// Handle secret special image upload
			if (!empty($_FILES['secret_special_image']['name'])) {
				
				// Delete old image first
				if (!empty($_POST['secret_special_current_image'])) {
					unlink(MEDIA_UPLOAD_DIR . $_POST['secret_special_current_image']);
				}
				
				// Create and upload new image
				$secret_special['secret_special_image'] = imgResize($_FILES['secret_special_image']['tmp_name'],2000, MEDIA_UPLOAD_DIR . $_FILES['secret_special_image']['name']);
				
			// Handle secret special current image
			} else if (!empty($_POST['secret_special_current_image'])) {
				
				$secret_special['secret_special_image'] = vempty($_POST['secret_special_current_image']);
				
			// Handle no secret special image
			} else {
				
				$secret_special['secret_special_image'] = 'NULL';
				
			}
			
		}
		
		// Get sister ads
		$sister_ads = getSisterAds($_POST['date']);
		
		// Loop through sister ads
		foreach ($sister_ads['ads'] as $ad) {
			
			// Validate zone id
			$secret_special['ad_id'] = vbool($ad['id']);
			
			// If the zone box is checked, update zone-specific details
			if (array_key_exists($ad['id'], $_POST['enable'])) {
				
				// Validate zone-specific info
				$secret_special['secret_special_regular_retail'] = vempty($_POST['retail'][$ad['id']]);
				
				if (strlen($_POST['prepended_zone_message'][$ad['id']]) >= 1 && $_POST['prepended_zone_message'][$ad['id']] != $_POST['prepended_message']) {
					$secret_special['prepended_message'] = vempty($_POST['prepended_zone_message'][$ad['id']]);
				} else {
					$secret_special['prepended_message'] = $prepended_message;
				}
				
				//  Check for appended articles
				if (is_array($_POST['article'][$ad['id']])) {
					$article_ids = implode(',', $_POST['article'][$ad['id']]);
					$secret_special['article_ids'] = vempty($article_ids);
				} else {
					$secret_special['article_ids'] = 'NULL';
				}
				
				//  Check for appended recipes
				if (is_array($_POST['recipe'][$ad['id']])) {
					$recipe_ids = implode(',', $_POST['recipe'][$ad['id']]);
					$secret_special['recipe_ids'] = vempty($recipe_ids);
				} else {
					$secret_special['recipe_ids'] = 'NULL';
				}
				
				unset($article_ids);
				unset($recipe_ids);
				
				// Update the ad record
				if (!isset($_POST['zoned'])) {
					$update = updateSecretSpecials($secret_special);
				} else {
					$update = updateZonedSecretSpecials($secret_special);
				}
				
			// Else set ALL secret special values to null
			} else {
				
				$null_secret_special = $secret_special;
				
				$null_secret_special['secret_special_title'] = 'NULL';
				$null_secret_special['secret_special_container'] = 'NULL';
				$null_secret_special['secret_special_description'] = 'NULL';
				$null_secret_special['secret_special_image'] = 'NULL';
				$null_secret_special['secret_special_disclaimers'] = 'NULL';
				$null_secret_special['secret_special_limit'] = 'NULL';
				$null_secret_special['secret_special_skus'] = 'NULL';
				$null_secret_special['secret_special_retail'] = 'NULL';
				$null_secret_special['secret_special_regular_retail'] = 'NULL';
				$null_secret_special['secret_special_from'] = 'NULL';
				$null_secret_special['secret_special_through'] = 'NULL';
				$null_secret_special['prepended_message'] = 'NULL';
				$null_secret_special['prepended_message_position'] = 'NULL';
				$null_secret_special['campaign_id'] = 'NULL';
				$null_secret_special['campaign_status'] = 'NULL';
				
				// Update the ad record
				$null_update = updateSecretSpecials($null_secret_special);
				
			}
			
		}
		
		// Set confirmation and redirect
		$_SESSION['confirm'] = 'Secret Special saved.';
		header("Location: /secret-specials/edit/{$secret_special['date']}/");
		exit();
		
	// Handle create draft 
	} else if (isset($_POST['create_draft'])) {
		
		$ad_id = vbool(key($_POST['create_draft']));
		
		$secret_special['ad_id'] 				= $ad_id;
		$secret_special['store_chain']			= $_POST['store_chain'][$ad_id];
		$secret_special['campaign_subject']		= $_POST['campaign_subject'][$ad_id];
		$secret_special['campaign_name']		= $_POST['campaign_name'][$ad_id];
		
		// Create draft
		$result = createDraftSecretSpecialEmail($secret_special);
		
		// Return response results
		if ($result['http_status_code'] < 300) {
			
			$_SESSION['confirm'] = 'A Secret Special draft email has been created.';
			
		// Return error
		} else {
			
			$_SESSION['error'] = 'The Secret Special draft email could not be created. ' . $result['message'];
			
		}
		
		// Redirect
		header("Location: /secret-specials/edit/{$_POST['date']}/");
		exit();
		
	// Handle schedule email
	} else if (isset($_POST['schedule_email'])) {
		
		$ad_id = vbool(key($_POST['schedule_email']));
		
		$secret_special['ad_id'] 				= $ad_id;
		$secret_special['campaign_id']			= $_POST['campaign_id'][$ad_id];
		$secret_special['email_send_date']		= $_POST['email_send_date'][$ad_id];
		
		// Schedule email
		$schedule = scheduleSecretSpecialEmail($secret_special);
		
		// Return response results
		if ($schedule['http_status_code'] < 300) {
			
			$_SESSION['confirm'] = 'The Secret Special email has been scheduled.';
			
		// Return error
		} else {
			
			$_SESSION['error'] = 'The Secret Special email could not be scheduled. ' . $schedule['message'];
			
		}
		
		// Redirect
		header("Location: /secret-specials/edit/{$_POST['date']}/");
		exit();
		
	// Handle delete draft
	} else if (isset($_POST['delete_draft'])) {
		
		$ad_id = vbool(key($_POST['delete_draft']));
		
		$secret_special['ad_id'] 		= $ad_id;
		$secret_special['campaign_id']	= $_POST['campaign_id'][$ad_id];
		
		// Delete draft
		$delete = deleteEmailDraft($secret_special);
		
		if ($delete['http_status_code'] < 300) {
			// Set confirmation message
			$_SESSION['confirm'] = "Email draft deleted.";
		} else {
			// Set confirmation message
			$_SESSION['error'] = "The email draft couldn’t be deleted due to the following error: <br /><pre>{$delete['message']->Message}</pre>";
		}
		
		// Redirect
		header("Location: /secret-specials/edit/{$_POST['date']}/");
		exit();
		
	// Handle unscheduling the email
	} else if (isset($_POST['unschedule_email'])) {
		
		$ad_id = vbool(key($_POST['unschedule_email']));
		
		$secret_special['ad_id'] = $ad_id;
		$secret_special['campaign_id'] = $_POST['campaign_id'][$ad_id];
		
		$unschedule = unscheduleSecretSpecialEmail($secret_special);
		
		// Cancel the email
		if ($unschedule['http_status_code'] < 300) {
			
			$_SESSION['confirm'] = 'The Secret Special email has been unscheduled.';
			
		} else {
			
			$_SESSION['error'] = "The Secret Special email could not be unscheduled due to the following error: <br /><pre>{$unschedule['message']->Message}</pre>";
			
		}
		
		// Redirect
		header("Location: /secret-specials/edit/{$_POST['date']}/");
		exit();
		
	// Handle delete secret special email
	} else if (isset($_POST['delete_secret_special_image'])) {
		
		$image_path = vempty($_POST['secret_special_current_image']);
		
		if ($update = updateSecretSpecialImages($image_path)) {
			
			unlink($_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/media/' . $_POST['secret_special_current_image']);
			$_SESSION['confirm'] = 'The image has been deleted.';
			
		} else {
			$_SESSION['error'] = 'The image could not be deleted.';
		}
		
		// Redirect
		header("Location: /secret-specials/edit/{$_POST['date']}/");
		exit();
		
		
	} else if (isset($_GET['action']) &&  $_GET['action'] == 'edit' && !empty($_GET['date'])) {
		
		// Get Secret Special
		$secret_special = getSecretSpecial($_GET['date']);
		
		// Get Sister Ads
		$sister_ads = getSisterAds($_GET['date']);
		
		// Build Secret Special Proof Email
		if ($sister_ads['secret_special_emails']) {
			
			// Include secret special item name if not zoned
			if (isset($secret_special['secret_special_title'])) {
				$secret_special_email_subject = rawurlencode(html_entity_decode($secret_special['secret_special_title'])) . ' Secret Special Email Proofs';
				$secret_special_email_proof_body = 'The ' . rawurlencode(html_entity_decode($secret_special['secret_special_title'])) . ' Secret Special emails for the week of ' . date('F j, Y', strtotime($secret_special['ad_date'])) . ' are ready to proof.';
			// Or don’t if zoned
			} else {
				$secret_special_email_subject = 'Secret Special Email Proofs for the Week of ' . date('F j, Y', strtotime($secret_special['ad_date']));
				$secret_special_email_proof_body = 'The Secret Special emails for the week of ' . date('F j, Y', strtotime($secret_special['ad_date'])) . ' are ready to proof.';
			}
			
			// Loop through ads and create links
			foreach ($sister_ads['secret_special_emails'] as $email) {
				$secret_special_email_proof_body .= '%0D%0D' . $email['name'] . ':%0Dhttps://specials.nuggetmarkets.net/email/' . $email['id'] . '/';
			}
			
			// Thank the team
			$secret_special_email_proof_body .= '%0D%0DThanks!';
			
			rawurlencode($secret_special_email_subject);
			rawurlencode($secret_special_email_proof_body);
			
		}
		
		// Build E-Ad Proof Email
		if ($sister_ads['ads']) {
			
			$ad_week = date('l, F j, Y', strtotime($sister_ads['ads'][0]['date_from']));
			
			$ead_proof_body = "The E-Ads for the week of $ad_week are ready to proof.";
			
			foreach ($sister_ads['ads'] as $sister_ad) {
				$ead_proof_body .= '%0D%0D'. $sister_ad['store_chain'] . ':%0Dhttps://specials.nuggetmarkets.net/specials/' . strtolower(str_replace(' ','-', $sister_ad['store_chain'])) . '/' . $sister_ad['date_from'] . "/";
			}
			
			$ead_proof_body .= "%0D%0DPlease reply to this email with any changes. Thanks!";
			
		}
		
	} else {
		
		// Get current page number
		if (isset($_GET['page'])) {
			$page_attrs['page_no'] = $_GET['page'];
		}
		
		$secret_specials = getSecretSpecials($page_attrs['page_no'], 25);
		
	}
