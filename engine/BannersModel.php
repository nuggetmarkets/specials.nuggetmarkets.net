<?php

	/**
	  * Select Banners
	  *
	  * Selects a paginated list of banners
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset Where to start when returing results
	  * @param int $limit The number of results to return
	  *
	  * @return array $banners An array of banners and metadata
	  *
	  */
	function selectBanners ($offset, $limit) {
		
		$banners = array();
		$count = FALSE;
		
		if ($banners['listing'] = runQuery("
			
			SELECT
				banners.*, 
				
				media.id AS `image_id`,
				media.name AS `image_name`,
				media.caption AS `image_caption`,
				media.alt_text AS `image_alt_text`,
				media.file AS `image_file`,
				media.mime_type AS `image_mime_type`,
				media.large_image,
				media.medium_image,
				media.small_image
				
			FROM
				banners
				
			LEFT JOIN
				media ON media.id = banners.feature_image
				
			ORDER BY
				banners.date_from DESC
				
			LIMIT
				$offset, $limit
				
		")) {
			
			$count = runQuery("
				
				SELECT
					COUNT(banners.id)
					
				FROM
					banners
					
			");
			
			$banners['count'] = $count[0];
			
			return $banners;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Banners By ID
	  *
	  * Selects the banner from the database with the specified ID
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $id ID of the banner you want to retrieve.
	  *
	  * @return array $result[0] An array of product attributes
	  *
	  */
	function selectBannerById ($id) {
		
		if ($result = runQuery("
			
			SELECT
				banners.*, 
				
				media.id AS `image_id`,
				media.name AS `image_name`,
				media.caption AS `image_caption`,
				media.alt_text AS `image_alt_text`,
				media.file AS `image_file`,
				media.mime_type AS `image_mime_type`,
				media.large_image,
				media.medium_image,
				media.small_image
				
			FROM
				banners
				
			LEFT JOIN
				media ON media.id = banners.feature_image
				
			WHERE
				banners.id = $id
				
		")) {
			
			return $result[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Banners in Date Range
	  *
	  * Selects banners from the database where current date is within the date_from and date_through ranges
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $from_date ID of the banner you want to retrieve.
	  * @param int $through_date ID of the banner you want to retrieve.
	  *
	  * @return array $banners An array of banners and attributes
	  *
	  */
	function selectBannersInDateRange ($from_date, $through_date) {
		
		if ($banners = runQuery("
			
			SELECT
				banners.*, 
				
				media.id AS `image_id`,
				media.name AS `image_name`,
				media.caption AS `image_caption`,
				media.alt_text AS `image_alt_text`,
				media.file AS `image_file`,
				media.mime_type AS `image_mime_type`,
				media.large_image,
				media.medium_image,
				media.small_image
				
			FROM
				banners
				
			LEFT JOIN
				media ON media.id = banners.feature_image
				
			WHERE
				date_from <= $from_date AND 
				date_through >= $through_date
				
		")) {
			
			return $banners;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert Banner
	  *
	  * Inserts a new row into the banners table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $banner An array of banner data.
	  *
	  * @return int $insert The mysql_insert_id of the record.
	  *
	  */
	function insertBanner($banner) {
		
		if ($insert = runQuery("
			
			INSERT INTO
				banners
			
			SET
				name			= {$banner['name']},
				url				= {$banner['url']},
				body_copy		= {$banner['body_copy']},
				sponsored		= {$banner['sponsored']}, 
				zones			= {$banner['zones']}, 
				department		= {$banner['department']},
				date_from		= {$banner['date_from']},
				date_through	= {$banner['date_through']},
				media_links		= {$banner['media_links']},
				feature_image	= {$banner['featured_image']}
				
		")) {
			
			return $insert;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update banner
	  *
	  * Updates a banner record in the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $banner An array of banner attributes.
	  *
	  * @return TRUE|FALSE $update Boolean
	  *
	  */
	function updateBanner ($banner) {
		
		if ($update = runQuery("
			
			UPDATE
				banners
				
			SET
				name			= {$banner['name']},
				url				= {$banner['url']},
				body_copy		= {$banner['body_copy']},
				sponsored		= {$banner['sponsored']}, 
				zones			= {$banner['zones']}, 
				department		= {$banner['department']},
				date_from		= {$banner['date_from']},
				date_through	= {$banner['date_through']},
				media_links		= {$banner['media_links']},
				feature_image	= {$banner['featured_image']}
				
			WHERE
				id = {$banner['banner_id']}
				
		")) {
			
			return $update;
			
		}
		
	}


	/**
	  * Delete banner
	  *
	  * Deletes a banner record in the database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $id The ID of the banner to delete.
	  *
	  * @return TRUE|FALSE $update Boolean
	  *
	  */
	function deleteBanner ($id) {
		
		if ($delete = runQuery("
			
			DELETE FROM
				banners
				
			WHERE
				id = $id
				
			LIMIT 1
			
		")) {
			
			return $delete;
			
		} else {
			
			return FALSE;
			
		}
		
	}
