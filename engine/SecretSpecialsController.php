<?php

	// Include dependencies
	require_once 'SecretSpecialsModel.php';
	require_once 'AdsModel.php';
	require_once 'AdPlansModel.php';


	/**
	  * Get Secret Specials
	  *
	  * Gets a paginated list of secret specials
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $offset The record to start from
	  * @param int $limit The numnber of recoreds to return
	  *
	  * @return array $ad_dates A multidimensional array of ad dates
	  *
	  */
	function getSecretSpecials ($page = 1, $limit = 25) {
		
		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}
		
		$page = vbool($page);
		$limit = vbool($limit);
		
		// Calculate page offset
		$offset = $page * $limit - ($limit);
		
		$secret_specials = array();
		
		// Get paginated list of ad dates
		if ($ad_dates = selectAdDates($offset, $limit)) {
			
			$i = 0;
			foreach ($ad_dates['listing'] as $ad_date) {
				
				// Validate input
				$date = vempty($ad_date['date_from']);
				
				// Select ad data and ad plan date
				$data = selectSecretSpecialZonesByAdDate($date);
				$plan = selectAdPlanByDate($date);
				$titles = array();
				
				// Create secret special title using the first array member title or an implopded string of titles if they differ by zone
				// Loop through secret specal and build array of titles
				foreach($data as $item) {
					// Exclude null values for zones without a secret special
					if (!is_null($item['secret_special_title'])) {
						$titles[] = $item['secret_special_title'];
					}
				}
				
				// Reduce and count the array
				$unique = array_unique($titles);
				$count = count($unique);
				
				// Set the name of the item depending on the description.
				if ($count > 1) {
					
					// Implode item names into a single title if there are more than one
					$secret_specials[$i]['item'] = implode(', ', $unique);
					
					// Flip zone flag on
					$secret_specials[$i]['zoned'] = TRUE;
					
				} else {
					
					// Use the title of the first member if they are all the same 
					if ($data[0]['secret_special_title']) {
						$secret_specials[$i]['item'] = $data[0]['secret_special_title'];
						
					// Use the ad plan description if the item isn’t set
					} else if ($plan['secret_special_title']) {
						$item = explode ("\r\n", $plan['secret_special_title']);
						$secret_specials[$i]['item'] = $item[0];
						
					// Set a default if there is no ad plan description
					} else {
						$secret_specials[$i]['item'] = '<span class="not-set">Item not set</span>';
					}
					
					// Flip zone flag off
					$secret_specials[$i]['zoned'] = FALSE;
					
				}
				
				// Create secret special date range
				if ($data[0]['secret_special_from']) {
					$secret_specials[$i]['secret_special_range'] = convertDatesToRange($data[0]['secret_special_from'], $data[0]['secret_special_through']);
				} else if ($plan['secret_special_from']) {
					$secret_specials[$i]['secret_special_range'] = convertDatesToRange($plan['secret_special_from'], $plan['secret_special_through']);
				} else {
					$secret_specials[$i]['secret_special_range'] = convertDatesToRange(date('Y-m-d', strtotime($ad_date['date_from'] . ' +2 days')), date('Y-m-d', strtotime($ad_date['date_from'] . ' +3 days')));
				}
				
				// Extract email status
				$d = 0;
				foreach ($data as $datum) {
					if (!$datum['campaign_status']) {
						$secret_specials[$i]['campaign_statuses'][$d]['class'] = 'email unscheduled';
					} else {
						$secret_specials[$i]['campaign_statuses'][$d]['class'] = 'email ' . $datum['campaign_status'];
					}
					$secret_specials[$i]['campaign_statuses'][$d]['store_chain'] = $datum['store_chain'];
					
					if (strstr($datum['store_chain'], '4')) {
						$secret_specials[$i]['campaign_statuses'][$d]['label'] = '4';
					} else {
						$secret_specials[$i]['campaign_statuses'][$d]['label'] = substr($datum['store_chain'], 0, 1);
					}
					
					$d++;
				}
				
				// Set ad date
				$secret_specials[$i]['date_from'] = $ad_date['date_from'];
				
				// Unset vars for next loop itteration
				unset($data);
				unset($plan);
				unset($item);
				unset($titles);
				unset($count);
				unset($unique);
				
				$i++;
				
			}
			
			$secret_specials['pagination'] = pagination($page, $limit, $ad_dates['count']);
			
			
		}
		
		return $secret_specials;
		
	}


	/**
	  * Get Secret Special
	  *
	  * Gets secret special ad settings from ads table for the specified date
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $date The date of the ad data to pull in YYYY-MM-DD format
	  *
	  * @return array $secret_special A multidimensional array of secret secial data for  the specified date
	  *
	  */
	function getSecretSpecial ($date) {
		
		$ad_date = vempty($date);
		$secret_special = array();
		
		// Select ad data and ad plan date
		$data = selectSecretSpecialZonesByAdDate($ad_date);
		$plan = selectAdPlanByDate($ad_date);
		
		// Check for lifestyle icon links
		if ($plan['secret_special_labels']) {
			
			// Get lifestyle icons
			$secret_special['secret_special_labels'] = getLabelsWithIds($plan['secret_special_labels']);
			
		}
		
		// Check for UPCs to be pictured
		if ($plan['secret_special_picture']) {
			
			// Get product images
			$pics = explode(',', $plan['secret_special_picture']);
			
			$i = 0;
			foreach ($pics as $pic) {
				
				if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/upc/' . $pic . '.webp')) {
					$secret_special['secret_special_picture'][$i]['picture'] = '/img/upc/' . $pic . '.webp';
				} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/upc/' . $pic . '.jpg')) {
					$secret_special['secret_special_picture'][$i]['picture'] = '/img/upc/' . $pic . '.jpg';
				} else {
					$secret_special['secret_special_picture'][$i]['picture'] = NULL;
				}
				
				$secret_special['secret_special_picture'][$i]['upc'] = $pic;
				
				$i++;
			}
			
		}
		
		if ($data || $plan) {
			
			// Create secret special title using the first array member title or an implopded string of titles if they differ by zone
			// Loop through secret specal and build array of titles
			foreach($data as $item) {
				// Exclude null values for zones without a secret special
				if (!is_null($item['secret_special_title'])) {
					$titles[] = $item['secret_special_title'];
				}
			}
			
			// Reduce and count the array
			$unique = array_unique($titles);
			$count = count($unique);
			$secret_special['imported_from_ad_plan'] = FALSE;
			
			// Set the name of the item depending on the description.
			if ($count > 1) {
				
				// Flip zone flag on
				$secret_special['zoned'] = TRUE;
				
			} else {
				
				// Create secret special title
				if ($data[0]['secret_special_title']) {
					$secret_special['secret_special_title'] = $data[0]['secret_special_title'];
				} else if ($plan['secret_special_title']) {
					$item = explode ("\r\n", $plan['secret_special_title']);
					$secret_special['secret_special_title'] = $item[0];
					$secret_special['imported_from_ad_plan'] = TRUE;
				} else {
					$secret_special['secret_special_title'] = 'Item not set';
				}
				
				if ($data[0]['secret_special_retail']) {
					$secret_special['secret_special_retail'] = $data[0]['secret_special_retail'];
				} else if ($plan['secret_special_retail']) {
					$secret_special['secret_special_retail'] = $plan['secret_special_retail'];
					$secret_special['imported_from_ad_plan'] = TRUE;
				}
				
				if ($data[0]['secret_special_limit']) {
					$secret_special['secret_special_limit'] = $data[0]['secret_special_limit'];
				} else if ($plan['secret_special_limit']) {
					$secret_special['secret_special_limit'] = $plan['secret_special_limit'];
					$secret_special['imported_from_ad_plan'] = TRUE;
				}
				
				if ($data[0]['secret_special_skus']) {
					$secret_special['secret_special_skus'] = $data[0]['secret_special_skus'];
				} else if ($plan['secret_special_upcs']) {
					$secret_special['secret_special_skus'] = "UPCs: " . str_replace(',', ', ', $plan['secret_special_upcs']);
					$secret_special['imported_from_ad_plan'] = TRUE;
				}
				
				if ($data[0]['secret_special_container']) {
					$secret_special['secret_special_container'] = $data[0]['secret_special_container'];
				}
				
				// Flip zone flag off
				$secret_special['zoned'] = FALSE;
				
			}
			
			// Capture secret special dates and create date range for display
			if ($data[0]['secret_special_from']) {
				$secret_special['secret_special_from'] = $data[0]['secret_special_from'];
				$secret_special['secret_special_through'] = $data[0]['secret_special_through'];
				$secret_special['secret_special_range'] = convertDatesToRange($data[0]['secret_special_from'], $data[0]['secret_special_through']);
			} else if ($plan['secret_special_from']) {
				$secret_special['secret_special_from'] = $plan['secret_special_from'];
				$secret_special['secret_special_through'] = $plan['secret_special_through'];
				$secret_special['secret_special_range'] = convertDatesToRange($plan['secret_special_from'], $plan['secret_special_through']);
			} else {
				$secret_special['secret_special_from'] = date('Y-m-d', strtotime(unescape($ad_date['date_from']) . ' +2 days'));
				$secret_special['secret_special_through'] = date('Y-m-d', strtotime(unescape($ad_date['date_from']) . ' +3 days'));
				$secret_special['secret_special_range'] = convertDatesToRange(date('Y-m-d', strtotime(unescape($ad_date['date_from']) . ' +2 days')), date('Y-m-d', strtotime($ad_date['date_from'] . ' +3 days')));
			}
			
			if ($data[0]['secret_special_description']) {
				$secret_special['secret_special_description'] = $data[0]['secret_special_description'];
			}
			
			if ($data[0]['secret_special_image']) {
				$secret_special['secret_special_image'] = $data[0]['secret_special_image'];
			}
			
			if ($data[0]['prepended_message']) {
				$secret_special['prepended_message'] = $data[0]['prepended_message'];
			}
			
			if ($data[0]['prepended_message_position']) {
				$secret_special['prepended_message_position'] = $data[0]['prepended_message_position'];
			}
			
			// Set ad date
			$secret_special['ad_date'] = $date;
			
			// Calculate email send date
			if (time() < strtotime($date . ' 06:00:00')) {
				$secret_special['email_send_date']['value'] = "$date 06:00";
			} else {
				$secret_special['email_send_date']['value'] = date('Y-m-d H:i', strtotime('+30 minutes'));
			}
			
			$secret_special['email_send_date']['display'] = date('n/j/Y @ g:i a', strtotime($secret_special['email_send_date']['value']));
			
			
			// Loop through each as zone
			foreach ($data as $zone) {
				
				// Create array of data for each zone
				$secret_special['zones'][$zone['ad_id']] = $zone;
				if (!$zone['campaign_status']) {
					$secret_special['zones'][$zone['ad_id']]['campaign_status'] = 'unscheduled';
				}
				$secret_special['zones'][$zone['ad_id']]['store_attributes'] = getStoreAttributesByName($zone['store_chain']);
				
				// Explode article and recipe IDs
				if ($zone['article_ids']) {
					$secret_special['zones'][$zone['ad_id']]['articles'] = explode(',', $zone['article_ids']);
				}
				
				if ($zone['recipe_ids']) {
					$secret_special['zones'][$zone['ad_id']]['recipes'] = explode(',', $zone['recipe_ids']);
				}
				
				// Capture remaining global variables from first instance of zone data available
				if (!isset($secret_special['secret_special_disclaimers'])) {
					$secret_special['secret_special_disclaimers'] = $zone['secret_special_disclaimers'];
				}
				
				if (!isset($secret_special['secret_special_limit'])) {
					$secret_special['secret_special_limit'] = $zone['secret_special_limit'];
				}
				
				if (!isset($secret_special['secret_special_skus'])) {
					$secret_special['secret_special_skus'] = $zone['secret_special_skus'];
				}
				
				if (!isset($secret_special['secret_special_retail'])) {
					$secret_special['secret_special_retail'] = $zone['secret_special_retail'];
				}
				
				// Check for zone regular retails
				if ($zone['secret_special_regular_retail']) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $zone['secret_special_regular_retail'];
				} else if (strstr('Nugget Markets Valley', $zone['store_chain'])) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $plan['secret_special_valley_regular_retail'];
				} else if (strstr('Nugget Markets Marin', $zone['store_chain'])) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $plan['secret_special_marin_regular_retail'];
				} else if (strstr('Sonoma Market', $zone['store_chain'])) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $plan['secret_special_sonoma_regular_retail'];
				} else if (strstr('Fork Lift', $zone['store_chain'])) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $plan['secret_special_fork_lift_regular_retail'];
				} else if (strstr('Food 4 Less', $zone['store_chain'])) {
					$secret_special['zones'][$zone['ad_id']]['secret_special_regular_retail'] = $plan['secret_special_food_4_less_regular_retail'];
				}
				
				// Get campaign stats if campaign ID exists
				if (isset($zone['campaign_id'])) {
					
					// Get the stats and append to the array
					$stats = getCampaignStats($zone);
					$secret_special['zones'][$zone['ad_id']]['campaign_stats'] = $stats;
					
					
					
					// If campaign is not in sent status and send date has passed
					if ($zone['campaign_status'] != 'sent' && time() > strtotime($date)) {
						
						// And total opened is greater than 0
						if ($stats['TotalOpened'] > 0) {
							
							// Update campaign status to sent
							$ad_id = vbool($zone['ad_id']);
							$campaign_id = vempty($zone['campaign_id']);
							$campaign_status = vempty('sent');
							updateAdCampaignId($ad_id, $campaign_id, $campaign_status);
							
							// Override campaign zone status for display
							$secret_special['zones'][$zone['ad_id']]['campaign_status'] = 'sent';
						}
						
					} else {
						
						// Capture stats for summary
						$forwards = $stats['Forwards'];
						$likes = $stats['Likes'];
						$mentions = $stats['Mentions'];
						$recipients = $stats['Recipients'];
						$totalopened = $stats['TotalOpened'];
						$clicks = $stats['Clicks'];
						$unsubscribed = $stats['Unsubscribed'];
						$bounced = $stats['Bounced'];
						$uniqueopened = $stats['UniqueOpened'];
						$spamcomplaints = $stats['SpamComplaints'];
						
						// Increment summary totals
						$total_forwards += $forwards;
						$total_likes += $likes;
						$total_mentions += $mentions;
						$total_recipients += $recipients;
						$total_totalopened += $totalopened;
						$total_clicks += $clicks;
						$total_unsubscribed += $unsubscribed;
						$total_bounced += $bounced;
						$total_uniqueopened += $uniqueopened;
						$total_spamcomplaints += $spamcomplaints;
						
					}
					
					// Unset temporary vars
					unset($stats, $forwards, $likes, $mentions, $recipients, $totalopened, $clicks, $unsubscribed, $bounced, $uniqueopened, $spamcomplaints);
				}
				
			}
			
			// Only declair campaign_summary if opend count is greater than zero
			if ($total_totalopened > 0) {
				
				// Capture campaign summary
				$secret_special['campaign_summary']['forwards'] = $total_forwards;
				$secret_special['campaign_summary']['likes'] = $total_likes;
				$secret_special['campaign_summary']['mentions'] = $total_mentions;
				$secret_special['campaign_summary']['recipients'] = $total_recipients;
				$secret_special['campaign_summary']['totalopened'] = $total_totalopened;
				$secret_special['campaign_summary']['clicks'] = $total_clicks;
				$secret_special['campaign_summary']['unsubscribed'] = $total_unsubscribed;
				$secret_special['campaign_summary']['bounced'] = $total_bounced;
				$secret_special['campaign_summary']['uniqueopened'] = $total_uniqueopened;
				$secret_special['campaign_summary']['spamcomplaints'] = $total_spamcomplaints;
				
			}
			
			return $secret_special;
			
		}
		
	}


	/**
	  * Create Draft Secret Special Email
	  *
	  * Creates a draft of the secret special email for the specified store
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $secret_special A multidimensional array of email data in the following format:
	  * array(
	  *		store_chain => 'The full name of the store chain/zone',
	  *		campaign_subject => 'The subject of the email',
	  * 	campaign_name => 'The name of the campaign',
	  *		ad_id => 'The ad ID',
	  * );
	  *
	  * @return array $response The API response from Campaign Monitor
	  *
	  */
	function createDraftSecretSpecialEmail ($secret_special) {
		
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		switch ($secret_special['store_chain']) {
			case 'Nugget Markets Valley':
				$from_name = 'Nugget Markets';
				$from_email = 'enews@nuggetmarket.com';
				// Omit list IDs because we are only sending to specific segments of this list and of the Fresh to Market list
				$list_id = array();
				// Weekly Specials and Fresh to Market Nugget Markets Valley Segments
				$segment_ids = array('52e0c2f9b660bf7851d64c07f1e0c325', '50d602ff4f3aeb25ca2eabd3c39474d9'); 
				break;
				
			case 'Nugget Markets Marin':
				$from_name = 'Nugget Markets';
				$from_email = 'enews@nuggetmarket.com';
				// Omit list IDs because we are only sending to specific segments of this list and of the Fresh to Market list
				$list_id = array();
				// Weekly Specials and Fresh to Market Nugget Markets Marin Segments
				$segment_ids = array('81c9ee666e60c0c538705bb485988b31', '212c75a6f94e5b72acaa53d5e253704c'); 
				break;
				
			case 'Sonoma Market':
				$from_name = 'Sonoma Market';
				$from_email = 'enews@nuggetmarket.com';
				// Sonoma Market Weekly Specails List
				$list_id = array('ba79f1951b459da9f16e7178b736f0f4');
				// Fresh to Market Sonoma Market Segment
				$segment_ids = array('4dae802c8b2bbe646ef443e2104b8115');
				break;
				
			case 'Food 4 Less':
				$from_name = 'Food 4 Less, Woodland';
				$from_email = 'enews@nuggetmarket.com';
				// Food 4 Less, Woodland Weekly Specails List
				$list_id = array('8bcd9202313ce7d4b5a9423b40ccee05');
				// Fresh to Market Food 4 Less, Woodland Segment
				$segment_ids = array('30d9de496a5cce600233da84d725fc5b');
				break;
				
			default:
				$from_name = 'Fork Lift';
				$from_email = 'enews@nuggetmarket.com';
				// Fork Lift Weekly Specails List
				$list_id = array('28eaa0536f1b19024809aaf0d1b0057b');
				// Fresh to Market Fork Lift Segment
				$segment_ids = array('7c35347c4bb2f2be57fe382c6fe2352c');
				break;
		}
		
		// Test segment ID. Uncomment for testing!
		// $segment_ids = array('7cb6ac5b53d462ad81bcea55bd1108c2');
		
		$wrap_draft = new CS_REST_Campaigns(NULL, $auth);
		
		// Create campaign for segment
		if (isset($segment_ids)) {
			
			$create_draft = $wrap_draft->create(CAMPAIGN_MONITOR_CLIENT_ID, array(
				'Subject' => $secret_special['campaign_subject'],
				'Name' => $secret_special['campaign_name'],
				'FromName' => $from_name,
				'FromEmail' => $from_email,
				'ReplyTo' => $from_email,
				'HtmlUrl' => 'https://specials.nuggetmarkets.net/email/' . $secret_special['ad_id'] . '/',
				'TextUrl' => 'https://specials.nuggetmarkets.net/email-text/' . $secret_special['ad_id'] . '/',
				'ListIDs' => $list_id,
				'SegmentIDs' => $segment_ids
			));
			
		// Create campagin for complete list
		} else {
			
			$create_draft = $wrap_draft->create(CAMPAIGN_MONITOR_CLIENT_ID, array(
				'Subject' => $secret_special['campaign_subject'],
				'Name' => $secret_special['campaign_name'],
				'FromName' => $from_name,
				'FromEmail' => $from_email,
				'ReplyTo' => $from_email,
				'HtmlUrl' => 'https://specials.nuggetmarkets.net/email/' . $secret_special['ad_id'] . '/',
				'TextUrl' => 'https://specials.nuggetmarkets.net/email-text/' . $secret_special['ad_id'] . '/',
				'ListIDs' => $list_id
			));
			
		}
		
		// Schedule the campaign
		if ($create_draft->was_successful()) {
			
			// Validate input
			$ad_id = $secret_special['ad_id'];
			$campaign_id = vempty($create_draft->response);
			$campaign_status = vempty('draft');
			
			// Update the campaign status
			updateAdCampaignId($ad_id, $campaign_id, $campaign_status);
			
		}
		
		// Capture the create draft response
		$response['http_status_code'] 	= $create_draft->http_status_code;
		$response['message'] 			= $create_draft->response;
		
		// Return the response
		return $response;
		
	}


	/**
	  * Schedule Secret Special Email
	  *
	  * Creates a draft of the secret special email for the specified store, then schedules it
	  * on the date and time provided
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $secret_special A multidimensional array of email data in the following format:
	  * array(
	  *		store_chain => 'The full name of the store chain/zone',
	  *		campaign_subject => 'The subject of the email',
	  * 	campaign_name => 'The name of the campaign',
	  *		ad_id => 'The ad ID',
	  * );
	  *
	  * @return array $response The API response from Campaign Monitor
	  *
	  */
	function scheduleSecretSpecialEmail ($secret_special) {
		
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		// Schedule the campaign
		$wrap_schedule = new CS_REST_Campaigns($secret_special['campaign_id'], $auth);
		$schedule = $wrap_schedule->send(array(
			'ConfirmationEmail' => 'mark.eagleton@nuggetmarket.com, rebecca.reichardt@nuggetmarket.com',
			'SendDate' => $secret_special['email_send_date']
		));
			
		if ($schedule->was_successful()) {
			
			// Validate input
			$ad_id = $secret_special['ad_id'];
			$campaign_status = vempty('scheduled');
			$campaign_id = vempty($secret_special['campaign_id']);
			
			// Update the campaign status
			updateAdCampaignId($ad_id, $campaign_id, $campaign_status);
			
		}
			
		// Capture the create draft response
		$response['http_status_code'] 	= $schedule->http_status_code;
		$response['message'] 			= $schedule->response;
		
		// Return the response
		return $response;
		
	}


	/**
	  * Unschedule Email
	  *
	  * Unschedules an email for the specified zone in Campaign Monitor, 
	  * then updates the status in the local database
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $newsletter A multidimensional array of email data in the following format:
	  * array(
	  * 	id => 'ID of the newsletter'
	  * 	name => 'name of the campaign'
	  * 	issue => 'issue name of the campaign'
	  * 	send_date => 'time stamp when to send the email'
	  * 	subject => 'Emal subject'
	  * 	send_nugget_valley => 'Zone send flag'
	  * 	send_nugget_marin => 'Zone send flag'
	  * 	send_sonoma => 'Zone send flag'
	  * 	send_forklift => 'Zone send flag'
	  * 	send_food4less => 'Zone send flag'
	  * 	zone_status => array(
	  * 		Array of zone settings (not used)
	  * 	)
	  * );
	  * @param string $zone_slug The slug of the zone to send to
	  *
	  * @return array $response The API response from Campaign Monitor
	  *
	  */
	  
	function unscheduleEmail ($newsletter, $zone_slug) {
		
		// Loop through zone statuses to get the campaign_id
		foreach ($newsletter['zone_status'] as $zone) {
			
			if ($zone['slug'] == $zone_slug) {
				$campaign_id = $zone['campaign_id'];
				break;
			}
			
		}
		
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		$wrap_unschedule = new CS_REST_Campaigns($campaign_id, $auth);
		$unschedule = $wrap_unschedule->unschedule();
		
		if ($unschedule->was_successful()) {
			
			// Validate input
			$newsletter_id = $newsletter['id'];
			$campaign_status = unescape(vempty('draft'));
			$zone_slug = unescape(vempty($zone_slug));
			
			// Update the campaign status to draft
			saveNewsletterZoneStatus($newsletter, $zone_slug, $campaign_id, $campaign_status);
			
		}
		
		// Capture the unschedule draft response
		$response['http_status_code'] 	= $unschedule->http_status_code;
		$response['message'] 			= $unschedule->response;
		
		// Return the response
		return $response;
		
	}
	
	
	/**
	  * Delete Email Draft
	  *
	  * Deletes the draft of the newsletter for the specified zone
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $secret_special A multidimensional array of email data in the following format:
	  * array(
	  * 	ad_id => 'ID of the ad'
	  * 	campaign_id => 'ID of the campaign'
	  * 	issue => 'issue name of the campaign'
	  * 	send_date => 'time stamp when to send the email'
	  * 	subject => 'Emal subject'
	  * 	send_nugget_valley => 'Zone send flag'
	  * 	send_nugget_marin => 'Zone send flag'
	  * 	send_sonoma => 'Zone send flag'
	  * 	send_forklift => 'Zone send flag'
	  * 	send_food4less => 'Zone send flag'
	  * 	zone_status => array(
	  * 		Array of zone settings (not used)
	  * 	)
	  * );
	  * @param string $zone_slug The slug of the zone to send to
	  *
	  * @return array $response The API response from Campaign Monitor
	  *
	  */
	function deleteEmailDraft ($secret_special) {
		
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		// Validate input
		$ad_id = $secret_special['ad_id'];
		$campaign_id = $secret_special['campaign_id'];
		
		$wrap_delete = new CS_REST_Campaigns($campaign_id, $auth);
		$delete = $wrap_delete->delete();
		
		if ($delete->was_successful()) {
			
			// Update the campaign status
			$campaign_id = 'NULL';
			$campaign_status = 'NULL';
			updateAdCampaignId($ad_id, $campaign_id, $campaign_status);
			
		}
		
		// Capture the delete draft response
		$response['http_status_code'] 	= $delete->http_status_code;
		$response['message'] 			= $delete->response;
		
		// Return the response
		return $response;
		
	}


	/**
	  * Get Campaign Stats
	  *
	  * Gets the stats of the specified campaign from Campaign Monitor 
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $secret_special A multidimensional array of secret special data:
	  * array(
	  * 	id => 'ID of the newsletter'
	  * 	name => 'name of the campaign'
	  * 	issue => 'issue name of the campaign'
	  * 	send_date => 'time stamp when to send the email'
	  * 	subject => 'Emal subject'
	  * 	send_nugget_valley => 'Zone send flag'
	  * 	send_nugget_marin => 'Zone send flag'
	  * 	send_sonoma => 'Zone send flag'
	  * 	send_forklift => 'Zone send flag'
	  * 	send_food4less => 'Zone send flag'
	  * 	zone_status => array(
	  * 		Array of zone settings
	  * 	)
	  * );
	  *
	  * @return array $newsletter Returns newsletter argument with updated data
	  *
	  */
	function getCampaignStats ($zone) {
		
		// Load CM
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		$wrap_summary = new CS_REST_Campaigns($zone['campaign_id'], $auth);
		$summary = $wrap_summary->get_summary();
		
		$response = (array) $summary->response;
		
		return $response;
		
	}


	/**
	  * Unschedule Secret Special Email
	  *
	  * Unschedules the secret special email for the specified store, then deletes it
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $secret_special A multidimensional array of email data in the following format:
	  * array(
	  *		campaign_id => 'The ID of the campaign to cancel',
	  *		ad_id => 'The ad ID'
	  * );
	  *
	  * @return array $response The API response from Campaign Monitor
	  *
	  */
	  
	function unscheduleSecretSpecialEmail ($secret_special) {
		
		require_once('createsend/csrest_campaigns.php');
		$auth = array('api_key' => CAMPAIGN_MONITOR_API_KEY);
		
		$wrap_unschedule = new CS_REST_Campaigns($secret_special['campaign_id'], $auth);
		$unschedule = $wrap_unschedule->unschedule();
		
		if ($unschedule->was_successful()) {
			
			$ad_id 				= vbool($secret_special['ad_id']);
			$campaign_id 		= vempty($secret_special['campaign_id']);
			$campaign_status 	= vempty('draft');
			
			// Update the ad ID
			updateAdCampaignId($ad_id, $campaign_id, $campaign_status);
			
		}
		
		// Capture the create draft response
		$response['http_status_code'] 	= $unschedule->http_status_code;
		$response['message'] 			= $unschedule->response;
		
		// Return the response
		return $response;
		
	}
