<?php

	// Include dependencies
	require_once 'Bootstrap.php';
	
	// Capture referer
	$referer = $_SERVER['HTTP_REFERER'];

	// Handle UPC file uploads
	if (isset($_FILES['upc_images']['name'][0])) {
		
		// Set directory
		$dir = $_SERVER['DOCUMENT_ROOT'].'/../httpdocs/img/upc/';
		
		// Loop through files
		foreach ($_FILES["upc_images"]["error"] as $key => $error) {
			
			if ($_FILES['upc_images']['type'][$key] != 'image/webp') {
				
				$_SESSION['error'] = '<p>One or more of the images uploaded was not in WebP format and could not be uploaded.</p>';
				
			} else if ($error == UPLOAD_ERR_OK) {
				$tmp_name = $_FILES["upc_images"]["tmp_name"][$key];
				$name = $_FILES["upc_images"]["name"][$key];
				move_uploaded_file($tmp_name, "$dir/$name");
			}
		}
		
		// Set confirmation
		$_SESSION['confirm'] = '<p>Image(s) uploaded.</p>';
		
	} else {
		
		$_SESSION['error'] = '<p>Image(s) couldn’t be uploaded.</p>';
		
	}

	// Redirect
	header("Location: $referer");
	exit();
