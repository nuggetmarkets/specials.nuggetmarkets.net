<?php 

    // Require dependencies
    require_once 'MediaModel.php';
    

	/**
	  * Gets media from DB
	  *
	  * Gets list of media items ordered by ID DESC
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @return attr $items An array of media objects
	  *
	  */
    function getMedia ($page = 1, $limit = 24) {
		
		$items = NULL;
		$item = NULL;
		
		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}
		
		$page = vbool($page);
		$limit = vbool($limit);
		
		// Calculate page offset
		$offset = $page * $limit - ($limit);
		
		if ($media = selectMedia($offset, $limit)) {
			
			foreach ($media['listing'] as $item) {
				
				// Create array of results
				$items[$item['id']]['id'] 			= $item['id'];
				$items[$item['id']]['name'] 		= $item['name'];
				$items[$item['id']]['caption'] 		= $item['caption'];
				$items[$item['id']]['alt_text'] 	= $item['alt_text'];
				$items[$item['id']]['catalog_text'] = $item['catalog_text'];
				$items[$item['id']]['file'] 		= $item['file'];
				$items[$item['id']]['mime_type']	= $item['mime_type'];
				$items[$item['id']]['large_image'] 	= $item['large_image'];
				$items[$item['id']]['medium_image'] = $item['medium_image'];
				$items[$item['id']]['small_image'] 	= $item['small_image'];
				
				// Get file path info
				$path = pathinfo($item['file']);
				
				// Set file name var
				$items[$item['id']]['file_name'] 	= $path['basename'];
				
				// Set file extension var
				$items[$item['id']]['extension'] 	= $path['extension'];
				
			}
			
			$items['listing'] = $items;
			
			$items['pagination'] = pagination($page, $limit, $media['count']);
			
			return $items;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Gets specified media item
	  *
	  * Get media item from database based on ID passed.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $item_id ID of record to return.
	  *
	  * @return array $item An array of columns from the returned row
	  *
	  */
	function getMediaById ($item_id) {
		
		$item = NULL;
		$id = vbool($item_id);
		
		if ($media = selectMediaById($id)) {
			
			// Create array of results
			$item['id'] 			= $media['id'];
			$item['name'] 			= $media['name'];
			$item['caption'] 		= $media['caption'];
			$item['alt_text'] 		= $media['alt_text'];
			$item['catalog_text']	= $media['catalog_text'];
			$item['file'] 			= $media['file'];
			$item['mime_type'] 		= $media['mime_type'];
			$item['large_image'] 	= $media['large_image'];
			$item['medium_image']	= $media['medium_image'];
			$item['small_image'] 	= $media['small_image'];
			
			// Get file path info
			$path 		= pathinfo($media['file']);
			$modified	= date('M j, Y @ g:i a', filemtime(MEDIA_UPLOAD_DIR . $media['file']));
			
			// Set file name var
			$item['file_name'] 	= $path['basename'];
			
			// Set file extension var
			$item['extension'] 	= $path['extension'];
			
			if (strstr($media['mime_type'], 'image')) {
				
				$item['dimensions']['small'] 	= getimagesize(MEDIA_UPLOAD_DIR . $media['small_image']);
				$item['filesize']['small']		= formatBytes(filesize(MEDIA_UPLOAD_DIR . $media['small_image']));
				$item['pathinfo']['small']		= pathinfo(MEDIA_UPLOAD_DIR . $media['small_image']);
				$item['modified']['small']		= date('M j, Y @ g:i a', filemtime(MEDIA_UPLOAD_DIR . $media['small_image']));
				
				$item['dimensions']['medium'] 	= getimagesize(MEDIA_UPLOAD_DIR . $media['medium_image']);
				$item['filesize']['medium']		= formatBytes(filesize(MEDIA_UPLOAD_DIR . $media['medium_image']));
				$item['pathinfo']['medium']		= pathinfo(MEDIA_UPLOAD_DIR . $media['medium_image']);
				$item['modified']['medium']		= date('M j, Y @ g:i a', filemtime(MEDIA_UPLOAD_DIR . $media['medium_image']));
				
				$item['dimensions']['large'] 	= getimagesize(MEDIA_UPLOAD_DIR . $media['large_image']);
				$item['filesize']['large']		= formatBytes(filesize(MEDIA_UPLOAD_DIR . $media['large_image']));
				$item['pathinfo']['large']		= pathinfo(MEDIA_UPLOAD_DIR . $media['large_image']);
				$item['modified']['large']		= date('M j, Y @ g:i a', filemtime(MEDIA_UPLOAD_DIR . $media['large_image']));
				
				$item['dimensions']['original'] = getimagesize(MEDIA_UPLOAD_DIR . $media['file']);
				$item['filesize']['original']	= formatBytes(filesize(MEDIA_UPLOAD_DIR . $media['file']));
				$item['pathinfo']['original']	= $path;
				$item['modified']['original']	= $modified;
			} else {
				
				$item['modified']	= $modified;
				
			}
			
			return $item;
			
		} else {
			
			return FALSE;
			
		}
		
    }


    /**
	  * Get media with IDs
	  *
	  * Gets rows from the medial table with the specified IDs
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array|string $media_ids The spcified IDs you wish to retrive as an array or comma delimited string.
	  *
	  * @return array $media_items
	  *
	  */
	function getMediaWithIds ($media_ids) {
		
		// Create arrays
		$media_items = array();
		$media = array();
		
		// If input is string, explode into an array
		if (is_array($media_ids)) {
			
			$media_items = $media_ids;
			
		// Or set new var
		} else {
			
			$media_items = explode(',', $media_ids);
			
		}
		
		// Loop through array and validate
		foreach ($media_items as $id) {
			
			$media_links[] = vbool($id);
			
		}
		
		// Get media items
		$media = selectMediaWithIds($media_items);
		
		// Return array
		return $media;
		
	}


	/**
	  * Search DB table for terms
	  *
	  * Searches database table using FULL TEXT index when 4 or more characters are
	  * provided, or using LIKE string matching if less than 4 characters.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string    $search_terms The terms to search for.
	  *
	  * @return array $items An assoc. array of query results or FALSE on fail.
	  *
	  */
	function getMediaBySearchTerms ($search_terms) {
		
		// Declare return vars
		$media = FALSE;
		$items = NULL;
		$path = NULL;
		
		// Validate search terms
		$terms = vempty($search_terms,1,0);
		
		// Use full text index if terms arg is 4 or more characters
		if (strlen($search_terms) >= 4) {
			
			$media = selectMediaMatchAgainstTerms($terms);
			
		// Use LIKE clause if terms string is under 4 characters
		} else {
			
			$media = selectMediaLikeTerms($terms);
			
		}
		
		if ($media) {
			
			// Loop through results
			foreach ($media as $item) {
				
				// Create array of results
				$items[$item['id']]['id'] 			= $item['id'];
				$items[$item['id']]['name'] 		= $item['name'];
				$items[$item['id']]['caption'] 		= $item['caption'];
				$items[$item['id']]['alt_text'] 	= $item['alt_text'];
				$items[$item['id']]['catalog_text']	= $item['catalog_text'];
				$items[$item['id']]['file'] 		= $item['file'];
				$items[$item['id']]['mime_type']	= $item['mime_type'];
				$items[$item['id']]['large_image'] 	= $item['large_image'];
				$items[$item['id']]['medium_image'] = $item['medium_image'];
				$items[$item['id']]['small_image'] 	= $item['small_image'];
				
				// Get file path info
				$path = pathinfo($item['file']);
				
				// Set file name var
				$items[$item['id']]['file_name'] 	= $path['basename'];
				
				// Set file extension var
				$items[$item['id']]['extension'] 	= $path['extension'];
				
			}
			
			$items['listing'] = $items;
			
			$items['pagination'] = FALSE;
			
			// Return items
			return $items;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Attaches media IDs to database rows that support links
	  *
	  * Appends comma delimited list of media IDs to the media_links column of the specified table
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_category Name of the table that contains the record you want to append.
	  * @param int $item_id ID of the record you want to append.
	  * @param array $media Array of media IDs to apend.
	  *
	  * @return boolean $update True on sucess, False on fail
	  *
	  */
	function appendMediaIds ($item_category, $item_id, $media) {
		
		$table = unescape($item_category);
		$id = vempty($item_id);
		$media_items = NULL;
		$existing_ids = NULL;
		
		// Force a valid table name
		switch ($table) {
			
			case 'articles' :
				$table = 'articles';
				$id_col = 'id';
				break;
			case 'products' :
				$table = 'products';
				$id_col = 'upc';
				break;
			case 'instances' :
				$table = 'instances';
				$id_col = 'id';
				break;
				
			default :
				return FALSE;
				
		}
		
		// Get existing media links
		if ($existing_ids = runQuery("
			
			SELECT
				media_links
				
			FROM
				$table
				
			WHERE
				$id_col = $id
				
		")) {
			
			if (strlen($existing_ids[0]) >= 1) {
				
				// Prepend results to string
				$media_items = $existing_ids[0] . ',';
				
			}
			
		}
		
		// Loop through new media links, validate and append them to string
		foreach ($media as $item) {
			
			$media_items .= vbool($item) . ',';
			
		}
		
		// Trim commas and validate
		$media_links = vempty(trim($media_items,','),1,0);
		
		// Insert the string into the media table
		if ($update = runQuery("
			
			UPDATE
				$table
				
			SET
				media_links = $media_links
				
			WHERE
				$id_col = $id
				
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Get an array of media_links for an item
	  *
	  * Selects comma separated list of media_links IDs from the specified item row and returns them as an array
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_table The table where the item lives.
	  *
	  * @return int $item_id The ID of the item in the row
	  *
	  */
	function getItemMediaLinks ($item_table, $item_id, $ad_id = FALSE) {
		
		// Declare vars
		$links = NULL;
		$table = NULL;
		$id = vbool($item_id);
		
		// Need to specify ad_id for instances
		$ad_id = vbool($ad_id);
		$ad_clause = NULL;
		
		// Force a valid table name
		switch ($item_table) {
			
			case 'articles' :
				$table = 'articles';
				$id_col = 'id';
				break;
				
			case 'products' :
				$table = 'products';
				$id_col = 'upc';
				break;
				
			case 'instances' :
				$table = 'instances';
				$id_col = 'family_group';
				
				// Append ad_id to WHERE clause
				$ad_clause = ' AND ad_id = ' . $ad_id;
				break;
				
			default :
				return FALSE;
				
		}
		
		// Select the links
		if ($item = runQuery("
			
			SELECT
				media_links
				
			FROM
				$table
				
			WHERE
				$id_col = $id
				$ad_clause
				
		")) {
			
			// Convert to an array
			$links = explode(',', $item[0]);
			
			// Return results
			return $links;
			
		} else {
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Parse Media Source
	  *
	  * Converts YouTube, Vimeo, Soundcould, Spotify, Apple Music, and Bandcamp links to 
	  * iframe source url
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $uri The URI of the media resource.
	  *
	  * @return string $src The converted source URI.
	  *
	  */
	function parseMediaSource ($uri) {
		
		$src = NULL;
		
		// Parse YouTube
		if (strstr($uri, 'youtu')) {
			
			$src = str_replace(array('watch?v=', 'https://youtu.be/'),array('embed/', 'https://www.youtube.com/embed/'),$uri);
			
		// Parse Vimeo
		} else if (strstr($uri, 'vimeo')) {
			
			$src = str_replace('vimeo.com/', 'player.vimeo.com/video/', $uri . '?title=0&byline=0&portrait=0');
			
		} else if (strstr($uri, 'soundcloud')) {
			
			$src = 'https://w.soundcloud.com/player/?url=' . $uri . '&color=%23ff5500&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=true';
			
		// Parse Bandcamp
		} else if (strstr($uri, 'bandcamp.com')) {
			
			$attr = array();
			$parts = explode('/', $uri);
			
			foreach ($parts as $part) {
				
				if (strstr($part, 'album=')) {
					
					$attr['album'] = $part;
					
				} else if (strstr($part, 'track=')) {
					
					$attr['track'] = $part;
					
				}
				
			}
			
			$src = 'https://bandcamp.com/EmbeddedPlayer/' . $attr['album'] . '/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/' . $attr['track'] . '/transparent=true/';
			
		// Parse Spotify
		} else if (strstr($uri, 'spotify')) {
			
			$src = 'https://embed.spotify.com/?uri=' . $uri;
			
		// Parse Apple Music
		} else if (strstr($uri, 'apple')) {
			
			$parts = parse_url($uri); 
			parse_str($parts['query'], $string); 
			$song_id = $string['i'];
			
			$src = 'https://tools.applemusic.com/embed/v1/song/' . $song_id . '?country=us';
			
		}
		
		return $src;
		
	}
