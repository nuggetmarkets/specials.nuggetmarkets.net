<?php


	/**
	  * Delete item record
	  *
	  * Deletes specified item from the specified table of the database
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_table Validated table name.
	  * @param int $item_id Validated item ID.
	  *
	  * @return boolean TRUE/FALSE
	  *
	  */
	function deleteItem ($item_table, $item_id) {
		
		if ($delete = runQuery("
			
			DELETE FROM
				$item_table
				
			WHERE
				id = $item_id
				
			LIMIT 1
			
		")) {
			
			return TRUE;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select ad description
	  *
	  * Selects specified ad record from database to display a deletion confirmation message
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $ad_id Validated article ID.
	  *
	  * @return array $ad Name of the article
	  *
	  */
	function selectAdDescription ($ad_id) {
		
		if ($ad = runQuery("
			
			SELECT
				CONCAT(store_chain, ' ', DATE_FORMAT(date_from, '%c/%d/%Y')) AS `name`,
				date_from
				
			FROM
				ads
				
			WHERE
				id = $ad_id
				
		")) {
			
			return $ad[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select media description
	  *
	  * Selects specified media item from database to display a deletion confirmation message.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $media_id Validated article ID.
	  *
	  * @return array $media Description of the media item
	  *
	  */
	function selectMediaDescription ($media_id) {
		
		if ($media = runQuery("
			
			SELECT
				alt_text AS `name`,
				file,
				small_image,
				mime_type
				
			FROM
				media
				
			WHERE
				id = $media_id
				
		")) {
			
			return $media[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select retraction description
	  *
	  * Selects specified retraction item from database to display a deletion confirmation message.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $retraction_id Validated article ID.
	  *
	  * @return array $retraction Description of the retraction item
	  *
	  */
	function selectRetractionDescription ($retraction_id) {
		
		if ($retraction = runQuery("
			
			SELECT
				name AS `name`,
				description
				
			FROM
				retractions
				
			WHERE
				id = $retraction_id
				
		")) {
			
			return $retraction[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Select label description
	  *
	  * Selects specified label item from database to display a deletion confirmation message.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $label_id Validated label ID.
	  *
	  * @return array $label Description of the retraction item
	  *
	  */
	function selectLabelDescription ($label_id) {
		
		if ($label = runQuery("
			
			SELECT
				name AS `name`,
				description
				
			FROM
				labels
				
			WHERE
				id = $label_id
				
		")) {
			
			return $label[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select secret special image description
	  *
	  * Selects specified secret special item from database to display a deletion confirmation message.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $ad_date The date of the secret special to return.
	  *
	  * @return array $secret_special_image Description of the retraction item
	  *
	  */
	function selectSecretSpecialImageDescription ($ad_date) {
		
		if ($secret_special_image = runQuery("
			
			SELECT
				id,
				secret_special_title,
				secret_special_image
				
			FROM
				ads
				
			WHERE
				date_from = $ad_date
				
		")) {
			
			return $secret_special_image;
			
		} else {
		
			return FALSE;
			
		}
		
	}


	/**
	  * Select banner description
	  *
	  * Selects specified banner item from database to display a deletion confirmation message.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $banner_id Validated label ID.
	  *
	  * @return array $banner Description of the retraction item
	  *
	  */
	function selectBannerDescription ($banner_id) {
		
		if ($banner = runQuery("
			
			SELECT
				banners.name AS `name`,
				banners.date_from, 
				banners.date_through
				
			FROM
				banners
				
			WHERE
				banners.id = $banner_id
				
		")) {
			
			return $banner[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select items with linked media
	  *
	  * Selects rows from the specified table with a link to specified media item
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $item_table The table to look for linked items in.
	  * @param int $item_id The ID of the media item.
	  *
	  * @return array $links An array of article ids and media link strings
	  *
	  */
	function selectItemsWithLinkedMedia ($item_table, $item_id) {
		
		// Set ID column for the where clause
		if ($item_table == 'products') {
			$column = 'upc';
		} else {
			$column = 'id';
		}
		
		if ($links = runQuery("
			
			SELECT
				$column as `id`,
				media_links,
				feature_image
				
			FROM
				$item_table
				
			WHERE
				media_links LIKE '$item_id,%' OR
				media_links LIKE '%,$item_id,%' OR
				media_links LIKE '%,$item_id' OR
				media_links LIKE '$item_id'
				
		")) {
			
			return $links;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select items with linked labels
	  *
	  * Selects rows from the specified table with a links to specified labels
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $item_table The table to look for linked items in.
	  * @param int $item_id The ID of the label item.
	  *
	  * @return array $links An array of item ids and label link strings
	  *
	  */
	function selectItemsWithLinkedLabels ($item_table, $item_id) {
		
		// Set ID column for the where clause
		if ($item_table == 'products') {
			$column = 'upc';
		} else {
			$column = 'id';
		}
		
		if ($links = runQuery("
			
			SELECT
				$column as `id`,
				labels
				
			FROM
				$item_table
				
			WHERE
				labels LIKE '$item_id,%' OR
				labels LIKE '%,$item_id,%' OR
				labels LIKE '%,$item_id' OR
				labels LIKE '$item_id'
				
		")) {
			
			return $links;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Remove medial links from table
	  *
	  * Updates media links and feature image columns for specified table row
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $table Table to update.
	  * @param string $id Row ID of table to update.
	  * @param string $media_links Comma separated list of remaining media links.
	  * @param int $featured_image ID of featured image or NULL if removed
	  *
	  * @return boolean $update TRUE/FALSE
	  *
	  */
	function updateMediaLinks ($table, $id, $media_links, $featured_image) {
		
		// Set ID column for the where clause
		if ($table == 'products') {
			$column = 'upc';
		} else {
			$column = 'id';
		}
		
		if ($update = runQuery("
			
			UPDATE
				$table
				
			SET
				media_links = $media_links,
				feature_image = $featured_image
				
			WHERE
				$column = $id
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
		return $update;
		
	}


	/**
	  * Remove medial links from table
	  *
	  * Updates label links columns for specified table row
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $table Table to update.
	  * @param string $id Row ID of table to update.
	  * @param string $label_links Comma separated list of remaining label links.
	  *
	  * @return boolean $update TRUE/FALSE
	  *
	  */
	function updateLabelLinks($table, $id, $label_links) {
		
		// Set ID column for the where clause
		if ($table == 'products') {
			$column = 'upc';
		} else {
			$column = 'id';
		}
		
		if ($update = runQuery("
			
			UPDATE
				$table
				
			SET
				labels = $label_links
				
			WHERE
				$column = $id
				
		")) {
			
			return $update;
			
		} else {
			
			return FALSE;
			
		}
		
		return $update;
		
	}


	/**
	  * Unlink media files
	  *
	  * Deletes all linked file assets from a media item based on row ID in the media table.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $file Path to the file.
	  *
	  * @return boolean $unlink
	  *
	  */
	function unlinkMediaFiles ($item_id) {
		
		$item = NULL;
		$unlink = FALSE;
		
		// Get file paths from DB
		if ($item = runQuery("
		
			SELECT
				file,
				mime_type,
				large_image,
				medium_image,
				small_image
				
			FROM
				media
				
			WHERE
				id = $item_id
				
		")) {
			
			// Unlink file
			$unlink = unlink(MEDIA_UPLOAD_DIR . $item[0]['file']);
			
			// Unlink smaller versions of images if mime_type is image
			if (strstr($item[0]['mime_type'], 'image')) {
				
				unlink(MEDIA_UPLOAD_DIR . $item[0]['large_image']);
				unlink(MEDIA_UPLOAD_DIR . $item[0]['medium_image']);
				unlink(MEDIA_UPLOAD_DIR . $item[0]['small_image']);
				
			}
			
		}
		
		return $unlink;
		
	}


	/**
	  * Unlink label files
	  *
	  * Deletes all linked file assets from a media item based on row ID in the media table.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $file Path to the file.
	  *
	  * @return boolean $unlink
	  *
	  */
	function unlinkLabelFiles ($item_id) {
		
		$item = NULL;
		$unlink = FALSE;
		
		// Get file paths from DB
		if ($item = runQuery("
			
			SELECT
				name, 
				type
				
			FROM
				labels
				
			WHERE
				id = $item_id
				
		")) {
			
			if ($item[0]['type'] == 'seasoning') {
				$file_name = 'seasoning-' . $item[0]['name'] . '.png';
			} else {
				$file_name = $item[0]['name'] . '.png';
			}
			
			// Unlink file
			$unlink = unlink('../httpdocs/img/labels/' . $file_name);
			
		}
		
		return $unlink;
		
	}
