<?php
	
	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'MediaModel.php';
	require_once 'AdsController.php';
	require_once 'LabelsController.php';

	// Get upcoming ads
	$ads = getUpcomingAds();

	if (is_array($ads)) {
		
		// Loop through ads
		foreach ($ads['listing'] as $ad) {
			
			// Declare features array
			$features = array();
			
			// Validate ad ID
			$ad_id = vbool($ad['id']);
			
			// Assign ad date to output
			$output = $ad;
			
			// Format duration
			$output['ad_duration'] = convertDatesToDuration($ad['date_from'], $ad['date_through']);
			
			// Set up file names and directory structure for cache and PDF version
			$store_slug = formatSlug($ad['store_chain']);
			$year = date('Y',strtotime($ad['date_from']));
			
			$cache_file_name = $store_slug . '-' . $ad['date_from'] . '.json';
			$cache_file_path = SERVER_PATH . '/httpdocs/cache/' . $store_slug . '/' . $year . '/';
			
			$pdf_file_name = $store_slug . '-specials-starting-' . $ad['date_from'] . '.pdf';
			$pdf_file_path = SERVER_PATH . '/httpdocs/print-versions/';
			
			// Get a list of departments with items in the ad
			$departments = selectDepartmentsByAd ($ad['id']);
			
			// See if print ad exists by getting file size
			if ($filesize = @filesize($pdf_file_path . $pdf_file_name)) {
				
				// Set file name
				$output['pdf']['file'] = '/print-versions/' . $pdf_file_name;
				
				// Round file size to MB
				$output['pdf']['size'] = round($filesize/1000000, 1) . ' MB';
				
			} else {
				
				$output['pdf'] = NULL;
				
			}
			
			// Get labels
			$output['labels'] = getLabels(0,9999);
			
			// Set up ad departments array
			$output['departments'] = array();
			
			// If ad exists		
			if (!empty($ad['date_from'])) {
				
				// Append the features to the ad array 
				$features = getWeeklyAd($ad_id, $ad['date_from'], FALSE, TRUE);
				
				if (is_array($features)) {
					if (count($features) >= 1) {
						$output['departments']['features'] = $features;
					}
				}
				
				// Loop through departments and append the products
				foreach ($departments as $department) {
					$output['departments'][$department] = getWeeklyAd($ad_id, $ad['date_from'], $department);
				}
				
			}
			
			// Encode JSON
			$json = json_encode($output, JSON_PRETTY_PRINT);
			
			// Create directory if nessasary
			if (!is_dir($cache_file_path)) {
				mkdir($cache_file_path, 0755, TRUE);
			}
			
			$file = $cache_file_path . $cache_file_name;
			
			// Write JSON to cache file
			file_put_contents($file, $json);
			
			chown($file, 'nuggetmarket');
			chgrp($file, 'psacln');
			
		}
		
	}
	
	if (ENVIRONMENT != 'dev') {
		
		echo 'Auto export completed at ' . date('Y-m-d H:i:s') . '.';
		
	}
