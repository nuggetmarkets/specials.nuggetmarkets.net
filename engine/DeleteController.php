<?php

	// Include dependencies
	require_once 'DeleteModel.php';
	
	/**
	  * Get item for deletion
	  *
	  * Gets name of item from database based on table and id
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_table Table to select from.
	  * @param string $item_id ID of the record to select
	  *
	  * @return array $item Array of item attibutes: name, table, and ID passed to it.
	  *
	  */
	function getItemName ($item_table, $item_id) {
		
		// Validate input
		$id = vbool($item_id);
		$item = NULL;
		$table = NULL;
		$singular = NULL;
		$name = FALSE;
		
		$item['referer'] = $_SERVER['HTTP_REFERER'];
		
		// Force a valid table name
		switch ($item_table) {
			
			case 'ads' :
				
				// Select article info
				$name = selectAdDescription($id);
				$table = 'ads';
				$singular = 'ad';
				break;
				
			case 'media' :
				
				// Select media info
				$name = selectMediaDescription($id);
				$table = 'media';
				
				// Grab relivant portion of substring for button text
				if (strstr($name['mime_type'],'/',TRUE) == 'application') {
					$singular = 'document';
				} else {
					$singular = strstr($name['mime_type'],'/',TRUE);
				}
				
				break;
				
			case 'upc-image' :
				
				$ex = explode('__', $item_id);
				
				// Select article info
				$name['name'] = $ex[0] . '.' . $ex[1];
				$name['small_image'] = '../img/upc/' . $name['name'];
				$id = $name['name'];
				$table = 'upc-image';
				$singular = 'UPC image';
				break;
				
			case 'retractions' :
				
				// Select article info
				$name = selectRetractionDescription($id);
				$table = 'retractions';
				$singular = 'retraction';
				$item['referer'] = '/retractions/';
				break;
				
			case 'labels' :
				
				// Select label info
				$name = selectLabelDescription($id);
				$table = 'labels';
				$singular = 'label';
				$item['referer'] = '/labels/';
				break;
				
			case 'banners' :
				
				// Select banner info
				$name = selectBannerDescription($id);
				$table = 'banners';
				$singular = 'banner';
				$item['referer'] = '/banners/';
				break;
				
			case 'print' :
				
				// Imediately delete print ad and set confirmation or error
				if (unlink($_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/print-versions/' . $item_id . '.pdf')) {
					$_SESSION['confirm'] = 'Print version deleted.';
				} else {
					$_SESSION['error'] = 'Print version was not deleted.';
				}
				
				header("Location: {$_SERVER['HTTP_REFERER']}");
				exit();
				break;
				
			default :
				return FALSE;
				
		}
		
		$item['description'] = $name;
		$item['category'] = $table;
		$item['singular'] = $singular;
		$item['id'] = $id;
		
		if ($name) {
			
			return $item;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Delete item
	  *
	  * Validates item and table and removes record from database
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $item_table Table to select from.
	  * @param string $item_id ID of the record to select
	  *
	  * @return array $item Array containing table and category if removed item for messaging.
	  *
	  */
	function deleteRecord ($item_table, $item_id) {
		
		// Validate input
		$id = vbool($item_id);
		$table = FALSE;
		$item = NULL;
		$delete = FALSE;
		
		// Allow deletion if approved table name table
		if ($table = vtablename($item_table)) {
			
			// Delete linked files if media table item
			if ($table == 'media') {
				$count = removeMediaLinks($id);
				unlinkMediaFiles($id);
			}
			
			// Delete foreign items from instances and retractions table
			if ($table == 'ads') {
				
				if ($ad = runQuery("
					
					SELECT
						store_chain,
						date_from,
						secret_special_image
						
					FROM
						ads
						
					WHERE
						id = $id
						
				")) {
					
					// Delete print version
					unlink($_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/print-versions/' . formatSlug($ad[0]['store_chain']) . '-specials-starting-' .  $ad[0]['date_from'] . '.pdf');
					unlink($_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/media/' . $ad[0]['secret_special_image']);
					
				}
				
			}
			
			// Delete label images
			if ($table == 'labels') {
				$count = removeLabelLinks($id);
				unlinkLabelFiles($id);
			}
			
			// Delete the record
			$delete = deleteItem($table, $id);
			
		} else if ($item_table == 'upc-image') {
			
			unlink($_SERVER['DOCUMENT_ROOT'] . '/../httpdocs/img/upc/' . $item_id);
			
			$delete['category'] = 'products/edit';
			$delete['item_id'] = $item_id;
		}
		
		// Set return values
		$item['category'] = $item_table;
		$item['singular'] = rtrim($item_table, 's');
		$item['count'] = $count;
		
		if ($delete) {
			
			return $item;
			
		} else {
			
			return FALSE;
			
		}
		
	}
	
	
	/**
	  * Remove links to media items
	  *
	  * Removes links to media items in foreign tables such as articles.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $item_id The ID of the media item to remove.
	  *
	  * @return int $count Total number of items the attachment was removed from.
	  *
	  */
	function removeMediaLinks ($item_id) {
		
		// Validate ID/declare vars
		$id = vbool($item_id);
		$exp = NULL;
		$item = NULL;
		$media_links = NULL;
		$media_items = NULL;
		$count = 0;
		
		$media_tables = explode(',', TABLES_WITH_MEDIA_LINKS);
		
		// Loop through all tables that have media links
		foreach ($media_tables as $table) {
			
			// Check for article links
			if ($links = selectItemsWithLinkedMedia($table, $id)) {
				
				// Loop through articles
				foreach ($links as $link) {
					
					// Explode links into array
					$exp = explode(',', $link['media_links']);
					
					// Loop through array
					foreach ($exp AS $item) {
						
						// Rebuild array without the id
						if ($item != $id) {
							$media_items .= $item . ',';
						}
						
					}
					
					// Trim commas and validate
					$media_links = vempty(trim($media_items,','),0,0);
					$link_id = vempty($link['id'],0,0);
					
					// Check for featured image and reset if nessary
					if ($link['feature_image'] == $id) {
						$featured_image = 'NULL';
					} else {
						$featured_image = vempty($link['feature_image'],0,0);
					}
					
					$update_links = updateMediaLinks($table, $link_id, $media_links, $featured_image);
					
					unset($media_links);
					unset($media_items);
					unset($feaured_image);
					unset($exp);
					
				}
				
				$count++;
				
			}
			
		}
		
		return $count;
		
	}
	
	
	/**
	  * Remove links to labels
	  *
	  * Removes links to labels in foreign tables products and instances.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $item_id The ID of the media item to remove.
	  *
	  * @return int $count Total number of items the attachment was removed from.
	  *
	  */
	function removeLabelLinks ($item_id) {
		
		// Validate ID/declare vars
		$id = vbool($item_id);
		$exp = NULL;
		$item = NULL;
		$label_links = NULL;
		$label_items = NULL;
		$count = 0;
		
		$media_tables = explode(',', TABLES_WITH_MEDIA_LINKS);
		
		// Loop through all tables that have media links
		foreach ($media_tables as $table) {
			
			// Check for article links
			if ($links = selectItemsWithLinkedLabels($table, $id)) {
				
				// Loop through articles
				foreach ($links as $link) {
					
					// Explode links into array
					$exp = explode(',', $link['labels']);
					
					// Loop through array
					foreach ($exp AS $item) {
						
						// Rebuild array without the id
						if ($item != $id) {
							$label_items .= $item . ',';
						}
						
					}
					
					// Trim commas and validate
					$label_links = vempty(trim($label_items,','),0,0);
					$link_id = vempty($link['id'],0,0);
					
					$update_label_links = updateLabelLinks($table, $link_id, $label_links);
					
					unset($label_links);
					unset($label_items);
					unset($exp);
					
				}
				
				$count++;
				
			}
			
		}
		
		return $count;
		
	}
