<?php

    // Include dependencies
    require_once 'RetractionsModel.php';


    /**
	  * Gets Retractions
	  *
	  * Gets paginated list of retractions in chronological order
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit How many results to return.
	  *
	  * @return array $items An array of retraction objects
	  *
	  */
	function getRetractions ($page = 1, $limit = 25) {

		// Set page to 1 if no page is not set
		if (!$page) {
			$page = 1;
		}

		$page = vbool($page);
		$limit = vbool($limit);

		// Calculate page offset
		$offset = $page * $limit - ($limit);

		if ($retractions = selectRetractions ($offset, $limit)) {

			foreach ($retractions['listing'] as $item) {

				// Create array of results
				$items[$item['id']]['id']			= $item['id'];
				$items[$item['id']]['name']			= $item['name'];
				$items[$item['id']]['description']	= $item['description'];
				$items[$item['id']]['store_chain']	= $item['store_chain'];
				$items[$item['id']]['date_from']	= $item['date_from'];
				$items[$item['id']]['date_through']	= $item['date_through'];

				if (strtotime($item['date_from']) <= time() && strtotime($item['date_through']) >= time()) {
					$items[$item['id']]['current_ad'] = 1;
				} else {
					$items[$item['id']]['current_ad'] = 0;
				}

			}

			$items['listing'] = $items;

			$items['pagination'] = pagination($page, $limit, $retractions['count']);

			return $items;

		} else {

			$retractions = FALSE;

		}

	}

	/**
	  * Gets Retraction
	  *
	  * Gets as specific retaction
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $page Current page number.
	  * @param int $limit How many results to return.
	  *
	  * @return array $items An array of retraction objects
	  *
	  */
	function getRetraction ($retraction_id) {

		// Validate retraction id
		$id = vbool($retraction_id);

		if ($retraction = selectRetractionById ($id)) {

			return $retraction;

		} else {

			$retraction = FALSE;

		}

	}

