<?php
	
	/**
	  * Select Product By UPC
	  *
	  * Slects product data from the database with the specified UPC
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $upc UPC code of the product you want to retrieve.
	  *
	  * @return array $result[0] An array of product attributes
	  *
	  */
	function selectProductByUpc ($upc) {
		
		if ($result = runQuery("
			
			SELECT
				*
				
			FROM
				products
				
			WHERE
				upc = $upc
				
		")) {
			
			return $result[0];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Insert Product
	  *
	  * Inserts a new row into the products table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of product data.
	  *
	  * @return int $product['insert_id'] The mysql_insert_id of the record
	  *
	  */
	function insertProduct($data) {
		
		if ($product = runQuery("
			
			INSERT INTO
				products
				
			SET
				upc 			= {$data['upc']},
				family_group 	= {$data['family_group']},
				pos_name 		= {$data['pos_name']},
				lifestyle 		= {$data['lifestyle']},
				department 		= {$data['department']},
				prefix 			= {$data['prefix']},
				display_name 	= {$data['display_name']},
				suffix 			= {$data['suffix']},
				note 			= {$data['note']},
				description 	= {$data['description']},
				retail_price 	= {$data['retail_price']},
				size 			= {$data['size']},
				unit 			= {$data['unit']},
				container 		= {$data['container']},
				`limit` 		= {$data['limit']},
				labels 			= {$data['labels']}
				
		")) {
			
			return $data['upc'];
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Update Product
	  *
	  * Updates a record in the products table
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param array $data An array of product data.
	  *
	  * @return array $result The id of the product record that was updated
	  *
	  */
	function updateProduct ($data) {
		
		if ($product = runQuery("
			
			UPDATE
				products
				
			SET
				prefix 			= {$data['prefix']},
				display_name 	= {$data['display_name']},
				suffix 			= {$data['suffix']},
				note 			= {$data['note']},
				description 	= {$data['description']},
				retail_price 	= {$data['retail_price']},
				size 			= {$data['size']},
				unit 			= {$data['unit']},
				container 		= {$data['container']},
				`limit` 		= {$data['limit']},
				labels 			= {$data['labels']}
				
			WHERE
				upc = {$data['upc']}
				
		")) {
			
			$result = array('upc' => $data['upc']);
			
			return $result;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * A natural language search
	  *
	  * Selects rows from products table using FULLTEXT index
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $terms The search terms to match against.
	  *
	  * @return array $result All search results in an assoc. array
	  *
	  */
	function selectProductsMatchAgainstTerms ($terms) {
		
		if ($result = runQuery("
			
			SELECT
				*
				
			FROM
				products
				
			WHERE
				
				MATCH (
					upc,
					prefix,
					pos_name,
					suffix,
					note,
					family_group,
					display_name
				) AGAINST (
					$terms
				)
				
		")) {
			
			return $result;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * A litteral search
	  *
	  * Selects rows from the products table using LIKE. This should be used when
	  * search_terms string is too short for a FULLTEXT index search.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $terms The search terms to match against.
	  *
	  * @return array $result All search results in an assoc. array
	  *
	  */
	function selectProductsLikeTerms ($terms) {
		
		$search_terms = unescape($terms);
		
		if ($result = runQuery("
			
			SELECT
				*
				
			FROM
				products
				
			WHERE
				
				(
					upc LIKE '%$search_terms%' OR
					prefix LIKE '%$search_terms%' OR
					pos_name LIKE '%$search_terms%' OR
					suffix LIKE '%$search_terms%' OR
					
					note LIKE '%$search_terms%' OR
					family_group LIKE '%$search_terms%' OR
					display_name LIKE '%$search_terms%'
				)
				
		")) {
			
			return $result;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Select Product By Family Group
	  *
	  * Slects product data from the database with the specified UPC
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $family_group The family group name.
	  *
	  * @return array $products An array of products that match the family group name
	  *
	  */
	function selectProductsByFamilyGroup ($family_group) {
		
		if ($products = runQuery("
			
			SELECT
				*
				
			FROM
				products
				
			WHERE
				family_group = $family_group
				
		")) {
			
			return $products;
			
		} else {
			
			return FALSE;
			
		}
		
	}
