<?php 

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'BannersController.php';
	
	if (!empty($_GET['date'])) {
		$banner_date = $_GET['date'];
		$year = date('Y', strtotime($_GET['date']));
	} else {
		$banner_date = date('Y-m-d');
		$year = date('Y');
	}
	
	// Get currently running banners
	$banners = getRunningBanners($banner_date);
	
	// Set up file name and directory
	$cache_file_name = 'banners-' . $banner_date . '.json';
	$cache_file_path = SERVER_PATH . '/httpdocs/cache/banners/' . $year . '/';
	
	// Concat the file name and path
	$file = $cache_file_path . $cache_file_name;
		
	// Create cache file if there are results
	if (is_array($banners)) {
		
		// Encode output
		$json = json_encode($banners, JSON_PRETTY_PRINT);
		
		// Create directory if nessasary
		if (!is_dir($cache_file_path)) {
			mkdir($cache_file_path, 0755, TRUE);
		}
		
		// Write JSON to cache file
		file_put_contents($file, $json);
		
		// Set ownership and permissions
		chown($file, 'nuggetmarket');
		chgrp($file, 'psacln');
		
	} else if (file_exists($file)) {
		
		unlink($file);
		
	}
