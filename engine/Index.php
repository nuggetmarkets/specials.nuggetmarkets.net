<?php

	// Include depenencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';


	// Get listing
	$page_attrs['title'] = "Dashboard";
	$page_attrs['class'] = 'dashboard';


	// Handle reply
	if (isset($_POST['send_reply'])) {
		
		$status_changed_message = NULL;
		
		// Validate input
		$item['ad_id'] 			= vbool($_POST['ad_id']);
		$item['department'] 	= vempty($_POST['department'], 1);
		$item['item_id'] 		= vempty($_POST['item_id'], 1);
		$item['item_name'] 		= vempty($_POST['item_name'], 1, 1);
		$item['family_group'] 	= (string)vempty($_POST['family_group'], 0, 0);
		$item['group_name'] 	= vempty($_POST['group_name'], 1, 1);
		$item['message'] 		= vempty($_POST['message'], 1, 1);
		$item['posted_by'] 		= vempty($_SESSION['full_name']);
		$item['posted_email'] 	= vemail($_SESSION['email']);
		
		//  Set item name for notification email
		if (isset($_POST['item_name'])) {
			$display_name = $_POST['item_name'];
		} else if (isset($_POST['group_name'])) {
			$display_name = $_POST['group_name'];
		} else {
			$display_name = $_POST['family_group'];
		}
		
		// Create message
		$message_id = createAdItemMessage($item);
		
		// Update status if new status is different
		if ($_POST['current_status'] != $_POST['status']) {
			
			// Validate status
			$item['status'] = vempty($_POST['status']);
			
			// Change item status
			changeAdItemStatus($item);
			
			$status_changed_message = ", and item status has been changed to {$_POST['status']}";
			
		}
		
		// Check for reply to
		if (isset($_POST['message_reply_to'])) {
			
			// Override notification emails for dev environment
			if (ENVIRONMENT == 'dev') {
				$addresses = array('Mark Eagleton <mark.eagleton@nuggetmarket.com>');
			} else {
				$addresses = array("{$_POST['message_reply_to']}");
			}
			
			// Set up notification message
			$notification_email_id = '5c6c4445-d8bb-46f7-ae64-6b97efb500cf';
			$notification_message = array(
				"To" => $addresses,
				"Data" => array(
					'item_name' => $display_name,
					'item_link' => SPECIALS_URL . "/proofs/{$_POST['ad_date']}/{$_POST['department']}/#item-{$_POST['item_id']}",
					'message' => $_POST['message'],
					'from' => $_SESSION['full_name']
				)
			);
			
			sendCMMail($notification_message, $notification_email_id, 'Unchanged', false);
			
		}
		
		$_SESSION['confirm'] = 'Reply has been sent' . $status_changed_message . '.';
		
		//  Redirect
		header("Location: /#message-$message_id");
		exit();
		
	// Handle dismiss message
	} else if (isset($_POST['message'])) {
		
		// Explode message ID
		$message_id = explode('-',$_POST['message']);
		
		// Dismiss the message
		dismissMessage($message_id[1]);
		
	} else if (!empty($_GET['action']) && $_GET['action'] == 'dismiss') {
		
		// Get current messages based on login
		if ($_SESSION['user_level'] == 1 && in_array('ads', $_SESSION['access'])) {
			$messages = getCurrentMessages();
		} else {
			$messages = getUserMessages();
		}
		
		// Loop through message threads
		foreach ($messages as $thread) {
			
			// Loop through the messages in thread
			foreach ($thread['messages'] as $message) {
				
				// Dismiss the message if status is approved 
				if ($message['status'] == 'approved') {
					dismissMessage($message['id']);
				}
			}
		}
		
		// Set confirmation and redirect
		$_SESSION['confirm'] = 'Approved item notifications dismissed.';
		header("Location: /");
		exit();
		
	}


	// Get current messages based on login
	if ($_SESSION['user_level'] == 1 && in_array('ads', $_SESSION['access'])) {
		
		$messages = getCurrentMessages();
		
	} else {
		
		$messages = getUserMessages();
		
	}

	// die(var_dump($messages));
