<?php

	function getUserByEmail ($email_address) {
		
		if ($user = runQuery("

            SELECT
				users.id,
				users.email,
				users.password, 
				users.token, 
				users.token_date, 
				users.date_created,
				users.last_login,
				users.disabled, 
				users.first_name,
				users.last_name, 
				users.avatar, 

				user_access.access
								
			FROM
				users
				
			LEFT JOIN
				user_access ON user_access.user_id = users.id AND 
				user_access.website = 'specials.nuggetmarkets.net'

			WHERE
				users.email = $email_address 

        ", 'profiles')) {

			return $user[0];

		} else {

			return FALSE;

		}
	}
	
	function getUserById ($user_id) {

		if ($user = runQuery("

            SELECT
                id,
                email,
				password, 
				token, 
				token_date, 
				date_created,
                last_login,
				disabled, 
				first_name,
				last_name, 
				sonomamarkets_access

            FROM
                users

            WHERE
                id = $user_id

        ", 'profiles')) {

			return $user[0];

		} else {

			return FALSE;

		}
	}
	
	function getUsers () {
		
		if ($users = runQuery("

			SELECT
				id,
				email,
				last_login,
				first_name,
				last_name, 
				disabled

			FROM
				users

			ORDER BY
				first_name,
				last_name

		", 'profiles')) {

			return $users;

		} else {

			return FALSE;

		}
		
	}

	function updateLastLogin ($user_id) {

		runQuery("
			UPDATE
				users
			SET
				last_login = NOW()
			WHERE
				id = $user_id
		", 'profiles');

	}

	function insertUser ($user) {

        if (checkDuplicateUser($user['email'])) {

            return FALSE;

        }

        $token = vempty(token(), 1, 0);

        if ($insert = runQuery("

            INSERT INTO
                users

            SET
                email = {$user['email']}, 
                password = {$user['password']}, 
                token = $token,
                date_created = NOW(), 
                first_name = {$user['first_name']}, 
                last_name = {$user['last_name']}

        ", 'profiles')) {

            return $insert;

        }

    }
	
	function updateUserContactInfo ($profile) {
		
        $user = runQuery("

            UPDATE
                users

            SET
                email = {$profile['email']},
                first_name = {$profile['first_name']},
                last_name = {$profile['last_name']}, 
                disabled = {$profile['disabled']}
               
			WHERE
				id = {$profile['user_id']}

        ", 'profiles');

        return $user;
		
	}

    function checkDuplicateUser ($email) {

        $user = runQuery("

            SELECT
                id

            FROM
                users

            WHERE
                email = $email

        ", 'profiles');

        if ($user[0]['id']) {

            return TRUE;

        } else {

            return FALSE;

        }

    }
    