<?php

	/**
	  * Run MySQL Query
	  *
	  * Runs a MySQL query
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $query String containing the SQL.
	  * @param string $database Database name to run the query on
	  *
	  * @return array|boolean $ar Returns results form select query as array else TRUE/FALSE
	  *
	  */
	function runQuery ($query, $database = 'specials') {
		
		// Require connection info
		require_once('DbConnect.php');
		
		// Create db link
		if ($database == 'profiles') {
			
			// Nugget Web Profiles database
			$link = mysqli_connect(PROFILE_HOST, PROFILE_USER, PROFILE_PASSWORD, PROFILE_NAME);
			
		} else if ($database == 'nugget') {
			
			// Nugget Website database
			$link = mysqli_connect(NUGGET_HOST, NUGGET_USER, NUGGET_PASSWORD, NUGGET_NAME);
			
		} else if ($database == 'sonoma') {
			
			// Sonoma Website database
			$link = mysqli_connect(SONOMA_HOST, SONOMA_USER, SONOMA_PASSWORD, SONOMA_NAME);
			
		} else {
			
			// Weekly Specials database
			$link = mysqli_connect(SPECIALS_HOST, SPECIALS_USER, SPECIALS_PASSWORD, SPECIALS_NAME);
			
		}
		
		// Throw connection error
		if (mysqli_connect_errno()) {
			echo ("Connect failed: %s\n". mysqli_connect_error());
			exit();
		}
		
		// Run query
		if ($result = mysqli_query($link, $query)) {
			
			// Build results array if returning rows
			if (@mysqli_num_rows($result) >= 1) {
				
				$ar = array();
				
				// Loop through results
				while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
					
					// Force multiple and single results into an array
					if (count($row) > 1) {
						$ar[] = $row;
					} else {
						foreach($row as $item) {
							$ar[] = $item;
						}
					}
				}
				
			// Return last insert id if an insert
			} else if (mysqli_insert_id($link)) {
				
				$ar['insert_id'] = mysqli_insert_id($link);
				
			} else if (!mysqli_error($link)) {
				
				$ar = TRUE;
				
			} else {
				
				$ar = FALSE;
				
			}
			
			// Free resources
			@mysqli_free_result($result);
			mysqli_close($link);
			
			// Return results
			return $ar;
			
		} else {
			
			// Free resources
			@mysqli_free_result($result);
			mysqli_close($link);
			
			// Return false
			return FALSE;
			
		}
		
	}


	/**
	  * Escape database input
	  *
	  * Sterilizes database input and wraps strings in single quotes.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string|int $value Value of the argument to be passed to DB query.
	  *
	  * @return string|int $value The starilized input
	  *
	  */
	function escape ($value, $database = 'specials') {
		
		// Require connection info
		require_once('DbConnect.php');
		
		// Create db link
		if ($database == 'profiles') {
			
			// Nugget Web Profiles database
			$link = mysqli_connect(PROFILE_HOST, PROFILE_USER, PROFILE_PASSWORD, PROFILE_NAME);
			
		} else if ($database == 'nugget') {
			
			// Nugget Website database
			$link = mysqli_connect(NUGGET_HOST, NUGGET_USER, NUGGET_PASSWORD, NUGGET_NAME);
			
		} else if ($database == 'sonoma') {
			
			// Sonoma Website database
			$link = mysqli_connect(SONOMA_HOST, SONOMA_USER, SONOMA_PASSWORD, SONOMA_NAME);
			
		} else {
			
			// Weekly Specials database
			$link = mysqli_connect(SPECIALS_HOST, SPECIALS_USER, SPECIALS_PASSWORD, SPECIALS_NAME);
			
		}
		
		// Stripslashes
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		
		// Quote if not a number or a numeric string
		if (!is_numeric($value)) {
			$value = "'" . mysqli_real_escape_string($link, trim($value)) . "'";
		}
		
		// Free resources
		mysqli_close($link);
		
		// Return result
		return $value;
		
		// return $value;
	}


	/**
	  * Unescape database input
	  *
	  * Removed single quotes and slashes from escaped database input.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $value String to be unescaped.
	  *
	  * @return string $value The unescaped input VALUE.
	  *
	  */
	function unescape ($value) {
		
		// Don’t process of value is NULL string
		if ($value != 'NULL') {
			
			// Remove backslashes, quotes from end of string
			$value = trim($value,"\\"."'");
			
			// Return the result
			return $value;
			
		} else {
			
			return NULL;
			
		}
	}


	/**
	  * Validate DB input string
	  *
	  * Checks for required DB input and uses escape function to sterilize, optionally taking HTML entities into account
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $input String to be validated
	  * @param boolean $require Flag for required
	  * @param boolean $require Flag for HTML entities
	  *
	  * @return string|FALSE|NULL $value The quoted and escaped input
	  *
	  */
	function vempty ($input, $require = FALSE, $entities = FALSE) {
		
		// Convert HTML entities if needed
		if ($entities) {
			
			$input = htmlentities($input,ENT_QUOTES,'UTF-8');
			
		}
		
		// Return value or FALSE if required
		if ($require) {
			
			if (empty($input)) {
				
				$value = FALSE;
				
				return $value;
				
			} else {
				
				$value = $input;
				
			}
			
		// Return value or NULL if not required
		} else {
			
			if (empty($input)) {
				
				$value = 'NULL';
				
				return $value;
				
			} else {
				
				$value = $input;
				
			}
			
		}
		
		// Return escaped input
		return escape($value);
	}


	/**
	  * Validate numeric DB input
	  *
	  * Checks input for an integer and returns it or 0
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param int $input Integer input.
	  * @param boolean $require Checks if value is required.
	  * @param boolean $entities Checks if to convert entities.
	  *
	  * @return int $value Returns the input, or 0 if input is not an integer.
	  *
	  */
	function vbool ($input) {
		
		if (is_numeric($input)) {
			
			$value = $input;
			
		} else {
			
			$value = 0;
			
		}
		
		return $value;
		
	}


	/**
	  * Validate DB email input
	  *
	  * Checks for a valid email address and sterilizes it for database input.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $email A string containing and email address.
	  * @param boolean $require Flag for required
	  *
	  * @return string|FALSE $email The escaped email address or FALSE
	  *
	  */
	function vemail ($email, $require = FALSE) {
		
		// Return false if email is empty and required
        if (!$email && !$require) {
			
            return FALSE;
			
        }
		
        // Return false if input is not a valid email address
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
			
            return FALSE;
			
		// Sterilize and escape email address string
        } else {
			
            $email = escape($email);
            return $email;
			
        }
		
    }


	/**
	  * Validate and hashes password for DB input
	  *
	  * Checks for a required password and creates a secured hash.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $password A string containing a password.
	  *
	  * @return string|FALSE $value The escaped and hashed password.
	  *
	  */
	function vpassword ($password) {
		
		if (!$password) {
			
			return FALSE;
			
		} else {
			
			$value = password_hash($password, PASSWORD_DEFAULT);
			
			return escape($value);
			
		}
		
	}

    function vdate ($year,$month,$day) {
		
		if (!$year || !$month || !$day) {
		
			$value = 'NULL';
			
			return $value;
			
		} else {
		
			$value = $year . '-' . str_pad($month,2,"0",STR_PAD_LEFT) . '-' . str_pad($day,2,"0",STR_PAD_LEFT);
			
			return escape($value);
			
		}
		
	}

	function vtime ($hour,$minute,$ampm) {
		
		if (!$hour || !$minute || !$ampm) {
			
			$value = '';
			
		} else {
			
			if ($ampm == 'pm') {
				if ($hour == 12) {
					$hour = 12;
				} else {
					$hour = $hour + 12;
				}
			} else if ($ampm == 'am' && $hour == 12) {
				$hour = '00';
			} else {
				$hour = $hour;
			}
			
			$value = str_pad($hour,2,"0",STR_PAD_LEFT) . ':' . str_pad($minute,2,"0",STR_PAD_LEFT) . ':00';
		}
		
		return escape($value);
	}


	function vdatetime ($year,$month,$day,$hour=false,$minute=false,$ampm=false) {
		
		if (!$year || !$month || !$day && ((!$hour || !$minute || !$ampm))) {
			
			$value = 'NULL';
			
			return $value;
			
		} else {
			
			if ($ampm == 'pm') {
				if ($hour == 12) {
					$hour = 12;
				} else {
					$hour = $hour + 12;
				}
			} else if ($ampm == 'am' && $hour == 12) {
				$hour = '00';
			} else {
				$hour = $hour;
			}
			
			$value = $year . '-' . str_pad($month,2,"0",STR_PAD_LEFT) . '-' . str_pad($day,2,"0",STR_PAD_LEFT) . ' ' . str_pad($hour,2,"0",STR_PAD_LEFT) . ':' . str_pad($minute,2,"0",STR_PAD_LEFT) . ':00';
			
		}
		
		return escape($value);
	}


	/**
	  * Validate URL frieldy string for DB input
	  *
	  * Converts string to all lowercase ASCII string and sterilizes for DB input
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $string Input string.
	  *
	  * @return string $clean The converted and sterilized string
	  *
	  */
	function vslug($string, $delimiter = '-') {
		
		// Convert special characters to nearest ASCII equivalent
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
		
		// Remove punctuation
		$clean = preg_replace("/[^a-zA-Z0-9_|+ -]/", '', $clean);
		
		// Convert to lower case and trip whitespace
		$clean = strtolower(trim($clean, '-'));
		
		// Insert specified delimiter
		$clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);
		
		// Return results
		return escape($clean);
	}


	/**
	  * Validate table name
	  *
	  * Compares submitted table name to array of allowed values
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $table The table name you wish to validate.
	  *
	  * @return string $table The approved table name, or false if not present in the array.
	  *
	  */
	function vtablename ($table) {
		
		// Array of allowed tabes
		$allowed_tables = array('ads', 'media', 'retractions', 'labels', 'banners');
		
		// Return table name if in array
		if (in_array($table, $allowed_tables)) {
			
			return $table;
			
		} else {
			
			return FALSE;
			
		}
		
	}


	/**
	  * Validate enumeration input
	  *
	  * Validates input for enum datatype
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $input The string name you wish to validate.
	  * @param boolean $require Bolean value true = require, false = not required.
	  *
	  * @return string $value The escaped input string or null if no value is passed.
	  *
	  */

	function venum ($input, $require) {
		
		// Check of required
		if ($require) {
			
			// Return FALSE if input empty but required
			if (empty($input)) {
				
				$value = FALSE;
				return $value;
				
			// Set value of output
			} else {
				$value = $input;
			}
			
		} else {
			
			// Return NULL if input empty but required
			if (empty($input)) {
				return 'NULL';
			} else {
				// Set value of output
				$value = $input;
			}
		}
		
		// Return escaped value
		return escape($value);
		
	}


	/**
	  * Validate telephone input
	  *
	  * Validates input for a telephone and returns it in a standard (xxx) xxx-xxxx format
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $phone The phone number.
	  * @param boolean $require Bolean value true = require, false = not required.
	  *
	  * @return string $phone The escaped input string or null if no value is passed.
	  *
	  */
	function vphone ($phone, $require = FALSE) {
		
		// Return false if required and empty
		if ($require && !$phone) {
			
			return FALSE;
			
		// Return null and not required
		} else if (!$require && !$phone) {
			
			$phone = 'NULL';
			
		// Format the phone number
		} else {
			
			// Strip out all non numeric characters
			$newphone = preg_replace("/[^0-9]/","",$phone);
			
			// Capture length
			$length = strlen($newphone);
			
			switch ($length) {
				
				case 7:
					$phone = substr($newphone,0,3) . '-' . substr($newphone,3,4);
					break;
					
				case 10:
					$phone = '(' . substr($newphone,0,3) . ') ' . substr($newphone,3,3) . '-' . substr($newphone,6,4);
					break;
					
				case 11:
					$phone = '+' . substr($newphone,0,1) . ' (' . substr($newphone,1,3) . ') ' . substr($newphone,4,3) . '-' . substr($newphone,7,4);
					break;
					
				default :
					$phone = '+' . substr($newphone,0,1) . ' (' . substr($newphone,1,3) . ') ' . substr($newphone,4,3) . '-' . substr($newphone,7,4) . ' x' . substr($newphone,11);
					break;
					
			}
			
		}
		
		return escape($phone);
		
	}


	/**
	  * Validate fractions
	  *
	  * Validates text input for plain text fields, simialrly to vempty, but also coverts fractions.
	  * Mostly used for recipe ingredients.
	  *
	  * @author  Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $input String to be validated
	  * @param boolean $require Flag for required
	  *
	  * @return string $output Converted and escaped string
	  *
	  */
	function vfractions($input, $require = FALSE) {
		
		// Take care of empty strings first
		if ($require && empty($input)) {
			
			// Return FALSE if empty and required
			return FALSE;
			
		} else if (empty($input)) {
			
			$value = 'NULL';
			
			// Return NULL if empty and not required
			return $value;
			
		}
		
		// Convert entities
		$string = htmlentities($input, ENT_QUOTES, 'UTF-8');
		
		// Declare fractions array
		$fractions = array(
			'1/4' => '&frac14;',
			'1/2' => '&frac12;',
			'3/4' => '&frac34;',
			'1/3' => '&#8531;',
			'⅓'   => '&#8531;',
			'2/3' => '&#8532;',
			'⅔'   => '&#8532;',
			'1/5' => '&#8533;',
			'⅕'   => '&#8533;',
			'2/5' => '&#8534;',
			'⅖'   => '&#8534;',
			'3/5' => '&#8535;',
			'⅗'   => '&#8535;',
			'4/5' => '&#8536;',
			'⅘'   => '&#8536;',
			'1/6' => '&#8537;',
			'⅙'   => '&#8537;',
			'5/6' => '&#8538;',
			'⅚'   => '&#8538;',
			'1/8' => '&#8539;',
			'⅛'   => '&#8539;',
			'3/8' => '&#8540;',
			'⅜'   => '&#8540;',
			'5/8' => '&#8541;',
			'⅝'   => '&#8541;',
			'7/8' => '&#8542;',
			'⅞'   => '&#8542;'
		);
		
		// Loop through fractions and process string
		foreach ($fractions as $key => $value) {
			
			if (strstr($string, $key)) {
				
				$output = str_replace($key, $value, $string);
				$string = $output;
				
			} else {
				
				$output = $string;
				
			}
			
		}
		
		return escape($output);
		
	}


	/**
	  * Validate URL
	  *
	  * Checks to see if input is valid url and formats it with proper protocol.
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $url Input to validate.
	  * @param boolean $require True to require input.
	  *
	  * @return string $url The formatted URL.
	  *
	  */
	function vurl ($url, $require = FALSE) {
		
		// Check for input and require
		if ($require && !$url) {
			
			// Return false if empty and required
			return FALSE;
			
		} else if ($url) {
			
			// Check for protocal
			if (strpos($url, '://')) {
				
				// Leave URL alone if protocal set
				$url = $url;
				
			} else {
				
				// Force https if no protocol present
				$url = 'https://' . $url;
				
			}
			
		} else {
			
			// Return NULL if empty and not required
			$url = 'NULL';
			return $url;
			
		}
		
		// Return escaped input string
		return escape($url);
		
	}
