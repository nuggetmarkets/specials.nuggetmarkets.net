<?php

	/*
		This template generator is not currently used.
		It didn’t have the intended effect of updating a Campaign Monitor Journy :(
	*/

	// Require dependencies
	require_once 'Bootstrap.php';
	require_once 'AdsController.php';
	require_once 'EmailController.php';


	/**
	  * Copy Template Assetes
	  *
	  * Copies email image assets on the local server to the specified cache folder
	  *
	  * @author Mark Eagleton <mark@thebigreason.com>
	  *
	  * @param string $image_path Source path of the image file to copy.
	  *
	  * @return string $image Path of the new destination file
	  *
	  */
	function copyTemplateAssets ($image_path, $destination_path) {

		// Set the source file
		$src = $image_path;

		// Extract the file name
		$exp = explode('/', $image_path);
		$img = array_pop($exp);

		// Set the destination
		$dst =  $destination_path . $img;

		// Copy the file
		copy ($src, $dst);

		// Capture the file path
		$image = $src;

		return $image;

	}


	// Set up zones array
	$zones = array(
		'Nugget Markets Valley', 
		'Nugget Markets Marin', 
		'Sonoma Market', 
		'Fork Lift'
	);


	// Loop through zones 
	foreach ($zones as $store) {

		// Set up an images array
		$images = array();

		// Get zone attributes
		$zone = getStoreAttributesByName($store);

		// Set the destination path for the files
		$destination_path = SERVER_PATH . '/httpdocs/cache/' . $zone['slug'] . '/subscriber-email-assets/';

		// Capture current files in destination dir
		$files = glob($destination_path . '*'); 

		// Loop through them
		foreach($files as $file) {
  			
  			// And delete any files found
  			if (is_file($file)) {
  				unlink($file);
  			}
  			
		}

		// Copy default assets
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/css/email.css', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/css/email-' . $zone['slug'] . '.css', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/' . $zone['email_logo_header'], $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/' . $zone['email_logo_print'], $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/weekly-specials-square-red-white-black.png', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/weekly-specials-square-black-white-red.png', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/905105000255.png', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/secret-special-email-heading.png', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/weekly-specials-scan-and-save.png', $destination_path);
		$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/weekly-specials.png', $destination_path);

		
		// Get current ad_id
		if ($ad_id = getAdIdByStoreChain($zone['slug'])) {
		
			// Set the page to retrieve
			$uri = SPECIALS_URL . '/email/' . $zone['slug'] . '/';

			// Get the page contents
			$email_file = getCurl($uri);

			// Write contents to file
			file_put_contents(SERVER_PATH . '/httpdocs/cache/' . $zone['slug'] . '/subcriber-template.html', $email_file);

			// Get the current Ad ID
			$ad_id = getAdIdByStoreChain($zone['slug']);

			// Get the ad
			$ad = getAdById($ad_id);

			// Capture the secret special image
			if ($ad['secret_special_image']) {
				$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/media/' . $ad['secret_special_image'], $destination_path);
			}


			// Loop through and capture any item and label images
			foreach ($ad['items'] as $item) {

				$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/media/' . $item['feature_photo_small'], $destination_path);

				if (is_array($item['labels'])) {
					foreach ($item['labels'] as $labels) {
						if (is_array($labels)) {
							foreach ($labels as $label) {
								$images[] = copyTemplateAssets(SERVER_PATH . '/httpdocs/img/labels/' . $label['name'] . '.png', $destination_path);
							}
						}
					}
				}

			}

			
			// Get any attached articles
			if ($articles = getArticles($ad)) {

				// Loop through and capture any article images
				foreach ($articles['articles'] as $article) {
					$images[] = copyTemplateAssets($zone['url'] . '/media/images/' . $article['tmb'], $destination_path);
				}

			}


			// Get any attached recipes
			if ($recipes = getRecipes($ad)) {

				// Loop through and capture any recipe images
				foreach ($recipes['recipes'] as $recipe) {
					$images[] = copyTemplateAssets($zone['url'] . '/media/images/' . $recipe['photo_thumb'], $destination_path);
				}

			}


			// Set the file path to archive
			$rootPath = realpath($destination_path);

			// Initialize archive object
			$zip = new ZipArchive();
			$zip->open(SERVER_PATH . '/httpdocs/cache/' . $zone['slug'] . '/subscriber-email-assets.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

			// Create recursive directory iterator
			$files = new RecursiveIteratorIterator (
			    new RecursiveDirectoryIterator($rootPath),
			    RecursiveIteratorIterator::LEAVES_ONLY
			);

			// Loop through the files
			foreach ($files as $name => $file) {
			    
			    // Skip any directories
			    if (!$file->isDir()) {
			        
			        // Get real and relative path for current file
			        $filePath = $file->getRealPath();
			        $relativePath = substr($filePath, strlen($rootPath) + 1);

			        // Add current file to the archive
			        $zip->addFile($filePath, $relativePath);
			    }
			}

			// Create the zip archive
			$zip->close();
		
		}
	}

	echo 'Template setup complete.';
