<? 
	include('../engine/AdPlans.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<section class="plans">
	<header>
		<h1>Ad Plans</h1>
		<p>Plan features and Secret Specials for upcoming ads.</p>
		<? if ($plans && $_SESSION['user_level'] == 1) { ?>
		<nav>
			<a href="/ad-plans/new/<?=$plans['next_ad']?>/" class="button">Create the <?=date('F j, Y', strtotime($plans['next_ad']))?> ad plan</a>
		</nav>
		<? } ?>
	</header>
	
	<? if ($plans) { ?>
		
		<table>
			<thead>
				<tr>
					<th>Ad Date</th>
					<th>Secret Special</th>
					<th>Features</th>
				</tr>
			</thead>
			<tbody>
				<? foreach ($plans['items'] as $plan) { ?>
					<tr class="<?=$plan['class']?>">
						<td>
							<a href="/ad-plans/edit/<?=$plan['id']?>/"><?=date('F j, Y', strtotime($plan['ad_start_date']))?></a><br />
							<small>Running through <?=date('F j, Y', strtotime($plan['ad_start_date'] . ' + 7 days'))?></small>
						</td>
						<td>
							<? if ($plan['secret_special_title']) { echo $plan['secret_special_title']; } else { echo '<img src="/img/warning.svg" alt="Warning icon" /> Not set'; } ?>
							<? if ($plan['secret_special_retail']) { echo "<br /><small>{$plan['secret_special_retail']}</small>"; } ?>
						</td>
						<td>
							<? if ($plan['nugget_notes']) { ?>
								<span class="flag set" title="Nugget Markets features set">N</span>
							<? } else { ?>
								<span class="flag notset" title="Nugget Markets features not set">N</span>
							<? } ?>
							<? if ($plan['sonoma_notes']) { ?>
								<span class="flag set" title="Sonoma Market features set">S</span>
							<? } else { ?>
								<span class="flag notset" title="Sonoma Market features not set">S</span>
							<? } ?>
							<? if ($plan['fork_lift_notes']) { ?>
								<span class="flag set" title="Fork Lift features set">F</span>
							<? } else { ?>
								<span class="flag notset" title="Fork Lift features not set">F</span>
							<? } ?>
							<? if ($plan['food_4_less_notes']) { ?>
								<span class="flag set" title="Food 4 Less features set">4</span>
							<? } else { ?>
								<span class="flag notset" title="Food 4 Less features not set">4</span>
							<? } ?>
							<? if ($plan['general_notes']) { ?>
								<span class="notes" title="Important note">✎</span>
							<? } ?>
						</td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	
	<? } else if ($plan) { ?>
	
		<h2>Ad Plan for Week of <?=date("F j, Y",strtotime($plan['ad_start_date']))?></h2>
		
		<form action="/ad-plans/" method="post">
			
			<fieldset>
				<legend>Secret Special</legend>
				<p>Enter details for the Secret Special that coincides with this&nbsp;ad.</p>
				<ul>
					<li class="inline">
						<label for="secret_special_from">Run from:</label> 
						<select name="secret_special_from" id="secret_special_from">
						<? for ($i = 0; $i < 7; $i++) { ?>
							<option value="<?=date('Y-m-d', strtotime($plan['ad_start_date'] . "+ $i days"))?>"
								<? 
									if (date('Y-m-d', strtotime($plan['ad_start_date'] . "+ $i days")) == $plan['secret_special_from']) {
										echo' selected="selected"';
									} else if (!$plan['secret_special_from'] && $i == 2) {
										echo' selected="selected"';
									}
									
								?>
								><?=date('M d (D)', strtotime($plan['ad_start_date'] . "+ $i days"))?>
							</option>
						<? } ?>
						</select> 
						<label for="secret_special_through">through:</label> 
						<select name="secret_special_through" id="secret_special_through">
						<? for ($i = 0; $i < 7; $i++) { ?>
							<option value="<?=date('Y-m-d', strtotime($plan['ad_start_date'] . "+ $i days"))?>"
								<?
									if (date('Y-m-d', strtotime($plan['ad_start_date'] . "+ $i days")) == $plan['secret_special_through']) {
										echo' selected="selected"';
									} else if (!$plan['secret_special_from'] && $i == 3) {
										echo' selected="selected"';
									}
								?>
								><?=date('M d (D)', strtotime($plan['ad_start_date'] . "+ $i days"))?></option>
						<? } ?>
						</select> 
					</li>
					<li>
						<label for="secret_special_title">Item Description:</label>
						<input type="text" id="secret_special_title" name="secret_special_title" value="<?=unescape($plan['secret_special_title'])?>" />
					</li>
				</ul>
				<ul class="field grid">
					<li>
						<label for="secret_special_retail">Secret Special Retail:</label>
						<input type="text" id="secret_special_retail" name="secret_special_retail" value="<?=unescape($plan['secret_special_retail'])?>" /> 
					</li>
					<li>
						<label for="secret_special_limit">Limit:</label>
						<input type="text" id="secret_special_limit" name="secret_special_limit" size="3" value="<?=unescape($plan['secret_special_limit'])?>" /> 
						<div class="note">A limit of 4 will be used if left blank.</div>
					</li>
				</ul>
				<p>Regular Retails:</p>
				<table>
					<colgroup>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
						<tr>
							<th colspan="2">Zone</th>
							<th>Base Retail</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>100</td>
							<td>Valley</td>
							<td><input type="number" id="secret_special_valley_regular_retail" name="secret_special_valley_regular_retail" size="5" class="currency symbol" step="0.01" value="<?=unescape($plan['secret_special_valley_regular_retail'])?>" /></td>
						</tr>
						<tr>
							<td>110</td>
							<td>Marin</td>
							<td><input type="number" id="secret_special_marin_regular_retail" name="secret_special_marin_regular_retail" size="5" class="currency symbol" step="0.01" value="<?=unescape($plan['secret_special_marin_regular_retail'])?>" /></td>
						</tr>
						<tr>
							<td>111</td>
							<td>Sonoma</td>
							<td><input type="number" id="secret_special_sonoma_regular_retail" name="secret_special_sonoma_regular_retail" size="5" class="currency symbol" step="0.01" value="<?=unescape($plan['secret_special_sonoma_regular_retail'])?>" /></td>
						</tr>
						<tr>
							<td>121</td>
							<td>Fork Lift</td>
							<td><input type="number" id="secret_special_fork_lift_regular_retail" name="secret_special_fork_lift_regular_retail" size="5" class="currency symbol" step="0.01" value="<?=unescape($plan['secret_special_fork_lift_regular_retail'])?>" /></td>
						</tr>
						<tr>
							<td>130</td>
							<td>Food 4 Less</td>
							<td>
								<input type="number" id="secret_special_food_4_less_regular_retail" name="secret_special_food_4_less_regular_retail" size="5" class="currency symbol" step="0.01" value="<?=unescape($plan['secret_special_food_4_less_regular_retail'])?>" />&nbsp;&nbsp;
								<i class="note">Use the Notes field below for Food 4 Less Secret Special info if a different item from&nbsp;above.</i>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="appender">
					<p>UPCs included:</p>
					<ul id="append-upcs">
						<? if (is_array($plan['secret_special_products'])) { $u = 0; foreach ($plan['secret_special_products'] as $product) { ?>
							<li class="inline">
								<input type="text" id="secret_special_upc_<?=$product['upc']?>" name="secret_special_upcs[]" minlength="13" maxlength="13" size="13" value="<?=$product['upc']?>" /> 
								<label><input type="checkbox" name="secret_special_picture[]" value="<?=$product['upc']?>" <?if(in_array($product['upc'], $plan['secret_special_picture'])) { echo ' checked="checked"'; }?> /> Picture item</label> 
								<a href="#" class="delete x remove">&times;</a>&nbsp;&nbsp;&nbsp;&nbsp;<?=$product['pos_name']?>
							</li>
						<? $u++; } } ?>
						<li id="append-upc" class="inline">
							<input type="text" id="secret_special_upc" name="secret_special_upcs[]" minlength="13" maxlength="13" size="13" /> 
							<label><input type="checkbox" name="secret_special_picture[]" value="<?=$product['upc']?>" /> Picture item</label> 
							<a href="#" class="delete x remove">&times;</a> <span class="note">13-digit UPCs with no dashes, please</span>
						</li>
					</ul>
					<p>
						<a href="#append-upc" class="add button">Add a UPC</a> or 
						<span class="text-button">
							<input type="text" id="secret_special_family_group" name="secret_special_family_group" size="16" placeholder="Family group name" /> 
							<input type="submit" name="append_family_group" value="Append family group" />
						</span>
					</p>
				</div>
				<p>Lifestyle Icons:</p>
				<ul id="badges" class="checker">
					<li class="inlinegrid">
						<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle') { ?>
							
							<? if (@in_array($label['id'],$plan['secret_special_labels'])) { ?>
							<label class="selected">
								<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
								<input type="checkbox" name="secret_special_labels[]" value="<?=$label['id']?>" checked="checked" />
							</label>
							
							<? } else { ?>
							
							<label>
								<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
								<input type="checkbox" name="secret_special_labels[]" value="<?=$label['id']?>" />
							</label>
							<? } ?>
							
						<? } } ?>
					</li>
				</ul>
			</fieldset>
			
			<fieldset>
				<legend>Features</legend>
				<p>The following items will be featured below the Secret Special in the email and at the top of the Weekly Specials&nbsp;page.</p>
				<ul class="field grid">
					<li>
						<label for="nugget_notes">Nugget Markets:</label>
						<textarea id="nugget_notes" name="nugget_notes" rows="8"><?=unescape($plan['nugget_notes'])?></textarea>
					</li>
					<li>
						<label for="sonoma_notes">Sonoma Market:</label>
						<textarea id="sonoma_notes" name="sonoma_notes" rows="8"><?=unescape($plan['sonoma_notes'])?></textarea>
					</li>
					<li>
						<label for="fork_lift_notes">Fork Lift:</label>
						<textarea id="fork_lift_notes" name="fork_lift_notes" rows="8"><?=unescape($plan['fork_lift_notes'])?></textarea>
					</li>
					<li>
						<label for="food_4_less_notes">Food 4 Less:</label>
						<textarea id="food_4_less_notes" name="food_4_less_notes" rows="8"><?=unescape($plan['food_4_less_notes'])?></textarea>
					</li>
				</ul>
			</fieldset>
			
			<fieldset>
				<legend>Notes</legend>
				<p>Special instructions for this ad.</p>
				<ul>
					<li>
						<textarea id="general_notes" name="general_notes" rows="5"><?=unescape($plan['general_notes'])?></textarea>
					</li>
				</ul>
			</fieldset>
			
			<ul class="actions">
				<li>
					<? if ($_GET['action'] == 'edit') { ?>
					<input type="hidden" name="ad_plan_id" value="<?=$_GET['id']?>" />
					<input type="submit" name="save" value="Save changes" />
					<? } else { ?>
					<input type="submit" name="add" value="Add item" />
					<? } ?>
				</li>
				<li>
					<a href="/ad-plans/">Cancel</a>
				</li>
				<? /* if ($_GET['action']=='edit') { ?>
				<li><a href="/delete/ad-plans/<?=$_GET['id']?>/" class="delete">Delete</a></li>
				<? } */ ?>
			</ul>
		</form>
	<? } ?>
</section>
<? include('inc/footer.inc.php'); ?>
