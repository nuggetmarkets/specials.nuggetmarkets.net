<? 
	include('../engine/Labels.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<section class="labels">
	<header>
		<h1>Labels</h1>
		<p>Manage the brand, seasoning, and organization labels attached to products in the ad&nbsp;builder.</p>
		<? if ($_SESSION['user_level'] == 1) { ?>
		<nav>
			<a href="/labels/new/" class="button">Add new</a>
		</nav>
		<? } ?>
	</header>
<? if (is_array($labels['listing'])) { ?>
	<ul class="items grid">
		<? foreach ($labels['listing'] as $label) { ?>
		<li>
			<a href="/labels/edit/<?=$label['id']?>/">
				<img src="/img/labels/<?=$label['image']?>" alt="<?=$label['description']?>" />
			</a>
			<p><a href="/labels/edit/<?=$label['id']?>/"><?=$label['description']?></a></p>
		</li>
		<? } ?>
	</ul>
	
	<footer>
		<? if ($labels['pagination']) { ?>
		<nav class="pagination">
		<? if (!is_null($labels['pagination']['first'])) { ?>
			<a href="/labels/page/1/">First</a>
			<a href="/labels/page/<?=$labels['pagination']['previous']?>/">Prev</a>
		<? } else { ?>
			<span class="deact">First</span>
			<span class="deact">Prev</span>
		<? } ?>
		<span class="total">Page <?=$labels['pagination']['current']?> of <?=$labels['pagination']['total']?></span>
		<? if (!is_null($labels['pagination']['last'])) { ?>
			<a href="/labels/page/<?=$labels['pagination']['next']?>/">Next</a>
			<a href="/labels/page/<?=$labels['pagination']['last']?>/">Last</a>
		<? } else { ?>
			<span class="deact">Next</span>
			<span class="deact">Last</span>
		<? } ?>
		</nav>
		<? } ?>
	</footer>
<? } else if ($label) { ?>
	<form action="/labels/" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>Image</legend>
			<p>The image to use for this label.</p>
			<ul>
			<? if ($_GET['action'] == 'edit') { ?>
				<li>
					<label>Current image:</label>
					<figure>
						<img src="/img/labels/<?=$label['image']?>" alt="<?=$label['description']?>" />
						<figcaption>
							<p><code><?=$label['image']?></code></p>
						</figcaption>
					</figure>
				</li>
				<li>
					<label for="new_file">Update image:</label>
					<input type="file" name="new_file" id="new_file" class="empty" />
					<div class="note required">Image should be in 24-bit PNG format, with an alpha channel, and 400px in its largest&nbsp;dimension.</div>
					<input type="hidden" name="current_file" id="current_file" value="<?=$label['image']?>" />
				</li>
			<? } else { ?>
				<li>
					<label for="new_file">Image file:</label>
					<input type="file" name="new_file" id="new_file" class="empty" required="required" />
					<div class="note required">Image should be in 24-bit PNG format, with an alpha channel, and 400px in its largest&nbsp;dimension.</div>
				</li>
			<? } ?>
				
			</ul>
		</fieldset>
		
		<fieldset>
			<legend>Description</legend>
			<p>The name and description for the&nbsp;label.</p>
			<ul>
				<li>
					<label for="description">Name:</label>
					<input type="text" name="description" id="description" value="<?=unescape($label['description'])?>" <?if(!$label['description']){echo'class="empty"';}?> required="required" />
					<div class="note required">Required</div>
					<div class="note">The label name to be used as the alt attribute for the image.</div>
				</li>
				<li>
					<label for="long_description">Description:</label>
					<textarea name="long_description" id="long_description" rows="4"><?=unescape($label['long_description'])?></textarea>
					<div class="note">The label description will be used as the title element for the&nbsp;image.</div>
				</li>
				<li>
					<label for="type">Type:</label>
					<select name="type">
						<option value="brand"<?if($label['type']=='brand'){echo ' selected="selected"';}?>>brand</option>
						<option value="lifestyle"<?if($label['type']=='lifestyle'){echo ' selected="selected"';}?>>lifestyle</option>
						<option value="organization"<?if($label['type']=='organization'){echo ' selected="selected"';}?>>organization</option>
						<option value="seasoning"<?if($label['type']=='seasoning'){echo ' selected="selected"';}?>>seasoning</option>
					</select>
					<span class="note">The type seting determines where labels show up in the listings, and on the item configuration&nbsp;screens.</span>
				</li>
			</ul>
		</fieldset>
		
		<ul class="actions">
			<li>
				<? if ($_GET['action'] == 'edit') { ?>
				<input type="hidden" name="item_id" value="<?=$_GET['id']?>" />
				<input type="submit" name="save" value="Save changes" />
				<? } else { ?>
				<input type="submit" name="add" value="Add item" />
				<? } ?>
			</li>
			<li>
				<a href="/media/">Cancel</a>
			</li>
			<? if ($_GET['action']=='edit') { ?>
			<li><a href="/delete/labels/<?=$_GET['id']?>/" class="delete">Delete</a></li>
			<? } ?>
		</ul>
	</form>
<? } ?>

</section>

<? include('inc/footer.inc.php'); ?>
