<? if (isset($sister_ads)) { ?>
<nav id="menu" class="subnav">
	<div class="menu next-prev-ads">
		<nav class="sister-ads">
		<h3><?=$ad['store_chain']?></h3>
			<?=date('n/j/Y', strtotime($ad['date_from']))?>
			<ul class="menu ">
			<? foreach ($sister_ads['ads'] as $sister_ad) {  ?>
				<li class="<?=formatSlug($sister_ad['store_chain'])?>">
					<a href="/ads/edit/<?=$sister_ad['id']?>/">
						<strong><?=$sister_ad['store_chain']?></strong>
						<div><?=date('n/j/Y', strtotime($sister_ad['date_from']))?></div>
					</a>
				</li>
			<? } ?>
			</ul>
		</nav>
	</div>
</nav>
<? } ?>
<form action="/ads/new/" method="post" enctype="multipart/form-data">
	<fieldset id="ad-dates">
		<legend>Run On</legend>
		<ul>
			<li>
				<label for="store_chain">Store / zone:</label>
				<select name="store_chain" id="store_chain" class="large">
					<option value="Nugget Markets Valley" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Nugget Markets Valley'){echo' selected="selected"';}?>>Nugget Markets Valley</option>
					<option value="Nugget Markets Marin" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Nugget Markets Marin'){echo' selected="selected"';}?>>Nugget Markets Marin</option>
					<!--
					<option value="Nugget Markets Sonoma" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Nugget Markets Sonoma'){echo' selected="selected"';}?>>Nugget Markets Sonoma</option>
					-->
					<option value="Nugget Markets" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Nugget Markets'){echo' selected="selected"';}?>>Nugget Markets</option>
					<option value="Sonoma Market" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Sonoma Market'){echo' selected="selected"';}?>>Sonoma Market</option>
					<option value="Fork Lift" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Fork Lift'){echo' selected="selected"';}?>>Fork Lift</option>
					<option value="Food 4 Less" <?if(isset($ad['store_chain'])&&$ad['store_chain']=='Food 4 Less'){echo' selected="selected"';}?>>Food 4 Less</option>
				</select>
			</li>
			<li>
				<label for="dfrom">From: </label>
				<select name="from_month" id="from_month">
					<option value="01" <?if(date('m',strtotime($ad['date_from']))=='01'){echo' selected="selected"';}?>>Jan</option>
					<option value="02" <?if(date('m',strtotime($ad['date_from']))=='02'){echo' selected="selected"';}?>>Feb</option>
					<option value="03" <?if(date('m',strtotime($ad['date_from']))=='03'){echo' selected="selected"';}?>>Mar</option>
					<option value="04" <?if(date('m',strtotime($ad['date_from']))=='04'){echo' selected="selected"';}?>>Apr</option>
					<option value="05" <?if(date('m',strtotime($ad['date_from']))=='05'){echo' selected="selected"';}?>>May</option>
					<option value="06" <?if(date('m',strtotime($ad['date_from']))=='06'){echo' selected="selected"';}?>>Jun</option>
					<option value="07" <?if(date('m',strtotime($ad['date_from']))=='07'){echo' selected="selected"';}?>>Jul</option>
					<option value="08" <?if(date('m',strtotime($ad['date_from']))=='08'){echo' selected="selected"';}?>>Aug</option>
					<option value="09" <?if(date('m',strtotime($ad['date_from']))=='09'){echo' selected="selected"';}?>>Sep</option>
					<option value="10" <?if(date('m',strtotime($ad['date_from']))=='10'){echo' selected="selected"';}?>>Oct</option>
					<option value="11" <?if(date('m',strtotime($ad['date_from']))=='11'){echo' selected="selected"';}?>>Nov</option>
					<option value="12" <?if(date('m',strtotime($ad['date_from']))=='12'){echo' selected="selected"';}?>>Dec</option>
				</select> /
				<select name="from_day" id="from_day">
					<? for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(date('d',strtotime($ad['date_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select> /
				<select name="from_year" id="from_year">
					<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(date('Y',strtotime($ad['date_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select>
			</li>
			<li>
				<label for="dthrough">Through: </label>
				<select name="through_month" id="through_month">
					<option value="01" <?if(date('m',strtotime($ad['date_through']))=='01'){echo' selected="selected"';}?>>Jan</option>
					<option value="02" <?if(date('m',strtotime($ad['date_through']))=='02'){echo' selected="selected"';}?>>Feb</option>
					<option value="03" <?if(date('m',strtotime($ad['date_through']))=='03'){echo' selected="selected"';}?>>Mar</option>
					<option value="04" <?if(date('m',strtotime($ad['date_through']))=='04'){echo' selected="selected"';}?>>Apr</option>
					<option value="05" <?if(date('m',strtotime($ad['date_through']))=='05'){echo' selected="selected"';}?>>May</option>
					<option value="06" <?if(date('m',strtotime($ad['date_through']))=='06'){echo' selected="selected"';}?>>Jun</option>
					<option value="07" <?if(date('m',strtotime($ad['date_through']))=='07'){echo' selected="selected"';}?>>Jul</option>
					<option value="08" <?if(date('m',strtotime($ad['date_through']))=='08'){echo' selected="selected"';}?>>Aug</option>
					<option value="09" <?if(date('m',strtotime($ad['date_through']))=='09'){echo' selected="selected"';}?>>Sep</option>
					<option value="10" <?if(date('m',strtotime($ad['date_through']))=='10'){echo' selected="selected"';}?>>Oct</option>
					<option value="11" <?if(date('m',strtotime($ad['date_through']))=='11'){echo' selected="selected"';}?>>Nov</option>
					<option value="12" <?if(date('m',strtotime($ad['date_through']))=='12'){echo' selected="selected"';}?>>Dec</option>
				</select> /
				<select name="through_day" id="through_day">
					<? for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(date('d',strtotime($ad['date_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select> /
				<select name="through_year" id="through_year">
					<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(date('Y',strtotime($ad['date_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select>
			</li>
		</ul>
	</fieldset>
<!--
	<fieldset>
		<legend>Other Ads this Week</legend>
		<ul>
			<li>
		<? foreach ($sister_ads['ads'] as $sister) { ?>
			<a href="/ads/edit/<?=$sister['id']?>/" class="button"><?=$sister['store_chain']?> <small><?=date('F j, Y', strtotime($sister['date_from']))?></small></a>
		<? } ?>
			</li>
		</ul>
	</fieldset>
-->
	<fieldset id="import-export">
		<legend>E-Ad Import/Export</legend>
		<ul>
			<li>
				<a href="http://apps.nuggetmarket.com/Marketing/weeklyad/WeeklyAd.aspx" class="button">Export items</a> - Export XML data from DROSTE to /productinfo/ directory on this server
			</li>
			<li>
				<? if ($ad['publish']) { ?>
				<a href="/ad-items-soft-import/<?=$ad['date_from']?>/<?=$ad['id']?>/" class="button">Import items</a>
				<? } else { ?>
				<a href="/ad-items-import/<?=$ad['date_from']?>/<?=$ad['id']?>/" class="button">Import items</a>
				<? } ?>
				 - Import XML data from DROSTE into this E-Ad
			</li>
			
		</ul>
	</fieldset>

	<? if ($page_attrs['action'] != 'new') { ?>
	<fieldset id="ad-toolkit">
		<legend>Ad Toolkit</legend>
		<ul>
			<li>
				<a href="/ad-builder/<?=$ad['id']?>/" class="button build" target="_blank">Ad Builder</a> –
				Manage grouping, names, and pricing for items in the ad.
			</li>
			<li>
				<a href="<?=$ad['store_attributes']['ad_url']?>date/<?=$ad['date_from']?>/" class="button preview" target="_blank">Ad Preview</a> –
				See what the ad will look like on the website before it goes&nbsp;live.
			</li>
			<li>
			<? if ($ad['cache_file_updated']) { ?>
				<a href="/ad-json-export/<?=$ad['id']?>/" class="button">Refresh cache</a> –
				Refresh the cache file that the E-Ads are built from. <span class="note">last refreshed <?=$ad['cache_file_updated']?></span>
			<? } else { ?>
				<a href="/ad-json-export/<?=$ad['id']?>/" class="button">Create cache</a> –
				Create the cache file that the E-Ads are built from.
			<? } ?>
			</li>
		</ul>
	</fieldset>
	<? } ?>
	
	<? /* ?>
	<fieldset id="print-version">
		<legend>Print Version</legend>
		<p>Upload a PDF version of the ad.</p>
		<ul>
			<? if ($ad['print_file']) { ?>
				<li>
					<label for="current_ad">Current Ad:</label>
					<a href="/print-versions/<?=$ad['print_file']['name']?>"><?=$ad['print_file']['name']?></a>
					<a href="/delete/print/<?=$ad['store_attributes']['slug']?>-specials-starting-<?=$ad['date_from']?>/" id="delete-print-ad" class="delete x">&times;</a>
				</li>
				<li>
					<label for="print_version">Replace File:</label>
					<input type="file" name="print_version" id="print_version" class="pdf" />
					<div class="note">Drop PDF file here</div>
				</li>
			<? } else { ?>
				<li>
					<label for="print_version">Upload File:</label>
					<input type="file" name="print_version" id="print_version" class="pdf" />
					<div class="note">Drop PDF file here</div>
				</li>
			<? } ?>
		</ul>
	</fieldset>
	<? */ ?>
	
	
	<fieldset id="append-items" class="appender">
		<legend>Append an Item</legend>
		<p>Put an item on ad that wasn&rsquo;t keyed. Check the Override impory checkbox to prevent it from being removed on the next import. Please note: The SKUs you add here may not ring up correctly at the register. It is always preferable that the buyer key the SKUs they want on the&nbsp;ads.</p>
		<ul id="append-skus">
			<li id="append-sku" class="inline">
				<label for="append_upc">UPC:</label>
				<input type="text" name="append_upc[]" value="" size="13" maxlength="13">
				
				<label for="append_retail">Retail:</label>
				<input type="text" name="append_retail[]" value="" size="6" class="currency">
				
				<label for="append_sale">Sale:</label>
				<input type="text" name="append_sale[]" value="" size="6" class="currency">
				
				<label>
					<input type="checkbox" name="append_override[]" value="1"> Override import
				</label>
				<a href="#" class="delete x remove">&times;</a>
			</li>
		</ul>
		<p><a href="#append-sku" class="add button x">+</a></p>
	</fieldset>
	
	
	<fieldset id="upc-images" class="hide">
		<legend>Upload UPC Product Images</legend>
		<p>Upload product images in JPG format named with 13 digit UPC code. Use this to replace old images, or to add new ones.</p>
		<ul>
			<li>
				<label for="upc_images">UPC Product Images:</label>
				<input type="file" name="upc_images[]" multiple="multiple" class="jpgs">
				<div class="note">Drop 20 of fewer JPG images with UPC names here</div>
			</li>
		</ul>
	</fieldset>
	
	<section id="secret-special" class="secret-special">
		<h2>Secret Special Email Zone Settings</h2>
		<fieldset>
			<legend>Run Date</legend>
			<p>This will default to the Friday and Saturday of the current ad week.</p>
			<ul>
				<li class="inline">
					<label for="secret_special_from">From: </label>
					<select name="secret_special_from_month" id="secret_special_from_month">
						<option value="" <?if(is_null($ad['secret_special_from'])) {echo' selected="selected"';}?>>Month</option>
						<option value="01" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='01'){echo' selected="selected"';}?>>Jan</option>
						<option value="02" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='02'){echo' selected="selected"';}?>>Feb</option>
						<option value="03" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='03'){echo' selected="selected"';}?>>Mar</option>
						<option value="04" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='04'){echo' selected="selected"';}?>>Apr</option>
						<option value="05" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='05'){echo' selected="selected"';}?>>May</option>
						<option value="06" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='06'){echo' selected="selected"';}?>>Jun</option>
						<option value="07" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='07'){echo' selected="selected"';}?>>Jul</option>
						<option value="08" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='08'){echo' selected="selected"';}?>>Aug</option>
						<option value="09" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='09'){echo' selected="selected"';}?>>Sep</option>
						<option value="10" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='10'){echo' selected="selected"';}?>>Oct</option>
						<option value="11" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='11'){echo' selected="selected"';}?>>Nov</option>
						<option value="12" <?if(!is_null($ad['secret_special_from']) && date('m',strtotime($ad['secret_special_from']))=='12'){echo' selected="selected"';}?>>Dec</option>
					</select> /
					<select name="secret_special_from_day" id="secret_special_from_day">
						<option value="" <?if(is_null($ad['secret_special_from'])) {echo' selected="selected"';}?>>Day</option>
						<? for ($i = 1; $i <= 31; $i++) { ?>
						<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($ad['secret_special_from']) && date('d',strtotime($ad['secret_special_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
						<? } ?>
					</select> /
					<select name="secret_special_from_year" id="secret_special_from_year">
						<option value="" <?if(is_null($ad['secret_special_from'])) {echo' selected="selected"';}?>>Year</option>
						<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
						<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($ad['secret_special_from']) && date('Y',strtotime($ad['secret_special_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
						<? } ?>
					</select>
				
					<label for="secret_special_through">Through: </label>
					<select name="secret_special_through_month" id="secret_special_through_month">
						<option value="" <?if(is_null($ad['secret_special_from'])) {echo' selected="selected"';}?>>Month</option>
						<option value="01" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='01'){echo' selected="selected"';}?>>Jan</option>
						<option value="02" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='02'){echo' selected="selected"';}?>>Feb</option>
						<option value="03" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='03'){echo' selected="selected"';}?>>Mar</option>
						<option value="04" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='04'){echo' selected="selected"';}?>>Apr</option>
						<option value="05" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='05'){echo' selected="selected"';}?>>May</option>
						<option value="06" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='06'){echo' selected="selected"';}?>>Jun</option>
						<option value="07" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='07'){echo' selected="selected"';}?>>Jul</option>
						<option value="08" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='08'){echo' selected="selected"';}?>>Aug</option>
						<option value="09" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='09'){echo' selected="selected"';}?>>Sep</option>
						<option value="10" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='10'){echo' selected="selected"';}?>>Oct</option>
						<option value="11" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='11'){echo' selected="selected"';}?>>Nov</option>
						<option value="12" <?if(!is_null($ad['secret_special_through']) && date('m',strtotime($ad['secret_special_through']))=='12'){echo' selected="selected"';}?>>Dec</option>
					</select> /
					<select name="secret_special_through_day" id="secret_special_through_day">
						<option value="" <?if(is_null($ad['secret_special_through'])) {echo' selected="selected"';}?>>Day</option>
						<? for ($i = 1; $i <= 31; $i++) { ?>
						<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($ad['secret_special_through']) && date('d',strtotime($ad['secret_special_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
						<? } ?>
					</select> /
					<select name="secret_special_through_year" id="secret_special_through_year">
						<option value="" <?if(is_null($ad['secret_special_through'])) {echo' selected="selected"';}?>>Year</option>
						<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
						<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($ad['secret_special_through']) && date('Y',strtotime($ad['secret_special_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
						<? } ?>
					</select>
				</li>
			</ul>
		</fieldset>
		
		<fieldset>
			<legend>Description</legend>
			<p>Enter the item name, package size, pricing and andy&nbsp;disclaimors.</p>
				
			<ul>
				<li class="hide">
					<a href="#manual" class="toggle button">Show campaign settings</a>
					<ul id="manual" class="hide manual">
						<li>
							You can use the settings below to manually schedule the email in Campaign&nbsp;Monitor.
						</li>
						<li>
							<label>Campaign Name:</label>
							<input type="text" name="text_select_campaign_name" class="auto-select" value="<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>" readonly size="50" />
							<span class="note">Click to select</span>
						</li>
						<li>
							<label>Subject Line:</label>
							<input type="text" name="text_select_campaign_subject" class="auto-select" value="<?=$ad['store_attributes']['public_name']?> Weekly Specials Starting <?=date('F j, Y', strtotime($ad['date_from']))?>" readonly size="50" />
							<span class="note">Click to select</span>
						</li>
						<li>
							<label>From Name and Address:</label>
							<input type="text" name="text_select_campaign_from_nam e" class="auto-select" value="<?=$ad['store_attributes']['public_name']?>" readonly size="20" />
							<input type="text" name="text_select_campaign_from_address" class="auto-select" value="<?=$ad['store_attributes']['email_from_address']?>" readonly size="25" />
							<span class="note">Click to select</span>
						</li>
						<li>
							<a href="https://nuggetmarkets.createsend.com/campaign/create/new" class="button" target="_blank">Define campaign on Campaign Monitor</a>
						</li>
					</ul>
				</li>
				<li>
					<label for="secret_special_title">Item name:</label>
					<input id="secret_special_title" type="text" name="secret_special_title" value="<?=(isset($ad['secret_special_title'])?$ad['secret_special_title']:'')?>" />
				</li>
				<li>
					<label for="secret_special_container">Container/size:</label>
					<input id="secret_special_container" type="text" name="secret_special_container" value="<?=(isset($ad['secret_special_container'])?$ad['secret_special_container']:'')?>" />
				</li>
				<li>
					<label for="secret_special_description">Ad copy:</label>
					<textarea id="secret_special_description" name="secret_special_description" class="tinymce" rows="4"><?=(isset($ad['secret_special_description'])?$ad['secret_special_description']:'')?></textarea>
					<div class="note">Use <code>[zoneurl]</code> instead of the domain name (<code>https://www.nuggetmarket.com</code>) when linking to an article or recipe on the&nbsp;website. <a href="/img/zoneurl-example.png">Example</a></div>
				</li>
				<li>
					<label for="secret_special_retail">Secret Special retail:</label>
					<input id="secret_special_retail" type="text" name="secret_special_retail" value="<?=(isset($ad['secret_special_retail'])?strip_tags($ad['secret_special_retail']):'')?>" size="60" /> 
					<div class="note">For multiple Secret Specials, separate prices with a pipe: | (shift + \)</div>
				</li>
				<li>
					<label for="secret_special_regular_retail">Regular retail:</label>
					<input id="secret_special_regular_retail" type="text" name="secret_special_regular_retail" value="<?=(isset($ad['secret_special_regular_retail'])?strip_tags($ad['secret_special_regular_retail']):'')?>" size="60" />
				</li>
				<li>
					<label for="secret_special_disclaimers">Disclaimers:</label>
					<textarea name="secret_special_disclaimers" id="secret_special_disclaimers"><?=(isset($ad['secret_special_disclaimers'])?$ad['secret_special_disclaimers']:'')?></textarea>
					<div class="note">These will be prepended above the limit and valid UPCs in small&nbsp;type.</div>
				</li>
				<li>
					<label for="secret_special_limit">Limit:</label>
					<input type="text" name="secret_special_limit" value="<?=(isset($ad['secret_special_limit'])?$ad['secret_special_limit']:'')?>" placeholder="ex. 10 packages" id="secret_special_limit" size="40" class="short" />
					<div class="note">Will display as &ldquo;Limit <span id="secret_special_limit_text"><?=(isset($ad['secret_special_limit'])?$ad['secret_special_limit']:'(limit field value)')?></span> per guest.&rdquo; Defaults to Limit 4 if left blank.</div>
				</li>
				<li>
					<label for="secret_special_skus">UPCs or PLUs Included:</label>
					<textarea name="secret_special_skus" id="secret_special_skus" placeholder="ex. UPCs: 0000123456789, 00045678910, 000987654321"><?=(isset($ad['secret_special_skus'])?$ad['secret_special_skus']:'')?></textarea>
					<div class="note">Will display as &ldquo;Only valid for <span id="secret_special_skus_text"><?=(isset($ad['secret_special_skus'])?$ad['secret_special_skus']:'(SKUs field value)')?></span>&rdquo;</div>
				</li>
				<? if ($picture_label) { ?>
					<li>
						<label>Lifestyle Icons:</label>
						<p class="lifestyle-icons">
						<? foreach ($secret_special['secret_special_labels'] as $label) { ?>
							<img src="/img/labels/<?=$label['image']?>" alt="<?=$label['description']?>" />	
						<? } ?>
						</p>
					</li>
					<li>
						<label>Picture these SKUs:</label>
						<ul class="picture-upcs">
						<? foreach ($secret_special['secret_special_picture'] as $picture) { if ($picture['picture']) {?>
							<li><img src="<?=$picture['picture']?>" alt="" /> - <?=$picture['upc']?></li>
						<? } else { ?>
							<li><?=$picture['upc']?></li>
						<? } } ?>
						</ul>
					</li>
				<? } ?>
			</ul>
		</fieldset>
		
		<fieldset>
			<legend>Image</legend>
			<ul>
				<? if (isset($ad['secret_special_image'])) { ?>
					<li class="current-image">
						<label for="secret_special_current_image">Current Image:</label>
						<img src="/media/<?=$ad['secret_special_image']?>" alt="" width="340" />
						<input type="hidden" name="secret_special_current_image" value="<?=$ad['secret_special_image']?>" />
						<a href="/delete.php?secret_special_image=<?=$item_id?>" class="delete">Delete</a>
					</li>
				<? } ?>
				<li>
					<label for="secret_special_image">New Image:</label>
					<input type="file" name="secret_special_image" id="secret_special_image" class="jpg" />
					<div class="note">Use landscape or square image. System will automatically resize to 680px wide.</div>
				</li>
			</ul>
		</fieldset>
		
		<fieldset>
			<legend>Editorial</legend>
			<p>Include a custom message and/or append articles or recipes to the&nbsp;email.
				<? if (!empty($_GET['id'])) { ?>
					<input type="hidden" name="features_file" value="<?=(isset($ad['features_file'])?$ad['features_file']:'')?>" />
				<? } ?>
			</p>
			<ul>
				<li>
					<label for="prepended_message">Message:</label>
					<textarea name="prepended_message" id="prepended_message" class="tinymce" rows="5"><?=(isset($ad['prepended_message'])?$ad['prepended_message']:'')?></textarea>
				</li>
				<li class="inline">
					Position:&nbsp;&nbsp;&nbsp;
					<label><input type="radio" name="prepended_message_position" value="1" <?if(!$ad['prepended_message_position'] || $ad['prepended_message_position'] == 1){echo'checked="checked"';}?> /> Beginning of email</label>
					<label><input type="radio" name="prepended_message_position" value="2" <?if($ad['prepended_message_position'] == 2){echo'checked="checked"';}?> /> After Secret Special</label>
					<label><input type="radio" name="prepended_message_position" value="3" <?if($ad['prepended_message_position'] == 3){echo'checked="checked"';}?> /> End of email</label>
				</li>
			</ul>
			
			<? if (isset($_GET['id'])) { ?>
			<ul>
				<li id="zones" class="articles-and-recipes">
					<div id="articles-<?=$ad['id']?>">
						<ul>
						<? if (isset($ad['articles'])) { $a = 0; foreach ($ad['articles'] as $article) { ?>
							<li id="article-<?=$ad['id']?>-<?=$article?>-<?=$a?>">
								<label for="article[<?=$ad['id']?>][<?=$a?>]">Article ID:</label>
								<input id="article[<?=$ad['id']?>][<?=$a?>]" type="text" name="article[<?=$ad['id']?>][<?=$a?>]" value="<?=$article?>" size="3" class="short" /> <span class="note">the number in the URL</span>
								<a href="#" data-delete="article-<?=$ad['id']?>-<?=$article?>-<?=$a?>" class="delete x remove">&times;</a>
							</li>
						<? $a++; } } ?>
						</ul>
						<p><button id="append-article-<?=$ad['id']?>" data-class="article" data-id="<?=$ad['id']?>" class="button">Article +</button> <a href="<?=$ad['store_attributes']['article_archive']?>" target="_blank">Article search</a></p>
					</div>
					<div id="recipes-<?=$ad['id']?>">
						<ul>
						<? if (isset($ad['recipes'])) { $r = 0; foreach ($ad['recipes'] as $recipe) { ?>
							<li id="recipe-<?=$ad['id']?>-<?=$recipe?>-<?=$r?>">
								<label for="recipe[<?=$ad['id']?>][<?=$r?>]">Recipe ID:</label>
								<input id="recipe[<?=$ad['id']?>][<?=$r?>]" type="text" name="recipe[<?=$ad['id']?>][<?=$r?>]" value="<?=$recipe?>" size="3" class="short" /> <span class="note">the number in the URL</span>
								<a href="#" data-delete="recipe-<?=$ad['id']?>-<?=$recipe?>-<?=$r?>" class="delete x remove">&times;</a>
							</li>
						<? $r++; } } ?>
						</ul>
						<p><button id="append-recipe-<?=$ad['id']?>" data-class="recipe" data-id="<?=$ad['id']?>" class="button">Recipe +</button> <a href="<?=$ad['store_attributes']['recipe_archive']?>" target="_blank">Recipes search</a></p>
					</div>
				</li>
			</ul>
			<? } ?>
		</fieldset>
		
		<fieldset>
			<legend>Proof Campaign</legend>
			<p>Preview the email campaign and/or email a link to the proof.</p>
			<ul>
				<li>
					Preview the email: <a href="/email/<?=$ad['id']?>/" target="_blank" class="button">HTML version</a>
					<a href="/email-text/<?=$ad['id']?>/" target="_blank" class="button">Plain text version</a>
					<? if (isset($secret_special_email_proof_body)) { ?><a href="mailto:Rebecca Reichardt<rebecca.reichardt@nuggetmarket.com>,Calista Marouk<calista.marouk@nuggetmarket.com>,Kim Scott<kim.scott@nuggetmarket.com>?subject=<?=$secret_special_email_subject?>&amp;body=<?=$secret_special_email_proof_body?>" class="button">Send Secret Special proof email</a><? } ?>
					<input type="hidden" name="campaign_name" value="<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>" />
					<input type="hidden" name="campaign_subject" value="<?=$ad['store_attributes']['public_name']?> Weekly Specials Starting <?=date('F j, Y', strtotime($ad['date_from']))?>" />
				</li>
			</ul>
		</fieldset>
		
		<fieldset>
			<legend>Manage Campaign</legend>
			<p>Manage drafts and scheduling from the Secret Specials screen.</p>
			<ul>
				<li>
					<a href="/secret-specials/edit/<?=$ad['date_from']?>/#zones" target="_blank" class="button">Manage Campaign</a>
				</li>
			</ul>
		</fieldset>
	</section>

	<fieldset id="publish-settings">
		<legend>Publish Settings</legend>
		<ul>
			<li>
				<label><input type="checkbox" name="truckload" value="1" id="truckload" <?if(isset($ad['truckload'])&&$ad['truckload']==1){echo'checked="checked"';}?>> Truckload <span class="note">display truckload banner on this ad</span></label>
				<ul>
					<li class="inline">
						<label for="truckload_dates">Truckload Dates:</label>
						<input type="text" name="truckload_dates" id="truckload_dates" value="<?=(isset($ad['truckload_dates'])?$ad['truckload_dates']:'')?>" size="40" placeholder="ex: January 10&ndash;23, <?=date('Y')?>" />
					</li>
				</ul>
			</li>
			<li>
				<label><input type="checkbox" name="ocean_feast" value="1" id="ocean_feast" <?if(isset($ad['ocean_feast'])&&$ad['ocean_feast']==1){echo'checked="checked"';}?>> Ocean Feast <span class="note">display ocean feast banner on this ad</span></label>
				<ul>
					<li class="inline">
						<label for="ocean_feast_dates">Ocean Feast Dates:</label>
						<input type="text" name="ocean_feast_dates" id="ocean_feast_dates" value="<?=(isset($ad['ocean_feast_dates'])?$ad['ocean_feast_dates']:'')?>" size="40" placeholder="ex: March 3, 4 &amp; 5, 9 a.m. to 6 p.m." />
					</li>
				</ul>
			</li>
			<li>
				<label><input type="checkbox" name="publish" value="1" id="publish" <?if(isset($ad['publish'])&&$ad['publish']==1){echo'checked="checked"';}?>> Active <span class="note">display this ad on the site when run period is in range</span></label>
			</li>
			<li>
				<label for="proof_email">Send proof email:</label>
				<? if ($ad['publish'] == 1) { ?>
				<a href="mailto:Adnug&lt;adnug@nuggetmarket.com&gt;,John Lynch&lt;john.lynch@nuggetmarket.com&gt;?subject=Ad Proofs for <?=date('M j, Y',strtotime($ad['date_from']))?>&amp;body=Hi everyone,%0D%0DThe <?=date('M j, Y',strtotime($ad['date_from']))?> ads are ready to proof:%0D%0Dhttps://specials.nuggetmarkets.net/proofs/%0D" class="button">Send ad proof email to Adnug</a>
				<? } else { ?>
					<span class="button disabled" disabled>Send ad proof email to Adnug</span>
					<span class="note">Ad must be active to send the proof&nbsp;email.</span>
				<? } ?>
			</li>
			
		</ul>
	</fieldset>
	<ul class="actions">
		<? if ($page_attrs['action'] == 'new') { ?>
		<li>
			<input type="submit" name="add" value="Create ad" />
		</li>
		<li><a href="/ads/">Cancel</a></li>
		<? } else { ?>
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="id" value="<?=$_GET['id']?>" />
		</li>
		<li><a href="/ads/">Cancel</a></li>
		<li><a href="/delete/ads/<?=$_GET['id']?>/" class="delete">Delete ad</a></li>
		<? } ?>
	</ul>
</form>
