<ol class="ad-listing">
	<? foreach ($ads as $ad) { ?>
	<li id="ad-<?=$ad['date_slug']?>" class="ad <?=$ad['status']?>">
		<h2><?=date('F j, Y', strtotime($ad['ad_date']))?> <small>running through <?=date('F j, Y', strtotime($ad['running_through']))?></small></h2>
		<ol class="timeline <?=$ad['current_milestone']?>">
		<? foreach ($ad['days'] as $day) { if (isset($day['milestone'])) { ?>
			<li class="day d-<?=$day['date']?> ms <?=$day['class']?>">
				<span class="milestone"><?=$day['milestone']?></span>
				<span class="date"><?=date('n/j', strtotime($day['date']))?></span>
				<? if ($day['date'] == date('Y-m-d')) { ?> 
					<span class="today">Today</span> 
				<? } else { ?>
					&nbsp;
				<? } ?>
			</li>
		<? } else { ?>
			<li class="day d-<?=$day['date']?> <?=$day['class']?>">
				<? if ($day['date'] == date('Y-m-d')) { ?> 
					<span class="today">Today</span> 
				<? } else { ?>
					&nbsp;
				<? } ?>
			</li>
		<? } } ?>
		</ol>
		<menu class="zones">
			<? foreach ($ad['zones'] as $zone) { ?>
			<li class="zone">
				<? if ($zone['publish']) { ?>
					<a href="/ads/publish-status/<?=$zone['publish']?>/id/<?=$zone['id']?>/" data-id="<?=$zone['id']?>" data-status="<?=$zone['publish']?>" class="status active" title="Active - click to deactivate">&#9679;</a>
				<? } else { ?>
					<a href="/ads/publish-status/<?=$zone['publish']?>/id/<?=$zone['id']?>/" data-id="<?=$zone['id']?>" data-status="<?=$zone['publish']?>" class="status not-active" title="Not active - click to activate">&#9675;</a>
				<? } ?>
				<h3><a href="/ads/edit/<?=$zone['id']?>/" title="Manage the settings for this ad"><?=$zone['store_chain']?></a></h3>
				<section class="updated">
					<dl>
						<dt>Import</dt>
						<dd>
						<? if ($zone['publish']) { ?>
							<a href="/ad-items-soft-import/<?=$zone['date_from']?>/<?=$zone['id']?>/" class="button import" title="Import data from DROSTE">Import items</a>
						<? } else { ?>
							<a href="/ad-items-import/<?=$zone['date_from']?>/<?=$zone['id']?>/" class="button import" title="Import data from DROSTE">Import items</a>
						<? } ?>
						<? if (isset($zone['last_import'])) { ?><small class="inline-date"><?=date('M j, Y @ g:i a', strtotime($zone['last_import']))?></small><? } ?>
						</dd>
					</dl>
					<dl>
						<dt>Cache</dt>
						<dd>
							<a href="/ad-json-export/<?=$zone['id']?>/" class="button refresh" title="Refresh the cache">Refresh cache</a>
							<? if (isset($zone['cache_file_updated'])) { ?>
								<small class="inline-date"><?=$zone['cache_file_updated']?></small>
							<? } else { ?>
								<small>Not created</small>
							<? } ?>
						</dd>
					</dl>
					
				</section>
				<a href="<?=$zone['store_attributes']['ad_url']?>date/<?=$zone['date_from']?>/" class="button preview" title="See a live preview of this ad" target="_blank">Ad Preview</a>
				<a href="/ad-builder/<?=$zone['id']?>/" class="button build" title="Open this ad in the ad builder">Ad Builder</a>
			</li>
			<? } ?>
		</menu>
	</li>
	<? } ?>
</ol>
