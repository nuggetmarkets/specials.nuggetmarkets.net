<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" href="/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="/css/application.css?i=56" />
		<? if (isset($page_attrs['append_head'])) { echo $page_attrs['append_head']; } ?>
		<title><?=$page_attrs['title']?></title>
	</head>
	<body class="<?=$page_attrs['class']?>">
		<a href="#main" class="skip-main" id="top">Skip to main content</a>
		<header class="mobile-header">
			<img src="/img/ad-builder.svg" alt="<?=SITE_NAME?>" title="There is no E-Ad. This IS the ad!" />
			<a href="#main-nav" id="show-menu"><img src="/img/hamburger.svg" /> Menu</a>
		</header>

		<nav id="main-nav" class="main-nav">
			<ul>
				<li class="close"><a href="#main-nav" id="close-menu">&times;</a></li>
				<li class="logo"><img src="/img/ad-builder.svg" alt="<?=SITE_NAME?>" title="There is no E-Ad. This is the ad!" /></li>
			<? if (isset($_SESSION['user_id'])) { ?>
				<li class="dashboard"><a href="/">Dashboard</a></li>
			</ul>
			<h3>Ads</h3>
			<ul>
				<li class="proofs"><a href="/proofs/">Proofs</a></li>
				<? if (in_array('plans', $_SESSION['access'])) { ?>
				<li class="ad-plans"><a href="/ad-plans/">Ad Plans</a></li>
				<? } ?>
				<? if (in_array('ads', $_SESSION['access']) && $_SESSION['user_level'] == 1) { ?>
				<li class="ads"><a href="/ads/">Ads</a></li>
				<li class="secret-specials"><a href="/secret-specials/">Secret Specials</a></li>
				<? } ?>
				<? if (in_array('retractions', $_SESSION['access'])) { ?>
				<li class="retractions"><a href="/retractions/">Retractions</a></li>
				<? } ?>
				<li class="archive"><a href="/archive/">Ad Archive</a></li>
			</ul>
			<h3>Settings</h3>
			<ul>
				<? if (in_array('products', $_SESSION['access'])) { ?>
				<li class="products"><a href="/products/">Products</a></li>
				<? } ?>
				<? if (in_array('media', $_SESSION['access'])) { ?>
				<li class="media"><a href="/media/">Photos</a></li>
				<? } ?>
				<? if (in_array('ads', $_SESSION['access'])) { ?>
				<li class="labels"><a href="/labels/">Labels</a></li>
				<li class="banners"><a href="/banners/">Banners</a></li>
			</ul>
			<h3>Help</h3>
			<ul>
				<li class="documentation"><a href="/media/27/08/using-the-e-ad-builder.pdf" title="Official documentation by Mark Eagleton">Using the Ad Builder</a></li>
				<li class="upc-images">
					<form action="/upc-image-uploader/" method="post" enctype="multipart/form-data">
						<ul>
							<li>
								<div class="upload-disclaimer">
									Upload clipped package photos with transparent backgrounds in WebP format&nbsp;only. 
								</div>
								<input type="file" name="upc_images[]" id="upc_images" multiple="multiple" class="image-drop" data-multiple-caption="{count} files selected" />
								<label for="upc_images"><span>Drop UPC images&nbsp;here</span></label>
								<input type="submit" name="upc_image_upload" value="Upload" class="upload-button" />
							</li>
						</ul>
					</form>
				</li>
				<? } ?>
				<li class="profile">
					<a href="<?=PROFILES_URL?>/profiles/edit/<?=$_SESSION['user_id']?>/" target="_blank"><img src="<?=$_SESSION['avatar']?>" alt="Your avatar" class="avatar" /></a>
					<ul id="profile">
						<li class="edit-profile"><a href="<?=PROFILES_URL?>/profiles/edit/<?=$_SESSION['user_id']?>/" target="_blank"><?=$_SESSION['full_name']?></a></li>
						<li class="logout"><a href="/logout/">Logout</a></li>
					</ul>
				</li>
			<? } else { ?>
				<li class="login"><a href="/login/">Login</a></li>
				<li class="reset"><a href="<?=PROFILES_URL?>/reset/" title="Go to profiles.nuggetmarkets.net">Reset Password &rarr;</a></li>
			<? } ?>
			</ul>
		</nav>

		<main id="main" class="main" tabindex="-1">
