<table class="listing">
	<thead>
		<tr>
			<th>Item/<small>date</small></th>
			<th>Ad Date</th>
			<th>
				Email Status<br /> 
				<span class="status">pending</span> 
				<span class="status draft">draft</span> 
				<span class="status scheduled">scheduled</span> 
				<span class="status sent">sent</span>
			</th>
		</tr>
	</thead>
	<tbody>
	<? if ($secret_specials) { foreach ($secret_specials as $secret_special) { ?>
		<tr>
			<td>
				<a href="/secret-specials/edit/<?=$secret_special['date_from']?>/"><?=$secret_special['item']?></a> <? if ($secret_special['zoned']) {?><span class="zoned">zoned</span> <?} ?><br />
				<em><small><?=$secret_special['secret_special_range']?></small></em>
			</td>
			<td class="ad-date"><?=date('F j, Y', strtotime($secret_special['date_from']))?></td>
			<td class="email-statuses">
				<? if (is_array($secret_special['campaign_statuses'])) { foreach ($secret_special['campaign_statuses'] as $status) { ?>
				<span class="flag <?=$status['class']?>" title="<?=$status['store_chain']?> <?=$status['class']?>"><?=$status['label']?></span>
				<? } } ?>
			</td>
		</tr>
	<? } } else { ?>
		<tr><td colspan="4">No Secret Specials to display.</td></tr>
	<? } ?>
	</tbody>
</table>

<footer>
	<? if ($secret_specials['pagination']) { ?>
	<nav class="pagination">
	<? if (!is_null($secret_specials['pagination']['first'])) { ?>
		<a href="/ads/page/1/">First</a>
		<a href="/ads/page/<?=$secret_specials['pagination']['previous']?>/">Prev</a>
	<? } else { ?>
		<span class="deact">First</span>
		<span class="deact">Prev</span>
	<? } ?>
	<span class="total">Page <?=$secret_specials['pagination']['current']?> of <?=$secret_specials['pagination']['total']?></span>
	<? if (!is_null($secret_specials['pagination']['last'])) { ?>
		<a href="/ads/page/<?=$secret_specials['pagination']['next']?>/">Next</a>
		<a href="/ads/page/<?=$secret_specials['pagination']['last']?>/">Last</a>
	<? } else { ?>
		<span class="deact">Next</span>
		<span class="deact">Last</span>
	<? } ?>
	</nav>
	<? } ?>
</footer>
