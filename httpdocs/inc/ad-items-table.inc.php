<tr>
	<td>

	<? if ($product['feature_photo_small']) { ?>
	<figure class="feature-image">
		<a href="/media/<?=$product['feature_photo_original']?>" target="_blank"><img src="/media/<?=$product['feature_photo_small']?>" /></a>
	</figure>
	<? }  ?>

	</td>
	<td>
		<? if ($product['prefix']) { ?><?=$product['prefix']?><br /><? } ?>
		<strong><?=$product['name']?></strong>
		<? if ($product['group_labels']) { ?>
		<ul class="badges">
		<? foreach ($product['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" /></li>
		<? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" /></li>
		<? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" /></li>
		<? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
			<li><img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" /></li>
		<? } } } ?>
		</ul>
		<? } ?>
		<? if ($product['suffix']) { ?><p><?=$product['suffix']?></p><? } ?>
	</td>
	<td>
		<ul class="upcs">
		<? $i = 0; foreach($product['upcs'] as $upc) { ?>
			<li>
				<? if (file_exists('img/upc/' . $upc . '.jpg')) { ?>
					<a href="/img/upc/<?=$upc?>.jpg"><img src="/img/upc/<?=$upc?>.jpg" alt="<?=$upc['upc']?> product image" /></a>
				<? } else { ?>
					<div>&nbsp;</div>
				<? } ?>
				<div>
					<?=$upc?> - <?=$product['pos_name'][$i]?><br />
					<a href="https://platform.syndigo.com/marketplace/all-products" target="_blank" class="button">Syndigo</a>
					<a href="//www.google.com/#q=<?=urlencode(strtolower($product['pos_name'][$i]))?>" target="_blank" class="button">Google</a>
					<? if (!strstr($_SERVER['HTTP_USER_AGENT'], 'Chrome')) { ?>
					<a href="afp://i.nuggetmarket.com/Marketing/" target="_blank" class="button">Server</a>
					<? } ?>
				</div>
			</li>
		<? $i++; } ?>
		</ul>
	</td>
	<td>
	<? if (
		isset($product['size']) &&
		!empty($product['container']) &&
		!strstr($product['sale_price'],'lb.') &&
		!strstr($product['sale_price'],'lbs.') &&
		!strstr($product['sale_price'],'/ea.') &&
		!strstr($product['sale_price'],'%') &&
		(
			$product['unit'] != 'CT' ||
			$product['size'] > 1
		)
	) { ?>
		<span class="pack-size"><?=$product['size']?> <?=$product['unit']?>. <?=$product['container']?></span>
	<? } else if (isset($product['container_sizes'])) {$i = 1; foreach ($product['container_sizes'] as $key => $value) { if ($value) { ?>
		<div class="pack-size"><?=$value?> <?=$product['units'][$i]?>. <?=$product['containers'][$key]?></div>
	<? $i++; } } } ?>
	</td>
	<td class="currency">
		<span class="sale"><?=$product['sale_price']?></span>
	</td>
	<td class="currency">
	<? if ($product['savings']) { ?>
		<span class="savings"><?=$product['savings']?></span>
	<? } else { ?>
		N/A
	<? } ?>
	</td>
</tr>
