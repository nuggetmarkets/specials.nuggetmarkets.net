			<footer>
				<p><?=SITE_NAME?> Copyright&nbsp;&copy;&nbsp;<?=date('Y')?></p>
			</footer>
		</main>
		<? if (
				isset($_GET['action']) == 'edit' ||
				isset($_GET['action']) == 'new' ||
				strstr($_SERVER['PHP_SELF'],'ad-item-group.php') ||
				strstr($_SERVER['PHP_SELF'],'ad-item-config.php') ||
				strstr($_SERVER['PHP_SELF'],'ads.php') ||
				strstr($_SERVER['PHP_SELF'],'index.php')
		) { ?>
			<script src="/js/tinymce/tinymce.min.js"></script>
			<script src="/js/tinymce-config.js"></script>
		<? } ?>
		<script src="/js/jquery-1.11.2.min.js"></script>
		<? if (strstr($_SERVER['PHP_SELF'],'ad-builder.php') || strstr($_SERVER['PHP_SELF'],'ad-report.php') || strstr($_SERVER['PHP_SELF'],'specials.php')) { ?>
			<script src="/js/jquery.scrollTo.min.js"></script>
		<? } ?>
		<script src="/js/application.js?i=9" charset="utf-8"></script>
		<? if (isset($page_attrs['append_footer'])) { echo $page_attrs['append_footer']; } ?>
	</body>
</html>
