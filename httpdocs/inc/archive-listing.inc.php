<table class="listing">
	<thead>
		<tr>
			<th>Ad</th>
			<th>View</th>
		</tr>
	</thead>
	<tbody>
	<? if ($ads['listing']) { foreach ($ads['listing'] as $ad) { ?>
		<tr class="current-<?=$ad['current_ad']?>">
			<td>
				<a href="/specials/<?=$ad['store_attributes']['slug']?>/<?=$ad['date_from']?>/"><?=date('F j, Y', strtotime($ad['date_from']))?> - <?=$ad['store_chain']?></a><br />
				<em><small>Running through <?=date('F j, Y', strtotime($ad['date_through']))?></small></em><br />
			</td>
			<td>
				<? if ($ad['publish']) { ?>
				<a href="<?=$ad['store_attributes']['ad_url']?>date/<?=$ad['date_from']?>/" class="button small">On website</a>
				<? } ?>
				<? if ($ad['secret_special_title']) { ?>
				<a href="/email/<?=$ad['id']?>/" class="button small">Secret Special email</a> 
				<? } ?>
				<? if ($ad['print_file']) { ?>
				<a href="/print-versions/<?=$ad['print_file']['name']?>" class="button small">Print ad</a>
				<? } ?>
			</td>
			
		</tr>
	<? } } else { ?>
		<tr><td colspan="4">No ads to display.</td></tr>
	<? } ?>
	</tbody>
</table>

<footer>
	<? if ($ads['pagination']) { ?>
	<nav class="pagination">
	<? if (!is_null($ads['pagination']['first'])) { ?>
		<a href="/archive/page/1/">First</a> 
		<a href="/archive/page/<?=$ads['pagination']['previous']?>/">Prev</a> 
	<? } else { ?>
		<span class="deact">First</span> 
		<span class="deact">Prev</span>
	<? } ?>
	<span class="total">Page <?=$ads['pagination']['current']?> of <?=$ads['pagination']['total']?></span>
	<? if (!is_null($ads['pagination']['last'])) { ?>
		<a href="/archive/page/<?=$ads['pagination']['next']?>/">Next</a> 
		<a href="/archive/page/<?=$ads['pagination']['last']?>/">Last</a> 
	<? } else { ?>
		<span class="deact">Next</span> 
		<span class="deact">Last</span>
	<? } ?>
	</nav>
	<? } ?>
</footer>
