<form action="/retractions/" method="post">

	<fieldset>
		<legend>Details</legend>
		<p>The item name, reason for the retraction, and which ad it is for.</p>
		<ul>
			<li>
				<label for="name">Item:</label>
				<input type="text" name="name" id="name" value="<?=unescape($retraction['name'])?>" required="required" />
				<div class="note required">Required</div>
			</li>
			<li>
				<label for="description">Description:</label>
				<textarea name="description" id="description" class="tinymce"><?=unescape($retraction['description'])?></textarea>
			</li>
			<li>
				<label for="ad_id">For Ad:</label>
				<select name="ad_id">
				<? foreach ($ads['listing'] as $ad) { ?>
					<? if ($ad['id'] == $retraction['ad_id']) { ?>
					<option value="<?=$ad['id']?>" selected="selected"><?=date('M j, Y', strtotime($ad['date_from']))?> <?=$ad['store_chain']?></option>
					<? } else { ?>
					<option value="<?=$ad['id']?>"><?=date('M j, Y', strtotime($ad['date_from']))?> <?=$ad['store_chain']?></option>
					<? } ?>
				<? } ?>
				</select>
				<? if (!empty($_GET['action']) && $_GET['action'] == 'duplicate') { ?>
					<span class="note error">Don&rsquo;t forget to select the appropreate ad</span>
				<? } ?>
			</li>

		</ul>
		<ul>
			<? if (isset($retraction['id'])) { ?>
			<li>
				<a href="/retractions/print/<?=$retraction['id']?>/" class="button">Print retraction</a>
				<? if ($_SESSION['user_level'] == 1) { ?>
					<a href="/retractions/duplicate/<?=$retraction['id']?>/" class="button">Duplicate</a>
				<? } ?>
			</li>
			<? } ?>
		</ul>
	</fieldset>

	<ul class="actions">
		<? if ($_GET['action'] == 'new' || $_GET['action'] == 'duplicate') { ?>
		<li>
			<input type="submit" name="add" value="Create retraction" />
		</li>
		<li><a href="/retractions/">Cancel</a></li>
		<? } else { ?>
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="retraction_id" value="<?=$_GET['id']?>" />
		</li>
		<li><a href="/retractions/">Cancel</a></li>
		<li><a href="/delete/retractions/<?=$_GET['id']?>/" class="delete">Delete retraction</a></li>
		<? } ?>
	</ul>
</form>
