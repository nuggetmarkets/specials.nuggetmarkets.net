<? if ($products['listing']) { ?>
<table class="listing">
	<thead>
		<tr>
			<th>UPC<small>/POS Name</small></th>
			<th>Family Group<small>/Department</small></th>
			<th>Retail Price</th>
		</tr>
	</thead>
	<tbody>
	<? foreach ($products['listing'] as $item) { ?>
		<tr>
			<td>
				<a href="/products/edit/<?=$item['upc']?>/"><?=$item['upc']?></a><br />
				<small><?=$item['pos_name']?></small>
			</td>
			<td>
				<? if ($_SESSION['user_level'] == 1) { ?>
					<a href="/ad-item-config/<?=$item['family_uri']?>/"><?=$item['family_group']?></a><br />
				<? } else { ?>
					<?=$item['family_group']?><br />
				<? } ?>
				<small><?=$item['department']?></small>
			</td>
			<td><?=$item['retail_price']?></td>
		</tr>
	<? } ?>
	</tbody>
</table>
<? } else if (isset($_GET['terms'])) { ?>
<p>No results found.</p>
<? } else { ?>
<p>Search results will be displayed here.</p>
<? } ?>
