<? if ($error) { ?>
<div id="message" class="message error" title="Click or tap to dismiss">
    <div class="icon">
        <img src="/img/exclaimation.svg" alt="check" />
    </div>
    <div class="text">
    <? foreach ($error as $err) { ?>
        <p><?=$err?></p>
    <? } unset($_SESSION['error']); ?>
    </div>
    <div class="close">
        <img src="/img/x.svg" alt="x" />
    </div>
</div>
<? } else if (isset($_SESSION['error'])) { ?>
<div id="message" class="message error" title="Click or tap to dismiss">
    <div class="icon">
        <img src="/img/exclaimation.svg" alt="check" />
    </div>
    <div class="text">
        <p><?=$_SESSION['error']?></p>
        <?unset($_SESSION['error'])?>
    </div>
    <div class="close">
        <img src="/img/x.svg" alt="x" />
    </div>
</div>
<? } else if (isset($confirm)) { ?>
<div id="message" class="message confirm" title="Click or tap to dismiss">
    <div class="icon">
        <img src="/img/check.svg" alt="check" />
    </div>
    <div class="text">
        <p><?=$confirm?></p>
    </div>
    <div class="close">
        <img src="/img/x.svg" alt="x" />
    </div>
</div>
<? } else if (isset($_SESSION['confirm'])) { ?>
<div id="message" class="message confirm" title="Click or tap to dismiss">
    <div class="icon">
        <img src="/img/check.svg" alt="check" />
    </div>
    <div class="text">
        <p><?=$_SESSION['confirm']?></p>
        <?unset($_SESSION['confirm'])?>
    </div>
    <div class="close">
        <img src="/img/x.svg" alt="x" />
    </div>
</div>
<? } ?>
