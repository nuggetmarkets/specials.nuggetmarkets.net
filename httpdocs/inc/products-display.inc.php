
<section>
	<legend>Item Identifier</legend>
	<? if (isset($item['upc'])) { ?>
	<p>These attributes are populated by DROSTE when an import operation is run and are not editable on this screen.</p>
	<? } else { ?>
	<p>These attributes are populated by DROSTE and may be overwritten when an import operation is run.</p>
	<p><a href="http://dsd99" target="_blank" class="button">Search for product in DSD</a></p>
	<? } ?>
	<dl>
		<dt>UPC:</dt>
		<dd><?=unescape($item['upc'])?></dd>
		<dt>POS Name:</dt>
		<dd><?=unescape($item['pos_name'])?></dd>
		<dt>Family Group:</dt>
		<dd><?=unescape($item['family_group'])?></dd>
		<dt>Department:</dt>
		<dd><?=unescape($item['department'])?></dd>
		<dt>Lifestyle:</dt>
		<dd><?=unescape($item['lifestyle'])?></dd>
	</dl>
</section>

<section>
	<legend>Item Name</legend>
	<p>The item name and description as it will appear on future ads.</p>
	<dl>
		<dt>Prefix:</dt>
		<dd><?=unescape($item['prefix'])?></dd>
		<dt>Display Name:</dt>
		<dd>
			<?=unescape($item['display_name'])?>
			<div class="note">Overrides POS name</div>
		</dd>
		<dt>Suffix:</dt>
		<dd><?=unescape($item['suffix'])?></dd>
		<dt for="note">Note:</dt>
		<dd><?=unescape($item['note'])?></dd>
		<dt>Description:</dt>
		<dd><?=unescape($item['description'])?></dd>
	</dl>
</section>

<section>
	<legend>Pricing and Container</legend>
	<p>Set pricing and container attributes for the item.</p>
	<dl>
			<dt>Retail Price:</dt>
			<dd><?=unescape($item['retail_price'])?> <? if ($item['crv'] == 1) { echo '+CRV'; } ?></dd>
			<dt>Size:</dt>
			<dd><?=unescape($item['size'])?> <?=unescape($item['unit'])?> <? if ($item['container']) { echo $item['container']; } ?></dd>
			<dt>Limit:</dt>
			<? if ($item['limit']) { echo "Limit {$item['limit']}"; } else { echo 'None'; } ?> 
			<? if ($item['supplies_last'] == 1) { echo "While supplies last"; } ?> 
	</dl>
</section>

<section>
	<legend>Labels</legend>
	<p>Lifestly, brand and organization labels attached to this item.</p>
	<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle') { ?>
		
		<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
			<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
		<? } ?>
		
	<? } } ?>
	<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'brand') { ?>
		
		<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
			<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
		<? } ?>
		
	<? } } ?>
	<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'organization') { ?>
		
		<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
			<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
		<? } ?>
		
	<? } } ?>
	<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'seasoning') { ?>
		
		<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
			<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
		<? } ?>
		
	<? } } ?>
</section>

<section>
	<legend>Image</legend>
	<p>The UPC image for this item.</p>
	<dl>
		<? if (isset($item['upc_image'])) { ?>
			<dd><img src="/img/upc/<?=$item['upc_image']?>" alt="" width="340" style="vertical-align: middle;" /></dd>
		<? } else { ?>
			<dt>&nbsp;</dt>
			<dd>No image set.</dd>
		<? } ?>
	</dl>
</section>
