<li id="item-<?=$ad['id']?>-<?=$product['family_id']?>" class="<?=$product['status']?>">
	<div>
		<? if ($product['feature'] > 0) { ?>
			<div class="feature pos-<?=$product['feature']?>">Feature <strong><?=$product['feature']?></strong> Position</div>
		<? } ?>
		<span class="sort" title="Manual sort order"><?=$product['sort']?></span>
		<? if ($product['feature_photo_small']) { ?>
		<figure class="feature-photo">
			<a href="/media/<?=$product['feature_photo_original']?>">
				<img src="/media/<?=$product['feature_photo_small']?>" loading="lazy" decoding="async" />
			</a>
			<span class="ribbon">Feature image</span>
		</figure>
		<? } else if ($product['upc_photos']) { $i = 0; ?>

		<figure class="upc-photos num-<?=count($product['upcs'])?>">
			<a href="/ad-item-group/<?=$ad['id']?>/<?=$product['family_uri']?>/" target="_blank">
			<? foreach ($product['upc_photos'] as $img) { ?>
				<img src="<?=$img?>" loading="lazy" decoding="async" />
			<? } ?>
			</a>
		</figure>

		<? $i++; } else { ?>
		<figure class="watermark-image">
			<a href="/ad-item-group/<?=$ad['id']?>/<?=$product['family_uri']?>/" target="_blank">
				<img src="/img/watermark-<?=$ad['store_attributes']['slug']?>.svg" alt="<?=$ad['store_chain']?> watermark" loading="lazy" decoding="async" />
			</a>
		</figure>
		<? } ?>

		<? if ($product['group_labels']) { ?>
		<ul class="badges">
		<? foreach ($product['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" loading="lazy" decoding="async" /></li>
		<? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" loading="lazy" decoding="async" /></li>
		<? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
			<li><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" loading="lazy" decoding="async" /></li>
		<? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
			<li><img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" class="<?=$label['type']?>" loading="lazy" decoding="async" /></li>
		<? } } } ?>
		</ul>
		<? } ?>

		<div class="details">

			<? if ($product['prefix']) { ?><p><?=$product['prefix']?></p><? } ?>
			<h3><?=$product['name']?></h3>
			<? if ($product['suffix']) { ?>
			<p><?=$product['suffix']?></p>
			<? } if ($product['selected_varieties']) { ?>
			<p>Selected Varieties</p>
			<? } ?>
			<p>
				<? if (
					isset($product['size']) &&
					!empty($product['container']) &&
					!strstr($product['sale_price'],'lb.') &&
					!strstr($product['sale_price'],'lbs.') &&
					!strstr($product['sale_price'],'/ea.') &&
					!strstr($product['sale_price'],'%') &&
					(
						$product['unit'] != 'CT' ||
						$product['size'] > 1
					)
				) { ?>
				<span class="pack-size"><?=$product['size']?> <?=$product['unit']?>. <?=$product['container']?></span>
				<? } else if (
					isset($product['container_sizes']) && !strstr($product['sale_price'],'%')) {
						$i = 1; 
						foreach ($product['container_sizes'] as $key => $value) { 
							if ($value) { ?>
					<div class="pack-size"><?=$value?> <?=$product['units'][$i]?>. <?=$product['containers'][$key]?></div>
				<? $i++; 
							} 
						} 
					} 
				?>
				<? if ($product['note']) { ?><p><small><?=$product['note']?></small></p><? } ?>
				<p class="sale-price"><?=$product['sale_price']?></p>
				
				<? if ($product['savings']) { ?>
					<p class="savings">Save <?=$product['savings']?></p>
				<? } ?>
				<? if ($product['regular_retail']) { ?>
					<p class="regular-retail"><small>Regularly <?=$product['regular_retail']?></small></p>
				<? } ?>
				<? if ($product['limit']) { ?> 
					<div><?=$product['limit']?></div>
				<? } ?>
				<? if ($product['description']) { ?> 
					<div class="description">
						<?=$product['description']?>
					</div>
				<? } ?>
			</p>
			<div class="edit-groups">
			<? if ($product['manual_group']) { $i = 1; foreach ($product['family_group_uris'] as $uri) { ?>
			<p class="actions"><a href="/ad-item-group/<?=$ad['id']?>/<?=$uri?>/" class="button" target="_blank">Edit group <?=$i?></a> <a href="/ad-item-group/<?=$ad['id']?>/<?=$uri?>/ungroup/" class="delete x">&times;</a></p>
			<? $i++; } } else { ?>
			<p class="actions"><a href="/ad-item-group/<?=$ad['id']?>/<?=$product['family_uri']?>/" class="button" target="_blank">Edit group</a></p>
			<? } ?>
			</div>
			<div class="upcs">
				<a href="#upcs-<? if (isset($product['features'])) { echo 'feature-'; } ?><?=$product['family_id']?>" class="toggle button">UPCs</a>
				<ul id="upcs-<? if (isset($product['features'])) { echo 'feature-'; } ?><?=$product['family_id']?>">
				<? $i = 0; foreach($product['upcs'] as $upc) {  ?>
					<li><?=$upc?><br /><?=$product['pos_name'][$i]?></li>
				<? $i++;} ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="status <?=$product['status']?>">
		<form action="/ad-builder/<?=$_GET['id']?>/" method="post">
		<? if ($product['status'] == 'pending') { ?>
			<p>Awaiting Approval</p>
			<? if ($product['messages']['listing']) { ?>
			<ol class="messages">
				<? foreach ($product['messages']['listing'] as $message) { ?>
					<li>
						<div class="text"><?=$message['message']?></div>
						<div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
					</li>
				<? } ?>
			</ol>
			<? } ?>
			<ul>
				<li>
					<input type="hidden" name="group_name" value="<?=$product['group_name']?>" />
					<input type="hidden" name="family_group" value="<?=$product['family_uri']?>" />
					<input type="hidden" name="item_name" value="<?=$product['name']?>" />
					<input type="hidden" name="item_id" value="<?=$ad['id']?>-<?=$product['family_id']?>" />
					<input type="hidden" name="deparment" value="<?=$product['department_url']?>" />
					<input type="hidden" name="ad_date" value="<?=$ad['date_from']?>" />
					<input type="hidden" name="ad_id" value="<?=$ad['id']?>" />
					<? if ($product['message_reply_to']) { foreach ($product['message_reply_to'] as $reply_to) { ?>
						<input type="hidden" name="message_reply_to[]" value="<?=$reply_to?>" />
					<? } } ?>
					<input type="submit" name="approve" value="Approve" />
				</li>
			</ul>
		<? } else if ($product['status'] == 'issue') { ?>
			<p>Correction Needed</p>
			<? if ($product['messages']['listing']) { ?>
			<ol class="messages">
				<? foreach ($product['messages']['listing'] as $message) { ?>
					<li>
						<div class="text"><?=$message['message']?></div>
						<div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
					</li>
				<? } ?>
			</ol>
			<? } ?>
			<ul>
				<li>
					<input type="hidden" name="group_name" value="<?=$product['group_name']?>" />
					<input type="hidden" name="family_group" value="<?=$product['family_uri']?>" />
					<input type="hidden" name="item_name" value="<?=$product['name']?>" />
					<input type="hidden" name="item_id" value="<?=$ad['id']?>-<?=$product['family_id']?>" />
					<input type="hidden" name="department" value="<?=$product['department_url']?>" />
					<input type="hidden" name="ad_date" value="<?=$ad['date_from']?>" />
					<input type="hidden" name="ad_id" value="<?=$ad['id']?>" />
					<? if ($product['message_reply_to']) { foreach ($product['message_reply_to'] as $reply_to) { ?>
						<input type="hidden" name="message_reply_to[]" value="<?=$reply_to?>" />
					<? } } ?>
				</li>
				<li>
					<a href="#correction-<?=$ad['id']?>-<?=$product['family_id']?>" class="button correction toggle">Request approval</a>
                </li>
			</ul>
			<div id="correction-<?=$ad['id']?>-<?=$product['family_id']?>" class="correction-form">
				<textarea name="message" id="message-<?=$ad['id']?>-<?=$product['family_id']?>" cols="30" rows="3"></textarea>
				<ul>
					<li>
						<input type="submit" name="request_approval" value="Submit" />
					</li>
				</ul>
			</div>
		<? } else { ?>
			<p>Approved</p>
			<? if ($product['messages']['listing']) { ?>
			<a href="#messages-<?=$ad['id']?>-<?=$product['family_id']?>" class="toggle button">Messages</a>
			<ol class="messages" id="messages-<?=$ad['id']?>-<?=$product['family_id']?>">
				<? foreach ($product['messages']['listing'] as $message) { ?>
					<li>
						<div class="text"><?=$message['message']?></div>
						<div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
					</li>
				<? } ?>
			</ol>
			<? } ?>
		<? } ?>
		</form>
	</div>
	<? if (!$product['manual_group']) { ?>
	<label><input type="checkbox" data-familyid="family-<?=$product['family_id']?>" name="group-item[]" value="<?=$product['family_uri']?>" class="group" /> Group</label>
	<a href="/ad-item-group/<?=$ad['id']?>/<?=$product['family_uri']?>/disable/" data-adid="<?=$ad['id']?>" data-familygroup="<?=$product['family_uri']?>" data-itemid="item-<?=$ad['id']?>-<?=$product['family_id']?>" class="disable">&times;</a>
	<? } else { ?>
	
	<form class="item-group-rename" id="item-group-rename[<?=$product['family_id']?>]" action="/ad-builder/<?=$ad['id']?>/" method="post">
		<fieldset>
			<legend><a href="#group-<?=$ad['id']?>-<?=$product['family_id']?>" class="button toggle">Rename Group</a></legend>
			<ul id="group-<?=$ad['id']?>-<?=$product['family_id']?>">
				<li>
					<label for="group_name">Group name:</label>
					<input type="text" name="new_group_name" required="required" value="<?=$product['group_name']?>" />
				</li>
				<li>
					<input type="hidden" name="current_group_name" value="<?=$product['group_name']?>" />
					<input type="hidden" name="ad_id" value="<?=$ad['id']?>" />
					<input type="hidden" name="item_id" value="item-<?=$ad['id']?>-<?=$product['family_id']?>" />
					<input type="submit" name="rename_group" value="Rename" />
				</li>
			</ul>
		</fieldset>
	</form>
	<? } ?>
</li>
