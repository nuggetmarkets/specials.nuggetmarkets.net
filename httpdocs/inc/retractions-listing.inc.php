<table class="listing">
	<thead>
		<tr>
			<th>Item</th>
			<th>Ad</th>
			<th>Manage</th>
		</tr>
	</thead>
	<tbody>
	<? if ($retractions['listing']) { foreach ($retractions['listing'] as $retraction) { ?>
		<tr class="current-<?=$retraction['current_ad']?>">
			<td>
			<? if ($_SESSION['user_level'] == 1) { ?>
				<a href="/retractions/edit/<?=$retraction['id']?>/"><?=$retraction['name']?></a>
			<? } else {?>
				<?=$retraction['name']?>
			<? } ?>
			</td>
			<td>
				<?=$retraction['store_chain']?><br />
				<?=date('F j, Y', strtotime($retraction['date_from']))?>
			</td>
			<td>
			<? if ($_SESSION['user_level'] == 1) { ?>
				<a href="/retractions/duplicate/<?=$retraction['id']?>/" class="button">Duplicate</a> 
			<? } ?>
				<a href="/retractions/print/<?=$retraction['id']?>/" class="button">Print</a> 
			</td>
		</tr>
	<? } } else { ?>
		<tr><td colspan="4">No retractions to display.</td></tr>
	<? } ?>
	</tbody>
</table>

<footer>
	<? if ($retractions['pagination']) { ?>
	<nav class="pagination">
	<? if (!is_null($retractions['pagination']['first'])) { ?>
		<a href="/retractions/page/1/">First</a> 
		<a href="/retractions/page/<?=$retractions['pagination']['previous']?>/">Prev</a> 
	<? } else { ?>
		<span class="deact">First</span> 
		<span class="deact">Prev</span>
	<? } ?>
	<span class="total">Page <?=$retractions['pagination']['current']?> of <?=$retractions['pagination']['total']?></span>
	<? if (!is_null($retractions['pagination']['last'])) { ?>
		<a href="/retractions/page/<?=$retractions['pagination']['next']?>/">Next</a> 
		<a href="/retractions/page/<?=$retractions['pagination']['last']?>/">Last</a> 
	<? } else { ?>
		<span class="deact">Next</span> 
		<span class="deact">Last</span>
	<? } ?>
	</nav>
	<? } ?>
</footer>
