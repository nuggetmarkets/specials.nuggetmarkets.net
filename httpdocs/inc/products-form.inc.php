<form action="/products/<?=$page_attrs['post_slug']?>" method="post" enctype="multipart/form-data">

	<fieldset>
		<legend>Item Identifier</legend>
		<? if (isset($item['upc'])) { ?>
		<p>These attributes are populated by DROSTE when an import operation is run and are not editable on this screen.</p>
		<? } else { ?>
		<p>These attributes are populated by DROSTE and may be overwritten when an import operation is run.</p>
		<p><a href="http://dsd99" target="_blank" class="button">Search for product in DSD</a></p>
		<? } ?>
		<ul>
			<li>
				<label for="upc">UPC:</label>
				<input type="text" name="upc" id="upc" value="<?=unescape($item['upc'])?>" maxlength="13" <?if(isset($item['upc'])){?>disabled="disabled"<?}else{?>required="required"<?}?> />
				<div class="note required">Required</div>
			</li>
			<li>
				<label for="pos_name">POS Name:</label>
				<input type="text" name="pos_name" id="pos_name" value="<?=unescape($item['pos_name'])?>" <?if(isset($item['upc'])){?>disabled="disabled"<?}else{?>required="required"<?}?> />
				<div class="note required">Required</div>
			</li>
			<li>
				<label for="family_group">Family Group:</label>
				<input type="text" name="family_group" id="family_group" value="<?=unescape($item['family_group'])?>" size="20" <?if(isset($item['upc'])){?>disabled="disabled"<?}else{?>required="required"<?}?> />
				<? if (isset($item['upc'])) { ?>
				<a href="/ad-item-config/<?=$item['family_uri']?>/" class="button">Edit group</a>
				<? } ?>
				<div class="note required">Required</div>
			</li>
			<li>
				<label for="department">Department:</label>
				<input type="text" name="department" id="department" value="<?=unescape($item['department'])?>" size="10" <?if(isset($item['upc'])){?>disabled="disabled"<?}?> />
			</li>
			<li>
				<label for="lifestyle">Lifestyle:</label>
				<input type="text" name="lifestyle" id="lifestyle" value="<?=unescape($item['lifestyle'])?>" size="10" <?if(isset($item['upc'])){?>disabled="disabled"<?}?> />
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Item Name</legend>
		<p>The item name and description as it will appear on future ads.</p>
		<ul>
			<li>
				<label for="prefix">Prefix:</label>
				<input type="text" name="prefix" id="prefix" value="<?=unescape($item['prefix'])?>" />
			</li>
			<li>
				<label for="display_name">Display Name:</label>
				<input type="text" name="display_name" id="display_name" value="<?=unescape($item['display_name'])?>" />
				<div class="note">Overrides POS name</div>
			</li>
			<li>
				<label for="suffix">Suffix:</label>
				<input type="text" name="suffix" id="suffix" value="<?=unescape($item['suffix'])?>" />
			</li>
			<li>
				<label for="note">Note:</label>
				<input type="text" name="note" id="note" value="<?=unescape($item['note'])?>" />
			</li>
			<li>
				<label for="description">Description:</label>
				<textarea name="description" id="description" class="tinymce"><?=unescape($item['description'])?></textarea>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Pricing and Container</legend>
		<p>Set pricing and container attributes for the item.</p>
		<ul>
			<li class="inline">
				<label for="retail_price">Retail Price:</label>
				<input type="text" name="retail_price" id="retail_price" class="currency" value="<?=unescape($item['retail_price'])?>" size="6" />

				<label class="check">
					<input type="checkbox" name="crv" value="1" <?if($item['crv'] == 1){echo 'checked="checked"';}?>> +CRV
				</label>

				<label for="size">Size:</label>
				<input type="number" name="size" id="size" value="<?=unescape($item['size'])?>" step=".01" size="6" />

				<label for="unit">Unit:</label>
				<input type="text" name="unit" id="unit" value="<?=unescape($item['unit'])?>" size="2" />

				<label for="container">Container:</label>
				<select name="container" class="containers">
					<option></option>
					<option <?if($item['container'] == 'package'){echo 'selected="selected"';}?> value="package">package</option>
					<option disabled="disabled">- - - - - </option>
					<option <?if($item['container'] == 'bag'){echo 'selected="selected"';}?> value="bag">bag</option>
					<option <?if($item['container'] == 'basket'){echo 'selected="selected"';}?> value="basket">basket</option>
					<option <?if($item['container'] == 'bottle'){echo 'selected="selected"';}?> value="bottle">bottle</option>
					<option <?if($item['container'] == 'bottles'){echo 'selected="selected"';}?> value="bottles">bottles</option>
					<option <?if($item['container'] == 'box'){echo 'selected="selected"';}?> value="box">box</option>
					<option <?if($item['container'] == 'brick'){echo 'selected="selected"';}?> value="brick">brick</option>
					<option <?if($item['container'] == 'bunch'){echo 'selected="selected"';}?> value="bunch">bunch</option>
					<option <?if($item['container'] == 'can'){echo 'selected="selected"';}?> value="can">can</option>
					<option <?if($item['container'] == 'cans'){echo 'selected="selected"';}?> value="cans">cans</option>
					<option <?if($item['container'] == 'canister'){echo 'selected="selected"';}?> value="canister">canister</option>
					<option <?if($item['container'] == 'carton'){echo 'selected="selected"';}?> value="carton">carton</option>
					<option <?if($item['container'] == 'case'){echo 'selected="selected"';}?> value="case">case</option>
					<option <?if($item['container'] == 'container'){echo 'selected="selected"';}?> value="container">container</option>
					<option <?if($item['container'] == 'cup'){echo 'selected="selected"';}?> value="cup">cup</option>
					<option <?if($item['container'] == 'cups'){echo 'selected="selected"';}?> value="cups">cups</option>
					<option <?if($item['container'] == 'cut'){echo 'selected="selected"';}?> value="cut">cut</option>
					<option <?if($item['container'] == 'honey bear'){echo 'selected="selected"';}?> value="honey bear">honey bear</option>
					<option <?if($item['container'] == 'jar'){echo 'selected="selected"';}?> value="jar">jar</option>
					<option <?if($item['container'] == 'jars'){echo 'selected="selected"';}?> value="jars">jars</option>
					<option <?if($item['container'] == 'jug'){echo 'selected="selected"';}?> value="jug">jug</option>
					<option <?if($item['container'] == 'loaf'){echo 'selected="selected"';}?> value="loaf">loaf</option>
					<option <?if($item['container'] == 'pack'){echo 'selected="selected"';}?> value="pack">pack</option>
					<option <?if($item['container'] == 'portion'){echo 'selected="selected"';}?> value="portion">portion</option>
					<option <?if($item['container'] == 'pouch'){echo 'selected="selected"';}?> value="pouch">pouch</option>
					<option <?if($item['container'] == 'pouches'){echo 'selected="selected"';}?> value="pouches">pouches</option>
					<option <?if($item['container'] == 'roll'){echo 'selected="selected"';}?> value="roll">roll</option>
					<option <?if($item['container'] == 'rolls'){echo 'selected="selected"';}?> value="rolls">rolls</option>
					<option <?if($item['container'] == 'stick'){echo 'selected="selected"';}?> value="stick">stick</option>
					<option <?if($item['container'] == 'tin'){echo 'selected="selected"';}?> value="tin">tin</option>
					<option <?if($item['container'] == 'tub'){echo 'selected="selected"';}?> value="tub">tub</option>
					<option <?if($item['container'] == 'tubs'){echo 'selected="selected"';}?> value="tubs">tubs</option>
					<option <?if($item['container'] == 'tube'){echo 'selected="selected"';}?> value="tube">tube</option>
					<option <?if($item['container'] == 'tubes'){echo 'selected="selected"';}?> value="tubes">tubes</option>
				</select>
			</li>
			<li class="inline">
				<label for="limit">Limit:</label>
				<input type="text" name="limit" id="limit" value="<?=unescape($item['limit'])?>" size="6" />

				<label class="check">
					<input type="checkbox" name="supplies_last" value="1" <?if($item['supplies_last'] == 1){echo 'checked="checked"';}?>> &ldquo;While supplies last&rdquo;
				</label>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Labels <a href="#badges" id="display-badges" class="toggle button">Toggle labels</a></legend>
		<p>Select the default labels to display on this item.</p>
		<ul id="badges" class="hide checker">
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle') { ?>

					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>

					<? } else { ?>

					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>

				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'brand') { ?>

					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>

					<? } else { ?>

					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>

				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'organization') { ?>

					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>

					<? } else { ?>

					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>

				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'seasoning') { ?>

					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>

					<? } else { ?>

					<label>
						<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>

				<? } } ?>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Image</legend>
		<p>Manage the UPC image for this item. Feature images can be managed on the <a href="/ad-item-config/<?=$item['family_uri']?>/">family group configuration screen</a>.</p>
		<ul>
			<? if (isset($item['upc_image'])) { ?>
			<li>
				<label for="current_upc_image">Current Image:</label>
				<img src="/img/upc/<?=$item['upc_image']?>" alt="" width="340" style="vertical-align: middle;" />
				<a href="/delete/upc-image/<?=$item['upc']?>/" class="delete x">&times;</a>
			</li>
			<? } ?>
			<li>
				<label for="upc_image">New UPC Image:</label>
				<input type="file" name="upc_image" id="upc_image" />
			</li>

		</ul>
	</fieldset>

	<ul class="actions">
		<? if ($_GET['action'] == 'new') { ?>
		<li>
			<input type="submit" name="add" value="Create product" />
		</li>
		<li><a href="/products/">Cancel</a></li>
		<? } else { ?>
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="upc" value="<?=$_GET['upc']?>" />
		</li>
		<li><a href="/products/">Cancel</a></li>
		<li><a href="/delete/product/<?=$_GET['upc']?>/" class="delete">Delete product</a></li>
		<? } ?>
	</ul>
</form>
