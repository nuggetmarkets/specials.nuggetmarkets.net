<h2>For ad week <?=date('F j, Y', strtotime($secret_special['ad_date']))?></h2>
<form method="post" action="/secret-specials/edit/" enctype="multipart/form-data">
	<fieldset>
		<legend>Run Date</legend>
		<p>This will default to the Friday and Saturday of the current ad&nbsp;week.</p>
		<ul>
			<li class="inline">
				<label for="secret_special_from">From: </label>
				<select name="secret_special_from_month" id="secret_special_from_month">
					<option value="" <?if(is_null($secret_special['secret_special_from'])) {echo' selected="selected"';}?>>Month</option>
					<option value="01" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='01'){echo' selected="selected"';}?>>Jan</option>
					<option value="02" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='02'){echo' selected="selected"';}?>>Feb</option>
					<option value="03" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='03'){echo' selected="selected"';}?>>Mar</option>
					<option value="04" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='04'){echo' selected="selected"';}?>>Apr</option>
					<option value="05" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='05'){echo' selected="selected"';}?>>May</option>
					<option value="06" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='06'){echo' selected="selected"';}?>>Jun</option>
					<option value="07" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='07'){echo' selected="selected"';}?>>Jul</option>
					<option value="08" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='08'){echo' selected="selected"';}?>>Aug</option>
					<option value="09" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='09'){echo' selected="selected"';}?>>Sep</option>
					<option value="10" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='10'){echo' selected="selected"';}?>>Oct</option>
					<option value="11" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='11'){echo' selected="selected"';}?>>Nov</option>
					<option value="12" <?if(!is_null($secret_special['secret_special_from']) && date('m',strtotime($secret_special['secret_special_from']))=='12'){echo' selected="selected"';}?>>Dec</option>
				</select> /
				<select name="secret_special_from_day" id="secret_special_from_day">
					<option value="" <?if(is_null($secret_special['secret_special_from'])) {echo' selected="selected"';}?>>Day</option>
					<? for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($secret_special['secret_special_from']) && date('d',strtotime($secret_special['secret_special_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select> /
				<select name="secret_special_from_year" id="secret_special_from_year">
					<option value="" <?if(is_null($secret_special['secret_special_from'])) {echo' selected="selected"';}?>>Year</option>
					<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($secret_special['secret_special_from']) && date('Y',strtotime($secret_special['secret_special_from']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select>
				
				<label for="secret_special_through">Through: </label>
				<select name="secret_special_through_month" id="secret_special_through_month">
					<option value="" <?if(is_null($secret_special['secret_special_from'])) {echo' selected="selected"';}?>>Month</option>
					<option value="01" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='01'){echo' selected="selected"';}?>>Jan</option>
					<option value="02" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='02'){echo' selected="selected"';}?>>Feb</option>
					<option value="03" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='03'){echo' selected="selected"';}?>>Mar</option>
					<option value="04" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='04'){echo' selected="selected"';}?>>Apr</option>
					<option value="05" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='05'){echo' selected="selected"';}?>>May</option>
					<option value="06" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='06'){echo' selected="selected"';}?>>Jun</option>
					<option value="07" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='07'){echo' selected="selected"';}?>>Jul</option>
					<option value="08" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='08'){echo' selected="selected"';}?>>Aug</option>
					<option value="09" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='09'){echo' selected="selected"';}?>>Sep</option>
					<option value="10" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='10'){echo' selected="selected"';}?>>Oct</option>
					<option value="11" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='11'){echo' selected="selected"';}?>>Nov</option>
					<option value="12" <?if(!is_null($secret_special['secret_special_through']) && date('m',strtotime($secret_special['secret_special_through']))=='12'){echo' selected="selected"';}?>>Dec</option>
				</select> /
				<select name="secret_special_through_day" id="secret_special_through_day">
					<option value="" <?if(is_null($secret_special['secret_special_through'])) {echo' selected="selected"';}?>>Day</option>
					<? for ($i = 1; $i <= 31; $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($secret_special['secret_special_through']) && date('d',strtotime($secret_special['secret_special_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select> /
				<select name="secret_special_through_year" id="secret_special_through_year">
					<option value="" <?if(is_null($secret_special['secret_special_through'])) {echo' selected="selected"';}?>>Year</option>
					<? for ($i = 2014; $i <= (int)date('Y',strtotime('next year')); $i++) { ?>
					<option value="<?=str_pad($i,2,'0',STR_PAD_LEFT)?>" <?if(!is_null($secret_special['secret_special_through']) && date('Y',strtotime($secret_special['secret_special_through']))==$i){echo' selected="selected"';}?>><?=$i?></option>
					<? } ?>
				</select>
			</li>
		</ul>
	</fieldset>
	
	<? if (!$secret_special['zoned']) { ?>

	<fieldset>
		<legend>Description</legend>
		<p>These settings will be used for all formats.</p>
		<ul>
			<li>
				<label for="secret_special_title">Item name:</label>
				<input type="text" name="secret_special_title" id="secret_special_title" value="<?=unescape($secret_special['secret_special_title'])?>" />
			</li>
			<li>
				<label for="secret_special_container">Container/size:</label>
				<input type="text" name="secret_special_container" id="secret_special_container" value="<?=unescape($secret_special['secret_special_container'])?>" />
			</li>
			<li>
				<label for="secret_special_description">Ad copy:</label>
				<textarea name="secret_special_description" id="secret_special_description" class="tinymce"><?=(isset($secret_special['secret_special_description'])?$secret_special['secret_special_description']:'')?></textarea>
				<div class="note">Use <code>[zoneurl]</code> instead of the domain name (<code>https://www.nuggetmarket.com</code>) when linking to an article or recipe on the&nbsp;website. <a href="/img/zoneurl-example.png">Example</a></div>
			</li>
			<li>
				<label for="secret_special_retail">Secret Special retail:</label>
				<input id="secret_special_retail" type="text" name="secret_special_retail" value="<?=(isset($secret_special['secret_special_retail'])?strip_tags($secret_special['secret_special_retail']):'')?>" size="60" /> 
				<div class="note">For multiple Secret Specials, separate prices with a pipe: | (shift + \)</div>
			</li>
			<li>
				<label for="secret_special_disclaimers">Disclaimers:</label>
				<textarea name="secret_special_disclaimers" id="secret_special_disclaimers"><?=(isset($secret_special['secret_special_disclaimers'])?$secret_special['secret_special_disclaimers']:'')?></textarea>
				<div class="note">These will be prepended above the limit and valid UPCs in small&nbsp;type.</div>
			</li>
			<li>
				<label for="secret_special_limit">Limit:</label>
				<input type="text" name="secret_special_limit" value="<?=(isset($secret_special['secret_special_limit'])?$secret_special['secret_special_limit']:'')?>" placeholder="ex. 10 packages" id="secret_special_limit" size="40" class="short" />
				<div class="note">Will display as &ldquo;Limit <span id="secret_special_limit_text"><?=(isset($secret_special['secret_special_limit'])?$secret_special['secret_special_limit']:'(limit field value)')?></span> per guest.&rdquo; Defaults to Limit 4 if left blank.</div>
			</li>
			<li>
				<label for="secret_special_skus">UPCs or PLUs Included:</label>
				<textarea name="secret_special_skus" id="secret_special_skus" placeholder="ex. UPCs: 0000123456789, 00045678910, 000987654321"><?=(isset($secret_special['secret_special_skus'])?$secret_special['secret_special_skus']:'')?></textarea>
				<div class="note">Will display as &ldquo;Only valid for <span id="secret_special_skus_text"><?=(isset($secret_special['secret_special_skus'])?$secret_special['secret_special_skus']:'(SKUs field value)')?></span>&rdquo;</div>
			</li>
			<li>
				<label>Lifestyle Icons:</label>
				<p class="lifestyle-icons">
				<? foreach ($secret_special['secret_special_labels'] as $label) { ?>
					<img src="/img/labels/<?=$label['image']?>" alt="<?=$label['description']?>" />	
				<? } ?>
				</p>
			</li>
			<? if (isset($secret_special['secret_special_picture'])) { ?>
				<li>
					<label>Picture these SKUs:</label>
					<ul class="picture-upcs">
					<? foreach ($secret_special['secret_special_picture'] as $picture) { if ($picture['picture']) {?>
						<li><img src="<?=$picture['picture']?>" alt="" /> - <?=$picture['upc']?></li>
					<? } else { ?>
						<li><?=$picture['upc']?></li>
					<? } } ?>
					</ul>
				</li>
			<? } ?>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Image</legend>
		<ul>
			<? if (isset($secret_special['secret_special_image'])) { ?>
				<li class="current-image">
					<label for="secret_special_current_image">Current Image:</label>
					<img src="/media/<?=$secret_special['secret_special_image']?>" alt="" width="340" />
					<input type="hidden" name="secret_special_current_image" value="<?=$secret_special['secret_special_image']?>" />
					<input type="submit" name="delete_secret_special_image" id="delete_secret_special_image" value="Delete" class="delete confirm" />
				</li>
			<? } ?>
			<li>
				<label for="secret_special_image">New Image:</label>
				<input type="file" name="secret_special_image" id="secret_special_image" class="jpg" />
				<div class="note">Use a landscape or square image. The system will automatically compress it and resize to 680px wide.</div>
			</li>
		</ul>
	</fieldset>
	
	<? } else { ?>
	
	<fieldset class="zoned-item-settings">
		<legend>Zoned Item Settings</legend>
		<p>This Secret Special has zoned items. You can manage these settings on their respective ad settings&nbsp;screens.</p>
		<ul>
		<? foreach ($secret_special['zones'] as $zone) { ?>
			<li>
				<? if (isset($zone['secret_special_image'])) { ?>
					<a href="/ads/edit/<?=$zone['ad_id']?>/#secret-special"><img src="/media/<?=$zone['secret_special_image']?>" alt="<?=$zone['secret_special_title']?>" /></a>
				<? } ?>
				<section>
					<h3><a href="/ads/edit/<?=$zone['ad_id']?>/#secret-special"><?=$zone['store_chain']?></a></h3>	 
				<? if (!is_null($zone['secret_special_title'])) { ?> 
					<p><?=$zone['secret_special_title']?></p>
				<? } else { ?>
					<p><span class="not-set">Item not set</span></p>
				<? } ?>
				<? if (!is_null($zone['secret_special_retail'])) { ?>
					<p><strong><?=$zone['secret_special_retail']?></strong></p>
				<? } ?>
				</section>
			</li>
		<? } ?>
		</ul>
		<input type="hidden" name="zoned" value="1" />
	</fieldset>
	
	<? } ?>

	<fieldset>
		<legend>Custom Message</legend>
		<p>Add a custom message to the email and select where it should&nbsp;appear.</p>
		<ul>
			<li>
				<label for="prepended_message">Message:</label>
				<textarea name="prepended_message" id="prepended_message" class="tinymce" rows="5"><?=(isset($secret_special['prepended_message'])?$secret_special['prepended_message']:'')?></textarea>
			</li>
			<li>Position:</li>
			<li>
				<label><input type="radio" name="prepended_message_position" value="1" <?if(!$secret_special['prepended_message_position'] || $secret_special['prepended_message_position'] == 1){echo'checked="checked"';}?> /> Beginning of email</label>
			</li>
			<li>
				<label><input type="radio" name="prepended_message_position" value="2" <?if($secret_special['prepended_message_position'] == 2){echo'checked="checked"';}?> /> After Secret Special</label>
			</li>
			<li>
				<label><input type="radio" name="prepended_message_position" value="3" <?if($secret_special['prepended_message_position'] == 3){echo'checked="checked"';}?> /> End of email</label>
			</li>
		</ul>
	</fieldset>

	<fieldset id="zones">
		<legend>Email &amp; Zone Settings</legend>
		<p>Select the zones you want to email, set regular retail for the secret special, and append any articles or&nbsp;recipes.</p>
		<ul class="grid">
		<? foreach ($secret_special['zones'] as $zone) { ?>
			<li>
				<h2><a href="/ads/edit/<?=$zone['ad_id']?>/#secret-special"><?=$zone['store_chain']?></a></h2>
				<label><input type="checkbox" name="enable[<?=$zone['ad_id']?>]" value="1" <?if($zone['secret_special_title']){?>checked="checked"<?}?> /> Email this zone</label>
				<ul>
					<li>
						<label for="">Regular retail:</label>
						<input type="text" name="retail[<?=$zone['ad_id']?>]" id="retail_<?=$zone['ad_id']?>" value="<?=(isset($zone['secret_special_regular_retail'])?strip_tags($zone['secret_special_regular_retail']):'')?>" placeholder="ex. $4.99" />
						<div class="note">Leave blank to omit Secret Special</div>
					</li>
					<? /*?>
					<li class="prepend-artice">
						<label for="prepended_message_<?=$zone['ad_id']?>">Custom message:</label>
						<textarea name="prepended_zone_message[<?=$zone['ad_id']?>]" id="prepended_zone_message_<?=$zone['ad_id']?>" class="tinymce" rows="3"><?=(isset($zone['prepended_message'])?$zone['prepended_message']:'')?></textarea>
					</li>
					<? */?>
					<li class="articles-and-recipes">
						<label>Append articles and recipes:</label>
						<? if (!empty($_GET['id'])) { ?>
							<input type="hidden" name="features_file" value="<?=(isset($secret_special['features_file'])?$secret_special['features_file']:'')?>" />
						<? } ?>
						<div id="articles-<?=$zone['ad_id']?>">
							<ul>
							<? if (isset($zone['articles'])) { $a = 0; foreach ($zone['articles'] as $article) { ?>
								<li id="article-<?=$zone['ad_id']?>-<?=$article?>-<?=$a?>">
									<label for="article[<?=$zone['ad_id']?>][<?=$a?>]">Article ID:</label>
									<input id="article[<?=$zone['ad_id']?>][<?=$a?>]" type="text" name="article[<?=$zone['ad_id']?>][<?=$a?>]" value="<?=$article?>" size="3" class="short" /> <span class="note">the number in the URL</span>
									<a href="#" data-delete="article-<?=$zone['ad_id']?>-<?=$article?>-<?=$a?>" class="delete x remove">&times;</a>
								</li>
							<? $a++; } } ?>
							</ul>
							<p><button id="append-article-<?=$zone['ad_id']?>" data-class="article" data-id="<?=$zone['ad_id']?>" class="button">Article +</button> <a href="<?=$zone['store_attributes']['article_archive']?>" target="_blank">Article search</a></p>
						</div>
						<div id="recipes-<?=$zone['ad_id']?>">
							<ul>
							<? if (isset($zone['recipes'])) { $r = 0; foreach ($zone['recipes'] as $recipe) { ?>
								<li id="recipe-<?=$zone['ad_id']?>-<?=$recipe?>-<?=$r?>">
									<label for="recipe[<?=$zone['ad_id']?>][<?=$r?>]">Recipe ID:</label>
									<input id="recipe[<?=$zone['ad_id']?>][<?=$r?>]" type="text" name="recipe[<?=$zone['ad_id']?>][<?=$r?>]" value="<?=$recipe?>" size="3" class="short" /> <span class="note">the number in the URL</span>
									<a href="#" data-delete="recipe-<?=$zone['ad_id']?>-<?=$recipe?>-<?=$r?>" class="delete x remove">&times;</a>
								</li>
							<? $r++; } } ?>
							</ul>
							<p><button id="append-recipe-<?=$zone['ad_id']?>" data-class="recipe" data-id="<?=$zone['ad_id']?>" class="button">Recipe +</button> <a href="<?=$zone['store_attributes']['recipe_archive']?>" target="_blank">Recipe search</a></p>
						</div>
						<? if ($zone['secret_special_title']) { ?>
							<input type="hidden" name="store_chain[<?=$zone['ad_id']?>]" value="<?=$zone['store_attributes']['name']?>" />
							<input type="hidden" name="campaign_name[<?=$zone['ad_id']?>]" value="<?=$zone['store_attributes']['slug']?>-weekly-specials-<?=$secret_special['ad_date']?>" />
							<input type="hidden" name="campaign_subject[<?=$zone['ad_id']?>]" value="<?=$zone['store_attributes']['public_name']?> Weekly Specials Starting <?=date('F j, Y', strtotime($secret_special['ad_date']))?>" />
						<? } ?>
					</li>
					<? /*?>
					<li>
						<a href="#manual-<?=$zone['ad_id']?>" class="toggle button">Email campaign settings</a>
						<a href="/email/<?=$zone['ad_id']?>/" target="_blank" class="button small">Preview</a>
						<ul id="manual-<?=$zone['ad_id']?>" class="hide manual">
							<li>
								You can use the settings below to manually schedule the email in Campaign&nbsp;Monitor.
							</li>
							<li>
								<label>Campaign Name:</label>
								<input type="text" name="text_select_campaign_name" class="auto-select" value="<?=$zone['store_attributes']['slug']?>-weekly-specials-<?=$secret_special['ad_date']?>" readonly />
								<div class="note">Click to select</div>
							</li>
							<li>
								<label>Subject Line:</label>
								<input type="text" name="text_select_campaign_subject" class="auto-select" value="<?=$zone['store_attributes']['public_name']?> Weekly Specials Starting <?=date('F j, Y', strtotime($secret_special['ad_date']))?>" readonly />
								<div class="note">Click to select</div>
							</li>
							<li>
								<label>From Name and Address:</label>
								<input type="text" name="text_select_campaign_from_nam e" class="auto-select" value="<?=$zone['store_attributes']['public_name']?>" readonly />
								<input type="text" name="text_select_campaign_from_address" class="auto-select" value="<?=$zone['store_attributes']['email_from_address']?>" readonly />
								<div class="note">Click to select</div>
							</li>
							<li>
								<a href="https://nuggetmarkets.createsend.com/campaign/create/new" class="button" target="_blank">Define on Campaign Monitor</a>
							</li>
						</ul>
					</li>
					
					<? if (isset($zone['publish']) && $zone['publish']==1 && is_null($zone['campaign_id']) && $zone['secret_special_title']) { ?>
					
					<li class="schedule">
						<label>Schedule email:</label>
						<input type="hidden" name="email_send_date[<?=$zone['ad_id']?>]" value="<?=$secret_special['email_send_date']['value']?>" />
						<input type="submit" name="schedule_email[<?=$zone['ad_id']?>]" value="Schedule for <?=$secret_special['email_send_date']['display']?>" />
					</li>
					
					<? } else if ($zone['campaign_status'] == 'scheduled' && strtotime($secret_special['email_send_date']['value']) > date()) { ?>
					
					<li class="scheduled">
						<label>Schedule email:</label>
						<p>The Secret Special email is scheduled for <strong><?=date('l, F j, Y', strtotime($secret_special['email_send_date']['value']))?> at&nbsp;6&nbsp;a.m.</strong></p>
						<input type="hidden" name="campaign_id[<?=$zone['ad_id']?>]" value="<?=$zone['campaign_id']?>" />
						<input type="submit" name="cancel_email[<?=$zone['ad_id']?>]" value="Cancel the email" class="delete" />
						<a href="https://nuggetmarkets.createsend.com/campaigns" target="_blank">Manage Campaigns</a>
					</li>
					
					<? } else if ($zone['campaign_status'] == 'scheduled') { ?>
					<li>This email has been sent.</li>
					<? } else { ?>
					<li>This email can be scheduled once the ad has been&nbsp;published.</li>
					<? } ?>
					
					<? */ ?>
					<li class="email-status">
						<p><a href="/email/<?=$zone['ad_id']?>/" class="button">Preview email</a></p>
						<ol class="timeline <?=$zone['campaign_status']?>">
							<li class="status draft">
								<span class="milestone">draft</span>
							</li>
							<li class="status scheduled">
								<span class="milestone">schdeduled</span>
							</li>
							<li class="status sent">
								<span class="milestone">sent</span>
							</li>
						</ol>
						
					<? if ($zone['campaign_status'] == 'unscheduled') { ?>
						<div class="message">
							<p>Click &ldquo;Create draft&rdquo; to email this zone.</p>
						</div>
						<input type="submit" name="create_draft[<?=$zone['ad_id']?>]" value="Create draft" />
					<? } else if ($zone['campaign_status'] == 'draft') { ?>
						<div class="message">
							<p>This email has not been scheduled.</p>
							<p><a href="https://nuggetmarkets.createsend.com/campaigns/<?=str_replace('/t/y-','',parse_url($zone['campaign_stats']['WebVersionURL'], PHP_URL_PATH))?>/create"target="_blank">View on Campaign Monitor</a></p>
						</div>
						<input type="hidden" name="email_send_date[<?=$zone['ad_id']?>]" value="<?=$secret_special['email_send_date']['value']?>" />
						<input type="hidden" name="campaign_id[<?=$zone['ad_id']?>]" value="<?=$zone['campaign_id']?>" />
						<input type="submit" name="schedule_email[<?=$zone['ad_id']?>]" value="Schedule for <?=$secret_special['email_send_date']['display']?>" class="schedule" />
						<p><input type="submit" name="delete_draft[<?=$zone['ad_id']?>]" value="Delete draft" class="delete" /></p>
					<? } else if ($zone['campaign_status'] == 'scheduled') { ?>
						<div class="message">
							<p>This email is scheduled to send on <strong><?=$secret_special['email_send_date']['display']?></strong>.</p>
							<p><a href="https://nuggetmarkets.createsend.com/campaigns/<?=str_replace('/t/y-','',parse_url($zone['campaign_stats']['WebVersionURL'], PHP_URL_PATH))?>/create"target="_blank">View on Campaign Monitor</a></p>
						</div>
						<input type="hidden" name="campaign_id[<?=$zone['ad_id']?>]" value="<?=$zone['campaign_id']?>" />
						<input type="submit" name="unschedule_email[<?=$zone['ad_id']?>]" value="Unschedule email" class="delete" />
					<? } else if ($zone['campaign_status'] == 'sent') { ?>
						<div class="message">
							<table>
								<tr>
									<th>Recipients</th>
									<th>Opens</th>
									<th>Clicks</th>
								</tr>
								<tr>
									<td><?=number_format($zone['campaign_stats']['Recipients'])?></td>
									<td><?=number_format($zone['campaign_stats']['UniqueOpened'] / $zone['campaign_stats']['Recipients'] * 100, 1)?>%</td>
									<td><?=number_format($zone['campaign_stats']['Clicks'] / $zone['campaign_stats']['Recipients'] * 100, 1)?>%</td>
								</tr>
							</table>
						</div>
						<p><a href="https://nuggetmarkets.createsend.com/campaigns/<?=str_replace('/t/y-','',parse_url($zone['campaign_stats']['WebVersionURL'], PHP_URL_PATH))?>/reports/snapshot" class="button" target="_blank">View detailed results</a></p>
					<? } ?>
					</li>
				</ul>
			</li>
		<? } ?>
		</ul>
		<ul>
			<li>
				<label for="proof_email">Send proof email:</label>
				<a href="mailto:Rebecca Reichardt<rebecca.reichardt@nuggetmarket.com>,Calista Marouk<calista.marouk@nuggetmarket.com>,Kim Scott<kim.scott@nuggetmarket.com>?subject=<?=$secret_special_email_subject?>&amp;body=<?=$secret_special_email_proof_body?>" class="button">Send Secret Special proof email</a>
			</li>
		</ul>
	</fieldset>
	
<? if (isset($secret_special['campaign_summary'])) { ?>
	<fieldset>
		<legend>Campaign Results</legend>
		<p>Aggregated results for individual zone&nbsp;campaigns.</p>
		<h3>Delivery &amp; Engagement</h3>
		<div class="stats-panel delivery">
			<section class="chart">
				<div class="pie-chart" style="
					background: 
						radial-gradient(
							transparent 66%, 
							white 0), 
						conic-gradient(
							var(--color-red) 0, 
							var(--color-red) <?=number_format($secret_special['campaign_summary']['bounced'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%, 
							var(--color-green) 0, 
							var(--color-green) <?=number_format($secret_special['campaign_summary']['clicks'] / $secret_special['campaign_summary']['recipients'] * 100, 1)?>%, 
							var(--color-blue) 0, 
							var(--color-blue) <?=number_format($secret_special['campaign_summary']['uniqueopened'] / $secret_special['campaign_summary']['recipients'] * 100, 1)?>%, 
							var(--color-yellow) 0, 
							var(--color-yellow) 100%
						);
				">&nbsp;</div>
			</section>
			<section class="delivered">
				<h4><?=number_format(($secret_special['campaign_summary']['recipients'] - $secret_special['campaign_summary']['bounced']) / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</h4>
				<p><?=number_format($secret_special['campaign_summary']['recipients'])?> <small>Delivered</small></p>
			</section>
			<section class="opens">
				<h4><?=number_format($secret_special['campaign_summary']['uniqueopened'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</h4>
				<p><?=number_format($secret_special['campaign_summary']['uniqueopened'])?> recipients <small>Opened</small></p>
			</section>
			<section class="clicks">
				<h4><?=number_format($secret_special['campaign_summary']['clicks'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</h4>
				<p><?=number_format($secret_special['campaign_summary']['clicks'])?> recipients <small>Clicked</small></p>
			</section>
			<section class="bounced">
				<h4><?=number_format($secret_special['campaign_summary']['bounced'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</h4>
				<p><?=number_format($secret_special['campaign_summary']['bounced'])?> <small>Bounced</small></p>
			</section>
		</div>
		<h3>Reactions</h3>
		<div class="stats-panel reactions">
			<section>
				<h4><?=number_format($secret_special['campaign_summary']['unsubscribed'])?></h4>
				<p>Unsubscribes <small><?=number_format($secret_special['campaign_summary']['unsubscribed'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</small></p>
			</section>
			<section>
				<h4><?=number_format($secret_special['campaign_summary']['spamcomplaints'])?></h4>
				<p>Marked as spam <small><?=number_format($secret_special['campaign_summary']['spamcomplaints'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</small></p>
			</section>
			<section>
				<h4><?=number_format($secret_special['campaign_summary']['forwards'] + $secret_special['campaign_summary']['likes'] + $secret_special['campaign_summary']['mentions'])?></h4>
				<p>Shares <small><?=number_format($secret_special['campaign_summary']['forwards'] + $secret_special['campaign_summary']['likes'] + $secret_special['campaign_summary']['mentions'] / $secret_special['campaign_summary']['recipients'] * 100, 2)?>%</small></p>
			</section>
		</div>
	</fieldset>
<? } ?>

	<ul class="actions">
		<? if ($secret_special['imported_from_ad_plan']) { ?>
			<li><small class="flag">Data imported from ad plan</small></li>
		<? } ?>
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="date" value="<?=$_GET['date']?>" />
		</li>
		<li><a href="/products/">Cancel</a></li>
	</ul>
</form>
