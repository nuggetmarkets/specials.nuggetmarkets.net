<?
	include('../engine/EmailSubscribed.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?=$zone['name']?> Weekly Specials Subscription Confirmed</title>
		<link rel="stylesheet" href="/css/email.css">
		<link rel="stylesheet" href="/css/email-<?=$zone['slug']?>.css">
		<? if (isset($_GET['print'])) { ?>
		<link rel="stylesheet" href="/css/email-print.css" media="print">
		<? } ?>
    </head>
	<body class="ws-email<?=$page_attrs['class']?>">
		<div class="ws-page">
			<div class="ws-preview-text">
				Your weekly specials subscription has been confirmed.
			</div>
			<div class="ws-header">
				<p class="ws-email-link"><webversion>View this email in your browser.</webversion></p>
				<div class="ws-logo">
					<img src="/img/<?=$zone['email_logo_print']?>" alt="<?=$zone['name']?>" />
				</div>
			</div>
			
			<div class="ws-confirmation">
				<div class="ws-container">
					<div class="ws-header">
						<h2>Subscription Confirmed</h2>
					</div>
					<p>Thank you for signing up to receive weekly specials from <?=$zone['name']?>. You will begin receiving our emails this coming Wednesday. In the mean time, you can check out this week&rsquo;s secret special email and get the exclusive&nbsp;coupon:<p>
					<p class="ws-view-all"><a href="/email/<?=$zone['slug']?>/" class="ws-button">Secret Special</a></p>

					<p>View everything on special this&nbsp;week:</p>
					<p class="ws-view-all"><a href="<?=$zone['ad_url']?>"  class="ws-button">Weekly Specials</a></p>
				</div>
			</div>

			

			<div class="ws-footer">
				<div class="ws-container">
					<p class="ws-disclaimer"><?=$zone['name']?> takes your privacy very seriously. We do not sell or otherwise share your personal information with anyone. See our <a href="https://www.nuggetmarket.com/privacy/">privacy policy</a> to learn how we store and protect the information you do trust us&nbsp;with.</p>
					<p class="ws-preferences">This email was sent to [email].<br /><unsubscribe>Unsubscribe</unsubscribe> or <preferences>update your email preferences</preferences>.</p>
					<p class="ws-tip">Pro-Tip: <em>Add us to your address book to help ensure that you continue receiving our emails.</em></p>
					<p class="ws-address">Copyright &copy; <?=date('Y')?> Nugget Market, Inc.<br />311 Mace Blvd., Davis, CA. 95618</p>
					<div class="ws-logo"><img src="/img/<?=$zone['email_logo_footer']?>?i=1" alt="<?=$zone['name']?>" /></div>
				</div>
			</div>
		</div>
	</body>
</html>
