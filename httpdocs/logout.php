<?
    require_once('../engine/Logout.php');
    include('inc/header.inc.php');
    include('inc/messages.inc.php');
?>

<section class="logout">
	<header>
		<h1>Logout</h1>
	</header>
	<p><a href="/login/">Log back in</a> or go to the <a href="/">home page</a>.</p>
</section>

<? include('inc/footer.inc.php'); ?>
