<? 
	include('../engine/Ads.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<section class="ads">
	<header>
		<h1>Ads</h1>
		<nav>
			<? if (isset($_SESSION['ad_sort']) && $_SESSION['ad_sort'] == 'desc') { ?>
				<a href="/ads/sort/asc/" class="button">Sort ▲</a>
			<? } else { ?>
				<a href="/ads/sort/desc/" class="button">Sort ▼</a>
			<? } ?>
			<a href="/ads/new/" class="button">Add new</a>
			<? if (ENVIRONMENT == 'dev') { ?>
				<a href="/refresh-ads.php" class="button">Refresh Ads</a>
			<? } ?>
		</nav>
	</header>

<?
	
	if ($ad || (!empty($_GET['action']) && $_GET['action'] == 'new')) {
		
		include('inc/ads-form.inc.php');
		
	} else {
		
		include('inc/ads-listing.inc.php');
		
	}
	
	include('inc/footer.inc.php');
?>
