<?
    require_once '../engine/Proofs.php';

    include 'inc/header.inc.php';
    include 'inc/messages.inc.php';
?>

<? if (isset($ads)) { ?>

<h1>Proof Items On Upcoming&nbsp;Ads</h1>

<ol class="ad-listing">
    <? foreach ($ads as $ad) { ?>
    <li id="ad-<?=$ad['date_slug']?>" class="ad current-<?=$ad['current']?>">
        <? if ($ad['current']) { ?>
        <header class="current-<?=$ad['current']?>">
            <h2><?=$ad['date_from']?> <small class="running">running</small></h2>
            <p><em>Runs through <?=$ad['date_through']?></em></p>
        </header>
        <? } else { ?>
        <header>
            <h2><?=$ad['date_from']?></h2>
            <p><em>Running through <?=$ad['date_through']?></em></p>
        </header>
        <? } ?>
        <div class="features">
            <h4>Features</h4>
            <? if (count($features[$ad['date_slug']]['zones']) > 0) { ?>
            <ul class="grid">
            <? foreach ($features[$ad['date_slug']]['zones'] as $zone => $items) { ?>
            <li>
                <p><?=$zone?></p>
                <span class="spot <?=$items['features'][1]?>">1</span>
                <span class="spot <?=$items['features'][2]?>">2</span>
                <span class="spot <?=$items['features'][3]?>">3</span>
                <span class="spot <?=$items['features'][4]?>">4</span>
                <span class="spot <?=$items['features'][5]?>">5</span>
                <span class="spot <?=$items['features'][6]?>">6</span>
                <span class="spot <?=$items['features'][7]?>">7</span>
                <span class="spot <?=$items['features'][8]?>">8</span>
            </li>
            <? } ?>
                <? if (count($features[$ad['date_slug']]['zones']) == 4) { ?>
                <li>&nbsp;</li>
                <? } ?>
                <li>
                    <p>&nbsp;</p>
                    <a href="/proofs/<?=$ad['date_slug']?>/features/" class="button">Proof <?=$ad['date_from']?> Features</a>
                </li>
            </ul>
            <? } else { ?>
                <p>No features have been selected&nbsp;yet.</p>
            <? } ?>
        </div>
        <ul class="listing">
            <? foreach ($ad['departments'] as $department) { ?>
            <li>
                <a href="/proofs/<?=$ad['date_slug']?>/<?=$department['department_slug']?>/">
                    <?=$department['department_name']?> <?=$department['status']?>
                </a>
            </li>
            <? } ?>
        </ul>
    </li>
    <? } ?>
</ol>

<? } else if ($ad_zones) { ?>

<header>
    <h1>Proof Ad Items</h1>
    <h2><?=$display_date?> / <?=$department?></h2>
    <p>Approve or correct <?=$department?> items appearing on E-Ads going live <?=$display_date?>. Items will not appear on ads until&nbsp;approved.</p>
    <div class="key">
        <h3><a href="#key" class="toggle button">Status Key</a></h3>
        <ul id="key">
            <li><span class="issue">Red</span> Correction Needed - Waiting on marketing to make a correction. These items will go to Awaiting Approval status once the correction is&nbsp;made.</li>
            <li><span class="pending">Yellow</span> Awaiting Approval - Waiting on buyers to approve the item. These items will not appear on the ad until they are approved by the&nbsp;buyer.</li>
            <li><span class="approved">Green</span> Approved - These items will appear on ad when&nbsp;scheduled.</li>
            <li>Please note: Items with missing images will not display on ad.</li>
            <li class="summary">Click approve to make all the boxes&nbsp;green!</li>
        </ul>
    </div>
</header>

<? foreach ($ad_zones as $zone) { ?>

<section>
    <header>
        <h3><?=$zone['attrs']['store_chain']?> <small>/ <?=$display_date?></small></h3>
        <nav><a href="<?=$zone['attrs']['store_attrs']['ad_url']?>date/<?=$zone['attrs']['date_from']?>/" target="_blank" class="button"> Preview on Website</a></nav>
    </header>

    <? if (is_array($zone['items'])) { ?>

    <ul class="items grid">
        <? foreach ($zone['items'] as $item) { ?>
        <li id="item-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="<?=$item['status']?>">
            
            <div class="details">
                <? if (isset($item['feature']) && $item['feature'] > 0) { ?>
                    <div class="feature pos-<?=$item['feature']?>">Feature <strong><?=$item['feature']?></strong> Position</div>
                <? } ?>
                <? if ($item['feature_photo_small']) { ?>
                <figure class="feature-photo">
                    <a href="/media/<?=$item['feature_photo_original']?>">
                        <img src="/media/<?=$item['feature_photo_small']?>" alt="<?=$item['name']?>" />
                    </a>
                    <span class="ribbon">Feature image</span>
                </figure>
                <? } else if ($item['upc_photos']) { ?>
                <figure class="upc-photos">
					<span>
                    <? foreach ($item['upc_photos'] as $photo) { ?>
                    	<img src="<?=$photo?>" alt="<?=$item['name']?>" />
                    <? } ?>
					</span>
                </figure>
                <? } else { ?>
                <figure class="watermark-image">
                    <img src="/img/watermark-<?=$zone['attrs']['store_slug']?>.svg" alt="<?=$ad['store_chain']?> watermark" />
                </figure>
                <? } ?>
                <? if (isset($item['group_labels'])) { ?>
                <ul class="badges">
                <? foreach ($item['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
                    <li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
                <? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
                    <li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
                <? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
                    <li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
                <? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
                    <li class="<?=$label['type']?>"><img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
                <? } } } ?>
                </ul>
                <? } ?>
                <? if ($item['prefix']) { ?>
                <p class="prefix"><?=$item['prefix']?></p>
                <? } ?>
                <h3 class="name"><?=$item['name']?></h3>
                <? if ($item['suffix']) { ?>
                <p class="suffix"><?=$item['suffix']?></p>
                <? } ?>
                <? if (
                    !empty($item['container']) &&
                    !strstr($item['sale_price'],'lb.') &&
                    !strstr($item['sale_price'],'lbs.') &&
                    !strstr($item['sale_price'],'/ea.') &&
                    !strstr($item['sale_price'],'%') && (
                        $item['unit'] != 'CT' ||
                        $item['size'] > 1
                    ) &&
                    isset($item['size'])) {
                ?>
                <span class="pack-size"><?=$item['size']?> <?=$item['unit']?>. <?=$item['container']?></span>
                <? } else if (isset($item['container_sizes']) && !strstr($item['sale_price'],'%')) {$i = 1; foreach ($item['container_sizes'] as $key => $value) {

                if ($value) { ?>
                    <div class="pack-size"><?=$value?> <?=$item['units'][$i]?>. <?=$item['containers'][$key]?></div>
                <? $i++; } } } if ($item['note']) { ?>
                <p><small><?=$item['note']?></small></p>
                <? } if ($item['selected_varieties']) { ?>
                <p class="selected-varieties">Selected varieties</p>
                <? } ?>
                <p class="sale-price"><?=$item['sale_price']?></p>
                <? if ($item['savings']) { ?>
                <p class="savings">Save <?=$item['savings']?></p>
                <? } ?>
				<? if ($item['regular_retail']) { ?>
				<p class="regular-retail" title="Regular retail is not displayed to guests"><small>Regular retail: <?=$item['regular_retail']?></small></p>
				<? } ?>
                <? if ($item['limit']) { ?> 
					<div><?=$item['limit']?></div>
				<? } ?>
                
                <? if ($item['description']) { ?> 
                    <div class="description">
                        <h4>Feature Ad Copy:</h4>
                        <?=$item['description']?>
                    </div>
                <? } ?>
				
				<div class="edit-groups">
                <? if (in_array('ads', $_SESSION['access'])) { if ($item['manual_group']) { $i = 1; foreach ($item['family_group_uris'] as $uri) { ?>
                
                <p class="actions"><a href="/ad-item-group/<?=$zone['attrs']['id']?>/<?=$uri?>/" class="button" target="_blank">Edit group <?=$i?></a> <a href="/ad-item-group/<?=$zone['attrs']['id']?>/<?=$uri?>/ungroup/" class="delete x">&times;</a></p>
                
                <? $i++; } } else { ?>
                
                <p class="actions"><a href="/ad-item-group/<?=$zone['attrs']['id']?>/<?=$item['family_uri']?>/" class="button" target="_blank">Edit group</a></p>
                
                <? } } ?>
				</div>
				
                <div class="upcs">
                    <a href="#upcs-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="toggle button">SKUs on ad</a>
                    <ul id="upcs-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>">
                    <? $i = 0; foreach($item['upcs'] as $upc) {  ?>
                        <li><?=$upc?><br /><?=$item['pos_name'][$i]?></li>
                    <? $i++;} ?>
                    </ul>
                </div>
            </div>
			
            <div class="status pending">
                <p>Awaiting Approval</p>
                <? if ($item['messages']['listing']) { ?>
                <ol class="messages">
                    <? foreach ($item['messages']['listing'] as $message) { ?>
                    <li>
                        <div class="text"><?=$message['message']?></div>
                        <div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
                    </li>
                    <? } ?>
                </ol>
                <? } ?>
				
                <ul>
                    <li>
                        <form action="/proofs/<?=$_GET['date']?>/<?=$_GET['department']?>/" method="post">
                            <input type="hidden" name="group_name" value="<?=$item['group_name']?>" />
                            <input type="hidden" name="family_group" value="<?=$item['family_uri']?>" />
                            <input type="hidden" name="item_name" value="<?=$item['name']?>" />
                            <input type="hidden" name="item_id" value="<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" />
                            <input type="hidden" name="department" value="<?=$item['department_url']?>" />
                            <input type="hidden" name="ad_id" value="<?=$zone['attrs']['id']?>" />
                            <input type="hidden" name="ad_date" value="<?=$_GET['date']?>" />
                            <input type="hidden" name="form_action" value="approve" />
                            <input type="hidden" name="user" value="<?=$_SESSION['full_name']?>" />
                            <input type="submit" name="approve" value="Approve" id="submit-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" />
                        </form>
                    </li>
                    <li>
                        <a href="#correction-pending-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="button correction toggle">Make correction</a>
                    </li>
                </ul>
				
                <div id="correction-pending-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="correction-form">
                    <form action="/proofs/<?=$_GET['date']?>/<?=$_GET['department']?>/" method="post">
                        <input type="hidden" name="group_name" value="<?=$item['group_name']?>" />
                        <input type="hidden" name="family_group" value="<?=$item['family_uri']?>" />
                        <input type="hidden" name="item_name" value="<?=$item['name']?>" />
                        <input type="hidden" name="item_id" value="<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" />
                        <input type="hidden" name="department" value="<?=$item['department_url']?>" />
                        <input type="hidden" name="ad_id" value="<?=$zone['attrs']['id']?>" />
                        <input type="hidden" name="ad_date" value="<?=$_GET['date']?>" />
                        <input type="hidden" name="form_action" value="submit_correction" />
                        <input type="hidden" name="user" value="<?=$_SESSION['full_name']?>" />
                        <textarea name="message" id="message-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" cols="30" rows="3" required="required"></textarea>
                        <ul>
                            <li>
                                <input type="submit" name="submit_correction" value="Submit" />
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
			
            <div class="status approved">
                <p>Approved</p>
                <? if ($item['messages']['listing']) { ?>
                <a href="#messages-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="toggle button">Messages</a>
                <ol class="messages" id="messages-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>">
                    <? foreach ($item['messages']['listing'] as $message) { ?>
                        <li>
                            <div class="text"><?=$message['message']?></div>
                            <div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
                        </li>
                    <? } ?>
                </ol>
                <? } ?>
                <form action="/proofs/<?=$_GET['date']?>/<?=$_GET['department']?>/" method="post">
                    <ul>
                        <li>
                            <input type="hidden" name="group_name" value="<?=$item['group_name']?>" />
                            <input type="hidden" name="family_group" value="<?=$item['family_uri']?>" />
                            <input type="hidden" name="item_name" value="<?=$item['name']?>" />
                            <input type="hidden" name="item_id" value="<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" />
                            <input type="hidden" name="department" value="<?=$item['department_url']?>" />
                            <input type="hidden" name="ad_id" value="<?=$zone['attrs']['id']?>" />
                            <input type="hidden" name="ad_date" value="<?=$_GET['date']?>" />
                            <input type="hidden" name="form_action" value="submit_correction" />
                            <input type="hidden" name="user" value="<?=$_SESSION['full_name']?>" />
                            <a href="#correction-approved-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="button correction toggle">Make correction</a>
                        </li>
                    </ul>
                    <div id="correction-approved-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" class="correction-form">
                        <textarea name="message" id="message-<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" cols="30" rows="3" required="required"></textarea>
                        <ul>
                            <li>
                                <input type="submit" name="submit_correction" value="Submit" />
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
			
            <div class="status issue">
                <p>Correction Needed</p>
            	<ol class="messages">
                    <? if ($item['messages']['listing']) { foreach ($item['messages']['listing'] as $message) { ?>
                        <li>
                            <div class="text"><?=$message['message']?></div>
                            <div class="meta">&mdash;<?=$message['posted_by']?>, <?=date('n/j/Y g:i a', strtotime($message['date_posted']))?></div>
                        </li>
                    <? } } ?>
                </ol>
				
                <ul>
                    <li>
                        <form action="/proofs/<?=$_GET['date']?>/<?=$_GET['department']?>/" method="post">
                            <input type="hidden" name="group_name" value="<?=$item['group_name']?>" />
                            <input type="hidden" name="family_group" value="<?=$item['family_uri']?>" />
                            <input type="hidden" name="item_name" value="<?=$item['name']?>" />
                            <input type="hidden" name="item_id" value="<?=$zone['attrs']['id']?>-<?=$item['family_id']?>" />
                            <input type="hidden" name="department" value="<?=$item['department_url']?>" />
                            <input type="hidden" name="ad_id" value="<?=$zone['attrs']['id']?>" />
                            <input type="hidden" name="ad_date" value="<?=$_GET['date']?>" />
                            <input type="hidden" name="form_action" value="cancel_correction" />
                            <input type="hidden" name="user" value="<?=$_SESSION['full_name']?>" />
                            <input type="submit" name="cancel_correction" value="Cancel correction" />
                        </form>
                    </li>
                </ul>
            </div>
        </li>
        <? } ?>
    </ul>
    <? } else { ?>
        <p>No items keyed.</p>
    <? } ?>
</section>
<? } ?>

<? } else { ?>
	<h1>Proof Items On Upcoming&nbsp;Ads</h1>
	<p>There are currently no ads awaiting approval.</p>
<? } ?>

<? include 'inc/footer.inc.php'; ?>
