<? 
	include('../engine/Retractions.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<section class="retractions">
	<header>
		<h1>Retractions</h1>
		<? if ($_SESSION['user_level'] == 1) { ?>
		<nav>
			<a href="/retractions/new/" class="button">Add new</a>
		</nav>
		<? } ?>
	</header>
<?
		
	if ($retraction || (!empty($_GET['action']) && ($_GET['action'] == 'new' || $_GET['action'] == 'duplicate'))) {
		
		include('inc/retractions-form.inc.php');
	
	} else {
		
		include('inc/retractions-listing.inc.php');
	
	}
	
?>

</section>

<? include('inc/footer.inc.php'); ?>
