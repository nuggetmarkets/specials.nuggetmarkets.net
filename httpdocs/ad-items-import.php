<? 
	include('../engine/AdItemsImport.php');
	include('inc/header.inc.php');
	include('inc/message.inc.php');
?>

<h1>Ad Import</h1>
<nav><a href="/ads/">Back to ads</a></nav>

<table class="listing">
	<thead>
		<tr>
			<th>Ad Date</th>
			<th>Last Import</th>
			<th>DROSTE Import</th>
		</tr>
	</thead>
	<tbody>
	<? if ($ads) { foreach ($ads as $ad) { ?>
		<tr>
			<td><?=date('F j, Y', strtotime($ad['date_from']))?></td>
			<td><?=date('n/j/Y @ h:i a', strtotime($ad['last_import']))?></td>
			<td><a href="ad-items-import.php?date=<?=$ad['date_from']?>&amp;id=<?=$ad['id']?>" class="button">Import items</a></td>
		</tr>
	<? } } else { ?>
		<tr><td colspan="3">No ads to display.</td></tr>
	<? } ?>
	</tbody>
</table>
	
<? include('inc/footer.inc.php'); ?>
