<?
	include('../engine/Specials.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

	<? if ($ad) { ?>
	<nav id="menu" class="subnav">
		<div class="menu next-prev-ads">
			<? if ($ad_nav_dates['prev']['id']) { ?>
			<a href="/specials/<?=$ad_nav_dates['prev']['store_slug']?>/<?=$ad_nav_dates['prev']['date_from']?>/"><?=date('n/j/Y', strtotime($ad_nav_dates['prev']['date_from']))?></a>
			<? } ?>
			<nav class="menu sister-ads">
				<h3><?=$ad['store_chain']?></h3>
				<?=date('n/j/Y', strtotime($ad['date_from']))?> 
				<ul class="menu ">
				<? foreach ($sister_ads['ads'] as $sister_ad) { ?>
					<li class="<?=formatSlug($sister_ad['store_chain'])?>">
						<a href="/specials/<?=formatSlug($sister_ad['store_chain'])?>/<?=$sister_ad['date_from']?>/">
							<strong><?=$sister_ad['store_chain']?></strong>
							<div><?=date('n/j/Y', strtotime($sister_ad['date_from']))?></div>
						</a>
					</li>
				<? } ?>
				</ul>
			</nav>

			<? if ($ad_nav_dates['next']['id']) { ?>
			<a href="/specials/<?=$ad_nav_dates['next']['store_slug']?>/<?=$ad_nav_dates['next']['date_from']?>/"><?=date('n/j/Y', strtotime($ad_nav_dates['next']['date_from']))?></a>
			<? } ?>
		</div>

		<ul class="menu departments">
			<? foreach ($navigation as $department) { ?>
			<li><a href="#<?=$department['slug']?>"><?=$department['name']?></a></li>
			<? } ?>
			<li class="top"><a href="#main">Top</a></li>
			<li class="disabled"><a href="#disabled">Disabled</a></li>
		</ul>
	</nav>
	<!--
	<nav id="menu" class="menu">
		<h3><img src="/img/<?=$ad['store_slug']?>-black.svg" alt="<?=$ad['store_chain']?>" /> <?=date('F j, Y',strtotime($ad['date_from']))?></h3>
		<? if (isset($ad_nav)) { ?>
		<ul class="next-prev-ads">
			<? if (isset($ad_nav['nugget_markets'])) { ?>
			<li><a href="<?=$ad_nav['nugget_markets']['url']?>"><?=$ad_nav['nugget_markets']['label']?></a></li>
			<? } ?>
			<? if (isset($ad_nav['sonoma_market'])) { ?>
			<li><a href="<?=$ad_nav['sonoma_market']['url']?>"><?=$ad_nav['sonoma_market']['label']?></a></li>
			<? } ?>
			<? if (isset($ad_nav['fork_lift'])) { ?>
			<li><a href="<?=$ad_nav['fork_lift']['url']?>"><?=$ad_nav['fork_lift']['label']?></a></li>
			<? } ?>
			<? if (isset($ad_nav['food_4_less'])) { ?>
			<li><a href="<?=$ad_nav['food_4_less']['url']?>"><?=$ad_nav['food_4_less']['label']?></a></li>
			<? } ?>
		</ul>
		<? } ?>
		<ul>
			<? foreach ($navigation as $department) { ?>
			<li><a href="#<?=$department['slug']?>"><?=$department['name']?></a></li>
			<? } ?>
			<li class="top"><a href="#main">Top</a></li>
			<li class="back"><a href="#disabled">Disabled</a></li>
		</ul>
	</nav>
	 -->

	<? foreach ($ad['departments'] as $department) { ?>
	<section id="<?=$department['slug']?>">
		<h2><?=$department['name']?></h2>
		<? if ($department['name'] == 'Features') { ?>
			<p>These items will be featured in the Secret Special email.</p>
		<? } ?>

		<? if ($department['items']) { ?>
		<ul class="items grid">
			<? foreach ($department['items'] as $item) { ?>
			<li id="item-<?=$item['family_id']?>">
				<div>
					<? if ($item['feature_photo_small']) { ?>
					<figure class="feature-photo">
						<img src="/media/<?=$item['feature_photo_small']?>" alt="<?=$item['name']?>" />
					</figure>
					<? } else if ($item['upc_photos']) { ?>
					<figure class="upc-photos">
						<? foreach ($item['upc_photos'] as $photo) { ?>
						<img src="<?=$photo?>" alt="<?=$item['name']?>" />
						<? } ?>
					</figure>
					<? } else { ?>
					<figure class="watermark-image">
						<img src="/img/watermark-<?=$ad['store_slug']?>.svg" alt="<?=$ad['store_chain']?> watermark" />
					</figure>
					<? } ?>
					<div class="details">
						<? if (isset($item['group_labels'])) { ?>
						<ul class="badges">
						<? foreach ($item['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
							<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
						<? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
							<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
						<? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
							<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
						<? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
							<li class="<?=$label['type']?>"><img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
						<? } } } ?>
						</ul>
						<? } ?>
						<? if ($item['prefix']) { ?>
						<p class="prefix"><?=$item['prefix']?></p>
						<? } ?>
						<h3 class="name"><?=$item['name']?></h3>
						<? if ($item['suffix']) { ?>
						<p class="suffix"><?=$item['suffix']?></p>
						<? } ?>
						<? if (
							!empty($item['container']) &&
							!strstr($item['sale_price'],'lb.') &&
							!strstr($item['sale_price'],'lbs.') &&
							!strstr($item['sale_price'],'/ea.') &&
							!strstr($item['sale_price'],'%') && (
								$item['unit'] != 'CT' ||
								$item['size'] > 1
							) &&
							isset($item['size'])) {
						?>
						<span class="pack-size"><?=$item['size']?> <?=$item['unit']?>. <?=$item['container']?></span>
						<? } else if (isset($item['container_sizes'])) {$i = 1; foreach ($item['container_sizes'] as $key => $value) {
		
						if ($value) { ?>
							<div class="pack-size"><?=$value?> <?=$item['units'][$i]?>. <?=$item['containers'][$key]?></div>
						<? $i++; } } } if ($item['note']) { ?>
						<p><small><?=$item['note']?></small></p>
						<? } if ($item['selected_varieties']) { ?>
						<p class="selected-varieties">Selected varieties</p>
						<? } ?>
						<p class="sale-price"><?=$item['sale_price']?></p>
						<? /* if ($item['savings']) { ?>
						<p class="savings">Save <?=$item['savings']?></p>
						<? } */ ?>
						<? if ($item['limit']) { ?> 
							<div><?=$item['limit']?></div>
						<? } ?>
						<div class="upcs">
							<a href="#upcs-<?=$item['family_id']?>-<?=$department['slug']?>" class="toggle button">SKUs on ad</a>
							<ul id="upcs-<?=$item['family_id']?>-<?=$department['slug']?>">
							<? $i = 0; foreach($item['upcs'] as $upc) {  ?>
								<li><?=$upc?><br /><?=$item['pos_name'][$i]?></li>
							<? $i++;} ?>
							</ul>
						</div>
					</div>
				</div>
			</li>
			<? } ?>
		</ul>
		<? } ?>
	</section>
	<? } ?>

	<section>
		<h2 id="disabled">Disabled Items</h2>
		<? if ($disabled) { ?>
			<p>These items were keyed for the ad, but were manually disabled. Contact a Marketing associate if you need the item&nbsp;re-enabled.</p>
			<ul class="listing">
			<? foreach ($disabled as $product) { ?>
				<li>
					<a href="/products/edit/<?=$product['upc']?>/" target="_blank"><?=$product['upc']?></a> -
					<?=$product['pos_name']?>
				</li>
			<? } ?>
			</ul>
		<? } else { ?>
			<p>Items that have been removed from the ad will be displayed here. They can be reenabled if needed.</p>
		<? } ?>
	</section>

	<? } else if ($department) { ?>

	<nav>
		<? foreach ($department['nav'] as $dept) { ?>
		<a href="/specials/<?=$department['store_slug']?>/<?=$department['ad_date']?>/departments/<?=$dept['slug']?>/"><?=$dept['name']?></a>
		<? } ?>
		<a href="/specials/<?=$department['store_slug']?>/<?=$department['ad_date']?>/">All Departments</a>
	</nav>

	<section id="<?=$department['slug']?>">
		<h2><?=$department['name']?></h2>

		<? if ($department['items']) { ?>
		<ul class="grid">
			<? foreach ($department['items'] as $item) { ?>
			<li>
				<? if ($item['feature_photo_small']) { ?>
				<figure class="feature-photo">
					<img src="/media/<?=$item['feature_photo_small']?>" alt="<?=$item['name']?>" />
				</figure>
				<? } else if ($item['upc_photos']) { ?>
				<figure class="upc-photos">
					<? foreach ($item['upc_photos'] as $photo) { ?>
					<img src="<?=$photo?>" alt="<?=$item['name']?>" />
					<? } ?>
				</figure>
				<? } else { ?>
				<figure class="watermark">
					<img src="/img/watermark-<?=$department['store_slug']?>.svg" alt="<?=$department['store_chain']?> watermark" />
				</figure>
				<? } ?>
				<? if (isset($item['group_labels'])) { ?>
				<ul class="badges">
				<? foreach ($item['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
					<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
				<? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
					<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
				<? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
					<li class="<?=$label['type']?>"><img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
				<? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
					<li class="<?=$label['type']?>"><img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" /></li>
				<? } } } ?>
				</ul>
				<? } ?>
				<? if ($item['prefix']) { ?>
				<p class="prefix"><?=$item['prefix']?></p>
				<? } ?>
				<h3 class="name"><?=$item['name']?></h3>
				<? if ($item['suffix']) { ?>
				<p class="suffix"><?=$item['suffix']?></p>
				<? } ?>
				<? if ($item['note']) { ?>
				<p class="note"><?=$item['note']?></p>
				<? } if (
					!strstr($item['sale_price'],'lb.') &&
					!strstr($item['sale_price'],'lbs.') &&
					!strstr($item['sale_price'],'/ea.') &&
					!strstr($item['sale_price'],'%') && (
						$item['unit'] != 'CT' ||
						$item['size'] > 1
					) &&
					isset($item['size'])) {
				?>
				<span class="pack-size"><?=$item['size']?> <?=$item['unit']?>. <?=$item['container']?></span>
				<? } else if (isset($item['container_sizes'])) {$i = 1; foreach ($item['container_sizes'] as $key => $value) { if ($value) { ?>
					<div class="pack-size"><?=$value?> <?=$item['units'][$i]?>. <?=$item['containers'][$key]?></div>
				<? $i++; } } } if ($item['selected_varieties']) { ?>
				<p class="selected-varieties">Selected varieties</p>
				<? } ?>
				<p class="sale-price"><?=$item['sale_price']?></p>
				<? if ($item['savings']) { ?>
				<p class="savings">Save <?=$item['savings']?></p>
				<? } ?>
				<? if ($item['limit']) { ?> 
					<div><?=$item['limit']?></div>
				<? } ?>
				<div class="upcs">
					<a href="#upcs-<?=$item['family_id']?>-<?=$department['slug']?>" class="toggle button">UPCs</a>
					<ul id="upcs-<?=$item['family_id']?>-<?=$department['slug']?>">
					<? foreach($item['upcs'] as $upc) { ?>
						<li><?=$upc?></li>
					<? } ?>
					</ul>
				</div>
			</li>
			<? } ?>
		</ul>
		<? } ?>
	</section>

	<? } ?>

<? include('inc/footer.inc.php'); ?>
