<?
	include('../engine/Retractions.php');
?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="/css/retractions-print.css?i=2" type="text/css" media="all" charset="utf-8" />
		<title><?=$retraction['file_name']?></title>
	</head>

	<body class="ad retractions print">

		<div class="main">

			<? for ($i = 0; $i < 3; $i++) { ?>

			<table class="section">
				<tr>
					<td class="date">
						<p>To our valued guests:<br /> Please note the following correction to our<br /> advertisement dated <strong><?=date('F j, Y',strtotime($retraction['date_from']))?></strong></p>
					</td>
					<td class="logo <?=$retraction['store_slug']?>">
						<img src="/img/<?=$retraction['logo']?>" alt="<?=$retraction['store_chain']?>" />
					</td>
				</tr>

				<tr>
					<td colspan="2" class="description">
						<?=$retraction['description']?>
					</td>
				</tr>

				<tr>
					<td class="apology">
						<p>We apologize for any inconvenience.<br /> Thank you for shopping with us!</p>
					</td>
					<td class="grid">
						<table cellspacing="0" cellpadding="0" border="0" class="adgrid">
							<tr>
								<th class="starts">Ad&nbsp;Starts <?=date('l',strtotime($retraction['date_from']))?></th>
								<td>
									<h2 class="effective">Ad Prices Effective <?=date('F',strtotime($retraction['date_from']))?>:</h2>
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<th><?=$ad['day_1']?></th>
											<th><?=$ad['day_2']?></th>
											<th><?=$ad['day_3']?></th>
											<th><?=$ad['day_4']?></th>
											<th><?=$ad['day_5']?></th>
											<th><?=$ad['day_6']?></th>
											<th><?=$ad['day_7']?></th>
										</tr>
										<tr>
											<td><?=$ad['date_1']?></td>
											<td><?=$ad['date_2']?></td>
											<td><?=$ad['date_3']?></td>
											<td><?=$ad['date_4']?></td>
											<td><?=$ad['date_5']?></td>
											<td><?=$ad['date_6']?></td>
											<td><?=$ad['date_7']?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2"><small>Limit quantity of 4 on all advertised specials, unless otherwise stated.</small></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<? } ?>
		</div>
	</body>
</html>
