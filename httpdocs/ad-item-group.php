<?
	include('../engine/AdItemGroup.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<h1>Group &ldquo;<?=$item['family_group']?>&rdquo;<br /><small>As appearing on the <?=date('n/j/Y', strtotime($item['on_ad_date']))?> <?=$ad['store_chain']?> ad</small></h1>
<p><a href="/ad-item-config/<?=$item['family_uri']?>/" class="button">Edit Product Configuration</a></p>

<form action="/ad-item-group/<?=$_GET['ad_id']?>/<?=encodeFamilyGroupUrlString($_GET['family'])?>/" method="post">

	<fieldset>
		<legend>Item Name</legend>
		<p>The item name and description as it appears on the ad.</p>
		<ul>
			<li>
				<label for="prefix">Prefix:</label>
				<input type="text" name="prefix" id="prefix" value="<?=$item['prefix']?>" />
			</li>
			<li>
				<label for="pos_name">POS Name:</label>
				<input type="text" name="pos_name" id="pos_name" value="<?=$item['pos_name']?>" disabled="disabled" />
			</li>
			<li>
				<label for="display_name">Display Name:</label>
				<input type="text" name="display_name" id="display_name" value="<?=$item['display_name']?>" />
				<div class="note">Overrides POS name</div>
			</li>
			<li>
				<label for="group_name">Group As:</label>
				<input type="text" name="group_name" id="group_name" value="<?=$item['group_name']?>" />
				<div class="note">Overrides display name</div>
			</li>
			
			<li>
				<label for="suffix">Suffix:</label>
				<input type="text" name="suffix" id="suffix" value="<?=$item['suffix']?>" />
			</li>
			<li>
				<label for="note">Note:</label>
				<input type="text" name="note" id="note" value="<?=$item['note']?>" />
			</li>
			<li>
				<label for="description">Description:</label>
				<textarea name="description" id="description" class="tinymce"><?=$item['description']?></textarea>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>UPCs</legend>
		<p>Manage the individual UPCs in this group.</p>
		<div id="upcs">
			<table>
				<thead>
					<tr>
						<th class="item">Item Details</th>
						<th class="enable">Enable</th>
						<th class="image">Images</th>
						<th class="image">Find Images</th>
					</tr>
				</thead>
				<tbody>
				<? foreach ($upcs['skus'] as $upc) { ?>
					<tr>
						<td>
							<?=$upc['upc']?> - <?=$upc['pos_name']?> <br />
							<input type="text" name="retail[<?=$upc['id']?>]" value="<?=$upc['retail_price']?>" size="6" class="retails currency" />
							<input type="text" name="size[<?=$upc['id']?>]" value="<?=$upc['size']?>" size="5" class="sizes" />
							<input type="text" name="unit[<?=$upc['id']?>]" value="<?=$upc['unit']?>" size="2" class="units" />
							<select name="container[<?=$upc['id']?>]" class="containers">
								<option></option>
								<option <?if($upc['container'] == 'package'){echo 'selected="selected"';}?> value="package">package</option>
								<option disabled="disabled">- - - - - </option>
								<option <?if($upc['container'] == 'bag'){echo 'selected="selected"';}?> value="bag">bag</option>
								<option <?if($upc['container'] == 'basket'){echo 'selected="selected"';}?> value="basket">basket</option>
								<option <?if($upc['container'] == 'bottle'){echo 'selected="selected"';}?> value="bottle">bottle</option>
								<option <?if($upc['container'] == 'bottles'){echo 'selected="selected"';}?> value="bottles">bottles</option>
								<option <?if($upc['container'] == 'bowl'){echo 'selected="selected"';}?> value="bowl">bowl</option>
								<option <?if($upc['container'] == 'box'){echo 'selected="selected"';}?> value="box">box</option>
								<option <?if($upc['container'] == 'brick'){echo 'selected="selected"';}?> value="brick">brick</option>
								<option <?if($upc['container'] == 'bucket'){echo 'selected="selected"';}?> value="bucket">bucket</option>
								<option <?if($upc['container'] == 'bunch'){echo 'selected="selected"';}?> value="bunch">bunch</option>
								<option <?if($upc['container'] == 'can'){echo 'selected="selected"';}?> value="can">can</option>
								<option <?if($upc['container'] == 'cans'){echo 'selected="selected"';}?> value="cans">cans</option>
								<option <?if($upc['container'] == 'canister'){echo 'selected="selected"';}?> value="canister">canister</option>
								<option <?if($upc['container'] == 'carton'){echo 'selected="selected"';}?> value="carton">carton</option>
								<option <?if($upc['container'] == 'case'){echo 'selected="selected"';}?> value="case">case</option>
								<option <?if($upc['container'] == 'container'){echo 'selected="selected"';}?> value="container">container</option>
								<option <?if($upc['container'] == 'cup'){echo 'selected="selected"';}?> value="cup">cup</option>
								<option <?if($upc['container'] == 'cups'){echo 'selected="selected"';}?> value="cups">cups</option>
								<option <?if($upc['container'] == 'cut'){echo 'selected="selected"';}?> value="cut">cut</option>
								<option <?if($upc['container'] == 'honey bear'){echo 'selected="selected"';}?> value="honey bear">honey bear</option>
								<option <?if($upc['container'] == 'jar'){echo 'selected="selected"';}?> value="jar">jar</option>
								<option <?if($upc['container'] == 'jars'){echo 'selected="selected"';}?> value="jars">jars</option>
								<option <?if($upc['container'] == 'jug'){echo 'selected="selected"';}?> value="jug">jug</option>
								<option <?if($upc['container'] == 'loaf'){echo 'selected="selected"';}?> value="loaf">loaf</option>
								<option <?if($upc['container'] == 'pack'){echo 'selected="selected"';}?> value="pack">pack</option>
								<option <?if($upc['container'] == 'portion'){echo 'selected="selected"';}?> value="portion">portion</option>
								<option <?if($upc['container'] == 'pouch'){echo 'selected="selected"';}?> value="pouch">pouch</option>
								<option <?if($upc['container'] == 'pouches'){echo 'selected="selected"';}?> value="pouches">pouches</option>
								<option <?if($upc['container'] == 'roll'){echo 'selected="selected"';}?> value="roll">roll</option>
								<option <?if($upc['container'] == 'rolls'){echo 'selected="selected"';}?> value="rolls">rolls</option>
								<option <?if($upc['container'] == 'stick'){echo 'selected="selected"';}?> value="stick">stick</option>
								<option <?if($upc['container'] == 'tin'){echo 'selected="selected"';}?> value="tin">tin</option>
								<option <?if($upc['container'] == 'tub'){echo 'selected="selected"';}?> value="tub">tub</option>
								<option <?if($upc['container'] == 'tubs'){echo 'selected="selected"';}?> value="tubs">tubs</option>
								<option <?if($upc['container'] == 'tube'){echo 'selected="selected"';}?> value="tube">tube</option>
								<option <?if($upc['container'] == 'tubes'){echo 'selected="selected"';}?> value="tubes">tubes</option>
								<option <?if($upc['container'] == 'wheel'){echo 'selected="selected"';}?> value="wheel">wheel</option>
							</select>
						</td>
						<td>
							<label>
								<input type="checkbox" name="enable_items[<?=$upc['id']?>]" value="1" <?if($upc['enabled']==1){echo 'checked="checked"';}?> class="enable_items" /> On ad
							</label>
						</td>
						<td class="inline">
						<? if (file_exists('img/upc/' . $upc['upc'] . '.webp')) { ?>
							<img src="/img/upc/<?=$upc['upc']?>.webp?t=<?=time()?>" alt="<?=$upc['upc']?> product image" />
							<label><input type="checkbox" name="upc_image_active[<?=$upc['id']?>]" value="1" <?if($upc['upc_image_active']==1){echo'checked="checked"';}?> />  Show image</label>
							<a href="/delete/upc-image/<?=$upc['upc']?>__webp/" class="delete x" title="Delete this image">&times;</a>
						<? } else if (file_exists('img/upc/' . $upc['upc'] . '.jpg')) { ?>
							<img src="/img/upc/<?=$upc['upc']?>.jpg?t=<?=time()?>" alt="<?=$upc['upc']?> product image" />
							<label><input type="checkbox" name="upc_image_active[<?=$upc['id']?>]" value="1" <?if($upc['upc_image_active']==1){echo'checked="checked"';}?> /> Show image</label>
							<a href="/delete/upc-image/<?=$upc['upc']?>__jpg/" class="delete x" title="Delete this image">&times;</a>
						<? } ?>
						</td>
						<td>
							<a href="https://platform.syndigo.com/marketplace/all-products" target="_blank" class="button">Syndigo</a>
							<a href="//www.google.com/search?q=<?=urlencode(strtolower($upc['pos_name']) . ' official website')?>" target="_blank" class="button">Google</a>
							<a href="https://nugget.n2r.io/maintenance/items?query=<?=$upc['upc']?>" target="_blank" class="button">N2</a>
							<? if ($item['is_perishable']) { ?>
								<a href="https://menu.nuggetmarket.com/intranet.v2/ad/ad2.aspx" target="_blank" class="button">V2</a>
							<? } else { ?>
								<a href="https://menu.nuggetmarket.com/intranet.v2/ad/ad1.aspx" target="_blank" class="button">V2</a>
							<? } ?>
						</td>
					</tr>
					
				<? } ?>
				</tbody>
				<tfoot>
					<tr>
						<td>
							Change all SKU values to:<br />
							<input type="text" name="override_retail" value="" size="6" rel="retails" class="currency symbol multiedit" />
							<input type="text" name="override_size" value="" size="5" rel="sizes" class="multiedit" />
							<input type="text" name="override_unit" value="" size="2" rel="units" class="multiedit" />
							<select name="override_container" rel="containers" class="multiedit">
								<option></option>
								<option value="package">package</option>
								<option disabled="disabled">- - - - - </option>
								<option value="bag">bag</option>
								<option value="basket">basket</option>
								<option value="bottle">bottle</option>
								<option value="bottles">bottles</option>
								<option value="bowl">bowl</option>
								<option value="box">box</option>
								<option value="brick">brick</option>
								<option value="bucket">bucket</option>
								<option value="bunch">bunch</option>
								<option value="can">can</option>
								<option value="cans">cans</option>
								<option value="canister">canister</option>
								<option value="carton">carton</option>
								<option value="case">case</option>
								<option value="container">container</option>
								<option value="cup">cup</option>
								<option value="cups">cups</option>
								<option value="cut">cut</option>
								<option value="honey bear">honey bear</option>
								<option value="jar">jar</option>
								<option value="jars">jars</option>
								<option value="jug">jug</option>
								<option value="loaf">loaf</option>
								<option value="pack">pack</option>
								<option value="portion">portion</option>
								<option value="pouch">pouch</option>
								<option value="pouches">pouches</option>
								<option value="roll">roll</option>
								<option value="rolls">rolls</option>
								<option value="stick">stick</option>
								<option value="tin">tin</option>
								<option value="tub">tub</option>
								<option value="tubs">tubs</option>
								<option value="tube">tube</option>
								<option value="tubes">tubes</option>
								<option value="wheel">wheel</option>
							</select>
						</td>
						<td colspan="3">
							<label><input type="checkbox" value="1" rel="enable_items" checked="checked" class="multicheck" /> Toggle all</label>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</fieldset>

	<fieldset>
		<legend>Pricing</legend>
		<p>To adjust the sale price for this item, type the new price into the <strong>Sale</strong> field, omitting the dollar sign. Use 2/3.00 to display two-for pricing such as 2/$3. Use the <strong>Custom Price</strong> field for &ldquo;Save up to 40%.&rdquo;</p>
		<ul>
			<li class="inline">
				<label for="sale_price">Sale:</label>
				<input type="text" name="sale_price" value="<?=unescape($item['sale_price'])?>" size="6" class="currency symbol" />
				
				<label for="override_price" title="Override the price that appears in the stop sign">Custom Price:</label>
				<input type="text" name="override_price" value="<?=unescape($item['override_price'])?>" size="36" />
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Limit and CRV</legend>
		<p>Set the any limits or CRV for all items in this family group.</p>
		<ul>
			<li class="inline">
				<label for="limit">Limit:</label>
				<input type="text" name="limit" id="limit" value="<?=unescape($item['limit'])?>" size="6" maxlength="2" />
				<label class="check">
					<input type="checkbox" name="supplies_last" value="1" <?if($item['supplies_last']==1){echo 'checked="checked"';}?> /> &ldquo;While supplies last&rdquo;
				</label>
				<label class="check">
					<input type="checkbox" name="crv" value="1" <?if($item['crv']==1){echo 'checked="checked"';}?> /> +CRV
				</label>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Labels <a href="#badges" id="display-badges" class="toggle button">Toggle labels</a></legend>
		<p>Select the default labels to display on this item.</p>
		<ul id="badges" class="hide checker">
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'brand') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'organization') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'seasoning') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Feature Images</legend>
		<p>Select from these frequently used images or browse for additional images.</p>
		<p><a href="/media-select.php?category=instances&amp;id=<?=$item['family_group']?>&amp;ad_id=<?=$ad['id']?>" class="button" id="browse-media" data-category="instances" data-id="<?=$item['family_group']?>" data-ad_id="<?=$ad['id']?>" data-action="/ad-item-group/<?=$_GET['family']?>/">Browse media</a> <label class="clear-default"><input type="radio" name="featured_image" value="">Clear default</label></p>
		<ul class="grid" id="attachments">
			<? if (isset($item['media_items'])) { foreach ($item['media_items'] as $media) {
				
				$path = pathinfo($media['file']);
				$media['file_name'] = $path['basename'];
				
			?>
				<li>
					<figure>
					<? if (strstr($media['mime_type'],'image')) { ?>
						<label for="media-item-<?=$media['id']?>"><img src="/media/<?=$media['small_image']?>" alt="<?=$media['alt_text']?>" /></label>
					<? } else if (strstr($media['mime_type'],'audio')) { ?>
						<audio controls="controls">
							<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
						</audio>
					<? } else if (strstr($media['mime_type'],'video')) { ?>
						<video controls="controls">
							<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
						</video>
					<? } else { ?>
						<label for="media-item-<?=$media['id']?>"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></label>
					<? } ?>
						<figcaption>
						<? if ($media['name']) { ?>
							<h3><?=$media['name']?></h3>
							<code><?=$media['file_name']?></code>
						<? } ?>
							<label><input type="checkbox" name="media[]" value="<?=$media['id']?>" id="media-item-<?=$media['id']?>" checked="checked" /> Attach to item</label>
						<? if (strstr($media['mime_type'],'image')) { ?>
							<label><input type="radio" name="featured_image" value="<?=$media['id']?>" <? if ($item['feature_image'] == $media['id']) { echo 'checked="checked"'; } ?> /> Make default</label>
						<? } ?>
						<a href="/media/edit/<?=$media['id']?>" class="button small">Edit</a>
						</figcaption>
					</figure>
				</li>
			<? } } else { ?>
				<li class="no-attachments">Media will be displayed here.</li>
			<? } ?>
			</ul>
	</fieldset>
	
	<fieldset class="features">
		<legend>Feature Settings</legend>
		<p>Feature this item at the top of the ad and in weekly specials&nbsp;email.</p>
		<ul>
			<li>
				<label for="feature">Position:</label>
				<select name="feature" id="feature">
					<option value="">Don&rsquo;t feature</option>
					<? for ($i = 1; $i <= 12; $i++) { ?>
						<option value="<?=$i?>" <? if ($item['feature'] == $i) { echo 'selected="selected"'; } ?>>Position <?=$i?></option>
					<? } ?>
				</select>
			</li>
		</ul>
		<p>Style:</p>
		<ul class="grid feature-position">
			<li <? if ($feature_config['feature_style'] == 'overlay') { echo 'class="selected"'; } ?>>
				<label><input type="radio" name="feature_style" value="overlay" rel="overlay" <? if ($feature_config['feature_style'] == 'overlay') { echo 'checked="checked"'; } ?> /> Overlay</label>
				<div class="caption-position">
					<? if ($image) { ?>
					<picture>
						<source srcset="/media/<?=$image['small_image']?>" media="(max-width: 320px)">
						<source srcset="/media/<?=$image['medium_image']?>" media="(max-width: 600px)">
						<source srcset="/media/<?=$image['large_image']?>">
						<img src="/media/<?=$image['medium_image']?>" alt="<?=$image['alt_text']?>" />
					</picture>
					<? } else { ?>
					<img src="/img/feature-image-needed.svg" alt="Feature image needed" />
					<? } ?>
					<ul class="grid radioer">
						<? foreach ($feat_edit_pos AS $key => $val ) { ?>
							<li>
							<? if ($feature_config['feature_caption_position'] == $val ) { ?>
								<label class="selected">
									<img src="/img/icon-editorial.svg" alt="Editorial icon" />
									<input type="radio" name="feature_caption_position" value="<?=$val?>" checked="checked" />
								</label>
							<? } else { ?>
								<label>
									<img src="/img/icon-editorial.svg" alt="Editorial icon" />
									<input type="radio" name="feature_caption_position" value="<?=$val?>" />
								</label>
							<? } ?>
							</li>
						<? } ?>
					</ul>
				</div>
				<ul class="caption-attributes">
					<li>
						<label for="feature_background_color">Background color:</label>
						<input type="text" name="feature_background_color" id="feature_background_color" value="<? if ($feature_config['feature_background_color']) { echo $feature_config['feature_background_color']; } ?>" size="14" placeholder="ex: 255,255,255,80" /> 
						<span class="note">In R,G,B or R,G,B,A format</span>
					</li>
					<li>
						<label for="feature_font_color">Font color:</label>
						<input type="text" name="feature_font_color" id="feature_font_color" value="<? if ($feature_config['feature_font_color']) { echo $feature_config['feature_font_color']; } ?>" size="14" placeholder="ex: 0,0,0" /> 
						<span class="note">In R,G,B or R,G,B,A format</span>
					</li>
					<li>Append recipe editorial</li>
					<li class="inline">
						<label for="feature_append_recipe">Recipe ID:</label>
						<input type="text" name="feature_append_recipe" id="feature_append_recipe" value="<? if ($feature_config['append_recipe']) { echo $feature_config['append_recipe']; } ?>" size="4" /> 
						<a href="https://www.nuggetmarket.com/recipes/">Recipe search</a>
					</li>
				</ul>
			</li>
			<li <? if ($feature_config['feature_style'] == 'side-by-side') { echo 'class="selected"'; } ?>>
				<label><input type="radio" name="feature_style" value="side-by-side" rel="side-by-side" <? if ($feature_config['feature_style'] == 'side-by-side') { echo 'checked="checked"'; } ?> /> Side by Side</label>
				<div class="flex">
					<? if ($image) { ?>
					<div><img src="/media/<?=$image['small_image']?>" alt="<?=$image['alt_text']?>" /></div>
					<? } else { ?>
					<div><img src="/img/feature-image-needed.svg" alt="Feature image needed" /></div>
					<? } ?>
					<div><img src="/img/icon-editorial.svg" alt="Editorial icon" /></div>
				</div>
			</li>
			<li class="mask <? if (!$item['feature']) { ?>masked<? } ?>"></li>
		</ul>
	</fieldset>
	
	<fieldset>
		<legend>Publish Settings</legend>
		<p>Use the controls below to set the order in which liners appear on the&nbsp;ad.</p>
		<ul>
			<li class="inline">
				<label>Sort Order:</label>
				<input type="number" name="sort" id="sort" value="<?=$item['sort']?>" size="2" />
				<span class="note">Set the order in which you want the item to appear in its&nbsp;department.</span>
			</li>
		</ul>
	</fieldset>

	<fieldset class="messages">
		<legend>Messages</legend>
		<p>Notification messages for this item.</p>
		<? if ($messages['listing']) { ?>
		<ol class="listing thread status-<?=$item['status']?>">
		<? foreach ($messages['listing'] as $message) { ?>
			<li>
				<div>
					<strong><?=$message['posted_by']?></strong>
					<time datetime="<?=date('c', strtotime($message['date_posted']))?>"><?=date('F j, Y g:i a', strtotime($message['date_posted']))?></time>
				</div>
				<div><?=$message['message']?> <span class="status <?=$item['status']?>"><?=$item['status']?></span></div>
			</li>
		<? } ?>
			<li class="reply">
				<ul>
					<li>
						<label for="message">Reply:</label>
						<textarea name="message" cols="16" rows="2"></textarea>
					</li>
				</ul>
				<ul class="radioer">
					<li>
						<label for="status">Change status:</label>
						
						<? if ($item['status'] == 'issue') { ?>
						<label class="selected issue"><input type="radio" name="status" value="issue" checked="checked" /> Issue</label>
						<? } else { ?>
						<label><input type="radio" name="status" value="issue" /> Issue</label>
						<? } ?>
						
						<? if ($item['status'] == 'pending') { ?>
						<label class="selected pending"><input type="radio" name="status" value="pending" checked="checked" /> Pending</label>
						<? } else { ?>
						<label><input type="radio" name="status" value="pending" /> Pending</label>
						<? } ?>
						
						<? if ($item['status'] == 'approved') { ?>
						<label class="selected approved"><input type="radio" name="status" value="approved" checked="checked" /> Approved</label>
						<? } else { ?>
						<label><input type="radio" name="status" value="approved" /> Approved</label>
						<? } ?>
						
						<input type="hidden" name="family_group" value="<?=$item['family_group']?>" />
						<input type="hidden" name="department" value="<?=unescape(vslug($item['department']))?>" />
						<input type="hidden" name="ad_date" value="<?=$ad['date_from']?>" />
						<input type="hidden" name="message_reply_to" value="<?=end($messages['reply_to'])?>" />
						<input type="hidden" name="current_status" value="<?=$item['status']?>" />
						<input type="hidden" name="message_pos_name" value="<?=$item['pos_name']?>" />
					</li>
				</ul>
			</li>
		</ol>
		<? } else { ?>
			<p>
				There are no messages for this item.
				<input type="hidden" name="status" value="<?=$item['status']?>" />
				<input type="hidden" name="current_status" value="<?=$item['status']?>" />
			</p>
			
		<? } ?>
	</fieldset>

	<ul class="actions">
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="ad_id" value="<?=$_GET['ad_id']?>" />
			<input type="hidden" name="ad_date" value="<?=$ad['date_from']?>" />
			<input type="hidden" name="item_id" value="<?=$_GET['ad_id']?>-<?=hash('md5',$item['family_uri'])?>" />
			<input type="hidden" name="ad_item_group_id" value="<?=encodeFamilyGroupUrlString($_GET['family'])?>" />
		</li>
		<li class="inline">
			<label class="check">
				<input type="checkbox" name="update_config" value="1" /> Apply to config.
			</label>
			<label class="check">
			<? if ($item['feature'] > 0) { ?>
				<span class="disabled"><input type="checkbox" name="update_sister_ads" value="0" disabled /> Apply to siblings is disabled for featured items</span>
			<? } else { ?>
				<input type="checkbox" name="update_sister_ads" value="1" /> Apply to siblings
			<? } ?>
			</label>
		</li>
		<li>
			<a href="/">Cancel</a>
		</li>
	</ul>
</form>

<? include('inc/footer.inc.php'); ?>
