<?
	include('../engine/SecretSpecials.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<section class="secret-specials">
	<header>
		<h1>Secret Specials</h1>
	</header>

<?

	if (isset($secret_special) || (!empty($_GET['action']) && $_GET['action'] == 'edit')) {
		
		include('inc/secret-specials-form.inc.php');
		
	} else {
		
		include('inc/secret-specials-listing.inc.php');
		
	}

	include('inc/footer.inc.php');
?>
