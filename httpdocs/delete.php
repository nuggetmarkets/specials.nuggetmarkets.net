<?
    require_once('../engine/Delete.php');
    include('inc/header.inc.php');
    include('inc/messages.inc.php');
?>

<section class="delete">
	<header>
		<h1>Delete <?=$_GET['category']?></h1>
	</header>
	
	<? if ($item) { ?>
	<form action="/delete/" method="post" accept-charset="utf-8">
		<fieldset>
			<legend>Confirm</legend>
			<ul>
				<li>
					<p>Are you sure you want to delete the following <?=$item['singular']?>?</p>
					<h3><?=$item['description']['name']?></h3>
					<? if (isset($item['description']['small_image'])) { ?>
						<img src="/media/<?=$item['description']['small_image']?>" alt="<?=$item['description']['name']?>" />
					<? } ?>
					
				</li>
			</ul>
		</fieldset>
		
		<ul class="actions">
			<li>
				<input type="hidden" name="item_category" value="<?=$_GET['category']?>" />
				<input type="hidden" name="item_id" value="<?=$item['id']?>" />
				<input type="hidden" name="referer" value="<?=$item['referer']?>" />
				<input type="submit" name="delete" class="delete" value="Delete <?=$item['singular']?>" />
			</li>
			<li>
				<a href="<?=$_SERVER['HTTP_REFERER']?>" class="cancel">Cancel</a>
			</li>
		</ul>
	</form>
	<? } else { ?>
		<p>This item is not available for deletion. <a href="<?=$_SERVER['HTTP_REFERER']?>">Go back</a></p>
	<? } ?>
</section>

<? include('inc/footer.inc.php'); ?>
