<?
    require_once('../engine/Media.php');
    include('inc/header.inc.php');
?>
<? if ($media_items) { ?>
<form action="/media-select.php?<?=$_SERVER['QUERY_STRING']?>" method="get" accept-charset="utf-8" class="search">
		<input type="search" name="terms" value="<?=$search_terms?>" placeholder="Search media" autofocus /> 
		<input type="hidden" name="category" value="<?=$_GET['category']?>" /> 
		<input type="hidden" name="ad_id" value="<?=$_GET['ad_id']?>" /> 
		<input type="hidden" name="id" value="<?=$_GET['id']?>" />
		<input type="submit" value="Search" name="search" />
</form>
<? } ?>
<section class="media">
	<header>
		<h1>Attachments</h1>
		
	</header>
	<form action="/media-select/<?=$_GET['category']?>/<?=$_GET['id']?>/" id="attachments_form" method="post" target="_parent" accept-charset="utf-8">
		<div id="response"></div>
	<? if ($media_items) { ?>
	<ol class="grid">
	<? foreach ($media_items['listing'] as $media) { ?>
		<li id="attachment-<?=$media['id']?>" data-mime="<?=$media['mime_type']?>">
			<label>
				<figure>
				<? if (strstr($media['mime_type'],'image')) { ?>
					<img src="/media/<?=$media['small_image']?>" alt="<?=$media['alt_text']?>" />
				<? } else if (strstr($media['mime_type'],'audio')) { ?>
					<audio controls="controls">
						<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
					</audio>
				<? } else if (strstr($media['mime_type'],'video')) { ?>
					<video controls="controls">
						<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
					</video>
				<? } else { ?>
					<img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" />
				<? } ?>
					<figcaption>
					<? if ($media['name']) { ?>
						<h3><?=$media['name']?></h3>
					<? } ?>
					
					<? if (!in_array($media['id'], $links)) { ?>
						<input type="checkbox" name="media[]" value="<?=$media['id']?>" id="media-item-<?=$media['id']?>" /> <code><?=$media['file_name']?></code>
					<? } else { ?>
						<span class="attached ribbon">Attached</span> <code><?=$media['file_name']?></code>
					<? } ?>
					</figcaption>
				</figure>
			</label>
		</li>
	<? } ?>
	</ol>
	<footer>
	<? if ($media_items['pagination']) { ?>
		<nav class="pagination">
		<? if (!is_null($media_items['pagination']['first'])) { ?>
			<a href="/media-select.php?page=1&amp;category=<?=$_GET['category']?>&amp;id=<?=$_GET['id']?>">First</a> 
			<a href="/media-select.php?page=<?=$media_items['pagination']['previous']?>&amp;category=<?=$_GET['category']?>&amp;id=<?=$_GET['id']?>">Prev</a> 
		<? } else { ?>
			<span class="deact">First</span> 
			<span class="deact">Prev</span>
		<? } ?>
		<span class="total">Page <?=$media_items['pagination']['current']?> of <?=$media_items['pagination']['total']?></span>
		<? if (!is_null($media_items['pagination']['last'])) { ?>
			<a href="/media-select.php?page=<?=$media_items['pagination']['next']?>&amp;category=<?=$_GET['category']?>&amp;id=<?=$_GET['id']?>">Next</a> 
			<a href="/media-select.php?page=<?=$media_items['pagination']['last']?>&amp;category=<?=$_GET['category']?>&amp;id=<?=$_GET['id']?>">Last</a> 
		<? } else { ?>
			<span class="deact">Next</span> 
			<span class="deact">Last</span>
		<? } ?>
		</nav>
	<? } ?>
	</footer>
	<? } else { ?>
	
		<p>No items found.</p>
	
	<? } ?>
		<ul class="actions">
			<li>
				<?/* foreach ($links as $link) { ?>
					<input type="hidden" name="media[]" value="<?=$link?>" />
				<? }*/ ?>
				<input type="hidden" name="item_category" id="item_category" value="<?=$_GET['category']?>" />
				<input type="hidden" name="item_id" id="item_id" value="<?=$_GET['id']?>" />
				<input type="hidden" name="attach" value="attach_selected" />
				<input type="submit" name="attach" id="attach_media" value="Attach selected" />
			</li>
			<li>
				<a href="/<?=$_GET['category']?>/edit/<?=$_GET['id']?>/" target="_parent" id="close-media">Cancel</a>
			</li>
		</ul>
	</form>
</section>
<? include('inc/footer.inc.php'); ?>
