<? 
	include('../engine/Products.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<form action="/products/search/" method="get" accept-charset="utf-8" class="search">
	<input type="search" name="terms" value="<?if(!empty($_GET['terms'])){echo $_GET['terms'];}?>" placeholder="Search products" autofocus >
	<input type="submit" value="Search" name="search">
</form>

<section class="products">
	<header>
		<h1>Products</h1>
		<? if ($_SESSION['user_level'] == 1) { ?>
		<p>Mange products imported fro DROSTE and V2.</p>
		<nav>
			<a href="/products/new/" class="button">Add new</a>
		</nav>
		<? } else { ?>
		<p>View products imported from DROSTE and V2.</p>
		<? } ?>
	</header>

<?
	
	if ((isset($item) || (!empty($_GET['action']) && $_GET['action'] == 'new')) && $_SESSION['user_level'] == 1) {
		
		include('inc/products-form.inc.php');
		
	} else if ((isset($item) || (!empty($_GET['action']) && $_GET['action'] == 'new'))  && $_SESSION['user_level'] == 2) {
		
		include('inc/products-display.inc.php');
		
	} else {
		
		include('inc/products-listing.inc.php');
		
	}
	
?>
</section>
<? include('inc/footer.inc.php'); ?>
