<?
	include('../engine/AdItemConfig.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<h1>Configuration for Group &ldquo;<?=$item['family_group']?>&rdquo;</h1>

<form action="/ad-item-config/<?=$_GET['family']?>/" method="post">

	<fieldset>
		<legend>Item Name</legend>
		<p>The item name and description as it will appear on future ads.</p>
		<ul>
			<li>
				<label for="prefix">Prefix:</label>
				<input type="text" name="prefix" id="prefix" value="<?=$item['prefix']?>" />
			</li>
			<li>
				<label for="pos_name">POS Name:</label>
				<input type="text" name="pos_name" id="pos_name" value="<?=$item['pos_name']?>" disabled="disabled" />
			</li>
			<li>
				<label for="display_name">Display Name:</label>
				<input type="text" name="display_name" id="display_name" value="<?=$item['display_name']?>" />
				<div class="note">Overrides POS name</div>
			</li>
			<li>
				<label for="suffix">Suffix:</label>
				<input type="text" name="suffix" id="suffix" value="<?=$item['suffix']?>" />
			</li>
			<li>
				<label for="note">Note:</label>
				<input type="text" name="note" id="note" value="<?=$item['note']?>" />
			</li>
			<li>
				<label for="description">Description:</label>
				<textarea name="description" id="description" class="tinymce"><?=$item['description']?></textarea>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Appearing On Ad</legend>
		<p>Change the way this item is displayed on the current or upcoming ads by clicking its date below.</p>
		<div>
		<table>
			<thead>
				<tr>
					<th>On Ad</th>
					<th>Display Name</th>
					<th>Retail Price</th>
					<th>Sale Price</th>
				</tr>
			</thead>

			<tbody>

			<? if (is_array($ads)) { ?>

			<? foreach ($ads as $ad) { ?>

				<tr>
					<td><a href="/ad-item-group/<?=$ad['id']?>/<?=encodeFamilyGroupUrlString($ad['family_group'])?>/" class="button"><?=$ad['store_chain']?> <?=date('m/j/Y',strtotime($ad['date_from']))?></a></td>
					<td>
						<? if ($ad['display_name']) {
							echo $ad['display_name'];
						} else {
							echo $ad['pos_name'];
						} ?>
					</td>
					<td>$<?=$ad['retail_price']?></td>
					<td>$<?=str_replace('/','/$',$ad['sale_price'])?></td>
				</tr>

			<? } } else { ?>

				<tr>
					<td colspan="4">This item isn&rsquo;t on any upcoming ads.</td>
				</tr>

			<? } ?>
			</tbody>
		</table>
		</div>
	</fieldset>

	<fieldset>
		<legend>UPCs</legend>
		<p>Manage the individual UPCs in this group.</p>
		<div id="upcs">
			<table>
				<thead>
					<tr>
						<th class="item">Item Details</th>
						<th class="image">Image</th>
						<th>Find Images</th>
					</tr>
				</thead>
				<tbody>
				<? foreach ($upcs['skus'] as $upc) { ?>
					<tr>
						<td>
							<?=$upc['upc']?> - <?=$upc['pos_name']?> <br />
							<input type="text" name="retail[<?=$upc['upc']?>]" value="<?=$upc['retail_price']?>" size="6" class="retails currency" />
							<input type="text" name="size[<?=$upc['upc']?>]" value="<?=$upc['size']?>" size="5" class="sizes" />
							<input type="text" name="unit[<?=$upc['upc']?>]" value="<?=$upc['unit']?>" size="2" class="units" />
							<select name="container[<?=$upc['upc']?>]" class="containers">
								<option></option>
								<option <?if($upc['container'] == 'package'){echo 'selected="selected"';}?> value="package">package</option>
								<option disabled="disabled">- - - - - </option>
								<option <?if($upc['container'] == 'bag'){echo 'selected="selected"';}?> value="bag">bag</option>
								<option <?if($upc['container'] == 'basket'){echo 'selected="selected"';}?> value="basket">basket</option>
								<option <?if($upc['container'] == 'bottle'){echo 'selected="selected"';}?> value="bottle">bottle</option>
								<option <?if($upc['container'] == 'bottles'){echo 'selected="selected"';}?> value="bottles">bottles</option>
								<option <?if($upc['container'] == 'bowl'){echo 'selected="selected"';}?> value="bowl">bowl</option>
								<option <?if($upc['container'] == 'box'){echo 'selected="selected"';}?> value="box">box</option>
								<option <?if($upc['container'] == 'brick'){echo 'selected="selected"';}?> value="brick">brick</option>
								<option <?if($upc['container'] == 'bucket'){echo 'selected="selected"';}?> value="bucket">bucket</option>
								<option <?if($upc['container'] == 'bunch'){echo 'selected="selected"';}?> value="bunch">bunch</option>
								<option <?if($upc['container'] == 'can'){echo 'selected="selected"';}?> value="can">can</option>
								<option <?if($upc['container'] == 'cans'){echo 'selected="selected"';}?> value="cans">cans</option>
								<option <?if($upc['container'] == 'canister'){echo 'selected="selected"';}?> value="canister">canister</option>
								<option <?if($upc['container'] == 'carton'){echo 'selected="selected"';}?> value="carton">carton</option>
								<option <?if($upc['container'] == 'case'){echo 'selected="selected"';}?> value="case">case</option>
								<option <?if($upc['container'] == 'container'){echo 'selected="selected"';}?> value="container">container</option>
								<option <?if($upc['container'] == 'cup'){echo 'selected="selected"';}?> value="cup">cup</option>
								<option <?if($upc['container'] == 'cups'){echo 'selected="selected"';}?> value="cups">cups</option>
								<option <?if($upc['container'] == 'cut'){echo 'selected="selected"';}?> value="cut">cut</option>
								<option <?if($upc['container'] == 'honey bear'){echo 'selected="selected"';}?> value="honey bear">honey bear</option>
								<option <?if($upc['container'] == 'jar'){echo 'selected="selected"';}?> value="jar">jar</option>
								<option <?if($upc['container'] == 'jars'){echo 'selected="selected"';}?> value="jars">jars</option>
								<option <?if($upc['container'] == 'jug'){echo 'selected="selected"';}?> value="jug">jug</option>
								<option <?if($upc['container'] == 'loaf'){echo 'selected="selected"';}?> value="loaf">loaf</option>
								<option <?if($upc['container'] == 'pack'){echo 'selected="selected"';}?> value="pack">pack</option>
								<option <?if($upc['container'] == 'portion'){echo 'selected="selected"';}?> value="portion">portion</option>
								<option <?if($upc['container'] == 'pouch'){echo 'selected="selected"';}?> value="pouch">pouch</option>
								<option <?if($upc['container'] == 'pouches'){echo 'selected="selected"';}?> value="pouches">pouches</option>
								<option <?if($upc['container'] == 'roll'){echo 'selected="selected"';}?> value="roll">roll</option>
								<option <?if($upc['container'] == 'rolls'){echo 'selected="selected"';}?> value="rolls">rolls</option>
								<option <?if($upc['container'] == 'stick'){echo 'selected="selected"';}?> value="stick">stick</option>
								<option <?if($upc['container'] == 'tin'){echo 'selected="selected"';}?> value="tin">tin</option>
								<option <?if($upc['container'] == 'tub'){echo 'selected="selected"';}?> value="tub">tub</option>
								<option <?if($upc['container'] == 'tubs'){echo 'selected="selected"';}?> value="tubs">tubs</option>
								<option <?if($upc['container'] == 'tube'){echo 'selected="selected"';}?> value="tube">tube</option>
								<option <?if($upc['container'] == 'tubes'){echo 'selected="selected"';}?> value="tubes">tubes</option>
								<option <?if($upc['container'] == 'wheel'){echo 'selected="selected"';}?> value="wheel">wheel</option>
							</select>
							<a href="/products/edit/<?=$upc['upc']?>/">Edit SKU</a>
						</td>
						<td class="inline">
						<? if (file_exists('img/upc/' . $upc['upc'] . '.webp')) { ?>
							<img src="/img/upc/<?=$upc['upc']?>.webp" alt="<?=$upc['upc']?> product image" /> 
							<label><input type="checkbox" name="upc_image_active[<?=$upc['upc']?>]" value="1" <?if($upc['upc_image_active']==1){echo'checked="checked"';}?> />  Show image</label>
							<a href="/delete/upc-image/<?=$upc['upc']?>__webp/" class="delete x" title="Delete this image">&times;</a>
						<? } else if (file_exists('img/upc/' . $upc['upc'] . '.jpg')) { ?>
							<img src="/img/upc/<?=$upc['upc']?>.jpg" alt="<?=$upc['upc']?> product image" />
							<label><input type="checkbox" name="upc_image_active[<?=$upc['upc']?>]" value="1" <?if($upc['upc_image_active']==1){echo'checked="checked"';}?> />  Show image</label>
							<a href="/delete/upc-image/<?=$upc['upc']?>__jpg/" class="delete x" title="Delete this image">&times;</a>
						<? } ?>
						</td>
						<td>
							<a href="https://platform.syndigo.com/marketplace/all-products" target="_blank" class="button">Syndigo</a>
							<a href="//www.google.com/search?q=<?=urlencode(strtolower($upc['pos_name']) . ' official website')?>" target="_blank" class="button">Google</a>
							<a href="https://nugget.n2r.io/maintenance/items?query=<?=$upc['upc']?>" target="_blank" class="button">N2</a>
							<? if ($item['is_perishable']) { ?>
								<a href="https://menu.nuggetmarket.com/intranet.v2/ad/ad2.aspx" target="_blank" class="button">V2</a>
							<? } else { ?>
								<a href="https://menu.nuggetmarket.com/intranet.v2/ad/ad1.aspx" target="_blank" class="button">V2</a>
							<? } ?>
						</td>
					</tr>
					
				<? } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							Change all SKU values to:<br />
							<input type="text" name="override_retail" value="" size="6" rel="retails" class="currency symbol multiedit" />
							<input type="text" name="override_size" value="" size="5" rel="sizes" class="multiedit" />
							<input type="text" name="override_unit" value="" size="2" rel="units" class="multiedit" />
							<select name="override_container" rel="containers" class="multiedit">
								<option></option>
								<option value="package">package</option>
								<option disabled="disabled">- - - - - </option>
								<option value="bag">bag</option>
								<option value="basket">basket</option>
								<option value="bottle">bottle</option>
								<option value="bottles">bottles</option>
								<option value="bowl">bowl</option>
								<option value="box">box</option>
								<option value="brick">brick</option>
								<option value="bucket">bucket</option>
								<option value="bunch">bunch</option>
								<option value="can">can</option>
								<option value="cans">cans</option>
								<option value="canister">canister</option>
								<option value="carton">carton</option>
								<option value="case">case</option>
								<option value="container">container</option>
								<option value="cup">cup</option>
								<option value="cups">cups</option>
								<option value="cut">cut</option>
								<option value="honey bear">honey bear</option>
								<option value="jar">jar</option>
								<option value="jars">jars</option>
								<option value="jug">jug</option>
								<option value="loaf">loaf</option>
								<option value="pack">pack</option>
								<option value="portion">portion</option>
								<option value="pouch">pouch</option>
								<option value="pouches">pouches</option>
								<option value="roll">roll</option>
								<option value="rolls">rolls</option>
								<option value="stick">stick</option>
								<option value="tin">tin</option>
								<option value="tub">tub</option>
								<option value="tubs">tubs</option>
								<option value="tube">tube</option>
								<option value="tubes">tubes</option>
								<option value="wheel">wheel</option>
							</select>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</fieldset>

	<fieldset>
		<legend>Limit and CRV</legend>
		<p>Set the any limits or CRV for all items in this family group.</p>
		<ul>
			<li class="inline">
				<label for="limit">Limit:</label>
				<input type="text" name="limit" id="limit" value="<?=unescape($item['limit'])?>" size="6" maxlength="2" />
				<label class="check">
					<input type="checkbox" name="supplies_last" value="1" <?if($item['supplies_last']==1){echo 'checked="checked"';}?> /> &ldquo;While supplies last&rdquo;
				</label>
				<label class="check">
					<input type="checkbox" name="crv" value="1" <?if($item['crv']==1){echo 'checked="checked"';}?> /> +CRV
				</label>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Labels <a href="#badges" id="display-badges" class="toggle button">Toggle labels</a></legend>
		<p>Select the default labels to display on this item.</p>
		<ul id="badges" class="hide checker">
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'brand') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
					
					<label>
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
					</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'organization') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
						
						<label>
							<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['name']?>" />
							<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
						</label>
					<? } ?>
					
				<? } } ?>
			</li>
			<li class="inlinegrid">
				<? foreach ($labels['listing'] as $label) { if ($label['type'] == 'seasoning') { ?>
					
					<? if (@in_array($label['id'],$item['selected_labels'])) { ?>
					<label class="selected">
						<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
						<input type="checkbox" name="labels[]" value="<?=$label['id']?>" checked="checked" />
					</label>
					
					<? } else { ?>
						
						<label>
							<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['name']?>" />
							<input type="checkbox" name="labels[]" value="<?=$label['id']?>" />
						</label>
					<? } ?>
					
				<? } } ?>
			</li>
		</ul>
	</fieldset>

	<fieldset>
		<legend>Feature Images</legend>
		<p>Attach images to use for item and set a default.</p>
		<p><a href="/media-select.php?category=products&amp;id=<?=$item['family_group']?>" class="button" id="browse-media" data-category="products" data-id="<?=$item['family_group']?>" data-action="/ad-item-config/<?=$_GET['family']?>/">Browse media</a> <label class="clear-default"><input type="radio" name="featured_image" value="">Clear default</label></p>
		<ul class="grid" id="attachments">
			<? if (isset($item['media_items'])) { foreach ($item['media_items'] as $media) {
				
				$path = pathinfo($media['file']);
				$media['file_name'] = $path['basename'];
				
			?>
				<li>
					<figure>
					<? if (strstr($media['mime_type'],'image')) { ?>
						<label for="media-item-<?=$media['id']?>"><img src="/media/<?=$media['small_image']?>" alt="<?=$media['alt_text']?>" /></label>
					<? } else if (strstr($media['mime_type'],'audio')) { ?>
						<audio controls="controls">
							<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
						</audio>
					<? } else if (strstr($media['mime_type'],'video')) { ?>
						<video controls="controls">
							<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
						</video>
					<? } else { ?>
						<label for="media-item-<?=$media['id']?>"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></label>
					<? } ?>
						<figcaption>
						<? if ($media['name']) { ?>
							<h3><?=$media['name']?></h3>
							<code><?=$media['file_name']?></code>
						<? } ?>
							<label><input type="checkbox" name="media[]" value="<?=$media['id']?>" id="media-item-<?=$media['id']?>" checked="checked" /> Attach</label>
						<? if (strstr($media['mime_type'],'image')) { ?>
							<label><input type="radio" name="featured_image" value="<?=$media['id']?>" <? if ($item['feature_image'] == $media['id']) { echo 'checked="checked"'; } ?> /> Make default</label>
						<? } ?>
						<a href="/media/edit/<?=$media['id']?>" class="button small">Edit</a>
						</figcaption>
					</figure>
				</li>
			<? } } else { ?>
				<li class="no-attachments">Media will be displayed here.</li>
			<? } ?>
			</ul>
	</fieldset>

	<ul class="actions">
		<li>
			<input type="submit" name="save" value="Save changes" />
			<input type="hidden" name="ad_item_group_id" value="<?=$item['family_group']?>" />
		</li>
		<li><a href="/">Cancel</a></li>
	</ul>
</form>

<? include('inc/footer.inc.php'); ?>
