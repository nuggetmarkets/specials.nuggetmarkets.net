<?
    require_once('../engine/Index.php');
    include('inc/header.inc.php');
    include('inc/messages.inc.php');
?>

<section class="dashboard">
	<header>
		<h1>Dashboard</h1>
	</header>
	<p>Hi <?=$_SESSION['first_name']?>,</p>
	<? if ($_SESSION['last_login']) { ?>
		<p>You last logged in on <strong><?=date('l, F j, Y \a\t g:i a',strtotime($_SESSION['last_login']))?></strong>. The email address you log in with is <strong><?=$_SESSION['email']?></strong>. You can change your email address or password at <a href="https://profiles.nuggetmarkets.net/profiles/edit/<?=$_SESSION['user_id']?>/" target="_blank">profiles.nuggetmarkets.net</a>.
	<? } else { ?>
		<p>Hey, it&rsquo;s your first time loggin&nbsp;in!</p>
		<p>Use the navigation menu at the top of the screen to manage ads, products, images, to log out, or return to this dashboard. You may also change your display name, password or email address at <a href="https://profiles.nuggetmarkets.net/profiles/edit/<?=$_SESSION['user_id']?>/" target="_blank">profiles.nuggetmarkets.net</a>.
	<? } ?>
	</p>
</section>


<section class="messages">
	<header>
		<h1>Notifications</h1>
		<p>Corrections requested for current and upcoming ads and their responses. Notification messages will be dismissed once an ad date has run, or you can dismiss approved item messages yourself!</p>
		<p><a href="/dismiss/" class="button">Dismiss all approved</a></p>
	</header>
	<? if ($messages) { ?>
	<ol class="listing">
	<? foreach ($messages as $thread) { ?>
		<li class="thread status-<?=$thread['status']?>">
			<ol class="listing">
			<? foreach ($thread['messages'] as $message) { ?>
				<li id="message-<?=$message['id']?>" class="status-<?=$message['status']?>">
					<div>
						<strong><?=$message['posted_by']?></strong>
						<time datetime="<?=date('c', strtotime($message['date_posted']))?>"><?=date('F j, Y g:i a', strtotime($message['date_posted']))?></time>
						<div class="ad-name"><?=date('n/j/Y', strtotime($message['date_from']))?> <?=$message['store_chain']?> / <?=$message['department']?></div>
						
						<? if ($_SESSION['user_level'] == 1 && in_array('ads', $_SESSION['access'])) { ?>
							
							<? if ($message['item_name']) { ?>
								<a href="/ad-builder/<?=$message['ad_id']?>/#item-<?=$message['item_id']?>"><?=$message['item_name']?></a>:
							<? } else if ($message['group_name']) { ?>
								<a href="/ad-builder/<?=$message['ad_id']?>/#item-<?=$message['item_id']?>"><?=$message['group_name']?></a>:
							<? } else { ?>
								<a href="/ad-builder/<?=$message['ad_id']?>/#item-<?=$message['item_id']?>"><?=$message['family_group']?></a>:
							<? } ?>
							
						<? } else { ?>
							
							<? if ($message['item_name']) { ?>
								<a href="/proofs/<?=$message['date_from']?>/<?=$message['department']?>/#item-<?=$message['item_id']?>"><?=$message['item_name']?></a>:
							<? } else if ($message['group_name']) { ?>
								<a href="/proofs/<?=$message['date_from']?>/<?=$message['department']?>/#item-<?=$message['item_id']?>"><?=$message['group_name']?></a>:
							<? } else { ?>
								<a href="/proofs/<?=$message['date_from']?>/<?=$message['department']?>/#item-<?=$message['item_id']?>"><?=$message['family_group']?></a>:
							<? } ?>
							
						<? } ?>
						
						<?=$message['message']?> <span class="status <?=$message['status']?>"><?=$message['status']?></span>
					</div>
					<div>
						<? if ($message['status'] == 'approved') { ?>
						<a href="#message-<?=$message['id']?>" class="dismiss button small" id="dismiss-<?=$message['id']?>">Dismiss</a>
					<? } ?>
					</div>
				</li>
			<? } ?>
			<? if ($_SESSION['user_level'] == 1 && in_array('ads', $_SESSION['access'])) { ?>
				<li class="reply">
					<form action="/" method="post">
						<a href="#form-<?=$thread['thread']?>" class="toggle button">Reply</a>
						<div id="form-<?=$thread['thread']?>" class="hide">
							<ul>
								<li>
									<label for="message">Message:</label>
									<textarea name="message" cols="16" rows="2"></textarea>
								</li>
							</ul>
							<ul class="radioer">
								<li>
									<label for="status">Change status:</label>
									
									<? if ($thread['status'] == 'issue') { ?>
									<label class="selected issue"><input type="radio" name="status" value="issue" checked="checked" /> Issue</label>
									<? } else { ?>
									<label><input type="radio" name="status" value="issue" /> Issue</label>
									<? } ?>
									
									<? if ($thread['status'] == 'pending') { ?>
									<label class="selected pending"><input type="radio" name="status" value="pending" checked="checked" /> Pending</label>
									<? } else { ?>
									<label><input type="radio" name="status" value="pending" /> Pending</label>
									<? } ?>
									
									<? if ($thread['status'] == 'approved') { ?>
									<label class="selected approved"><input type="radio" name="status" value="approved" checked="checked" /> Approved</label>
									<? } else { ?>
									<label><input type="radio" name="status" value="approved" /> Approved</label>
									<? } ?>
									
								</li>
							</ul>
							<ul class="actions">
								<li>
									<input type="hidden" name="group_name" value="<?=$thread['messages'][key($thread['messages'])]['group_name']?>" />
									<input type="hidden" name="family_group" value="<?=$thread['messages'][key($thread['messages'])]['family_group']?>" />
									<input type="hidden" name="item_name" value="<?=$thread['messages'][key($thread['messages'])]['item_name']?>" />
									<input type="hidden" name="item_id" value="<?=$thread['messages'][key($thread['messages'])]['item_id']?>" />
									<input type="hidden" name="department" value="<?=$thread['messages'][key($thread['messages'])]['department']?>" />
									<input type="hidden" name="ad_date" value="<?=$message['date_from']?>" />
									<input type="hidden" name="ad_id" value="<?=$thread['messages'][key($thread['messages'])]['ad_id']?>" />
									<input type="hidden" name="message_reply_to" value="<?=$thread['reply_to']?>" />
									<input type="hidden" name="current_status" value="<?=$thread['status']?>" />
									<input type="submit" name="send_reply" value="Reply" />
								</li>
							</ul>
						</div>
					</form>
				</li>
			<? } ?>
			</ol>
		</li>

	<? } ?>
	</ol>
	<? } else { ?>
	<p>There are no notification messages for upcoming ads.</p>
	<? } ?>
</section>




<? include('inc/footer.inc.php'); ?>
