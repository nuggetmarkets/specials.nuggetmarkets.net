<? include('../engine/Email.php'); header("Content-Type: text/plain"); ?>
<? if ($ad['subscribed']) { ?>
Subscription Confirmed

Thank you for signing up to receive <?=$ad['store_attributes']['public_name']?> weekly specials. You will receive an email containing our secret special coupon when our weekly specials are announced every <? if (strstr($ad['store_attributes']['name'], 'Sonoma')) { ?>Tuesday<? } else { ?>Wednesday<? } ?>.
<? } else { ?>
<? if (is_array($ad['items'])) { foreach ($ad['items'] as $item) { ?><?=html_entity_decode($item['name'])?>, <? } ?>and more on special this week at <?=$ad['store_attributes']['public_name']?> starting <?=date('F j, Y',strtotime($ad['date_from'])); } ?>
<? } ?>

--------------------

<?=$ad['store_attributes']['public_name']?>

<? if ($ad['prepended_message']) { ?>
    
<?=strip_tags(str_replace(array("<br />", "<p>", "</p>"), array("\n", "\n", ""), html_entity_decode($ad['prepended_message'])))?>

<?

preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $ad['prepended_message'], $links);

if (isset($links)) {
    foreach ($links[0] as $link) {
        if (!strstr($link, '/media/')) {
            echo "[$link] \r\n";
        }
    }
}

?>

--------------------
<? } ?>
<? if (isset($ad['secret_special_title']) && isset($ad['secret_special_description']) && isset($ad['secret_special_regular_retail'])) { ?>

Secret Special <?=html_entity_decode($ad['secret_special_duration'])?>


<?=html_entity_decode($ad['secret_special_title'])?> <?=html_entity_decode($ad['secret_special_container'])?> <?=strip_tags($ad['secret_special_retail'])?>

<? if ($ad['secret_special_limit']) { ?>Limit <?=$ad['secret_special_limit']?> per guest.
<? } ?>
While supplies last!
<? if ($ad['secret_special_regular_retail']) { ?>Regularly: <?=$ad['secret_special_regular_retail']?>

<? } ?>
<?=strip_tags(str_replace(array("<br />", "<p>", "</p>"), array("\n", "\n", ""), html_entity_decode($ad['secret_special_description'])))?>

<? if ($ad['secret_special_disclaimers']) { echo $ad['secret_special_disclaimers']; } ?>

<? if ($ad['secret_special_limit']) { ?>Limit <?=$ad['secret_special_limit']?> per guest.<? } else { ?>Limit 4 per guest.<? } ?>

One redemption per guest, per promotional period.
Prices only valid <?=html_entity_decode($ad['secret_special_duration'])?>.
<? if ($ad['secret_special_skus']) { ?>
Only valid for <?=$ad['secret_special_skus']?>
<? } ?>
No further discounts apply.
Only while supplies last.

<? if ($ad['date_from'] >= '2023-02-08') { ?>
Present this coupon code at checkout: 905105000255
<? } else { ?>
Present this coupon code at checkout: 905105000057
<? } ?>

<? } else if (isset($ad['secret_special_title']) && $ad['secret_special_expired']) { ?>
--------------------

<?=$ad['store_attributes']['public_name']?>


This Week’s Secret Special Has Expired:

<?=$ad['secret_special_title']?> <?=$ad['secret_special_container']?> <?=$ad['secret_special_retail']?>


Last week’s deal has already expired, but don’t worry! Look for next week’s Secret Special coupon in your email later this week!
<? } ?>
<? if (is_array($ad['items'])) { ?>

--------------------

Hot Items This Week at <?=$ad['store_attributes']['public_name']?><? if (strstr($ad['store_attributes']['slug'], 'nugget-markets')) { ?> [PreferredStore,fallback=Davis, Covell]
Update your store preference: [preferences]<? } ?>

Prices effective <?=html_entity_decode($ad['ad_duration'])?>

<? foreach ($ad['items'] as $item) { if ($item['status'] == 'approved') { ?>

<? if ($item['prefix']) { ?><?=$item['prefix']?>
<? } ?><?=html_entity_decode($item['name'])?>
<? if ($item['suffix']) { ?>

<?=$item['suffix']?><? } ?>

<? if ($item['description']) { ?>
<?=strip_tags($item['description'])?><? } ?>

<? if ($item['selected_varieties']) { ?>

Selected Varieties<? } ?>
<? if ($item['note']) { ?><?=$item['note']?><? } ?>
<? if ($item['group_labels']) { ?>
<? foreach ($item['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id'] && $label['description']) { ?>

* <?=$label['description']?>
<? } if ($label['type'] == 'organization' && $key == $label['id'] && $label['description']) { ?>

* <?=$label['description']?>
<? } if ($label['type'] == 'seasoning' && $key == $label['id'] && $label['description']) { ?>

* <?=$label['description']?>
<? } } } } ?>

<? if ( !strstr($item['sale_price'],'lb.') && !strstr($item['sale_price'],'lbs.') && !strstr($item['sale_price'],'/ea.') && !strstr($item['sale_price'],'%') && ( $item['unit'] != 'CT' || $item['size'] > 1)) { ?><?=$item['size']?> <?=strtolower($item['unit'])?>. <?=$item['container']?>

<? } else if (isset($item['container_sizes'])) {$i = 1; foreach ($item['container_sizes'] as $key => $value) { if ($value) { ?><?=$value?> <?=strtolower($item['units'][$i])?>. <?=$item['containers'][$key]?><? $i++; } } } ?>
<?=strip_tags($item['sale_price'])?>

<? if ($item['savings']) { ?>Save <?=$item['savings']?><? } ?>

<? if ($item['limit']) { echo strip_tags($item['limit']); } ?>

<? } } ?>

View everything on special this week on our website:
[<?=$ad['store_attributes']['ad_url']?>]

<? if (isset($ad['print_file'])) { ?>
Download a PDF file of our specials this week:
[https://specials.nuggetmarkets.net/print-versions/<?=$ad['store_attributes']['slug']?>-specials-starting-<?=$ad['date_from']?>.pdf?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=pdf-version]

Our website contains many more items than we have room to publish in the PDF version of our weekly specials flyer!
<? } else { ?>
We didn’t publish a PDF version of our weekly specials flyer this week, but our website is printer-friendly, and contains many more items than we typically feature in the PDF version.
<? } ?>

<? } ?>
<? if (is_array($articles) || is_array($recipes)) { ?>
--------------------

Further Reading On Our Website

<? if (is_array($articles)) { foreach ($articles as $article) { ?>
<?=$article['article_title']?>
<?=strip_tags(str_replace(array("<br />", "<p>", "</p>"), array("\n", "\n", ""), html_entity_decode($article['article_excerpt'])))?>

Full article on our website:
[<?=$ad['store_attributes']['article_archive']?><?=$article['article_id']?>/<?=$article['article_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$article['article_slug']?>]

<? } } ?>
<? if (is_array($recipes)) { foreach ($recipes as $recipe) { ?>
<?=$recipe['recipe_name']?>
<?=strip_tags(str_replace(array("<br />", "<p>", "</p>"), array("\n", "\n", ""), html_entity_decode($recipe['recipe_description'])))?>

Full recipe on our website:
[<?=$ad['store_attributes']['recipe_archive']?><?=$recipe['recipe_id']?>/<?=$recipe['recipe_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$recipe['recipe_slug']?>]

<? } } } ?>
--------------------

<? if (strstr($ad['store_attributes']['slug'], 'nugget-markets')) { ?>Prices and selection on this email are for Nugget Markets [PreferredStore,fallback=Davis, Covell] only. <? } ?>All sale items are limited to <?=ITEM_LIMIT?> items per household through the sales effective dates unless otherwise noted. We reserve the right to limit individual purchases to four packages of any item for sale, except items where otherwise noted. Sale items are not available to any commercial dealer or wholesaler. Items are subject to stock on hand. All items not available at all stores. Photos and Illustrations are for display purposes only and do not necessarily depict items that are on sale. We reserve the right to correct all image and/or typographical errors.

This email was sent to [email]. You may unsubscribe by visiting this page:
[unsubscribe]

Or update your email preferences by visiting this page:
[preferences]

Prices Valid <?=html_entity_decode($ad['ad_duration'])?>

Pro-Tip: Add us to your address book to help ensure that you continue receiving our emails.

<?=$ad['store_attributes']['public_name']?>

Copyright © <?=date('Y')?> Nugget Market, Inc.
311 Mace Boulevard, Davis, CA. 95618
