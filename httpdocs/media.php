<?
    require_once('../engine/Media.php');
    include('inc/header.inc.php');
    include('inc/messages.inc.php');
?>

<form action="/media/search/" method="get" accept-charset="utf-8" class="search">
	<input type="search" name="terms" value="<?=$search_terms?>" placeholder="Search media" autofocus />
	<input type="submit" value="Search" name="search" />
</form>

<section class="media">
	<header>
		<h1>Photos</h1>
		<? if ($_SESSION['user_level'] === 1) { ?>
		<nav>
			<a href="/media/new/" class="button">Add new</a>
		</nav>
		<? } ?>
	</header>
	<? if ($media_items['listing']) { ?>
	<ol class="grid">
	<? foreach ($media_items['listing'] as $media) { ?>
		<li>
			<figure>
			<? if (strstr($media['mime_type'],'image')) { ?>
				<a href="/media/<?=$action?>/<?=$media['id']?>/"><img src="/media/<?=$media['small_image']?>" alt="<?=$media['alt_text']?>" /></a>
			<? } else if (strstr($media['mime_type'],'audio')) { ?>
				<audio controls="controls">
					<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
				</audio>
			<? } else if (strstr($media['mime_type'],'video')) { ?>
				<video controls="controls">
					<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
				</video>
			<? } else { ?>
				<a href="/media/<?=$action?>/<?=$media['id']?>/"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></a>
			<? } ?>
				<figcaption>
				<? if ($media['name']) { ?>
					<h3><?=$media['name']?></h3>
				<? } ?>
					<a href="/media/<?=$action?>/<?=$media['id']?>/"><?=$media['file_name']?></a>
				</figcaption>
			</figure>
		</li>
	<? } ?>
	</ol>

	<footer>
		<? if ($media_items['pagination']) { ?>
		<nav class="pagination">
		<? if (!is_null($media_items['pagination']['first'])) { ?>
			<a href="/media/page/1/">First</a>
			<a href="/media/page/<?=$media_items['pagination']['previous']?>/">Prev</a>
		<? } else { ?>
			<span class="deact">First</span>
			<span class="deact">Prev</span>
		<? } ?>
		<span class="total">Page <?=$media_items['pagination']['current']?> of <?=$media_items['pagination']['total']?></span>
		<? if (!is_null($media_items['pagination']['last'])) { ?>
			<a href="/media/page/<?=$media_items['pagination']['next']?>/">Next</a>
			<a href="/media/page/<?=$media_items['pagination']['last']?>/">Last</a>
		<? } else { ?>
			<span class="deact">Next</span>
			<span class="deact">Last</span>
		<? } ?>
		</nav>
		<? } ?>
	</footer>

	<? } else if ($media) { ?>

		<? if ($page_attrs['action'] == 'edit' || $page_attrs['action'] == 'new') { ?>

		<form action="/media/" method="post" enctype="multipart/form-data">
			<fieldset>
				<legend>File</legend>
				<ul>
				<? if ($page_attrs['action'] == 'edit') { ?>
					<li>
						<figure>
						<? if (strstr($media['mime_type'],'image')) { ?>
							<a href="/media/<?=$media['file']?>">
								<picture>
									<source srcset="/media/<?=$media['small_image']?>" media="(max-width: 320px)" />
									<source srcset="/media/<?=$media['medium_image']?>" media="(max-width: 375px)" />
									<source srcset="/media/<?=$media['large_image']?>" media="(max-width: 600px)" />
									<source srcset="/media/<?=$media['file']?>" />
									<img src="/media/<?=$media['medium_image']?>" alt="<?=$media['alt_text']?>" />
								</picture>
							</a>
							<figcaption>
								<? if (isset($media['name'])) { ?>
								<h2><?=($media['name'])?></h2>
								<? } ?>
								<legend>Sizes:</legend>
                                <ul class="version-sizes">
									<li>
										<div class="icon">
											<a href="/media/<?=$media['file']?>">
												<img src="/media/<?=$media['file']?>?inc=<?=time()?>" alt="<?=unescape($media['alt_text'])?> original size" />
											</a>
										</div>
										<div>
											Original: <a href="/media/<?=$media['file']?>"><?=$media['pathinfo']['original']['basename']?></a>
											<p>
												<?=$media['dimensions']['original'][0]?>px &times; <?=$media['dimensions']['original'][1]?>px, 
												<?=$media['filesize']['original']?>,  
												<small>Updated: <?=$media['modified']['original']?></small>
											</p>
											<p>Replace this size: <input type="file" name="new_image_original" /></p>
										</div>
									</li>
                                    <li>
										<div class="icon">
											<a href="/media/<?=$media['large_image']?>">
												<img src="/media/<?=$media['large_image']?>" alt="<?=unescape($media['alt_text'])?> large size" />
											</a>
										</div>
										<div>
											Large: <a href="/media/<?=$media['large_image']?>"><?=$media['pathinfo']['large']['basename']?></a>
											<p>
												<?=$media['dimensions']['large'][0]?>px &times; <?=$media['dimensions']['large'][1]?>px, 
												<?=$media['filesize']['large']?>,  
												<small>Updated: <?=$media['modified']['large']?></small>
											</p>
											<p>Replace this size: <input type="file" name="new_image_large" /></p>
										</div>
                                    </li>
    								<li>
										<div class="icon">
											<a href="/media/<?=$media['medium_image']?>">
												<img src="/media/<?=$media['medium_image']?>" alt="<?=unescape($media['alt_text'])?> medium size" />
											</a>
										</div>
										<div>
											Medium: <a href="/media/<?=$media['medium_image']?>"><?=$media['pathinfo']['medium']['basename']?></a>
											<p>
												<?=$media['dimensions']['medium'][0]?>px &times; <?=$media['dimensions']['medium'][1]?>px, 
												<?=$media['filesize']['medium']?>,  
												<small>Updated: <?=$media['modified']['medium']?></small>
											</p>
											<p>Replace this size: <input type="file" name="new_image_medium" /></p>
										</div>
                                    </li>
    								<li>
										<div class="icon">
											<a href="/media/<?=$media['small_image']?>">
												<img src="/media/<?=$media['small_image']?>" alt="<?=unescape($media['alt_text'])?> small size" />
											</a>
										</div>
										<div>
											Small: <a href="/media/<?=$media['small_image']?>"><?=$media['pathinfo']['small']['basename']?></a>
											<p>
												<?=$media['dimensions']['small'][0]?>px &times; <?=$media['dimensions']['small'][1]?>px, 
												<?=$media['filesize']['small']?>,  
												<small>Updated: <?=$media['modified']['small']?></small>
											</p>
											<p>Replace this size: <input type="file" name="new_image_small" /></p>
										</div>
                                    </li>
                                </ul>
							</figcaption>
						<? } else if (strstr($media['mime_type'],'audio')) { ?>
							<audio controls="controls">
								<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>"/>
								
							</audio>
							<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
						<? } else if (strstr($media['mime_type'],'video')) { ?>
							<video controls="controls">
								<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>"/>
								
							</video>
							<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
						<? } else { ?>
							<figcaption>
								<a href="/media/<?=$media['file']?>"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></a>
								<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
							</figcaption>
						<? } ?>
						</figure>
					</li>
					<li>
						<label for="media-id">Media ID:</label>
						<input type="text" readonly="readonly" value="<?=$media['id']?>" class="media-id" class="media-id" />
					</li>
					<li>
						<label for="new_file">Replace File:</label>
						<input type="file" name="new_file" id="new_file" data="new_file_name" />
						<div class="note">Old version will be overwritten. Feature images for the ads should be 1920 &times; 1280.</div>
					</li>
				<? } else { ?>
					<li>
						<label for="file">File:</label>
						<input type="file" name="file" id="file" data="file_name" class="empty" required />
						<div class="note required">Required. Feature images for the ads should be 1920 &times; 1280.</div>
					</li>
				<? } ?>
				</ul>
			</fieldset>
			
			<fieldset>
				<legend>Description</legend>
				<ul>
					<li>
						<label for="alt_text">Alt Text:</label>
						<input type="text" name="alt_text" id="alt_text" value="<?=unescape($media['alt_text'])?>" <?if(!$media['alt_text']){echo'class="empty"';}?> required />
						<div class="note required">Required - Describe the subject of this image for the visually&nbsp;impaird.</div>
					</li>
					<li>
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" value="<?=(isset($media['name'])?unescape($media['name']):'')?>" />
						<div class="note">Used to label the file on the back end. This field is not public-facing.</div>
					</li>
					<li>
						<label for="caption">Caption:</label>
						<textarea name="caption" id="caption" class="tinymce"><?=unescape($media['caption'])?></textarea>
					</li>
					<li>
						<label for="catalog_text">Meta Data:</label>
						<input type="text" name="catalog_text" id="catalog_text" value="<?=unescape($media['catalog_text'])?>" <?if(!$media['catalog_text']){echo'class="empty"';}?> />
						<div class="note">Use this field for any internal catalog codes or search terms. This field is not public-facing.</div>
					</li>
					<? if (strstr($media['mime_type'],'image')) { ?>
					<li>
						<label for="embed_code">Embed Code:</label>
						<textarea name="embedcode" class="code" rows="9" readonly><figure>
	<picture>
		<source srcset="https://specials.nuggetmarkets.net/media/<?=$media['large_image']?>" media="(min-width: 960px)" />
		<source srcset="https://specials.nuggetmarkets.net/media/<?=$media['medium_image']?>" media="(min-width: 600px)" />
		<source srcset="https://specials.nuggetmarkets.net/media/<?=$media['small_image']?>" media="(min-width: 300px)" />
		<source srcset="https://specials.nuggetmarkets.net/media/<?=$media['file']?>" />
		<img src="https://specials.nuggetmarkets.net/media/<?=$media['file']?>" alt="<?=$media['alt_text']?>" />
	</picture>
<? if (isset($media['caption'])) { ?>
	<figcaption><?=$media['caption']?></figcaption>
<? } ?>
</figure></textarea>
					</li>
					<? } ?>
				</ul>
			</fieldset>
			<ul class="actions">
				<li>
					<? if ($_GET['action'] == 'edit') { ?>
					<input type="hidden" name="item_id" value="<?=$_GET['id']?>" />
					<input type="submit" name="save" value="Save changes" />
					<? } else { ?>
					<input type="submit" name="add" value="Add item" />
					<? } ?>
				</li>
				<li>
					<a href="/media/">Cancel</a>
				</li>
				<? if ($_GET['action']=='edit') { ?>
				<li><a href="/delete/media/<?=$_GET['id']?>/" class="delete">Delete</a></li>
				<? } ?>
			</ul>
		</form>

		<? } else if ($page_attrs['action'] == 'view') { ?>
		<fieldset>
			<figure>
				<? if (strstr($media['mime_type'],'image')) { ?>
					<a href="/media/<?=$media['file']?>">
						<picture>
							<source srcset="/media/<?=$media['small_image']?>" media="(max-width: 320px)" />
							<source srcset="/media/<?=$media['medium_image']?>" media="(max-width: 375px)" />
							<source srcset="/media/<?=$media['large_image']?>" media="(max-width: 600px)" />
							<source srcset="/media/<?=$media['file']?>" />
							<img src="/media/<?=$media['medium_image']?>" alt="<?=$media['alt_text']?>" />
						</picture>
					</a>
					<figcaption>
						<legend>Sizes:</legend>
						<ul>
							<li>
								Original: <a href="/media/<?=$media['file']?>"><?=$media['dimensions']['original'][0]?>&times;<?=$media['dimensions']['original'][1]?></a>
								(<?=$media['filesize']['original']?>)
							</li>
							<li>
								Large: <a href="/media/<?=$media['large_image']?>"><?=$media['dimensions']['large'][0]?>&times;<?=$media['dimensions']['large'][1]?></a>
								(<?=$media['filesize']['large']?>)
							</li>
							<li>
								Medium: <a href="/media/<?=$media['medium_image']?>"><?=$media['dimensions']['medium'][0]?>&times;<?=$media['dimensions']['medium'][1]?></a>
								(<?=$media['filesize']['medium']?>)
							</li>
							<li>
								Small: <a href="/media/<?=$media['small_image']?>"><?=$media['dimensions']['small'][0]?>&times;<?=$media['dimensions']['small'][1]?></a>
								(<?=$media['filesize']['small']?>)
							</li>
						</ul>
					</figcaption>
				<? } else if (strstr($media['mime_type'],'audio')) { ?>
					<audio controls="controls">
						<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>"/>
					</audio>
					<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
				<? } else if (strstr($media['mime_type'],'video')) { ?>
					<video controls="controls">
						<source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>"/>
					</video>
					<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
				<? } else { ?>
					<figcaption>
						<a href="/media/<?=$media['file']?>"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></a>
						<pre><a href="/media/<?=$media['file']?>"><?=$media['file_name']?></a></pre>
					</figcaption>
				<? } ?>
			</figure>

		</fieldset>

		<fieldset>
			<legend>Info.</legend>
			<ul>
				<li>
					<h2><?=$media['name']?></h2>
					<? if ($media['caption']) { echo $media['caption']; } ?>
				</li>
				<li>
					<label for="uri">Link to this resource:</label>
					<input type="text" value="<?=SPECIALS_URL?>/media/view/<?=$media['id']?>/" readonly class="auto-select" />
					<div class="note">Click to select</div>
				</li>
			</ul>
		</fieldset>
		<? } ?>

	<? } else { ?>

		<p>No items found.</p>

	<? } ?>
</section>

<? include('inc/footer.inc.php'); ?>
