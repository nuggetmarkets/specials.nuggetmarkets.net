$(document).ready(function(){
	
	// Set up sticky menu
	$("#menu").sticky({topSpacing:0});
	
	// Handle toggles
	$('.toggle').click(function(e){
		
		e.preventDefault();
		
		// Get the value of the clicked item
		var item = $(this).attr('href');
		
		// Toggle it
		$(item).slideToggle('fast');
		$(item).toggleClass('hide');
		$(this).toggleClass('toggled');
		
	});

});
