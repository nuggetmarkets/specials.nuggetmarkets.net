tinymce.init({ 
	branding: false,
	selector:'.tinymce', 
	content_css : '/css/application.css',
	menubar: false, 
	plugins: 'lists, code, paste, link, image, wordcount', 
	toolbar: 'styleselect | bold italic | bullist numlist | image link code', 
	convert_urls: false, 
	style_formats: [
		{title: 'Paragraph', block: 'p'}, 
		{title: 'Heading 1', block: 'h1'}, 
		{title: 'Heading 2', block: 'h2'}, 
		{title: 'Heading 3', block: 'h3'}, 
		{title: 'Heading 4', block: 'h4'},
		{title: 'Heading 5', block: 'h5'}, 
		{title: 'Blockquote', block: 'blockquote'}, 
		{title: 'Preformatted', block: 'pre'}, 
		{title: 'Superscript', inline: 'sup'} 
	], 
	valid_elements: 'h2,h3,h4,h5,h6,p[class],ul,ol,li,div[class],article,a[href|target|class],strong/b,em/i,br,blockquote,pre,sup,figure,figcaption,img[src|alt|class],small', 
	paste_word_valid_elements: 'h3,h4,h5,h6,p,strong/b,em/i'
});
