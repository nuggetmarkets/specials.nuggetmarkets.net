$(document).ready(function(){

	// Capture page class
	var page = $('body').attr('class');

	// Error and confirmation messages
	$('#message').click(function(){
		$(this).fadeOut();
	});

	// Close item
	$('a.close').click(function(e){
		e.preventDefault();
		
		var item = $(this).attr('href');
		
		$(item).hide();
	});

	// Toggle main navigation
	$('#close-menu').click(function(e){
	
		e.preventDefault();
		
		$('#main-nav').animate({
			left: "-240px"
		}, 100);

	});

	$('#show-menu').click(function(e){
		
		e.preventDefault();
		
		$('#main-nav').animate({
			left: "0"
		}, 200);
		
	});
	
	// Handle upc image dropper
	$('.image-drop').each(function(){
		var $input	 = $(this),
			$label	 = $input.next('label'),
			upcImages = $('.main-nav .upc-images'),
			labelVal = $label.html();
			
		$input.on('change', function(e){
			var fileName = '';
			
			if (this.files && this.files.length > 0) {
				fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
			} else if (e.target.value) {
				fileName = e.target.value.split('\\').pop();
			}
			
			if (fileName) {
				$label.find('span').html(fileName);
				upcImages.addClass('loaded');
			} else {
				$label.html(labelVal);
			}
		});
		
		// Firefox bug fix
		$input
		.on('focus', function(){ $input.addClass('has-focus'); })
		.on('blur', function(){ $input.removeClass('has-focus'); });
		
	});

	$('.delete.confirm').click(function(e){
		
		if (!confirm("Are you sure you want to delete this?")) {
			return false;
		}
		
	});
	
	// Play import sound
	$("a.import").click(function(e){
		
		e.preventDefault();
		
		var uri = $(this).attr('href');
		var importSound = new Audio("/audio/quoiik.mp3");
		importSound.play();
		
		setTimeout(function(){
			window.location = uri;
		}, 1000);
		
	});
	
	// Play export sound
	$("a.export").click(function(e){
		
		e.preventDefault();
		
		var uri = $(this).attr('href');
		var exportSound = new Audio("/audio/thx.mp3");
		exportSound.play();
		
		setTimeout(function(){
			window.location = uri;
		}, 1000);
		
	});
	
	// Make required fields red when empty
	$('input[required]').change(function(){
		var value = $(this).val();
		if (value.length < 1) {
			$(this).addClass('empty');
		} else {
			$(this).removeClass('empty');
		}
	});

	// Auto calculate ad through date
	$('#from_month, #from_day, #from_year').change(function(){
		
		var d = new Date($('#from_year').val() +'-'+ $('#from_month').val() +'-'+ $('#from_day').val());
		d.setDate(d.getDate()+7);
		
		var day = ('0'+d.getDate()).slice(-2);
		var month = ('0'+(d.getMonth()+1)).slice(-2);
		var year = d.getFullYear();
		
		$('#through_month').val(month);
		$('#through_day').val(day);
		$('#through_year').val(year);
		
	});

	// Handle toggles
	$('.toggle').click(function(e){
		
		e.preventDefault();
		
		// Get the value of the clicked item
		var item = $(this).attr('href');
		
		// Toggle it
		$(item).slideToggle('fast');
		$(item).toggleClass('hide');
		$(this).toggleClass('toggled');
		
	});

	$('.appender').on("click", ".button", function(e) {
        e.preventDefault();
        var itemID = $(this).attr('href');
        $(itemID).clone().appendTo(itemID + 's').find('input').val("");
    });

    $('.appender').on("click", ".remove", function(e) {
        e.preventDefault();
		
		$(this).parent().fadeOut('fast', function(){
			$(this).remove();
		})
		
    });

	// Remove form elements
	$('form').on("click", ".remove", function(e){
		
		e.preventDefault();
		
		var deleteItem = $(this).attr('data-delete');
		
		$('#' + deleteItem).fadeOut('fast', function(){
			$('#' + deleteItem).remove();
		});
		
	});

	// Handle autoselect
	$('.auto-select').click(function(e){
		$(this).select();
	});

	// Set ad builder link
	$('#from_month, #from_day, #from_year').change(function(){
		
		var d = $('#from_year').val() +'-'+ $('#from_month').val() +'-'+ $('#from_day').val();
		$('#ad-builder-link').attr('href','https://www.nuggetmarket.com/cms/ad-preview.php?date=' + d);
		
	});

	// Highlight selected checkboxes
	$('.checker input, .checker label').click(function(){
		
		// Capture element clicked
		var element = $(this);
		
		// If input was clicked, set parent class
		if (element.is('input')) {
			
			$(element).parent('label').toggleClass('selected');
			
		// If label was clicked, set label class
		} else if (element.is('label')) {
			
			$(element).toggleClass('selected');
			
		}
		
	});

	// Highlight selected radio buttons
	$('.radioer input').click(function(){
		
		// Capture element clicked and parent container
		var element = $(this);
		
		// Removed select status of item label
		$(element).parents('li').find('label').removeClass('selected');
		
		// Add select status to new item label
		$(element).parent('label').addClass('selected');
		
	});
	
	// Enable/disable feature style
	$("#feature").on("change", function() {
		
		// Capture feature position and radio buttons
		var featureVal = $(this).val();
		var overlay = $("input[rel='overlay']");
		var sidebyside = $("input[rel='side-by-side']");
		
		if (featureVal.length >= 1) {
			$(".feature-position .mask").removeClass('masked');
		} else {
			$(".feature-position .mask").addClass('masked');
			$(".feature-position > li").removeClass('selected');
			$(".feature-position input[type='radio']").prop('checked', false);
		}
		
		// Select overlay style for position 1
		if (featureVal === '1') {
			$(overlay).prop('checked','checked');
			$(".feature-position > li").removeClass('selected');
			$(overlay).parent().parent().addClass('selected');
			
			// Select first overlay panel
			$(overlay).parent().parent().find('.caption-position li:first-child label').addClass('selected');
			$(overlay).parent().parent().find('.caption-position input[type=radio]')[0].checked = true;
			
		// Select side-by-side style for all other positions
		} else if (featureVal > '1') {
			$(sidebyside).prop('checked','checked');
			$(".feature-position > li").removeClass('selected');
			$(sidebyside).parent().parent().addClass('selected');
		}
		
	});
	
	// Highlight selected feature items
	$('.feature-position > li > label').click(function(){
		
		// Capture element clicked and parent container
		var element = $(this);
		
		// Reset select statuses and radio buttons
		$(element).parents('ul').find('li').removeClass('selected');
		$(element).parents('ul').find('.caption-position label').removeClass('selected');
		$(element).parents('ul').find('.caption-position input[type=radio]').attr('checked',false);
		
		// Update select statuses and radio buttons
		$(element).parent('li').addClass('selected');
		$(element).parent('li').find('.caption-position li:first-child label').addClass('selected');
		$(element).parent('li').find('.caption-position input[type=radio]')[0].checked = true;
		
	});
	
	// Select overlay style if caption positions are selected
	$(".caption-position input[name='feature_caption_position']").click(function(){
		var overlay = $("input[rel='overlay']");
		$(".feature-position > li").removeClass('selected');
		$(overlay).prop('checked','checked');
	});

	// Handle multiedit form fields
	$(".multiedit").change(function(){
		
		var item = '.' + $(this).attr('rel');
		var new_val = $(this).val();
		
		$(item).val(new_val);
		
	});

	$(".multicheck").change(function(){
		
		var status = $(this).prop('checked');
		var item = '.' + $(this).attr('rel');
		
		if (status == false) {
			
			$(item).removeAttr('checked');
			
		} else {
			
			$(item).prop('checked','checked');
			
		}
		
	});
	
	
	// Publish ad AJAX
	$('.zone .status').click(function(e){
		
		e.preventDefault();
		var id 		= $(this).attr('data-id');
		var status 	= $(this).attr('data-status');
		
		$.ajax({
			url:	'/ads.php?action=publish&status=' + status + '&id=' + id,
			type:	'GET'
		})
			
		if (status == 1) {
			
			$(this).removeClass("active");
			$(this).addClass("not-active");
			$(this).text("○");
			
		} else {
			
			$(this).removeClass("not-active");
			$(this).addClass("active");
			$(this).text("●");
			
		}		
		
	});
	

	// Handle file upload widgets
	$("input[type='file']").change(function(){
		
		var file_name = $(this).val().replace("C:\\fakepath\\", "");
		var item = '#' + $(this).attr('data');
		
		$(item).text(file_name);
		
	});

	// Throw delete print ad confirmation
	$('#delete-print-ad').click(function(){
		confirm('Are you sure you want to delete the print verison of this ad?');
	});

	// Append ad articles
	$('#zones').on("click", "button", function(e){
		
		e.preventDefault();
		
		var parentID = $(this).attr('data-id');
		var parentClass = $(this).attr('data-class');
		var newID = $('#'+parentClass+'s-'+parentID+' li').length;
		
		$(`#${parentClass}s-${parentID} ul`).append(`
			<li id="${parentClass}-${parentID}-${newID}">
				<label for="${parentClass}[${parentID}][${newID}]">${parentClass} ID:</label>
				<input type="text" name="${parentClass}[${parentID}][${newID}]" value="" id="${parentClass}[${parentID}][${newID}]" size="3" class="short" />
				<span class="note">the number in the URL</span>
				<a href="#" data-delete="${parentClass}-${parentID}-${newID}" class="delete x remove">&times;</a>
			</li>
		`);
		
	});


	// Trigger media browser
	$('#browse-media').click(function(e){
		
		// Override click
		e.preventDefault();
		
		// Capture item category and id
		var category = $(this).attr('data-category');
		var id = $(this).attr('data-id');
		var ad_id = $(this).attr('data-ad_id');
		var action = $(this).attr('data-action');
		
		// Append media browser iframe
		$('.'+category).append('<iframe src="/media-select.php?category=' + category + '&amp;id=' + id + '&amp;ad_id=' + ad_id + '" class="media-browser"></iframe><div class="backdrop">&nbsp;</div>');
		
	});

	/*

		Attempt to process status changes via ajax
		*/

	// Handle item approval AJAX submit
	$('.proofs form').on('submit', function(e){
		
		// Set button var
		var button = $(this).find(':input[type=submit]');
		
		// Disable button to prevent multiple submits
		$(button).prop('disabled', true);
		
		// Override click
		e.preventDefault();
		
		// Set POST URI and
		var uri = "/proofs.php";
		
		// Capture post data
		var values = $(this).serialize();
		
		// Convert querystring into array
		var return_array = values.split('&');
		var obj = {};
		for (var i = 0; i < return_array.length; i++) {
			var split = return_array[i].split('=');
			obj[split[0].trim()] = split[1].trim();
		}
		
		// Handle form
		$.ajax({
			url:	uri,
			type:	'POST',
			data:	values
		}).done(function(){
			
			// Change classes
			if (obj['form_action'] == 'submit_correction') {
				
				$("#item-" + obj['item_id']).attr('class', 'issue');
				
				// Append the message to the item
				$("#item-" + obj['item_id'] + ' .status.issue .messages').append('<li><div class="text">' + (decodeURIComponent(obj['message'])).replace(/\+/g, " ") + '</div><div class="meta">&mdash;' + (obj['user']).replace(/\+/g, " ") + '</div></li>');
				
			} else if (obj['form_action'] == 'approve' || obj['form_action'] == 'cancel_correction') {
				
				$("#item-" + obj['item_id']).attr('class', 'approved');
				
			}
			
			// Re-enable button
			$(button).prop('disabled', false);
			
		});
		
	});


	// Handle media attachment AJAX submit
	$('#attach_media').click(function(e){
		
		// Override click
		e.preventDefault();
		
		// Set POST URI and
		var uri = "/media-select.php";
		
		// Capture attachment IDs
		var attachments = $("input[type='checkbox']:checked").map(function () {
			return this.value;
		}).get();
		
		// Save attachments to database
		$.ajax({
			url:	uri,
            type:	'POST',
            data:	$("#attachments_form").serialize()
			
        });
		
        // Loop through attachments
        $.each(attachments, function(key, val){
			
	        // Get HTML attributes
	        var mime = $("#attachment-" + val).attr('data-mime');
	        var name = $("#attachment-" + val).find('h3').html();
	        var file = $("#attachment-" + val).find('code').html();
			
	        // Build HTML based on mime/type
	        if (mime.search('image') != -1) {
				
		        var img = $("#attachment-" + val).find('img').attr('src');
				var alt = $("#attachment-" + val).find('img').attr('alt');
				var obj = '<li><figure><label for="media-item-'+val+'"><img src="'+img+'" alt="'+alt+'"></label><figcaption><h3>'+name+'</h3><code>'+file+'</code><label><input type="checkbox" name="media[]" value="'+val+'" id="media-item-'+val+'" checked="checked"> Attach</label><label><input type="radio" name="featured_image" value="'+val+'"> Make default</label><a href="/media/edit/'+val+'" class="button small">Edit</a></figcaption></figure></li>';
				
			} else if (mime.search('audio') != -1) {
			
				var src = $("#attachment-" + val).find('source').attr('src');
				var obj = '<li><figure><label for="media-item-'+val+'"><audio controls="controls"><source src="/media/'+src+'" type="'+mime+'" /></audio></label><figcaption><h3>'+name+'</h3><label><input type="checkbox" name="media[]" value="'+val+'" id="media-item-'+val+'" checked="checked"> <code>'+file+'</code></label><a href="/media/edit/'+val+'" class="button small">Edit</a></figcaption></figure></li>';
				
			} else if (mime.search('video') != -1) {
				
				var src = $("#attachment-" + val).find('source').attr('src');
				var obj = '<li><figure><label for="media-item-'+val+'"><video controls="controls"><source src="/media/'+src+'" type="'+mime+'" /></video></label><figcaption><h3>'+name+'</h3><label><input type="checkbox" name="media[]" value="'+val+'" id="media-item-'+val+'" checked="checked"> <code>'+file+'</code></label><a href="/media/edit/'+val+'" class="button small">Edit</a></figcaption></figure></li>';
				
	        } else {
				
				var obj = '<li><figure><label for="media-item-'+val+'"><img src="/img/file-icon.svg" alt="Icon for file '+mime+'" /></label><figcaption><h3>'+name+'</h3><label><input type="checkbox" name="media[]" value="'+val+'" id="media-item-'+val+'" checked="checked"> <code>'+file+'</code></label><a href="/media/edit/'+val+'" class="button small">Edit</a></figcaption></figure></li>';
				
			}
			
	        // Append node to article screen
	        $("#attachments", parent.document).append(obj);
			
        });
		
        // Remove no-attachments message
        $('#attachments .no-attachments', parent.document).remove();
		
		// Close and remove browser and backdrop
		$('.media-browser, .backdrop', parent.document).fadeOut('fast', function(){
			$('.media-browser, .backdrop').remove();
		});
		
	});

	// Handle media browser close
	$('#close-media').click(function(e){
		
		// Override click
		e.preventDefault();
		
		// Removed browser and backdrop
		$('.media-browser, .backdrop', parent.document).fadeOut('fast', function(){
			$('.media-browser, .backdrop').remove();
		});
		
	});

	// Trigger media browser close
	$(document).keyup(function(e) {
		if (e.keyCode === 27) $('#close-media').click();
	});

	if (page == 'dashboard') {
		
		$('.dismiss').click(function(e){
			
			// Override click
			e.preventDefault();
			
			// Set POST URI and
			var uri = "/index.php";
			
			// Get message ID
			var message = $(this).attr('href');
			
			// Capture thread container
			var thread = $(message).parent().parent();
			
			// Get reply field if it exists
			var reply = $(thread).find('.reply').length;
			
			// Count messages in thread
			var count = $(thread).children().children().length;
			
			// Handle message dismiss
			$.ajax({
				url:	uri,
				type:	'POST',
				data:	{'message': message}
			});
			
			// Remove thread with reply button
			if (reply && count === 2) {
				
				// Fadout and remove thread container from DOM
				$(thread).fadeOut('slow', function(){
					$(this).remove();
				});
				
			// Remove thread without reply button
			} else if (count === 1) {
				
				// Fadout and remove thread container from DOM
				$(thread).fadeOut('slow', function(){
					$(this).remove();
				});
				
			// Remove message
			} else {
				
				// Fadout and remove message from DOM
				$(message).fadeOut('slow', function(){
					$(this).remove();
				});
				
			}
			
		});
		
	}

	if (~page.indexOf('ads') || ~page.indexOf('secret-specials')) {
		
		$("#secret_special_limit").on('keyup', function(e){
			
			var limit_text = $(this).val();
			
			$("#secret_special_limit_text").html(limit_text);
			
		});
		
		$("#secret_special_skus").on('keyup', function(e){
			
			var skus_text = $(this).val();
			
			$("#secret_special_skus_text").html(skus_text);
			
		});
		
	}

	if (~page.indexOf('builder detail') || ~page.indexOf('specials builder') || page == 'report' || page == 'specials') {
		
		// Handle scroll to
		$('#menu .departments a[href*=#]').click(function(e){
			
			e.preventDefault();
			var section = $(this).attr("href");
			
			// Set up scrollable
			$.scrollTo(section, {
				interrupt: true,
				duration: 200,
				offset: -130
			});
			
		});
		
	}

	if (~page.indexOf('builder detail')) {
		
		$('.group').click(function() {
			
			$('#item-group-form').show();
			
			var familyid = $(this).attr('data-familyid');
			var group = $(this).attr('value');
			
			if ($(this).prop('checked') == true) {
				$('#grouped-items').append('<input type="hidden" id="'+familyid+'" name="group['+familyid+']" value="'+group+'" />');
			} else {
				$('#'+familyid).remove();
				console.log('#'+group);
			}
			
		});
		
		$('.disable').click(function(e) {
			
			// Override click
			e.preventDefault();
			
			// Capture post data
			var familygroup = $(this).attr('data-familygroup');
			var ad_id = $(this).attr('data-adid');
			var item_id = '#' + $(this).attr('data-itemid');
			
			// Set POST URI and
			var uri = "/ad-item-group.php";
			
			// Handle form
			$.ajax({
				url:	uri,
				type:	'GET',
				data:	{'family': familygroup, 'ad_id': ad_id, 'action': 'disable'}
			}).done(function(){
				
				// Fadout and remove item from DOM
				$(item_id).fadeOut('slow', function(){
					$(this).remove();
				});
				
			});
			
		});
		
	}
	
	// Ad Plans Screen
	if (page == 'ad-plans') {
		
		$('.edit, .cancel').click(function(e){
			
			e.preventDefault();
			var plan = '#' + $(this).attr("rel");
			var label = $(this).text();
			
			$(plan + ' section, ' + plan + ' form').slideToggle('fast');
			
		});
		
	}

});
