<? 
	require_once '../engine/Banners.php';
	include 'inc/header.inc.php';
	include 'inc/messages.inc.php';
?>

<section class="banners">
	<header>
		<h1>Banners</h1>
        <p>Add promotional banner images to the top of department listings on the&nbsp;E-Ads.</p>
		<nav>
			<a href="/banners/new/" class="button">Add new</a>
		</nav>
	</header>

    <? if (is_array($banners['listing'])) { ?>
		
        <ol class="listing">
            <? foreach ($banners['listing'] as $banner) { ?>
                <li>
                    <h3><a href="/banners/edit/<?=$banner['id']?>/"><?=$banner['name']?></a></h3>
                    <a href="/banners/edit/<?=$banner['id']?>/">
                       <figure>
                            <picture>
                                <source srcset="/media/<?=$banner['small_image']?>" media="(max-width: 320px)">
                                <source srcset="/media/<?=$banner['medium_image']?>" media="(max-width: 600px)">
                                <source srcset="/media/<?=$banner['large_image']?>">
                                <a href="/banners/edit/<?=$banner['id']?>/"><img src="/media/<?=$banner['large_image']?>"  alt="<?=$banner['image_alt_text']?>" /></a>
                            </picture>
							<? if ($banner['sponsored']) { ?>
								<div class="sponsored">Sponsored</div>
							<? } ?>
                            <? if ($banner['image_caption']) { ?>
							<figcaption>
                                <?=$banner['image_caption']?>
                            </figcaption>
                            <? } ?>
                        </figure>
                    </a>
					<? if ($banner['zones']) { ?>
                    <p>Running on 
						<? $i = 0; foreach ($banner['zones'] as $zone) { ?> 
						<strong><?=$zones[$zone]['name']?></strong><? if ($zone != end($banner['zones'])) { ?>, <? } ?> 
						<? $i++; } ?> 
						in <strong><?=$banner['department']?></strong> from <strong><?=$banner['date_range']?></strong>
					</p>
					<? } else { ?>
						<p>No zones have been selected for this banner.</p>
					<? } ?>
                </li>
            <? } ?>
        </ol>
        
        <footer>
            <? if ($banners['pagination']) { ?>
            <nav class="pagination">
            <? if (!is_null($banners['pagination']['first'])) { ?>
                <a href="/banners/page/1/">First</a>
                <a href="/banners/page/<?=$banners['pagination']['previous']?>/">Prev</a>
            <? } else { ?>
                <span class="deact">First</span>
                <span class="deact">Prev</span>
            <? } ?>
            <span class="total">Page <?=$banners['pagination']['current']?> of <?=$banners['pagination']['total']?></span>
            <? if (!is_null($banners['pagination']['last'])) { ?>
                <a href="/banners/page/<?=$banners['pagination']['next']?>/">Next</a>
                <a href="/banners/page/<?=$banners['pagination']['last']?>/">Last</a>
            <? } else { ?>
                <span class="deact">Next</span>
                <span class="deact">Last</span>
            <? } ?>
            </nav>
            <? } ?>
        </footer>
        
    <? } else if ($banner) { ?>
		
        <form action="/banners/" method="post">
            
            <fieldset>
                <legend>Description</legend>
                <p>The name of the banner is not displayed on the&nbsp;websites.</p>
                <ul>
                    <li>
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?=unescape($banner['name'])?>" <?if(!$banner['name']){echo'class="empty"';}?> required="required" />
                        <div class="note required">Required</div>
                    </li>
                    <li>
                        <label for="url">URL:</label>
                        <input type="text" name="url" id="url" value="<?=unescape($banner['url'])?>" <?if(!$banner['url']){echo'class="empty"';}?> /> 
                        <div class="note">Optional URL for the banner image to link to.</div>
                    </li>
                    <li>
                        <label for="body_copy">Ad Copy:</label>
                        <textarea name="body_copy" id="body_copy" class="tinymce"><?=$banner['body_copy']?></textarea>
                    </li>
					<li>
						<label for="sponsored">
							<input type="checkbox" name="sponsored" id="sponsored" value="1" <?if($banner['sponsored']){echo'checked="checked"';}?> /> 
							Sponsored Banner <span class="note">Displays &ldquo;Sponsored&rdquo; under banner on websitem</span>
						</label>
					</li>
                </ul>
            </fieldset>
			
            <fieldset>
                <legend>Banner Images</legend>
                <p>Select the default image to display for this banner. Select from the images below used images below or browse for additional&nbsp;images.</p>
                <p><a href="/media-select.php?category=banners&amp;id=<?=$banner['id']?>" class="button" id="browse-media" data-category="banners" data-id="<?=$banner['id']?>" data-action="/banners/">Browse media</a> <label class="clear-default"><input type="radio" name="featured_image" value="">Clear default</label></p>
                <ul class="grid" id="attachments">
                    <? if (isset($banner['media_items']['images'])) { foreach ($banner['media_items']['images'] as $media) {
						
                        $path = pathinfo($media['file']);
                        $media['file_name'] = $path['basename'];
						
                    ?>
                        <li>
                            <figure>
                            <? if (strstr($media['mime_type'],'image')) { ?>
                                <label for="media-item-<?=$media['id']?>"><img src="/media/<?=$media['small_image']?>" alt="<?=$media['alt_text']?>" /></label>
                            <? } else if (strstr($media['mime_type'],'audio')) { ?>
                                <audio controls="controls">
                                    <source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
                                </audio>
                            <? } else if (strstr($media['mime_type'],'video')) { ?>
                                <video controls="controls">
                                    <source src="/media/<?=$media['file']?>" type="<?=$media['mime_type']?>" />
                                </video>
                            <? } else { ?>
                                <label for="media-item-<?=$media['id']?>"><img src="/img/file-icon.svg" alt="Icon for file <?=$media['file_name']?>" /></label>
                            <? } ?>
                                <figcaption>
                                <? if ($media['name']) { ?>
                                    <h3><?=$media['name']?></h3>
                                    <code><?=$media['file_name']?></code>
                                <? } ?>
                                    <label><input type="checkbox" name="media[]" value="<?=$media['id']?>" id="media-item-<?=$media['id']?>" checked="checked" /> Attach to item</label>
                                <? if (strstr($media['mime_type'],'image')) { ?>
                                    <label><input type="radio" name="featured_image" value="<?=$media['id']?>" <? if ($banner['feature_image'] == $media['id']) { echo 'checked="checked"'; } ?> /> Make default</label>
                                <? } ?>
                                <a href="/media/edit/<?=$media['id']?>" class="button small">Edit</a>
                                </figcaption>
                            </figure>
                        </li>
                    <? } } else { ?>
                        <li class="no-attachments">Media will be displayed here.</li>
                    <? } ?>
                    </ul>
            </fieldset>
			
            <fieldset>
                <legend>Publish Settings</legend>
                <p>Specify where and when this banner should&nbsp;display.</p>
                <ul>
                    <li>
                        <label for="zones">Zones:</label>
                        <ul>
                        <? foreach ($zones as $zone) { ?>
                            <li><label><input type="checkbox" name="zones[]" value="<?=$zone['zone_number']?>" <?if(in_array($zone['zone_number'], $banner['zones'])){echo 'checked="checked"';}?> /> <?=$zone['name']?></label></li>
                        <? } ?>
                        </ul>
                    </li>
                    <li>
                        <label for="department">Department:</label>
                        <select name="department" id="department">
                            <optgroup label="Departments">
                                <option value="adult beverages" <? if ($banner['department'] == 'adult beverages') { echo 'selected="selected"'; } ?>>Adult Beverages</option>
                                <option value="bakery" <? if ($banner['department'] == 'bakery') { echo 'selected="selected"'; } ?>>Bakery</option>
                                <option value="coffee bar" <? if ($banner['department'] == 'coffee bar') { echo 'selected="selected"'; } ?>>Coffee Bar</option>
                                <option value="dairy" <? if ($banner['department'] == 'dairy') { echo 'selected="selected"'; } ?>>Dairy</option>
                                <option value="deli" <? if ($banner['department'] == 'deli') { echo 'selected="selected"'; } ?>>Deli</option>
                                <option value="floral" <? if ($banner['department'] == 'floral') { echo 'selected="selected"'; } ?>>Floral</option>
                                <option value="frozen" <? if ($banner['department'] == 'frozen') { echo 'selected="selected"'; } ?>>Frozen</option>
                                <option value="general merchandise" <? if ($banner['department'] == 'general merchandise') { echo 'selected="selected"'; } ?>>General Merchandise</option>
                                <option value="healthy living" <? if ($banner['department'] == 'healthy living') { echo 'selected="selected"'; } ?>>Healthy Living</option>
                                <option value="kitchen" <? if ($banner['department'] == 'kitchen') { echo 'selected="selected"'; } ?>>Kitchen</option>
                                <option value="meat" <? if ($banner['department'] == 'meat') { echo 'selected="selected"'; } ?>>Meat &amp; Seafood</option>
                                <option value="produce" <? if ($banner['department'] == 'produce') { echo 'selected="selected"'; } ?>>Produce</option>
                                <option value="specialty cheese" <? if ($banner['department'] == 'specialty cheese') { echo 'selected="selected"'; } ?>>Specialty Cheese</option>
                                <option value="staples" <? if ($banner['department'] == 'staples') { echo 'selected="selected"'; } ?>>Staples</option>
                            </optgroup>
                            <optgroup label="Feature Options">
                                <option value="features" <? if ($banner['department'] == 'features') { echo 'selected="selected"'; } ?>>Features</option>
                                <option value="takeover" <? if ($banner['department'] == 'takeover') { echo 'selected="selected"'; } ?>>Hero Takeover</option>
                            </optgroup>
                        </select> 
                        <span class="note">The banner will display at the top of this department in the&nbsp;E-Ad.</span>
                    </li>
                    <li>
                        <label for="date_from">Display from:</label>
                        <select name="month_from">
                        <? foreach ($date_from['months'] as $key => $month) { ?>
                            <option value="<?=$key?>"<?if($date_from['select_month']==$key){?> selected="selected"<?}?>><?=$month?></option>
                        <? } ?>
                        </select>
                        /
                        <select name="day_from">
                        <? foreach ($date_from['days'] as $key => $day) { ?>
                            <option value="<?=$key?>"<?if($date_from['select_day']==$key){?> selected="selected"<?}?>><?=$day?></option>
                        <? } ?>
                        </select>
                        /
                        <select name="year_from">
                        <? foreach ($date_from['years'] as $key => $year) { ?>
                            <option value="<?=$key?>"<?if($date_from['select_year']==$key){?> selected="selected"<?}?>><?=$year?></option>
                        <? } ?>
                        </select>
                        <span class="note">The banner will start displaying at the beginning of this&nbsp;day.</span>
                    </li>
                    <li>
                        <label for="date_through">Display through:</label>
                        <select name="month_through">
                        <? foreach ($date_through['months'] as $key => $month) { ?>
                            <option value="<?=$key?>"<?if($date_through['select_month']==$key){?> selected="selected"<?}?>><?=$month?></option>
                        <? } ?>
                        </select>
                        /
                        <select name="day_through">
                        <? foreach ($date_through['days'] as $key => $day) { ?>
                            <option value="<?=$key?>"<?if($date_through['select_day']==$key){?> selected="selected"<?}?>><?=$day?></option>
                        <? } ?>
                        </select>
                        /
                        <select name="year_through">
                        <? foreach ($date_through['years'] as $key => $year) { ?>
                            <option value="<?=$key?>"<?if($date_through['select_year']==$key){?> selected="selected"<?}?>><?=$year?></option>
                        <? } ?>
                        </select>
                        <span class="note">The banner will go away at the end of this&nbsp;day.</span>
                        
                    </li>
                </ul>
            </fieldset>
            
            <ul class="actions">
                <li>
                    <? if ($_GET['action'] == 'edit') { ?>
                    <input type="hidden" name="banner_id" value="<?=$_GET['id']?>" />
                    <input type="submit" name="save" value="Save changes" />
                    <? } else { ?>
                    <input type="submit" name="add" value="Add item" />
                    <? } ?>
                </li>
                <li>
                    <a href="/media/">Cancel</a>
                </li>
                <? if ($_GET['action']=='edit') { ?>
                <li><a href="/delete/banners/<?=$_GET['id']?>/" class="delete">Delete</a></li>
                <? } ?>
            </ul>
        </form>

    <? }  ?>

</section>

<? include('inc/footer.inc.php'); ?>
