<?
    require_once('../engine/Login.php');
    include('inc/header.inc.php');
    include('inc/messages.inc.php');
?>

<section class="login">
	<form method="post" action="/login/">
		<fieldset>
			<legend><img src="https://profiles.nuggetmarkets.net/img/profile-mom.svg" alt="Nugget Markets Profiles" class="profile-mom"/> Log in</legend>
			<p>Log in with your Nugget Markets <a href="https://profiles.nuggetmarkets.net" target="_blank">web&nbsp;profile</a>.</p>
			<ul>
				<li>
					<label for="email">Email Address:</label>
					<input type="email" name="email" id="email" value="<?=(isset($_POST['email'])?$_POST['email']:'')?>" tabindex="1" required />
					<div class="note required">Required</div>
				</li>
				<li>
					<label for="password">Password:</label>
					<input type="password" name="password" id="password" tabindex="2" required />
					<div class="note required">Required</div>
				</li>
            </ul>
			
		</fieldset>
		<ul class="actions">
			<li><input type="submit" name="login" id="login" tabindex="3" value="Log in" /></li>
		</ul>
	</form>
</section>

<? include('inc/footer.inc.php'); ?>
