<?
	include('../engine/AdBuilder.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>

<nav id="menu" class="subnav">
	<div class="menu next-prev-ads">
		<? if ($ad_nav_dates['prev']['id']) { ?>
		<a href="/ad-builder/<?=$ad_nav_dates['prev']['id']?>/" title="Jump to previous week"><span class="label"><?=date('n/j/Y', strtotime($ad_nav_dates['prev']['date_from']))?></span></a>
		<? } ?>
		<nav class="sister-ads" title="Jump to other zones for this ad date">
			<h3><?=$ad['store_chain']?></h3>
			<?=date('n/j/Y', strtotime($ad['date_from']))?>
			<ul class="menu">
				<? foreach ($sister_ads['ads'] as $sister_ad) { ?>
					<li class="<?=formatSlug($sister_ad['store_chain'])?>">
						<a href="/ad-builder/<?=$sister_ad['id']?>/">
							<strong><?=$sister_ad['store_chain']?></strong>
							<div><?=date('n/j/Y', strtotime($sister_ad['date_from']))?></div>
						</a>
					</li>
				<? } ?>
			</ul>
		</nav>
		<? if ($ad_nav_dates['next']['id']) { ?>
		<a href="/ad-builder/<?=$ad_nav_dates['next']['id']?>/" title="Jump to next week"><span class="label"><?=date('n/j/Y', strtotime($ad_nav_dates['next']['date_from']))?></span></a>
		<? } ?>
	</div>

	<ul class="menu departments">
		<? if ($adultbev) { ?><li><a href="#adult-bev">Adult Bev.</a></li><? } ?>
		<? if ($bakery) { ?><li><a href="#bakery">Bakery</a></li><? } ?>
		<? if ($coffeebar) { ?><li><a href="#coffee">Coffee Bar</a></li><? } ?>
		<? if ($dairy) { ?><li><a href="#dairy">Dairy</a></li><? } ?>
		<? if ($deli) { ?><li><a href="#deli">Deli</a></li><? } ?>
		<? if ($floral) { ?><li><a href="#floral">Floral</a></li><? } ?>
		<? if ($frozen) { ?><li><a href="#frozen">Frozen</a></li><? } ?>
		<? if ($general) { ?><li><a href="#general">General Merchandise</a></li><? } ?>
		<? if ($healthy) { ?><li><a href="#healthy">Healthy Living</a></li><? } ?>
		<? if ($kitchen) { ?><li><a href="#kitchen">Kitchen</a></li><? } ?>
		<? if ($meat) { ?><li><a href="#meat">Meat &amp; Seafood</a></li><? } ?>
		<? if ($produce) { ?><li><a href="#produce">Produce</a></li><? } ?>
		<? if ($cheese) { ?><li><a href="#cheese">Specialty Cheese</a></li><? } ?>
		<? if ($grocery) { ?><li><a href="#staples">Staples</a></li><? } ?>
		<li class="top"><a href="#top">Top</a></li>
		<li class="disabled"><a href="#disabled">Disabled</a></li>
		<? if ($ad['publish'] == 1) { ?>
		<li class="preview"><a href="<?=$ad['store_attributes']['ad_url']?>date/<?=$ad['date_from']?>/" target="_blank">Preview</a></li>
		<? } ?>
	</ul>
</nav>

<form class="item-group-form" id="item-group-form" action="/ad-builder/<?=$ad['id']?>/" method="post">
	<a href="#item-group-form" class="close">&times;</a>
	<fieldset>
		<legend>Group Selected Items</legend>
		<ul>
			<li>
				<label for="group_name">Group name:</label>
				<input type="text" name="group_name" required="required" />
			</li>
			<li id="grouped-items"></li>
			<li>
				<label><input type="checkbox" name="update_sister_ads" value="1" /> Apply to siblings</label>
			</li>
			<li>
				<input type="hidden" name="ad_id" value="<?=$ad['id']?>" /> 
				<input type="hidden" name="ad_date" value="<?=$ad['date_from']?>" />
				<input type="submit" name="group_items" value="Group Items" /> 
			</li>
		</ul>
	</fieldset>
</form>

	<? if ($features) { ?>
		<h2 id="features">Featured</h2>
		<ul class="items grid">
			<? foreach ($features as $product) {
				
				// Append
				$product['features'] = TRUE;
				
				include('inc/ad-items-listing.inc.php');
				
			} ?>
		</ul>
	<? } ?>

	<? if ($produce) { ?>
		<h2 id="produce">Produce</h2>
		<ul class="items grid">
			<? foreach ($produce as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($frozen) { ?>
		<h2 id="frozen">Frozen</h2>
		<ul class="items grid">
			<? foreach ($frozen as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($dairy) { ?>
		<h2 id="dairy">Dairy</h2>
		<ul class="items grid">
			<? foreach ($dairy as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($grocery) { ?>
		<h2 id="staples">Staples</h2>
		<ul class="items grid">
			<? foreach ($grocery as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($deli) { ?>
		<h2 id="deli">Deli</h2>
		<ul class="items grid">
			<? foreach ($deli as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($kitchen) { ?>
		<h2 id="kitchen">Kitchen</h2>
		<ul class="items grid">
			<? foreach ($kitchen as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($meat) { ?>
		<h2 id="meat">Meat &amp; Seafood</h2>
		<ul class="items grid">
			<? foreach ($meat as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($cheese) { ?>
		<h2 id="cheese">Specialty Cheese</h2>
		<ul class="items grid">
			<? foreach ($cheese as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($adultbev) { ?>
		<h2 id="adult-bev">Adult Beverages</h2>
		<ul class="items grid">
			<? foreach ($adultbev as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($bakery) { ?>
		<h2 id="bakery">Bakery</h2>
		<ul class="items grid">
			<? foreach ($bakery as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($coffeebar) { ?>
		<h2 id="coffee">Coffee Bar</h2>
		<ul class="items grid">
			<? foreach ($coffeebar as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($floral) { ?>
		<h2 id="floral">Floral</h2>
		<ul class="items grid">
			<? foreach ($floral as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($general) { ?>
		<h2 id="general">General Merchandise</h2>
		<ul class="items grid">
			<? foreach ($general as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>

	<? if ($healthy) { ?>
		<h2 id="healthy">Healthy Living</h2>
		<ul class="items grid">
			<? foreach ($healthy as $product) { include('inc/ad-items-listing.inc.php'); } ?>
		</ul>
	<? } ?>
	<h2 id="disabled">Disabled Items</h2>
	<? if ($disabled) { ?>
		<ul class="listing">
		<? foreach ($disabled as $product) { ?>
			<li>
				<a href="/ad-item-group/<?=$ad['id']?>/<?=encodeFamilyGroupUrlString($product['family_group'])?>/" target="_blank"><?=$product['upc']?></a> -
				<?=$product['pos_name']?> <a href="/ad-builder/<?=$ad['id']?>/enable/<?=$product['upc']?>/" class="button pill small">Enable</a>
			</li>
		<? } ?>
		</ul>
	<? } else { ?>
		<p>Items that have been removed from the ad will be displayed here. They can be reenabled if needed.</p>
	<? } ?>

<? include('inc/footer.inc.php'); ?>
