<? 
	include('../engine/AdReport.php');
	include('inc/header.inc.php');
	include('inc/messages.inc.php');
?>
<nav id="menu" class="subnav">
	<div class="menu next-prev-ads">
		<? if ($ad_nav_dates['prev']['id']) { ?>
		<a href="/ad-builder/<?=$ad_nav_dates['prev']['id']?>/"><?=date('n/j/Y', strtotime($ad_nav_dates['prev']['date_from']))?></a>
		<? } ?>
		<nav class="sister-ads">
			<h3><?=$ad['store_chain']?></h3>
			<?=date('n/j/Y', strtotime($ad['date_from']))?> 
			<ul class="menu ">
				<? foreach ($sister_ads['ads'] as $sister_ad) { ?>
					<li class="<?=formatSlug($sister_ad['store_chain'])?>">
						<a href="/ad-report/<?=$sister_ad['id']?>/">
							<strong><?=$sister_ad['store_chain']?></strong>
							<div><?=date('n/j/Y', strtotime($sister_ad['date_from']))?></div>
						</a>
					</li>
				<? } ?>
			</ul>
		</nav>
		<? if ($ad_nav_dates['next']['id']) { ?>
		<a href="/ad-builder/<?=$ad_nav_dates['next']['id']?>/"><?=date('n/j/Y', strtotime($ad_nav_dates['next']['date_from']))?></a>
		<? } ?>
	</div>

	<ul class="menu departments">
		<? if ($adultbev) { ?><li><a href="#adult-bev">Adult Bev.</a></li><? } ?>
		<? if ($bakery) { ?><li><a href="#bakery">Bakery</a></li><? } ?>
		<? if ($coffeebar) { ?><li><a href="#coffee">Coffee Bar</a></li><? } ?>
		<? if ($dairy) { ?><li><a href="#dairy">Dairy</a></li><? } ?>
		<? if ($deli) { ?><li><a href="#deli">Deli</a></li><? } ?>
		<? if ($floral) { ?><li><a href="#floral">Floral</a></li><? } ?>
		<? if ($frozen) { ?><li><a href="#frozen">Frozen</a></li><? } ?>
		<? if ($general) { ?><li><a href="#general">General Merchandise</a></li><? } ?>
		<? if ($grocery) { ?><li><a href="#staples">Grocery</a></li><? } ?>
		<? if ($healthy) { ?><li><a href="#healthy">Healthy Living</a></li><? } ?>
		<? if ($kitchen) { ?><li><a href="#kitchen">Kitchen</a></li><? } ?>
		<? if ($meat) { ?><li><a href="#meat">Meat &amp; Seafood</a></li><? } ?>
		<? if ($produce) { ?><li><a href="#produce">Produce</a></li><? } ?>
		<? if ($cheese) { ?><li><a href="#cheese">Specialty Cheese</a></li><? } ?>
		<li class="top"><a href="#top">Top</a></li>
		<!-- <li class="disabled"><a href="#disabled">Disabled</a></li> -->
	</ul>
</nav>

<? /* if ($features) { ?>
<table id="features">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Features</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($features as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } */ ?>

<? if ($produce) { ?>
<table id="produce">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Produce</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($produce as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($meat) { ?>
<table id="meat">
<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Meat &amp; Seafood</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($meat as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($grocery) { ?>
<table id="staples">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Grocery</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($grocery as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($frozen) { ?>
<table id="frozen">
<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Frozen</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($frozen as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($dairy) { ?>
<table id="dairy">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Dairy</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($dairy as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($deli) { ?>
<table id="deli">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Deli</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($deli as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($kitchen) { ?>
<table id="kitchen">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Kitchen</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($kitchen as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>


<? if ($adultbev) { ?>
<table id="adult-bev">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Adult Bev.</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($adultbev as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($cheese) { ?>
<table id="cheese">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Specialty Cheese</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($cheese as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($bakery) { ?>
<table id="bakery">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Bakery</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($bakery as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($coffeebar) { ?>
<table id="coffee">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Coffee Bar</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($coffeebar as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($floral) { ?>
<table id="floral">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Floral</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($floral as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($healthy) { ?>
<table id="healthy">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">Healthy Living</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($healthy as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? if ($general) { ?>
<table id="general">
	<colgroup class="images">
	<colgroup class="group">
	<colgroup class="items">
	<colgroup class="size">
	<colgroup span="2" class="pricing">
	<thead>
		<tr>
			<th colspan="6">General Merchandice</th>
		</tr>
		<tr>
			<th>Image</th>
			<th>Item Name / Description</th>
			<th>UPCs / Item Names</th>
			<th>Size / Unit</th>
			<th class="currency">Sale Price</th>
			<th class="currency">Savings</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($general as $product) { include('inc/ad-items-table.inc.php'); } ?>
	</tbody>
</table>
<? } ?>

<? include('inc/footer.inc.php'); ?>