<?
	include('../engine/Email.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?=$ad['store_attributes']['public_name']?> Weekly Specials Starting <?=date('M. d, Y',strtotime($ad['date_from']))?></title>
		<style>
			body.ws-email {
				background: #fff;
				color: #000;
				font: 1em/1.4 "Helvetica Neue", "Helvetica", "Roboto", Arial, sans-serif;
				-webkit-font-smoothing: antialiased;
				height: 100% !important;
				margin: 0;
				padding: 0;
				width: 100% !important;
			}
			
			body.ws-email table {
				width: 100%;
			}
			
			body.ws-email table, 
			body.ws-email table tbody,
			body.ws-email table tr, 
			body.ws-email table td {
				border-collapse: collapse;
				border-width: 0;
				border-color: #000;
				padding: 0;
			}
			
			.ws-page {
				background: #fff;
				color: #000;
				font-size: 0.95em;
				line-height: 1.4;
			}
			
			.ws-page img {
				margin: 0;
				max-width: 100%;
				padding: 0;
				vertical-align: middle;
			}
			
			.ws-email .ws-page a {
				color: #000;
			}
			
			.ws-page .ws-preview-text {
				height: 0;
				margin: 0;
				overflow: hidden;
				opacity: 0;
				text-indent: -9999em;
			}
			
			.ws-page .ws-email-link {
				font-size: smaller;
			}
			
			.ws-page .ws-logo img {
				width: 200px;
			}
			
			.ws-page .ws-logo a {
				border: 0;
				text-decoration: none;
			}
			
			.ws-page > .ws-header {
				background-position: bottom center;
				border-bottom: 6px double #000;
				text-align: center;
			}
			
			.ws-page > .ws-header td {
				padding-bottom: 10px;
			}
			
			.ws-page .ws-footer {
				background: #000;
				border-top: 6px double #fff;
				color: #fff;
				margin-top: 40px;
				padding: 1em 0 2em 0;
				text-align: center;
			}
			
			.ws-page .ws-container {
				margin: auto;
				max-width: 340px;
				margin: 0 auto;
				padding: 0 20px;
			}
			
			.ws-page .ws-prepended-message td {
				padding-bottom: 0.5em;
			}
			
			.ws-page .ws-secret-special {
				box-shadow: 0 5px 10px rgba(0,0,0,0.15);
				margin: 40px 0;
			}
			
			.ws-page .ws-secret-special .ws-header {
				background: #000;
			}
			
			.ws-page .ws-secret-special .ws-logo-container {
				width: 150px;
			}
			
			.ws-page .ws-secret-special .ws-date {
				color: #fff;
				height: 80px;
				padding-right: 10px;
				padding-bottom: 10px;
				text-align: right;
				vertical-align: bottom;
			} 
			
			.ws-page .ws-secret-special .ws-body, 
			.ws-page .ws-secret-special .ws-printer-friendly {
				background: #fdda81 linear-gradient(145deg, #fdda81, #c3a35e);
				text-align: center;
			}
			
			.ws-page .ws-secret-special .ws-body td, 
			.ws-page .ws-secret-special .ws-printer-friendly td {
				padding: 10px 20px;
			}
			
			.ws-page .ws-secret-special .ws-sale-price {
				background: #000;
				border-radius: 3px;
				color: #fff;
				display: inline-block;
				font-size: 2.75em;
				margin-bottom: 0.15em;
				padding: 0.05em 0.25em;
			}
			
			.ws-page .ws-secret-special .ws-sale-price .crv {
				font-size: 50%;
			}
			
			.ws-page .ws-secret-special .ws-sale-price div {
				font-size: 40%;
				font-weight: normal;
				vertical-align: middle;
			}
			
			.ws-page .ws-secret-special .ws-sale-price {
				margin: 0.12em;
			}
			
			.ws-page .ws-secret-special .ws-body .ws-description {
				text-align: left;
			}
			
			.ws-page .ws-secret-special .ws-body .ws-fineprint {
				font-size: 0.85em;
				text-align: left;
			}
			
			.ws-page .ws-secret-special .ws-barcode {
				background: #fff;
				border: 1px solid #000;
				border-width: 1px 0;
				margin: 1em -20px;
				padding: 1em 20px 0.5em 20px;
				text-align: center;
			}
			
			.ws-page .ws-ocean-feast {
				background: #62b1cb;
				border-radius: 10px;
				color: #fff;
				font-size: 1.25em;
				font-weight: bold;
				text-align: center;
			}
			
			.ws-page .ws-ocean-feast p {
				padding: 0 20px;
			}
			
			.ws-page .ws-ocean-feast img {
				margin-bottom: -40px;
			}
			
			.ws-page .ws-truckload img {
				margin-top: 20px;
			}
			
			.ws-page .ws-truckload {
				font-size: 1.25em;
				font-weight: bold;
				text-align: center;
			}
			
			.ws-email .ws-page .ws-button {
				background: #000;
				border-radius: 3px;
				color: #fff;
				display: inline-block;
				margin: 0.5em 0;
				padding: 0.5em 1em;
				text-decoration: none;
			}
			
			.ws-page .ws-item {
				margin-top: 20px;
				text-align: center;
			}
			
			.ws-page .ws-item {
				border-top: 6px double #000;
				padding-top: 
			}
			
			.ws-page .ws-item:first-child {
				border-top: 0 !important;
			}
			
			.ws-page .ws-item .ws-image td {
				padding-top: 30px;
			}
			
			.ws-page .ws-item .ws-image img {
				border-radius: 15px;
				max-height: 227px;
			}
			
			.ws-page .ws-editorial .ws-item .ws-image img {
				max-height: inherit;
			}
			
			.ws-page .ws-item .ws-label {
				max-height: 80px;
				max-width: 80px;
			}
			
			.ws-page .ws-item-name {
				font-size: 1.75em;
			}
			
			.ws-page .ws-item-container {
				font-size: 1.25em;
				margin: 0 0 0.5em 0;
			}
			
			.ws-page .ws-description {
				text-align: left;
			}
			
			.ws-page .ws-hot-items .ws-header td {
				background: #d1353b url('/img/weekly-specials-square-red-white-black.svg') top center repeat-x;
				background-size: 15px;
				color: #fff;
				padding-top: 20px;
				text-align: center;
			}
			
			.ws-page .ws-header h1 {
				background: #d1353b url('/img/weekly-specials-square-red-white-black.png') bottom center repeat-x;
				background-size: 15px;
				margin: 0;
				padding-bottom: 0.75em;
			}
			
			.ws-page .ws-hot-items .ws-header img {
				width: 300px;
			}
			
			.ws-page .ws-header .ws-prices-effective {
				background: #000;
				color: #fff;
				margin: 0;
				padding: 0.5em 0;
			}
			
			.ws-page .ws-item .ws-sale-price {
				background: none;
				color: #cd383f;
				font-size: 2em;
				font-weight: bold;
			}
			
			.ws-page .ws-item .ws-save {
				margin: 0;
				font-weight: bold;
			}
			
			.ws-page .ws-item .ws-size {
				text-transform: lowercase;
			}
			
			.ws-page .ws-item .ws-description .button {
				background: #ddd;
				border-radius: 2px;
				text-decoration: none;
				padding: 0.25em 0.5em;
			} 
			
			.ws-page .ws-view-all {
				text-align: center;
			}
			
			.ws-page .ws-view-all .ws-button {
				font-size: 1.75em;
			}
			
			.ws-page .ws-view-all .ws-download-print .ws-button {
				font-size: 1.25em;
			}
			
			.ws-page .ws-editorial .ws-header td {
				background: #000 url('/img/weekly-specials-square-black-white-red.png') top center repeat-x;
				background-size: 15px;
				color: #fff;
				padding-top: 40px;
				text-align: center;
			}
			
			.ws-page .ws-editorial .ws-header h2 {
				border-top: 6px double #000;
				font-size: 2em;
				padding-top: 0.5em;
			}
			
			.ws-page .ws-editorial .ws-header h2, 
			.ws-page .ws-editorial .ws-header p {
				background: #fff;
				color: #000;
				margin: 0
			}
			
			.ws-page .ws-editorial .ws-item {
				text-align: left;
			}
			
			.ws-page .ws-disclaimer {
				font-size: small;
				text-align: left;
			}
			
			.ws-page .ws-footer a {
				color: #fff;
			}
			
			.ws-page .ws-footer .ws-logo img {
				margin-bottom: 2em;
				width: 100px;
			}
			
			.ws-page .ws-prefix, 
			.ws-page .ws-item-name, 
			.ws-page .ws-suffix, 
			.ws-page .ws-sale-price, 
			.ws-page .ws-selected-varieties, 
			.ws-page .ws-size, 
			.ws-page .ws-retail-price {
				margin: 0;
			}
			
			.ws-page .ws-sale-price .cent, 
			.ws-page .ws-sale-price .perlb, 
			.ws-page .ws-sale-price .perea {
				font-size: 70%;
			}
			
			@media print {
			
				body.ws-email {
					font-size: 0.7em;
				}
				
				.ws-page img {
					filter: saturate(0);
				}
				
				.ws-page > .ws-header .ws-logo img {
					width: 150px;
				}
				
				.ws-page .ws-email-link, 
				.ws-page .ws-header h1, 
				.ws-page .ws-header .ws-duration, 
				.ws-page .ws-printer-friendly, 
				.ws-page .ws-hot-items, 
				.ws-page .ws-editorial, 
				.ws-page .ws-preferences, 
				.ws-page .ws-prices-valid, 
				.ws-page .ws-tip, 
				.ws-page .ws-secret-special .ws-logo-container,
				.ws-page > .ws-footer {
					display: none;
				}
				
				.ws-page .ws-header {
					border: 0;
					padding: 0.5em 0;
				}
				
				.ws-page .ws-container .ws-header {
					background: transparent;
					border: 0;
					color: #000;
				}
				
				.ws-page .ws-container > .ws-header + * {
					padding-top: 40px;
				}
				
				.ws-page .ws-secret-special .ws-date {
					color: #000;
					height: inherit;
					padding: 0;
					text-align: center;
				}
				
				.ws-page .ws-secret-special {
					box-shadow: none;
					margin: 0 auto;
				}
				
				.ws-page .ws-secret-special .ws-image img {
					display: block;
					margin: auto;
					width: 200px;
				}
				
				.ws-page .ws-secret-special .ws-sale-price {
					background: transparent;
					border: 1px solid #000;
					color: #000;
				}
				
				.ws-page .ws-secret-special .ws-body {
					background: transparent;
				}
				
				.ws-page .ws-secret-special .ws-barcode {
					border-bottom: 0;
				}
				
				.ws-page .ws-secret-special .ws-barcode img:first-child {
					display: block;
					margin: auto;
					width: 200px;
				}
				
				.ws-page .ws-disclaimer, 
				.ws-page .ws-address {
					font-size: 0.9em;
				}
			}
		</style>
    </head>
	<body class="ws-email<?=$page_attrs['class']?>">
		<table>
		<tr>
		<td class="ws-page">
			
		<? if (isset($ad['secret_special_title']) && isset($ad['secret_special_description']) && isset($ad['secret_special_regular_retail'])) { ?>
			<p class="ws-preview-text">Our secret special this week is&hellip;</p>
		<? } else { ?>
			<p class="ws-preview-text">Featured items on special this week&hellip;</p>
		<? } ?>
			<table class="ws-header">
			<tr>
			<td>
				<p class="ws-email-link"><webversion>View this email in your browser.</webversion></p>
				<div class="ws-logo">
					<? if ($page_attrs['class']) { ?>
					<a href="<?=$ad['store_attributes']['url']?>"><img src="/img/<?=$ad['store_attributes']['email_logo_print']?>" alt="<?=$ad['store_attributes']['public_name']?>" /></a>
					<? } else { ?>
					<a href="<?=$ad['store_attributes']['url']?>"><img src="/img/<?=$ad['store_attributes']['email_logo_print']?>" alt="<?=$ad['store_attributes']['public_name']?>" /></a>
					<? } ?>
				</div>
			</td>
			</tr>
			</table>
			
			<? if ($ad['prepended_message'] && $ad['prepended_message_position'] == 1) { ?>
			
			<table class="ws-container">
			<tr class="ws-prepended-message">
			<td>
				
				<table>
				<tr>
				<td>
					<?=$ad['prepended_message']?>
				</td>
				</tr>
				</table>	
				
			</td>
			</tr>
			</table><!-- .ws-container -->
			
			<? } ?>
			
			<? if (isset($ad['secret_special_title']) && isset($ad['secret_special_description'])) { ?>
			
			<table class="ws-container">
			<tr>
			<td>
				
				<table class="ws-secret-special">
				<tr class="ws-header">
					<td class="ws-logo-container" width="150" height="100"><img src="/img/secret-special-peek-300.png" width="150" alt="Secret Special" /></td>
					<td class="ws-date" height="100">Secret Special offer valid:<br /><?=$ad['secret_special_duration']?></td>
				</tr>
				<? if (isset($ad['secret_special_image'])) { ?>
				<tr class="ws-image">
					<td colspan="2">
						<img src="/media/<?=$ad['secret_special_image']?>" alt="<?=$ad['secret_special_title']?>" />
					</td>
				</tr>
				<? } ?>
				<tr class="ws-body">
					<td colspan="2">
						
						<h2 class="ws-item-name"><?=$ad['secret_special_title']?></h2>
						<? if (isset($ad['secret_special_container'])) { ?>
						<h4 class="ws-item-container"><?=$ad['secret_special_container']?></h4>
						<? } ?>
						<?=$ad['secret_special_retail']?>
						<? if ($ad['secret_special_retail']) { ?>
						<p class="ws-retail-price">
							<? if (stristr($ad['secret_special_retail'], 'save')) { echo $ad['secret_special_retail']; } else if ($ad['secret_special_regular_retail']) { ?>
							Regularly: <?=$ad['secret_special_regular_retail']?>
							<? } ?>
						</p>
						<? } ?>
						<div class="ws-description">
							<?=$ad['secret_special_description']?>
						</div>
						<div class="ws-fineprint">
							<p>
							<? if ($ad['secret_special_disclaimers']) { echo str_replace("\r\n", "<br />", $ad['secret_special_disclaimers']) . "<br />"; } ?>
							<? if ($ad['secret_special_limit']) { ?>
							Limit <?=$ad['secret_special_limit']?> per&nbsp;guest.<br />
							<? } else { ?>
							Limit 4 per&nbsp;guest.<br />
							<? } ?>
							<? if ($ad['secret_special_skus']) { ?>
							Only valid for <?=$ad['secret_special_skus']?><br />
							<? } ?>
							One redemption per guest, per promotional&nbsp;period.<br />
							Prices only valid <?=$ad['secret_special_duration']?>.<br />
							No further discounts apply.<br />
							Only while supplies last.
							</p>
						</div>
					<? if ($ad['store_chain'] == 'Food 4 Less') { ?>
						<div class="ws-fineprint" lang="es">
							<p>
							<? if ($ad['secret_special_limit']) { ?>
							L&iacute;mite de <?=$ad['secret_special_limit']?> por invitado<br />
							<? } else { ?>
							L&iacute;mite de 4 por invitado<br />
							<? } ?>
							<? if ($ad['secret_special_skus']) { ?>
							S&oacute;lo v&aacute;lido para el  <?=$ad['secret_special_skus']?><br />
							<? } ?>
							Un canje por invitado, por per&iacute;odo promocional.
							Precios v&aacute;lidos solo el <?=$ad['secret_special_duration']?><br />
							No se aplica ning&uacute;n otro descuento.<br />
							Solo hasta agotar existencias.
							</p>
						</div>
					<? } ?>
					</td>
				</tr>
				<tr>
				<? if ($ad['store_chain'] == 'Food 4 Less') { ?>
					<td colspan="2" class="ws-barcode">
						<? if ($ad['date_from'] >= '2023-02-08') { ?>
						<img src="/img/905105000255.png" alt="Secret Special barcode: 905105000255">
						<? } else { ?>
						<img src="/img/905105000057.png" alt="Secret Special barcode: 905105000057">
						<? } ?>
						<table>
							<tr>
								<td width="30px"><img src="/img/weekly-specials-scan-and-save.png" alt="scan-and-save" width="25px"></td>
								<td>
									We can scan your mobile phone!
									<span lang="es">&iexcl;Podemos escanear tu tel&eacute;fono m&oacute;vil!</span>
								</td>
							</tr>
						</table>
					</td>
				<? } else { ?>
					<td colspan="2" class="ws-barcode">
						<? if ($ad['date_from'] >= '2023-02-08') { ?>
						<img src="/img/905105000255.png" alt="Secret Special barcode: 905105000255">
						<? } else { ?>
						<img src="/img/905105000057.png" alt="Secret Special barcode: 905105000057">
						<? } ?>
						<img src="/img/weekly-specials-scan-and-save.png" alt="scan-and-save" width="25"> We can scan your mobile phone!
					</td>
				<? } ?>
				</tr>
				<tr class="ws-printer-friendly">
					<td colspan="2">
						<? if (!isset($_GET['print'])) { ?>
						<a href="/email/<?=$ad['id']?>/print/" class="ws-button">
							Printer-Friendly Coupon
							<? if ($ad['store_chain'] == 'Food 4 Less') { ?>
								<br /><span lang="en">Cup&oacute;n para imprimir</span>
							<? } ?>
						</a>
						<? } ?>
					</td>
				</tr>
				
				</table><!-- .ws-secret-special -->
				
			</td>
			</tr>
			</table><!-- .ws-container -->
			
			<? } ?>
			
			<? if ($ad['prepended_message'] && $ad['prepended_message_position'] == 2) { ?>
			
			<table class="ws-container">
			<tr class="ws-prepended-message">
			<td>
				
				<table>
				<tr>
				<td>
					<?=$ad['prepended_message']?>
				</td>
				</tr>
				</table>	
				
			</td>
			</tr>
			</table><!-- .ws-container -->
			
			<? } ?>
			
			<? if (is_array($ad['items']) && !isset($_GET['print'])) { ?>
				
				<table class="ws-hot-items">
				<tr class="ws-header">
					<td>
						<h1><img src="/img/weekly-specials.png" alt="Weekly Specials" /></h1>
						<? if (strstr($ad['store_attributes']['slug'], 'nugget-markets')) { ?>
						<p class="ws-prices-effective">For Nugget Markets [PreferredStore,fallback=Davis, Covell].<br /><preferences class="ws-button">Update your store preference</preferences><br />Prices effective <?=$ad['ad_duration']?></p>
						<? } else { ?>
						<p class="ws-prices-effective">Prices effective <?=$ad['ad_duration']?></p>
						<? } ?>
					</td>
				</tr>
				</table>
				
				<table class="ws-container">
				<tr>
				<td>
					
					<? if ($ad['truckload']) { ?>
					<table class="ws-truckload">
					<tr>
					<td>
						<img src="/img/truckload.png" alt="Truckload Sale!" />
						<? if ($ad['truckload_dates']) { ?>
							<p><?=$ad['truckload_dates']?></p>
						<? } ?>
					</td>
					</tr>
					</table><!-- .ws-truckload -->
					<? } ?>
					
					<? if ($ad['ocean_feast']) { ?>
					<table class="ws-ocean-feast">
					<tr>
					<td>
						<img src="/img/ocean-feast.png" alt="Ocean Feast Friday, Saturday &amp; Sunday" />
						<? if ($ad['ocean_feast_dates']) { ?>
							<p><?=$ad['ocean_feast_dates']?></p>
						<? } ?>
					</td>
					</tr>
					</table><!-- .ws-ocean-feast -->
					<? } ?>
					
					<? foreach ($ad['items'] as $item) { if ($item['status'] == 'approved') { ?>
					
					<table class="ws-item">
					<tr class="ws-image">
					<td>
						<a href="<?=$ad['store_attributes']['ad_url']?>&amp;zone=<?=$ad['store_attributes']['zone']?>&amp;utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=item-<?=$item['family_id']?>#item-<?=$item['family_id']?>">
						<? if ($item['feature_photo_small']) { ?>
							<img src="/media/<?=$item['feature_photo_small']?>" />
						<? } else { ?>
							<img src="/img/watermark-<?=$ad['store_attributes']['slug']?>.svg" alt="<?=$item['name']?>" />
						<? } ?>
						</a>
					</td>
					</tr>
					<? if ($item['group_labels']) { ?>
						<tr class="ws-labels">
						<td>
						<? foreach ($item['group_labels'] as $key => $value) { foreach ($labels['listing'] as $label) { if ($label['type'] == 'lifestyle' && $key == $label['id']) { ?>
							<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="ws-label <?=$label['type']?>" />
						<? } if ($label['type'] == 'brand' && $key == $label['id']) { ?>
							<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="ws-label <?=$label['type']?>" />
						<? } if ($label['type'] == 'organization' && $key == $label['id']) { ?>
							<img src="/img/labels/<?=$label['name']?>.png" alt="<?=$label['description']?>" class="ws-label <?=$label['type']?>" />
						<? } if ($label['type'] == 'seasoning' && $key == $label['id']) { ?>
							<img src="/img/labels/seasoning-<?=$label['name']?>.png" alt="<?=$label['description']?>" class="ws-label <?=$label['type']?>" />
						<? } } } ?>
						</td>
						</tr>
					<? } ?>
					<tr>
					<td>
						<? if ($item['prefix']) { ?>
						<p class="ws-prefix"><?=$item['prefix']?></p>
						<? } ?>
						<h3 class="ws-item-name">
							<a href="<?=$ad['store_attributes']['ad_url']?>&amp;zone=<?=$ad['store_attributes']['zone']?>&amp;utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=item-<?=$item['family_id']?>#item-<?=$item['family_id']?>">
								<?=$item['name']?>
							</a>
						</h3>
						<? if ($item['suffix']) { ?>
						<p class="ws-suffix"><?=$item['suffix']?></p>
						<? } ?>
						<? if ($item['description']) { ?>
						<div class="ws-description"><?=$item['description']?></div>
						<? } ?>
						<? if ($item['selected_varieties']) { ?>
						<p class="ws-selected-varieties">Selected Varieties</p>
						<? } if ($item['note']) { ?>
						<p class="ws-note"><?=$item['note']?></p>
						<? } if (
							!empty($item['container']) &&
							!strstr($item['sale_price'],'lb.') &&
							!strstr($item['sale_price'],'lbs.') &&
							!strstr($item['sale_price'],'/ea.') &&
							!strstr($item['sale_price'],'%') && (
								$item['unit'] != 'CT' ||
								$item['size'] > 1
							) &&
							isset($item['size'])) {
						?>
							<p class="ws-size"><?=$item['size']?> <?=$item['unit']?>. <?=$item['container']?></p>
						<? } else if (isset($item['container_sizes'])) {$i = 1; foreach ($item['container_sizes'] as $key => $value) { if ($value) { ?>
							<p class="ws-size"><?=$value?> <?=$item['units'][$i]?>. <?=$item['containers'][$key]?></p>
						<? $i++; } } } ?>
						<div class="ws-sale-price"><?=$item['sale_price']?></div>
						<? if ($item['savings']) { ?>
							<p class="ws-save">Save <?=$item['savings']?></p>
						<? } ?>
						<? if ($item['limit']) { ?> 
							<div class="ws-limit"><?=$item['limit']?></div>
						<? } ?>
					</td>
					</tr>
					</table>
					
					<? } } ?>
					
					<table class="ws-view-all">
					<tr>
					<td>
						<p class="ws-view-all"><a href="<?=$ad['store_attributes']['ad_url']?>&amp;zone=<?=$ad['store_attributes']['zone']?>&amp;utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=view-all-specials" class="ws-button">View all items on special this&nbsp;week!</a></p>
						<? if (isset($ad['print_file'])) { ?>
							<p class="ws-download-print"><a href="/print-versions/<?=$ad['print_file']['name']?>?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=pdf-version" class="ws-button">Download <?=$ad['print_size']?> PDF Version</a></p>
							<p>Our website contains many more items than we have room to publish in the PDF version of our weekly specials&nbsp;flyer!</p>
						<? } ?>
					</td>
					</tr>
					</table>
					
				</td>
				</tr>
				</table><!-- .ws-container -->
				
			<? } ?>
			
			<? if (is_array($articles) || is_array($recipes) || is_array($meal_planning) && !isset($_GET['print'])) { ?>
				
				<table class="ws-editorial">
				<tr class="ws-header">
					<td>
						<h2>Further Reading</h2>
						<p>On our website</p>
					</td>
				</tr>
				</table>
				
				<table class="ws-container">
				<tr class="ws-editorial">
				<td>
					
					<? if (is_array($meal_planning)) { ?>
						<table class="ws-item">
							<? if ($meal_planning['image_small']) { ?>
								<tr class="ws-image">
								<td class="ws-figure">
									<a href="<?=$ad['store_attributes']['url']?>/page/<?=$meal_planning['slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=meal-planning-<?=$meal_planning['slug']?>">
										<img src="<?=$meal_planning['image_small']?>" alt="<?=$meal_planning['image_alt_text']?>" />
									</a>
								</td>
								</tr>
							<? } ?>
							<tr>
							<td>
								<h3><?=$meal_planning['title']?></h3>
								<?=$meal_planning['body']?>
								<p class="ws-view-all"><a href="<?=$ad['store_attributes']['url']?>/page/<?=$meal_planning['slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=meal-planning-<?=$meal_planning['slug']?>" class="ws-button">Get this week&rsquo;s recipes</a></p>
							</td>
							</tr>
						</table>
					<? } ?>
					
					<? if (is_array($articles)) { foreach ($articles as $article) { ?>
						<table class="ws-item">
						<? if ($article['photo_thumb']) { ?>
							<tr class="ws-image">
							<td class="ws-figure">
								<a href="<?=$ad['store_attributes']['article_archive']?><?=$article['article_id']?>/<?=$article['article_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$article['article_slug']?>"><img src="<?=$ad['store_attributes']['media_url']?><?=$article['photo_thumb']?>" alt="<?=$article['photo_description']?>" /></a>
							</td>
							</tr>
						<? } else if ($article['image_small']) { ?>
							<tr class="ws-image">
							<td class="ws-figure">
								<a href="<?=$ad['store_attributes']['article_archive']?><?=$article['article_id']?>/<?=$article['article_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$article['article_slug']?>"><img src="<?=$article['image_small']?>" alt="<?=$article['image_alt_text']?>" /></a>
							</td>
							</tr>
						<? } ?>
						<tr>
						<td>
							<h3><?=$article['article_title']?></h3>
							<?=$article['article_excerpt']?>
							<p><a href="<?=$ad['store_attributes']['article_archive']?><?=$article['article_id']?>/<?=$article['article_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$article['article_slug']?>" class="ws-button">Full article</a></p>
						</td>
						</tr>
						</table>
						
					<? } } ?>
					
					<? if (is_array($recipes)) { foreach ($recipes as $recipe) { ?>
					<table class="ws-item">
						<? if ($recipe['photo_small']) { ?>
							<tr class="ws-image">
							<td class="ws-figure">
								<img src="<?=$ad['store_attributes']['media_url']?><?=$recipe['photo_small']?>" alt="<?=$recipe['photo_alt_text']?>" />
							</td>
							</tr>
						<? } else if ($recipe['image_small']) { ?>
							<tr class="ws-image">
							<td class="ws-figure">
								<img src="<?=$recipe['image_small']?>" alt="<?=$recipe['image_alt_text']?>" />
							</td>
							</tr>
						<? } ?>
						<tr>
						<td>
							<h3><?=$recipe['recipe_name']?></h3>
							<?=$recipe['recipe_description']?>
							<p><a href="<?=$ad['store_attributes']['recipe_archive']?><?=$recipe['recipe_id']?>/<?=$recipe['recipe_slug']?>/?utm_medium=email&amp;utm_source=email-marketing&amp;utm_campaign=<?=$ad['store_attributes']['slug']?>-weekly-specials-<?=$ad['date_from']?>&amp;utm_content=article-<?=$recipe['recipe_slug']?>" class="ws-button">Full recipe</a></p>
						</td>
						</tr>
					</table>
					<? } } ?>
					
				</td>
				</tr>
				</table><!-- .ws-container -->
			<? } ?>
			
			<? if ($ad['prepended_message'] && $ad['prepended_message_position'] == 3) { ?>
			
			<table class="ws-container">
			<tr class="ws-prepended-message">
			<td>
				
				<table>
				<tr>
				<td>
					<?=$ad['prepended_message']?>
				</td>
				</tr>
				</table>	
				
			</td>
			</tr>
			</table><!-- .ws-container -->
			
			<? } ?>
			
			<table class="ws-footer">
			<tr>
			<td>
				
				<table class="ws-container">
				<tr>
				<td>
					<p class="ws-disclaimer"><? if (strstr($ad['store_attributes']['slug'], 'nugget-markets')) { ?>Prices and selection on this email are for <strong>Nugget Markets [PreferredStore,fallback=Davis, Covell]</strong> only. <? } ?>All sale items are limited to <?=ITEM_LIMIT?> items per household through the sales effective dates unless otherwise noted. We reserve the right to limit individual purchases to four packages of any item for sale, except items where otherwise noted. Sale items are not available to any commercial dealer or wholesaler. Items are subject to stock on hand. All items not available at all stores. Photos and Illustrations are for display purposes only and do not necessarily depict items that are on sale. We reserve the right to correct all image and/or typographical&nbsp;errors.</p>
					<p class="ws-preferences">This email was sent to [email].<br /><unsubscribe>Unsubscribe</unsubscribe> or <preferences>update your email preferences</preferences>.</p>
					<p class="ws-prices-valid">Prices Valid <?=$ad['ad_duration']?></p>
					<p class="ws-tip">Pro-Tip: <em>Add us to your address book to help ensure that you continue receiving our emails.</em></p>
					<p class="ws-address">Copyright &copy; <?=date('Y')?> Nugget Market, Inc.<br />311 Mace Boulevard, Davis, CA. 95618</p>
					<div class="ws-logo"><a href="<?=$ad['store_attributes']['url']?>"><img src="/img/<?=$ad['store_attributes']['email_logo_footer']?>?i=1" alt="<?=$ad['store_attributes']['public_name']?>" /></a></div>
				</td>
				</tr>
				</table>
				
			</td>
			</tr>
			</table><!-- .ws-footer -->
			
		</td><!-- .ws-page -->
		</tr>
		</table>
	</body>
</html>
